#include "SiteDatabase.h"
#include "../../../game/ProjectStatic/Default/project_database_placeholder.hpp"
#include "PreparedStatement.h"

void SiteDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_SITEDATABASE_STATEMENTS);

    PROJECT_DATABASE_SITE_PREPARED_STATEMENTS_IMPLEMENTATION
}
