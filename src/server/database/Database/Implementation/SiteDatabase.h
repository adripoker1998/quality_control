#ifndef PROJECT_SITEDATABASE_H
#define PROJECT_SITEDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"
#include "../../../game/ProjectStatic/Default/project_database_placeholder.h"

enum SiteDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SEL/INS/UPD/DEL/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */

    PROJECT_DATABASE_SITE_PREPARED_STATEMENTS

    MAX_SITEDATABASE_STATEMENTS
};

class TC_DATABASE_API SiteDatabaseConnection : public MySQLConnection
{
public:
    typedef SiteDatabaseStatements Statements;

    //- Constructors for sync and async connections
    SiteDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) { }
    SiteDatabaseConnection(ProducerConsumerQueue<SQLOperation*>* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) { }

    //- Loads database type specific prepared statements
    void DoPrepareStatements() override;
};

#endif
