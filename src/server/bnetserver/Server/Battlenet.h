#pragma once

#include "Common.h"
#include <boost/asio/ip/address.hpp>

struct Bnet
{
    uint32 Id;
    std::string ExternalAddress;
    std::string LocalAddress;
    uint16 bnetPort;
    uint16 restPort;
};

extern Bnet bnet;
