/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "WorldSession.h"
#include "Group.h"
#include "LFGMgr.h"
#include "Log.h"
#include "ObjectMgr.h"
#include "Opcodes.h"
#include "Player.h"
#include "WorldPacket.h"
#include "LFGPackets.h"

void BuildPlayerLockDungeonBlock(WorldPackets::LFG::LFGBlackList& data, lfg::LfgLockMap const& lock)
{
    for (lfg::LfgLockMap::const_iterator it = lock.begin(); it != lock.end(); ++it)
    {
        TC_LOG_TRACE("lfg", "BuildPlayerLockDungeonBlock:: DungeonID: %u Lock status: %u Required itemLevel: %u Current itemLevel: %f",
            (it->first & 0x00FFFFFF), it->second.lockStatus, it->second.requiredItemLevel, it->second.currentItemLevel);
        WorldPackets::LFG::LFGBlackListInfo info;
        info.Slot = it->first;
        info.Reason = it->second.lockStatus;
        info.SubReason1 = it->second.requiredItemLevel;
        info.SubReason2 = it->second.currentItemLevel;
        data.Info.push_back(info);
    }
}

void BuildPartyLockDungeonBlock(WorldPackets::LFG::LfgPartyInfo& /* data */, lfg::LfgLockPartyMap const& lockMap)
{
    for (lfg::LfgLockPartyMap::const_iterator it = lockMap.begin(); it != lockMap.end(); ++it)
    {
        WorldPackets::LFG::LFGBlackList info;
        info.PlayerGuid = it->first;
        BuildPlayerLockDungeonBlock(info, it->second);
    }
}

void BuildQuestReward(WorldPacket& data, Quest const* quest, Player* player)
{
    uint8 rewCount = quest->GetRewItemsCount() + quest->GetRewCurrencyCount();

    data << uint32(player->GetQuestMoneyReward(quest));
    data << uint32(player->GetQuestXPReward(quest));
    data << uint8(rewCount);
    if (rewCount)
    {
        for (uint8 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
        {
            if (uint32 currencyId = quest->RewardCurrencyId[i])
            {
                data << uint32(currencyId);
                data << uint32(0);
                data << uint32(quest->RewardCurrencyCount[i]);
                data << uint8(1);                                           // Is currency
            }
        }

        for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
        {
            if (uint32 itemId = quest->RewardItemId[i])
            {
                //ItemTemplate const* item = sObjectMgr->GetItemTemplate(itemId);
                data << uint32(itemId);
                data << uint32(/*item ? item->DisplayInfoID :*/ 0);
                data << uint32(quest->RewardItemCount[i]);
                data << uint8(0);                                           // Is currency
            }
        }
    }
}

void WorldSession::HandleLfgJoinOpcode(WorldPackets::LFG::DfJoin& join)
{
    return;
    if (!sLFGMgr->isOptionEnabled(lfg::LFG_OPTION_ENABLE_DUNGEON_FINDER | lfg::LFG_OPTION_ENABLE_RAID_BROWSER) ||
        (GetPlayer()->GetGroup() && GetPlayer()->GetGroup()->GetLeaderGUID() != GetPlayer()->GetGUID() &&
        (GetPlayer()->GetGroup()->GetMembersCount() == MAX_GROUP_SIZE || !GetPlayer()->GetGroup()->isLFGGroup())))
        return;
    uint32 roles = join.Roles;
    if (join.Slots.empty())
    {
        TC_LOG_DEBUG("lfg", "CMSG_LFG_JOIN %s no dungeons selected", GetPlayerInfo().c_str());
        return;
    }
    lfg::LfgDungeonSet newDungeons;
    for (uint32 slot : join.Slots)
    {
        uint32 dungeon = slot & 0x00FFFFFF;
        if (sLFGDungeonsStore.LookupEntry(dungeon))
            newDungeons.insert(dungeon);
    }

    TC_LOG_DEBUG("lfg", "CMSG_DF_JOIN %s roles: %u, Dungeons: %u", GetPlayerInfo().c_str(), join.Roles, uint8(newDungeons.size()));

    sLFGMgr->JoinLfg(GetPlayer(), uint8(join.Roles), newDungeons);
}

void WorldSession::HandleLfgLeaveOpcode(WorldPackets::LFG::DfLeave& Leave)
{
    ObjectGuid leaveGuid;
    Group* group = GetPlayer()->GetGroup();
    ObjectGuid guid = GetPlayer()->GetGUID();
    ObjectGuid gguid = group ? group->GetGUID() : guid;
    leaveGuid = Leave.Ride.RequesterGuid;
    TC_LOG_DEBUG("lfg", "CMSG_LFG_LEAVE %s in group: %u sent guid %u.",
        GetPlayerInfo().c_str(), group ? 1 : 0, static_cast<unsigned int>((leaveGuid).GetCounter()));

    // Check cheating - only leader can leave the queue
    if (!group || group->GetLeaderGUID() == guid)
        sLFGMgr->LeaveLfg(gguid);
}

void WorldSession::HandleLfgProposalResultOpcode(WorldPackets::LFG::DfProposalResponse& packet)
{
    TC_LOG_DEBUG("lfg", "CMSG_LFG_PROPOSAL_RESULT %s proposal: %u accept: %u",
        GetPlayerInfo().c_str(), packet.ProposalID, packet.Accepted ? 1 : 0);

    sLFGMgr->UpdateProposal(packet.ProposalID, GetPlayer()->GetGUID(), packet.Accepted);
}

void WorldSession::HandleLfgSetRolesOpcode(WorldPackets::LFG::DfSetRoles& packet)
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    Group* group = GetPlayer()->GetGroup();
    if (!group)
    {
        TC_LOG_DEBUG("lfg", "CMSG_DF_SET_ROLES %s Not in group",
            GetPlayerInfo().c_str());
        return;
    }
    ObjectGuid gguid = group->GetGUID();
    TC_LOG_DEBUG("lfg", "CMSG_DF_SET_ROLES: Group %s, Player %s, Roles: %u",
        gguid.ToString().c_str(), GetPlayerInfo().c_str(), packet.RolesDesired);
    sLFGMgr->UpdateRoleCheck(gguid, guid, packet.RolesDesired);
}

void WorldSession::HandleLfgSetBootVoteOpcode(WorldPackets::LFG::DfBootPlayerVote& packet)
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    TC_LOG_DEBUG("lfg", "CMSG_LFG_SET_BOOT_VOTE %s agree: %u",
        GetPlayerInfo().c_str(), packet.Vote ? 1 : 0);
    sLFGMgr->UpdateBoot(guid, packet.Vote);
}

void WorldSession::HandleLfgTeleportOpcode(WorldPackets::LFG::DfTeleport& packet)
{
    TC_LOG_DEBUG("lfg", "CMSG_DF_TELEPORT %s out: %u",
        GetPlayerInfo().c_str(), packet.TeleportOut ? 1 : 0);
    sLFGMgr->TeleportPlayer(GetPlayer(), packet.TeleportOut, true);
}

void WorldSession::HandleDFGetSystemInfo(WorldPackets::LFG::DfGetSystemInfo& packet)
{
    TC_LOG_DEBUG("lfg", "CMSG_DF_GET_SYSTEM_INFO %s for %s", GetPlayerInfo().c_str(), (packet.isPlayer ? "player" : "party"));

    if (packet.isPlayer)
        SendLfgPlayerLockInfo();
    else
        SendLfgPartyLockInfo();
}

void WorldSession::SendLfgPartyLockInfo()
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    Group* group = GetPlayer()->GetGroup();
    if (!group)
        return;

    // Get the locked dungeons of the other party members
    lfg::LfgLockPartyMap lockMap;
    for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
    {
        Player* plrg = itr->GetSource();
        if (!plrg)
            continue;

        ObjectGuid pguid = plrg->GetGUID();
        if (pguid == guid)
            continue;

        lockMap[pguid] = sLFGMgr->GetLockedDungeons(pguid);
    }

    uint32 size = 0;
    for (lfg::LfgLockPartyMap::const_iterator it = lockMap.begin(); it != lockMap.end(); ++it)
        size += 8 + 4 + uint32(it->second.size()) * (4 + 4 + 4 + 4);

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PARTY_INFO %s", GetPlayerInfo().c_str());
    WorldPackets::LFG::LfgPartyInfo partyInfo;
    BuildPartyLockDungeonBlock(partyInfo, lockMap);
    SendPacket(partyInfo.Write());
}

void WorldSession::HandleLfrJoinOpcode(WorldPacket& recvData)
{
    uint32 entry;                                          // Raid id to search
    recvData >> entry;
    TC_LOG_DEBUG("lfg", "CMSG_LFG_LFR_JOIN %s dungeon entry: %u",
        GetPlayerInfo().c_str(), entry);
    //SendLfrUpdateListOpcode(entry);
}

void WorldSession::HandleLfrLeaveOpcode(WorldPacket& recvData)
{
    uint32 dungeonId;                                      // Raid id queue to leave
    recvData >> dungeonId;
    TC_LOG_DEBUG("lfg", "CMSG_LFG_LFR_LEAVE %s dungeonId: %u",
        GetPlayerInfo().c_str(), dungeonId);
    //sLFGMgr->LeaveLfr(GetPlayer(), dungeonId);
}

void WorldSession::HandleDFGetJoinStatus(WorldPackets::LFG::DfGetJoinStatus& /*packet*/)
{
    TC_LOG_DEBUG("lfg", "CMSG_DF_GET_JOIN_STATUS %s", GetPlayerInfo().c_str());

    if (!GetPlayer()->isUsingLfg())
        return;

    ObjectGuid guid = GetPlayer()->GetGUID();
    lfg::LfgUpdateData updateData = sLFGMgr->GetLfgStatus(guid);

    if (GetPlayer()->GetGroup())
    {
        SendLfgUpdateStatus(updateData, true);
        updateData.dungeons.clear();
        SendLfgUpdateStatus(updateData, false);
    }
    else
    {
        SendLfgUpdateStatus(updateData, false);
        updateData.dungeons.clear();
        SendLfgUpdateStatus(updateData, true);
    }
}

void WorldSession::SendLfgUpdateStatus(lfg::LfgUpdateData const& updateData, bool party)
{
    bool join = false;
    bool queued = false;
    uint8 size = uint8(updateData.dungeons.size());
    ObjectGuid guid = _player->GetGUID();
    time_t joinTime = sLFGMgr->GetQueueJoinTime(_player->GetGUID());
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());

    switch (updateData.updateType)
    {
        case lfg::LFG_UPDATETYPE_JOIN_QUEUE_INITIAL:            // Joined queue outside the dungeon
            join = true;
            break;
        case lfg::LFG_UPDATETYPE_JOIN_QUEUE:
        case lfg::LFG_UPDATETYPE_ADDED_TO_QUEUE:                // Rolecheck Success
            join = true;
            queued = true;
            break;
        case lfg::LFG_UPDATETYPE_PROPOSAL_BEGIN:
            join = true;
            break;
        case lfg::LFG_UPDATETYPE_UPDATE_STATUS:
            join = updateData.state != lfg::LFG_STATE_ROLECHECK && updateData.state != lfg::LFG_STATE_NONE;
            queued = updateData.state == lfg::LFG_STATE_QUEUED;
            break;
        default:
            break;
    }

    TC_LOG_DEBUG("lfg", "SMSG_LFG_UPDATE_STATUS %s updatetype: %u, party %s",
        GetPlayerInfo().c_str(), updateData.updateType, party ? "true" : "false");

    WorldPackets::LFG::LfgUpdateStatus Update;
    Update.Ride.RequesterGuid = guid;
    Update.Ride.Time = joinTime;  // Join date
    Update.Ride.Id = queueId;
    Update.Ride.Type = 2;
    Update.SubType = 1;
    Update.Reason = updateData.updateType;
    Update.IsParty = party;
    Update.Joined = join;
    Update.LfgJoined = join;
    Update.Queued = queued;
    Update.NotifyUI = true;
    Update.RequestedRoles = sLFGMgr->GetRoles(_player->GetGUID());

    for (lfg::LfgDungeonSet::const_iterator it = updateData.dungeons.begin(); it != updateData.dungeons.end(); ++it)
        Update.Slots.push_back(*it);

    SendPacket(Update.Write());
}

void WorldSession::SendLfgRoleChosen(ObjectGuid guid, uint8 roles)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_ROLE_CHOSEN %s guid: %s roles: %u",
        GetPlayerInfo().c_str(), guid.ToString().c_str(), roles);

    WorldPackets::LFG::LfgRoleChosen lfg;
    lfg.Guid = guid;
    lfg.Roles = roles;
    SendPacket(lfg.Write());
}

void WorldSession::SendLfgRoleCheckUpdate(lfg::LfgRoleCheck const& roleCheck)
{
    lfg::LfgDungeonSet dungeons;
    if (roleCheck.rDungeonId)
        dungeons.insert(roleCheck.rDungeonId);
    else
        dungeons = roleCheck.dungeons;

    TC_LOG_DEBUG("lfg", "SMSG_LFG_ROLE_CHECK_UPDATE %s", GetPlayerInfo().c_str());
    WorldPackets::LFG::LfgRoleCheckUpdate data;

    data.RoleCheckStatus = roleCheck.state;                       // Check result
    data.IsBeginning = roleCheck.state == lfg::LFG_ROLECHECK_INITIALITING;
    for (lfg::LfgDungeonSet::iterator it = dungeons.begin(); it != dungeons.end(); ++it)
        data.Slots.push_back(*it); // Dungeon

    if (!roleCheck.roles.empty())
    {
        // Leader info MUST be sent 1st :S
        WorldPackets::LFG::LfgRoleCheckUpdate::RoleCheckUpdateMember memberleader;
        ObjectGuid guid = roleCheck.leader;
        uint8 roles = roleCheck.roles.find(guid)->second;
        Player* player = ObjectAccessor::FindConnectedPlayer(guid);
        memberleader.guid = guid;                                      // Guid
        memberleader.Complete = roles > 0;                          // Ready
        memberleader.RolesDesired = roles;                             // Roles
        memberleader.Level = player ? player->getLevel() : 0;    // Level
        data.MemberUpdate.push_back(memberleader);

        for (lfg::LfgRolesMap::const_iterator it = roleCheck.roles.begin(); it != roleCheck.roles.end(); ++it)
        {
            if (it->first == roleCheck.leader)
                continue;

            WorldPackets::LFG::LfgRoleCheckUpdate::RoleCheckUpdateMember member;
            guid = it->first;
            roles = it->second;
            player = ObjectAccessor::FindConnectedPlayer(guid);
            member.guid = guid;                                  // Guid
            member.Complete = roles > 0;                      // Ready
            member.RolesDesired = roles;                         // Roles
            member.Level = player ? player->getLevel() : 0;// Level
            data.MemberUpdate.push_back(member);
        }
    }
    SendPacket(data.Write());
}

void WorldSession::SendLfgJoinResult(lfg::LfgJoinResultData const& joinData)
{
    uint32 size = 0;
    ObjectGuid guid = GetPlayer()->GetGUID();
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());
    for (lfg::LfgLockPartyMap::const_iterator it = joinData.lockmap.begin(); it != joinData.lockmap.end(); ++it)
        size += 8 + 4 + uint32(it->second.size()) * (4 + 4 + 4 + 4);
    TC_LOG_DEBUG("lfg", "SMSG_LFG_JOIN_RESULT %s checkResult: %u checkValue: %u",
        GetPlayerInfo().c_str(), joinData.result, joinData.state);
    WorldPackets::LFG::LfgJoinResult Result;
    Result.Ride.RequesterGuid = guid;

    for (lfg::LfgLockPartyMap::const_iterator it = joinData.lockmap.begin(); it != joinData.lockmap.end(); ++it)
    {
        WorldPackets::LFG::LFGBlackList blacklist;
        blacklist.PlayerGuid = it->first;
        for (lfg::LfgLockMap::const_iterator itr = it->second.begin(); itr != it->second.end(); ++itr)
        {
            WorldPackets::LFG::LFGBlackListInfo info;
            info.Slot = itr->first;
            info.Reason = itr->second.lockStatus;
            info.SubReason1 = itr->second.currentItemLevel;
            info.SubReason2 = itr->second.requiredItemLevel;
            blacklist.Info.push_back(info);
        }
        Result.Blacklists.push_back(blacklist);
    }
    Result.Ride.Time = sLFGMgr->GetQueueJoinTime(guid);              // Join date
    Result.Ride.Id = uint32(queueId);                               // Queue Id
    Result.Ride.Type = 2;
    Result.ResultDetails = joinData.result;                        // Check Result
    Result.Result = joinData.state;                         // Check Value
    SendPacket(Result.Write());
}

void WorldSession::SendLfgQueueStatus(lfg::LfgQueueStatusData const& queueData)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_QUEUE_STATUS %s state: %s, dungeon: %u, waitTime: %d, "
        "avgWaitTime: %d, waitTimeTanks: %d, waitTimeHealer: %d, waitTimeDps: %d, "
        "queuedTime: %u, tanks: %u, healers: %u, dps: %u",
        GetPlayerInfo().c_str(), lfg::GetStateString(sLFGMgr->GetState(GetPlayer()->GetGUID())).c_str(), queueData.dungeonId, queueData.waitTime, queueData.waitTimeAvg,
        queueData.waitTimeTank, queueData.waitTimeHealer, queueData.waitTimeDps,
        queueData.queuedTime, queueData.tanks, queueData.healers, queueData.dps);

    ObjectGuid guid = _player->GetGUID();
    WorldPackets::LFG::LfgQueueStatus Queue;
    Queue.Ride.RequesterGuid = guid;
    Queue.QueuedTime = queueData.queuedTime;                            // Player wait time in queue
    Queue.Ride.Time = queueData.joinTime;                               // Join time
    Queue.Options[0].AverageWaitTimeByRole = queueData.waitTimeTank;    // Wait Tanks
    Queue.Options[0].LastNeeded = queueData.tanks;                      // Tanks needed
    Queue.Options[1].AverageWaitTimeByRole = queueData.waitTimeHealer;  // Wait Healers
    Queue.Options[1].LastNeeded = queueData.healers;                    // Healers needed
    Queue.Options[2].AverageWaitTimeByRole = queueData.waitTimeDps;     // Wait Dps
    Queue.Options[2].LastNeeded = queueData.dps;                        // Dps needed
    Queue.Ride.Id = queueData.queueId;                                  // Queue Id
    Queue.Ride.Type = 2;
    Queue.Slot = queueData.dungeonId;
    Queue.AverageWaitTime = queueData.waitTime;                         // Wait Time             <====== not sure
    Queue.AverageWaitTimeSelf = queueData.waitTimeAvg;                  // Average Wait time     <====== not sure
    SendPacket(Queue.Write());
}

void WorldSession::SendLfgBootProposalUpdate(lfg::LfgPlayerBoot const& boot)
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    lfg::LfgAnswer playerVote = boot.votes.find(guid)->second;
    uint8 votesNum = 0;
    uint8 agreeNum = 0;
    uint32 secsleft = uint8((boot.cancelTime - time(NULL)) / 1000);
    for (lfg::LfgAnswerContainer::const_iterator it = boot.votes.begin(); it != boot.votes.end(); ++it)
    {
        if (it->second != lfg::LFG_ANSWER_PENDING)
        {
            ++votesNum;
            if (it->second == lfg::LFG_ANSWER_AGREE)
                ++agreeNum;
        }
    }
    TC_LOG_DEBUG("lfg", "SMSG_LFG_BOOT_PROPOSAL_UPDATE %s inProgress: %u - "
        "didVote: %u - agree: %u - victim: %s votes: %u - agrees: %u - left: %u - "
        "needed: %u - reason %s",
        GetPlayerInfo().c_str(), uint8(boot.inProgress), uint8(playerVote != lfg::LFG_ANSWER_PENDING),
        uint8(playerVote == lfg::LFG_ANSWER_AGREE), boot.victim.ToString().c_str(), votesNum, agreeNum,
        secsleft, lfg::LFG_GROUP_KICK_VOTES_NEEDED, boot.reason.c_str());

    lfg::LFGQueue& queue = sLFGMgr->GetQueue(guid);
    uint8 voteKicksNeeded = lfg::LFG_GROUP_KICK_VOTES_NEEDED;
    if (queue.isScenario)
        voteKicksNeeded = 2;

    WorldPackets::LFG::LfgBootPlayer bootInfo;
    bootInfo.Boot.VoteInProgress = boot.inProgress;
    bootInfo.Boot.MyVote = 0;
    bootInfo.Boot.MyVoteCompleted = playerVote != lfg::LFG_ANSWER_PENDING;
    bootInfo.Boot.BootVotes = playerVote == lfg::LFG_ANSWER_AGREE;
    bootInfo.Boot.Target = boot.victim;
    bootInfo.Boot.TotalVotes = votesNum;
    bootInfo.Boot.VotePassed = agreeNum >= voteKicksNeeded;
    bootInfo.Boot.TimeLeft = secsleft;
    bootInfo.Boot.VotesNeeded = voteKicksNeeded;
    bootInfo.Boot.Reason = boot.reason;
    SendPacket(bootInfo.Write());
}

void WorldSession::SendLfgUpdateProposal(lfg::LfgProposal const& proposal)
{
    ObjectGuid guid = GetPlayer()->GetGUID();
    ObjectGuid gguid = proposal.players.find(guid)->second.group;
    bool silent = !proposal.isNew && gguid == proposal.group;
    uint32 dungeonEntry = proposal.dungeonId;
    uint32 queueId = sLFGMgr->GetQueueId(_player->GetGUID());
    time_t joinTime = sLFGMgr->GetQueueJoinTime(_player->GetGUID());

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PROPOSAL_UPDATE %s state: %u",
        GetPlayerInfo().c_str(), proposal.state);

    // show random dungeon if player selected random dungeon and it's not lfg group
    if (!silent)
    {
        lfg::LfgDungeonSet const& playerDungeons = sLFGMgr->GetSelectedDungeons(guid);
        if (playerDungeons.find(proposal.dungeonId) == playerDungeons.end())
            dungeonEntry = (*playerDungeons.begin());
    }
    
    WorldPackets::LFG::LfgProposalUpdate data;
    data.Ride.Time = joinTime;
    data.CompletedMask = proposal.encounters;                   // Encounters done
    data.Ride.Id = queueId;                               // Queue Id
    data.Ride.Type = 2;
    data.Slot = dungeonEntry;                          // Dungeon
    data.ProposalID = proposal.id;                           // Proposal Id
    data.State = proposal.state;                         // State

    if (proposal.state)
        data.IsRequeue = true;

    data.Ride.RequesterGuid = !guid.IsEmpty() ? guid : gguid;

    for (lfg::LfgProposalPlayerContainer::const_iterator it = proposal.players.begin(); it != proposal.players.end(); ++it)
    {
        lfg::LfgProposalPlayer const& player = it->second;
        WorldPackets::LFG::LfgProposalUpdate::PlayerInfo info;

        if (!player.group.IsEmpty())
        {
            info.SameParty = player.group == proposal.group;      // Is group in dungeon
            info.MyParty = player.group == gguid;               // Same group as the player
        }

        info.Accepted = player.accept == lfg::LFG_ANSWER_AGREE;
        info.Responded = player.accept != lfg::LFG_ANSWER_PENDING;
        info.Me = it->first == guid;
        info.Roles = player.role;
        data.Players.push_back(info);
    }

    SendPacket(data.Write());
}

void WorldSession::SendLfgLfrList(bool /*update*/)
{
    //TC_LOG_DEBUG("lfg", "SMSG_LFG_LFR_LIST %s update: %u",
    //    GetPlayerInfo().c_str(), update ? 1 : 0);
    //WorldPacket data(SMSG_LFG_UPDATE_SEARCH, 1);
    //data << uint8(update);                                 // In Lfg Queue?
    //SendPacket(&data);
}

void WorldSession::SendLfgDisabled()
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_DISABLED %s", GetPlayerInfo().c_str());
    SendPacket(WorldPackets::LFG::LfgDisabled().Write());
}

void WorldSession::SendLfgOfferContinue(uint32 dungeonEntry)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_OFFER_CONTINUE %s dungeon entry: %u",
        GetPlayerInfo().c_str(), dungeonEntry);

    WorldPackets::LFG::LfgOfferContinue offer;
    offer.DungeonEntry = dungeonEntry;
    SendPacket(offer.Write());
}

void WorldSession::SendLfgTeleportError(uint8 err)
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_TELEPORT_DENIED %s reason: %u",
        GetPlayerInfo().c_str(), err);
    WorldPacket data(SMSG_LFG_TELEPORT_DENIED, 4);
    data << uint32(err);                                   // Error
    SendPacket(&data);
}

/*
void WorldSession::SendLfrUpdateListOpcode(uint32 dungeonEntry)
{
    TC_LOG_DEBUG("network", "SMSG_LFG_UPDATE_LIST %s dungeon entry: %u",
        GetPlayerInfo().c_str(), dungeonEntry);
    WorldPacket data(SMSG_LFG_UPDATE_LIST);
    SendPacket(&data);
}
*/

void WorldSession::SendLfgPlayerLockInfo()
{
    TC_LOG_DEBUG("lfg", "SMSG_LFG_PLAYER_INFO %s", GetPlayerInfo().c_str());

    // Get Random dungeons that can be done at a certain level and expansion
    uint8 level = GetPlayer()->getLevel();
    lfg::LfgDungeonSet const& randomDungeons = sLFGMgr->GetRandomAndSeasonalDungeons(level, GetExpansion());

    WorldPackets::LFG::LfgPlayerInfo lfgPlayerInfo;

    // Get player locked Dungeons
    /*for (auto const& lock : sLFGMgr->GetLockedDungeons(_player->GetGUID()))
        lfgPlayerInfo.BlackList.Info.emplace_back(lock.first, lock.second.lockStatus, lock.second.requiredItemLevel, lock.second.currentItemLevel);*/

    /*for (uint32 slot : randomDungeons)
    {
        lfgPlayerInfo.Dungeons.emplace_back();
        WorldPackets::LFG::DungeonInfo& playerDungeonInfo = lfgPlayerInfo.Dungeons.back();
        playerDungeonInfo.Slot = slot;
        playerDungeonInfo.CompletionQuantity = 1;
        playerDungeonInfo.CompletionLimit = 1;
        playerDungeonInfo.CompletionCurrencyID = 0;
        playerDungeonInfo.SpecificQuantity = 0;
        playerDungeonInfo.SpecificLimit = 1;
        playerDungeonInfo.OverallQuantity = 0;
        playerDungeonInfo.OverallLimit = 1;
        playerDungeonInfo.PurseWeeklyQuantity = 0;
        playerDungeonInfo.PurseWeeklyLimit = 0;
        playerDungeonInfo.PurseQuantity = 0;
        playerDungeonInfo.PurseLimit = 0;
        playerDungeonInfo.Quantity = 1;
        playerDungeonInfo.CompletedMask = 0;

        if (lfg::LfgReward const* reward = sLFGMgr->GetRandomDungeonReward(slot, level))
        {
            if (Quest const* quest = sObjectMgr->GetQuestTemplate(reward->firstQuest))
            {
                playerDungeonInfo.FirstReward = !GetPlayer()->CanRewardQuest(quest, false);
                if (!playerDungeonInfo.FirstReward)
                    quest = sObjectMgr->GetQuestTemplate(reward->otherQuest);

                if (quest)
                {
                    playerDungeonInfo.Rewards.RewardMoney = _player->GetQuestMoneyReward(quest);
                    playerDungeonInfo.Rewards.RewardXP = _player->GetQuestXPReward(quest);
                    for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
                        if (uint32 itemId = quest->RewardItemId[i])
                            playerDungeonInfo.Rewards.Item.emplace_back(itemId, quest->RewardItemCount[i]);

                    for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
                        if (uint32 curencyId = quest->RewardCurrencyId[i])
                            playerDungeonInfo.Rewards.Currency.emplace_back(curencyId, quest->RewardCurrencyCount[i]);
                }
            }
        }
    }*/

    SendPacket(lfgPlayerInfo.Write());;
}

#ifndef PROJECT_CUSTOM

void WorldSession::SendLfgPlayerReward(lfg::LfgPlayerRewardData const& rewardData)
{
    /*if (!rewardData.rdungeonEntry || !rewardData.sdungeonEntry || !rewardData.quest)
        return;

    TC_LOG_DEBUG("lfg", "SMSG_LFG_PLAYER_REWARD %s rdungeonEntry: %u, sdungeonEntry: %u, done: %u",
        GetPlayerInfo().c_str(), rewardData.rdungeonEntry, rewardData.sdungeonEntry, rewardData.done);

    WorldPackets::LFG::LFGPlayerReward lfgPlayerReward;
    lfgPlayerReward.QueuedSlot = rewardData.rdungeonEntry;
    lfgPlayerReward.ActualSlot = rewardData.sdungeonEntry;
    lfgPlayerReward.RewardMoney = GetPlayer()->GetQuestMoneyReward(rewardData.quest);
    lfgPlayerReward.AddedXP = GetPlayer()->GetQuestXPReward(rewardData.quest);

    for (uint8 i = 0; i < QUEST_REWARD_ITEM_COUNT; ++i)
        if (uint32 itemId = rewardData.quest->RewardItemId[i])
            lfgPlayerReward.Rewards.emplace_back(itemId, rewardData.quest->RewardItemCount[i], 0, false);

    for (uint32 i = 0; i < QUEST_REWARD_CURRENCY_COUNT; ++i)
        if (uint32 curencyId = rewardData.quest->RewardCurrencyId[i])
            lfgPlayerReward.Rewards.emplace_back(curencyId, rewardData.quest->RewardCurrencyCount[i], 0, true);

    SendPacket(lfgPlayerReward.Write());*/
}

#endif
