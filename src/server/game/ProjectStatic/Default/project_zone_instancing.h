#ifndef PROJECT_ZONE_INSTANCING_H
#define PROJECT_ZONE_INSTANCING_H

#include "project.h"

class MapManager;
class Map;

namespace PROJECT
{
    namespace ZoneInstancing
    {
        enum Defines
        {
            INSTANCE_ID_START = 10000,
        };

        typedef std::map<uint32, std::vector<uint32>> InstancingList;
        extern InstancingList Zones;
        extern std::map<uint32, uint32> FallbackZones;

        uint32 TC_GAME_API GetInstanceIdForMapZone(uint32 mapId, uint32 zoneId = 0, uint32 areaId = 0);
        TC_GAME_API Map* GetInstance(uint32 mapId, float x, float y, float z);
        TC_GAME_API Map* GetInstance(uint32 mapId, Player* player, float x, float y, float z);
        void TC_GAME_API UpdateInstanceForPlayer(Player * player, uint32 newZone, uint32 newArea);
        bool TC_GAME_API IsForcedInstance(uint32 mapId);
        void TC_GAME_API InitializeInstances(MapManager * mapManager);
    }
}

#endif
