#include "project.h"
#include "project_language.h"
#ifdef PROJECT_CUSTOM
#include "project_warden.h"
#include "project_battle_pay.h"
#include "project_black_market.h"
#endif
#include "Config.h"
#include "World.h"

namespace PROJECT {

namespace StaticConfig
{
    uint32 MaxPacketsPerSessionUpdate = 100;
}

TC_GAME_API _Config * Config = _Config::instance();

_Config* _Config::instance()
{
    static _Config instance;
    return &instance;
}

void _Config::Reload(bool full /* = true */)
{
    if (full)
        sWorld->LoadConfigSettings(true);

    Clear();

    // allow adding new variables
    EnsureVariablesAlreadyExist(false);
    
    // booleans
    _CONFIG_LOAD_BOOL("DungeonFinderOnlyPremade", false);
    _CONFIG_LOAD_BOOL("ChannelChatDisabled", false);
    _CONFIG_LOAD_BOOL("ChannelChatDisabledForGMs", false);
    _CONFIG_LOAD_BOOL("Chat.LinksExtraInfo", false);
    _CONFIG_LOAD_BOOL("SpellDebug", false);
    _CONFIG_LOAD_BOOL("SpellProcDebug", false);
    _CONFIG_LOAD_BOOL("SpellCritChanceDebug", false);
    _CONFIG_LOAD_BOOL("DamageTakenDebug", false);
    _CONFIG_LOAD_BOOL("AuraDebug", false);
    _CONFIG_LOAD_BOOL("SpellEffectsDebug", false);
    _CONFIG_LOAD_BOOL("AuraDropChargeDebug", false);
    _CONFIG_LOAD_BOOL("LogAutomuteReasons", false);
    _CONFIG_LOAD_BOOL("AutoMute", true);
    _CONFIG_LOAD_BOOL("SpamFilter", true);
    _CONFIG_LOAD_BOOL("PlayerSaveCompletlyDisabled", false);
    _CONFIG_LOAD_BOOL("AreatriggerDebug", false);
    _CONFIG_LOAD_BOOL("CanTakeQuestDebug", false);
    _CONFIG_LOAD_BOOL("CanGMsCastSpellsInInstancedMaps", false);
    _CONFIG_LOAD_BOOL("ForcePvPOnEverywhere", false);
    _CONFIG_LOAD_BOOL("ForcePvPOnEverywhereLevel85", false);
    _CONFIG_LOAD_BOOL("ForcePvPOffInContestedAndNeutral", false);
    _CONFIG_LOAD_BOOL("AllPetsDisabled", false);
    _CONFIG_LOAD_BOOL("AllowGMHonorableKill", false);
    _CONFIG_LOAD_BOOL("GuildAchievementCriteriaDebug", false);
    _CONFIG_LOAD_BOOL("HideGameMastersInGuildRosters", true);
    _CONFIG_LOAD_BOOL("OpcodesDebugAll", false);
    _CONFIG_LOAD_BOOL("OpcodesDebugDumpPackets", false);
    _CONFIG_LOAD_BOOL("BossOrderDisabled", false);
    _CONFIG_LOAD_BOOL("EntityNewOrSummonDebug", false);
    _CONFIG_LOAD_BOOL("SpellNewDebug", false);
    _CONFIG_LOAD_BOOL("RaidFinder.CrossFaction", false);
    _CONFIG_LOAD_BOOL("DetectUselessSpellDbcOverrides", false);
    _CONFIG_LOAD_BOOL("ZoneLevelInstancingDisabled", false);
    _CONFIG_LOAD_BOOL("PetBattles.Enabled", true);
    _CONFIG_LOAD_BOOL("PetBattles.SaveEnabled", true);
    _CONFIG_LOAD_BOOL("PetBattles.DebugMode", false);
    _CONFIG_LOAD_BOOL("Battlegrounds.CrossFaction", false);
    _CONFIG_LOAD_BOOL("TestRealm", false);
    _CONFIG_LOAD_BOOL("ArenaSoloQueue.Enabled", true);
    _CONFIG_LOAD_BOOL("PremadeGroup.CrossFaction", true);
    _CONFIG_LOAD_BOOL("RatedPvP.Enabled", true);
    _CONFIG_LOAD_BOOL("DumpSQL.Spells", false);
    _CONFIG_LOAD_BOOL("DumpSQL.Hotfixes", false);
    _CONFIG_LOAD_BOOL("Archaeology.MarkDigPositions", false);
    _CONFIG_LOAD_BOOL("Archaeology.RegenerateDigSites", false);
    _CONFIG_LOAD_BOOL("Launcher.RequireLauncher", true);
    _CONFIG_LOAD_BOOL("Launcher.ClientHashCheck", true);
    _CONFIG_LOAD_BOOL("Launcher.NoLauncherForGMs", false);
    _CONFIG_LOAD_BOOL("Launcher.NoClientHashCheckForGMs", false);
    _CONFIG_LOAD_BOOL("Launcher.LauncherUpdateEnabled", true);
    _CONFIG_LOAD_BOOL("Launcher.LogUnlistedHashes", true);
    _CONFIG_LOAD_BOOL("Launcher.LogBannedModules", true);
    _CONFIG_LOAD_BOOL("DetectUselessSpellDbcOverrides", false);
    _CONFIG_LOAD_BOOL("Throttle.DBReply", true);
    _CONFIG_LOAD_BOOL("MythicMode.Enabled", true);
    _CONFIG_LOAD_BOOL("DungeonFinder.PlayersInRangeAnnouncer", true);
    _CONFIG_LOAD_BOOL("Mounts.LinkedToAccount", false);
    _CONFIG_LOAD_BOOL("Toys.LinkedToAccount", false);
    _CONFIG_LOAD_BOOL("Heirlooms.LinkedToAccount", false);
    _CONFIG_LOAD_BOOL("Transmog.LinkedToAccount", false);
    _CONFIG_LOAD_BOOL("Chat.LogEverything", false);

    // integers
    _CONFIG_LOAD_INT32("CrowdControlSpellsDelay", 200);
    _CONFIG_LOAD_INT32("StartingGuildLevel", 1);
    _CONFIG_LOAD_INT32("RealmLanguage", PROJECT::Language::Languages::ENGLISH);
    _CONFIG_LOAD_INT32("GUIDRecycleStart", 1000000);
    _CONFIG_LOAD_INT32("GUIDRecycleEnd", 4000000);
    _CONFIG_LOAD_INT32("ArenaBootyRunMinPlayers", 3);
    _CONFIG_LOAD_INT32("ArenaBootyRunIntervalMinutes", 180);
    _CONFIG_LOAD_INT32("ArenaBootyRunAnnounceIntervalMinutes", 15);
    _CONFIG_LOAD_INT32("DungeonFinderMinIlvlRandomHoT", 353);
    _CONFIG_LOAD_INT32("DungeonFinderMinIlvlRandomCataHeroic", 329);
    _CONFIG_LOAD_INT32("GroupUpdateCooldown", 10);
    _CONFIG_LOAD_INT32("AFKKickTimer", 60 * 5);
    _CONFIG_LOAD_INT32("AFKKickReminderTimer", 60 * 4);
    _CONFIG_LOAD_INT32("GuardsBerserkMinPlayers", 0);
    _CONFIG_LOAD_INT32("DamageHackLimitPlayer", 500000);
    _CONFIG_LOAD_INT32("DamageHackLimitPet", 250000);
    _CONFIG_LOAD_INT32("ValorPointsGainPercent", 100);
    _CONFIG_LOAD_INT32("BaseResilience", 0);
    _CONFIG_LOAD_INT32("BattleFatigue", 0);
    _CONFIG_LOAD_INT32("AuraTargetMapUpdateInterval", 1000);
    _CONFIG_LOAD_INT32("MMapsDisableTimeout", 10);
    _CONFIG_LOAD_INT32("PetBattles.InitialLevel", 1);
    _CONFIG_LOAD_INT32("PetBattles.InitialSlotsCount", 1);
    _CONFIG_LOAD_INT32("PetBattles.RoundTime", 30);
    _CONFIG_LOAD_INT32("PetBattles.QueueLevelDiscardTimer", 10);
    _CONFIG_LOAD_INT32("PetBattles.WildForfeitPenalty", 10);
    _CONFIG_LOAD_INT32("PetBattles.LesserCharmAmount", 5);
    _CONFIG_LOAD_INT32("Loot.Special.Chance10Man", 20);
    _CONFIG_LOAD_INT32("Loot.Special.Chance25Man", 50);
    _CONFIG_LOAD_INT32("Loot.BonusRoll.Gold", 300);
    _CONFIG_LOAD_INT32("Loot.BonusRoll.BaseChance", 20);
    _CONFIG_LOAD_INT32("LFR.NerfHP", 40);
    _CONFIG_LOAD_INT32("LFR.NerfDamage", 40);
    _CONFIG_LOAD_INT32("LFR.DebugReqPlayers", 0);
    _CONFIG_LOAD_INT32("LFR.MinWipeDurationForBuff", 30);
    _CONFIG_LOAD_INT32("DungeonFinder.DebugReqPlayers", 0);
    _CONFIG_LOAD_INT32("Battlegrounds.ReportAfkThreshold", 3);
    _CONFIG_LOAD_INT32("Scenario.DebugReqPlayers", 0);
    _CONFIG_LOAD_INT32("ArenaSoloQueue.QueueUpdateTimer", 5);
    _CONFIG_LOAD_INT32("ArenaSoloQueue.MinItemLevel.Skirmish", 0);
    _CONFIG_LOAD_INT32("ArenaSoloQueue.MinItemLevel.Rated", 0);
    _CONFIG_LOAD_INT32("ArenaSoloQueue.DebugReqPlayers", 0);
    _CONFIG_LOAD_INT32("ArenaSoloQueue.DeserterDuration", 3600);
    _CONFIG_LOAD_INT32("HPScaling.PerPlayer", 5);
    _CONFIG_LOAD_INT32("RealmRequiresSpecialAccess", 0);
    _CONFIG_LOAD_INT32("OnLogin.AddAchievement", 0);
    _CONFIG_LOAD_INT32("OnLogin.Conversation.Id", 0);
    _CONFIG_LOAD_INT32("OnLogin.Conversation.Duration", 0);
    _CONFIG_LOAD_INT32("Arena.StartMatchmakerRating", 1500);
    _CONFIG_LOAD_INT32("PvP.PvEItemLevelDownscalingLimit", 0);
    _CONFIG_LOAD_INT32("PvP.PvEItemLevelDownscalingDiff", 0);
    _CONFIG_LOAD_INT32("PvP.Reward.RandomBgFirst", 143680);
    _CONFIG_LOAD_INT32("PvP.Reward.RandomBgNth", 138880);
    _CONFIG_LOAD_INT32("PvP.Reward.SkirmishArenaFirst", 143713);
    _CONFIG_LOAD_INT32("PvP.Reward.SkirmishArenaNth", 138864);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedBgFirst", 143716);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedBgNth", 138881);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedArena2v2First", 143714);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedArena2v2Nth", 138865);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedArena3v3First", 143677);
    _CONFIG_LOAD_INT32("PvP.Reward.RatedArena3v3Nth", 147198);
    _CONFIG_LOAD_INT32("PvP.Reward.Honor.RandomBg", 300);
    _CONFIG_LOAD_INT32("PvP.Reward.Honor.RatedBg", 600);
    _CONFIG_LOAD_INT32("PvP.Reward.Honor.SkirmishArena", 160);
    _CONFIG_LOAD_INT32("PvP.Reward.Honor.RatedArena2v2", 200);
    _CONFIG_LOAD_INT32("PvP.Reward.Honor.RatedArena3v3", 200);
    _CONFIG_LOAD_INT32("DeletedCharacters.NameReservationDuration", 30);
    _CONFIG_LOAD_INT32("DeletedCharacters.KeepInDatabaseMinLevel", sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL));

    // floats
    _CONFIG_LOAD_FLOAT("Rate.XP.80Plus", 1.0f);
    _CONFIG_LOAD_FLOAT("Rate.XP.Dungeons", 1.0f);
    _CONFIG_LOAD_FLOAT("Rate.XP.ContentMOP", 1.0f);
    _CONFIG_LOAD_FLOAT("Rate.XP.ContentWOD", 1.0f);
    _CONFIG_LOAD_FLOAT("Rate.XP.ContentLEGION", 1.0f);
    _CONFIG_LOAD_FLOAT("Rate.Currency.JusticePoints", 1.0f);
    _CONFIG_LOAD_FLOAT("WorldUpdateSpeed", 1.0f);
    _CONFIG_LOAD_FLOAT("Loot.Legendaries.BaseDropChance", 0.5f);
    _CONFIG_LOAD_FLOAT("Generators.SQL.InhabitAirLimit", 5.0f);

    // strings
    _CONFIG_LOAD_STRING("OnLoginMessage.RO", "");
    _CONFIG_LOAD_STRING("OnLoginMessage.EN", "");
    _CONFIG_LOAD_STRING("DataStores.Prefix", "715_");
    _CONFIG_LOAD_STRING("Launcher.Directory", "launcher");

    // static configs
    StaticConfig::MaxPacketsPerSessionUpdate = sConfigMgr->GetIntDefault("PROJECT.Network.MaxPacketsPerSessionUpdate", 100);

#ifdef PROJECT_CUSTOM
    // separate configs
    PROJECT_WardenManager->LoadConfig();
    PROJECT_BlackMarketManager->LoadConfig();
    PROJECT_BattlePayManager->LoadConfig();
#endif
    // no longer allow adding/retrieving new variables
    EnsureVariablesAlreadyExist(true);
}

namespace HardcodedVariables {
HARDCODED_VARIABELS_DECLARATIONS
}

} // namespace PROJECT
