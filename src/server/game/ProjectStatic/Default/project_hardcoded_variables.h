#ifndef PROJECT_HARDCODED_VARS_H
#define PROJECT_HARDCODED_VARS_H

/*
    Some functions are extensively used, we want to avoid the memory allocation/free for the std::string. While this partially beats the purpose of generic variables,
    the CPU usage is drastically decreased and this also greatly decreases locks for malloc and free
*/

#define DEFINE_HARDCODED_VARIABLE(v) extern const std::string v
namespace PROJECT
{
    namespace HardcodedVariables
    {
        DEFINE_HARDCODED_VARIABLE(DisableAchievements);
        DEFINE_HARDCODED_VARIABLE(EligibleGuild);
        DEFINE_HARDCODED_VARIABLE(DebugUpdateOnlyCriteriaId);
        DEFINE_HARDCODED_VARIABLE(DebugIgnoreCriteriaConditions);
        DEFINE_HARDCODED_VARIABLE(CantSeeOrBeSeen);
        DEFINE_HARDCODED_VARIABLE(ScenesLastNpc);
        DEFINE_HARDCODED_VARIABLE(ArenaSpectator);
        DEFINE_HARDCODED_VARIABLE(IsPersonalEntity);
        DEFINE_HARDCODED_VARIABLE(PersonalEntityOwner);
        DEFINE_HARDCODED_VARIABLE(IsHidden);
        DEFINE_HARDCODED_VARIABLE(ForceUpdateTimers);
        DEFINE_HARDCODED_VARIABLE(Cooldowns);
        DEFINE_HARDCODED_VARIABLE(EvadeCounter);
        DEFINE_HARDCODED_VARIABLE(TickLinkedAuras);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataMapHeight);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataDontRecalculateZoneAndAreaID);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataZoneID);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataAreaID);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataLastPosition);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataForceCacheForMapHeight);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataLastCache);
        DEFINE_HARDCODED_VARIABLE(ChatFloodTimeSinceLastMsg);
        DEFINE_HARDCODED_VARIABLE(SpellsChargeRepathTargetGUID);
        DEFINE_HARDCODED_VARIABLE(SpellsAutorepeatActiveClientside);
        DEFINE_HARDCODED_VARIABLE(ChallengesTeam);
        DEFINE_HARDCODED_VARIABLE(ArmoryLogItemSource);
        DEFINE_HARDCODED_VARIABLE(ArmoryLogData2);
        DEFINE_HARDCODED_VARIABLE(CacheMapDataLastWaterState);
    }
}

#define DECLARE_HARDCODED_VARIABLE(v) const std::string v = #v
#define DECLARE_HARDCODED_VARIABLE_EXPLICIT(v,n) const std::string v = n
#define HARDCODED_VARIABELS_DECLARATIONS \
    DECLARE_HARDCODED_VARIABLE(DisableAchievements); \
    DECLARE_HARDCODED_VARIABLE(EligibleGuild); \
    DECLARE_HARDCODED_VARIABLE(CantSeeOrBeSeen); \
    DECLARE_HARDCODED_VARIABLE(IsPersonalEntity); \
    DECLARE_HARDCODED_VARIABLE(IsHidden); \
    DECLARE_HARDCODED_VARIABLE(ForceUpdateTimers); \
    DECLARE_HARDCODED_VARIABLE(Cooldowns); \
    DECLARE_HARDCODED_VARIABLE(EvadeCounter); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(DebugUpdateOnlyCriteriaId, "Debug.UpdateOnlyCriteriaId"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(DebugIgnoreCriteriaConditions, "Debug.IgnoreCriteriaConditions"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ScenesLastNpc, "Scenes.LastNpc"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ArenaSpectator, "Arena.Spectator"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(PersonalEntityOwner, "PersonalEntity.Owner"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(TickLinkedAuras, "Tick.LinkedAuras"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataMapHeight, "Cache.MapData.MapHeight"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataDontRecalculateZoneAndAreaID, "Cache.MapData.DontRecalculateZoneAndAreaID"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataZoneID, "Cache.MapData.ZoneID"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataAreaID, "Cache.MapData.AreaID"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataLastPosition, "Cache.MapData.LastPosition"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataForceCacheForMapHeight, "Cache.MapData.ForceCacheForMapHeight"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataLastCache, "Cache.MapData.LastCache"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ChatFloodTimeSinceLastMsg, "Chat.Flood.TimeSinceLastMsg"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(SpellsChargeRepathTargetGUID, "Spells.ChargeRepath.TargetGUID"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(SpellsAutorepeatActiveClientside, "Spells.AutorepeatActiveClientside"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ChallengesTeam, "Challenges.Team"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ArmoryLogItemSource, "Armory.Log.ItemSource"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(ArmoryLogData2, "Armory.Log.Data2"); \
    DECLARE_HARDCODED_VARIABLE_EXPLICIT(CacheMapDataLastWaterState, "Cache.MapData.LastWaterState"); \


#endif
