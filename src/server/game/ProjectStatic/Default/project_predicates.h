#ifndef PROJECT_PREDICATES_H
#define PROJECT_PREDICATES_H

#include "project_util_placeholder.h"
#include "GridNotifiersImpl.h"
#include "CellImpl.h"

namespace PROJECT
{
    namespace Predicates
    {
        namespace Order
        {
            class HealthPct
            {
                public:
                    HealthPct(bool ascending = true) : m_ascending(ascending) {}
                    bool operator() (const WorldObject* aObj, const WorldObject* bObj) const
                    {
                        Unit const* a = aObj->ToUnit();
                        Unit const* b = bObj->ToUnit();
                        if (!a || !b)
                            return false;
                        float rA = a->GetMaxHealth() ? float(a->GetHealth()) / float(a->GetMaxHealth()) : 0.0f;
                        float rB = b->GetMaxHealth() ? float(b->GetHealth()) / float(b->GetMaxHealth()) : 0.0f;
                        return m_ascending ? rA < rB : rA > rB;
                    }
                private:
                    const bool m_ascending;
            };

            class PowerPct
            {
                public:
                    PowerPct(Powers power, bool ascending = true) : _power(power), _ascending(ascending) { }

                    bool operator()(WorldObject const* objA, WorldObject const* objB) const
                    {
                        Unit const* a = objA->ToUnit();
                        Unit const* b = objB->ToUnit();
                        float rA = (a && a->GetMaxPower(_power)) ? float(a->GetPower(_power)) / float(a->GetMaxPower(_power)) : 0.0f;
                        float rB = (b && b->GetMaxPower(_power)) ? float(b->GetPower(_power)) / float(b->GetMaxPower(_power)) : 0.0f;
                        return _ascending ? rA < rB : rA > rB;
                    }

                    bool operator()(Unit const* a, Unit const* b) const
                    {
                        float rA = a->GetMaxPower(_power) ? float(a->GetPower(_power)) / float(a->GetMaxPower(_power)) : 0.0f;
                        float rB = b->GetMaxPower(_power) ? float(b->GetPower(_power)) / float(b->GetMaxPower(_power)) : 0.0f;
                        return _ascending ? rA < rB : rA > rB;
                    }

                private:
                    Powers const _power;
                    bool const _ascending;
            };
        }

        class Team
        {
            public:
                Team(TeamId teamId) : m_teamId(teamId) {}
                bool operator() (Player * player) const
                {
                    if (player->GetTeamId() != m_teamId)
                        return false;

                    return true;
                }
            private:
                const TeamId m_teamId;
        };

        struct ArenaAnnouncerEligible : public std::unary_function<Player *, bool>
        {
            ArenaAnnouncerEligible() {}

            bool operator() (Player * player) const
            {
                if (PROJECT::Utils::Players::PlayerHasFlag(player, PROJECT::Constants::PlayerFlags::ANNOUNCER_3V3_IGNORE_BROADCASTS))
                    return false;

                return true;
            }
        };

        class Language
        {
            public:
                Language(PROJECT::Language::Languages language) : lang(language) {}
                bool operator() (Player * player) const
                {
                    if (PROJECT::Language::GetLanguageForPlayer(player) != lang)
                        return false;

                    return true;
                }
            private:
                const PROJECT::Language::Languages lang;
        };

        class AnyUnfriendlyAttackableUnitInRange
        {
            public:
                AnyUnfriendlyAttackableUnitInRange(Unit const* funit, float range, bool ignoreStealth = false)
                    : i_funit(funit), i_range(range), i_ignoreStealth(ignoreStealth) { }

                bool operator()(const Unit* u)
                {
                    return u->IsAlive()
                        && i_funit->IsWithinDistInMap(u, i_range)
                        && !i_funit->IsFriendlyTo(u)
                        && i_funit->IsValidAttackTarget(u, i_ignoreStealth)
                        && u->GetCreatureType() != CREATURE_TYPE_CRITTER;
                }
            private:
                Unit const* i_funit;
                float i_range;
                bool i_ignoreStealth;
        };

        class UnitOwner
        {
            public:
                UnitOwner(Unit* unit, bool isOwner) : _unit(unit), _isOwner(isOwner) { }
                bool operator()(WorldObject* object) const
                {
                    if (!object->ToCreature())
                        return true;
                    if (object->ToCreature()->GetOwner() == _unit)
                        return _isOwner ? true : false;
                    else
                        return _isOwner ? false : true;
                }

                bool operator()(Creature* creature) const
                {
                    if (creature->GetOwner() == _unit)
                        return _isOwner ? true : false;
                    else
                        return _isOwner ? false : true;
                }

                bool operator()(Unit* unit) const
                {
                    if (!unit->ToCreature())
                        return true;
                    if (unit->ToCreature()->GetOwner() == _unit)
                        return _isOwner ? true : false;
                    else
                        return _isOwner ? false : true;
                }

            private:
                Unit* _unit;
                bool _isOwner;
        };
    }
}

#endif
