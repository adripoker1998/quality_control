#include "project_zone_instancing.h"
#include "Map.h"
#include "MapManager.h"
#include "WorldSession.h"
#include "DB2Structure.h"
#include "DB2Stores.h"
#include "Player.h"
#include "ObjectMgr.h"

namespace PROJECT {
namespace ZoneInstancing {

InstancingList Zones;
std::map<uint32, uint32> FallbackZones;

uint32 GetInstanceIdForMapZone(uint32 mapId, uint32 zoneId, uint32 areaId)
{
    InstancingList::iterator it = Zones.find(mapId);
    // tried creating instance for non-instanced zone, check InitializeInstances
    if (it == Zones.end())
        return 0;

    // find if the current zoneid/areaid needs to be instanced separately
    // if yes, return the zoneid/areaid as instance id
    for (auto currentZoneOrArea : it->second)
        if (currentZoneOrArea == zoneId || currentZoneOrArea == areaId)
            return currentZoneOrArea;
    
    // if not, there should be a fallback one for it's map (grouped zones in a single instance)
    auto fallbackItr = FallbackZones.find(mapId);
    if (fallbackItr == FallbackZones.end()) // no fallback? return 0 (shouldn't happen, check InitializeInstances)
        return 0;
    return fallbackItr->second;
}

Map* GetInstance(uint32 mapId, float x, float y, float z)
{
    return GetInstance(mapId, nullptr, x, y, z);
}

Map* GetInstance(uint32 mapId, Player* player, float x, float y, float z)
{
    Map* map = player ? sMapMgr->CreateMap(mapId, player) : sMapMgr->CreateBaseMap(mapId);

    if (map->GetEntry()->IsForcedInstance())
    {
        uint32 zoneId = 0;
        uint32 areaId = 0;
        map->GetZoneAndAreaId(zoneId, areaId, x, y, z);
        if (zoneId)
            if (auto newMap = sMapMgr->CreateMap(mapId, player, 0, zoneId, areaId))
                map = newMap;
    }

    return map;
}

void UpdateInstanceForPlayer(Player * player, uint32 newZone, uint32 newArea)
{
    // if an update is already in progress, don't do anything else (probably update was triggered by zone change and now it wants to trigger from area change too)
    if (player->Variables.IsSet("ZoneInstancing.PendingUpdate"))
        return;

    if (!IsForcedInstance(player->GetMapId()))
        return;

    // check if already in the expected instance (on login for example)
    uint32 currentInstance = player->GetInstanceId();
    uint32 expectedInstance = GetInstanceIdForMapZone(player->GetMapId(), newZone, newArea);
    if (currentInstance == expectedInstance)
        return;

    // do not use newMap->AddPlayerToMap(player) here because that wouldn't be thread safe. instead, schedule a fake CMSG_WORLD_PORT_RESPONSE
    // to allow the SessionHandler to handle the teleport in a thread-safe way
    player->SetSemaphoreTeleportFar(true);
    player->m_teleport_dest = WorldLocation(player->GetMapId(), player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation());
    player->m_teleport_options = TELE_TO_SEAMLESS;
    // tell the session handler to also remove player from map, don't remove it directly here because other code will most likely trigger after the zone change
    // and require the player to be in map
    player->Variables.Set("ZoneInstancing.PendingUpdate", true);

    WorldPacket * packet = new WorldPacket(CMSG_WORLD_PORT_RESPONSE);
    player->GetSession()->QueuePacket(packet);
}

bool IsForcedInstance(uint32 mapId)
{
    if (MapEntry const* mapEntry = sMapStore.LookupEntry(mapId))
        return mapEntry->IsForcedInstance();
    
    return false;
}

void InitializeInstances(MapManager * mapManager)
{
    if (PROJECT::Config->GetBool("ZoneLevelInstancingDisabled"))
    {
        mapManager->_nextInstanceId = 1;
        return;
    }

    // define instances used for zone instancing
    mapManager->_nextInstanceId = INSTANCE_ID_START;
    mapManager->_instanceIds.resize(mapManager->_nextInstanceId);
    for (int i = 0; i < INSTANCE_ID_START; i++)
        mapManager->RegisterInstanceId(i);
    
    // load data from DB
    if (QueryResult result = WorldDatabase.Query("SELECT map, zone FROM project_zone_instancing"))
    {
        PROJECT::Defines::UInt32Vector mapsHandled;
        do 
        {
            Field * fields = result->Fetch();
            uint32 mapId = fields[0].GetUInt32();
            int32 zoneId = fields[1].GetInt32();
            if (zoneId > 0)
                Zones[mapId].push_back(zoneId);
            else
                FallbackZones[mapId] = abs(zoneId);

            // set map data and template
            if (std::find(mapsHandled.begin(), mapsHandled.end(), mapId) != mapsHandled.end())
                continue;
            // instanced map
            auto * map = const_cast<MapEntry*>(sMapStore.AssertEntry(mapId));
            map->InstanceType = MAP_INSTANCE;
            map->Flags[1] |= PROJECT::ZoneInstancing::Flags::FORCED_INSTANCE;
            // instance template
            sObjectMgr->_instanceTemplateStore[mapId] = { 0, 0, true };

            mapsHandled.push_back(mapId);
        } while (result->NextRow());
    }

    // generate instance saves for instanced zones
    for (auto zoneData : Zones)
        for (auto zoneId : zoneData.second)
            (new InstanceSave(zoneData.first, zoneId, DIFFICULTY_NONE, 0, 0, false))->SaveToDB();
}

} // ZoneInstancing
} // PROJECT
