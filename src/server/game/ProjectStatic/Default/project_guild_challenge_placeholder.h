#ifndef PROJECT_GUILD_CHALLENGE_PLACEHOLDER_H
#define PROJECT_GUILD_CHALLENGE_PLACEHOLDER_H

#include "project.h"
#include "Guild.h"

#ifdef PROJECT_CUSTOM
#include "project_guild_challenge.h"
#else

namespace PROJECT
{
    namespace GuildChallenges
    {
        class Data
        {
            public:
                Data(Guild * guild) {};

                void LoadFromDB(Field * fields) {};
                void SaveToDB() {};
                void Disband(SQLTransaction trans) {};

        };
    }
}

#endif
#endif