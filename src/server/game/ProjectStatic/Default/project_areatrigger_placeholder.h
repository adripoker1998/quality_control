#ifndef PROJECT_AREATRIGGER_PLACEHOLDER_H
#define PROJECT_AREATRIGGER_PLACEHOLDER_H

#include "project.h"
#include "AreaTrigger.h"

#ifdef PROJECT_CUSTOM
#include "project_areatrigger.h"
#else

namespace PROJECT
{
    namespace Hooks
    {
        namespace AreaTriggers
        {
            inline void Update(AreaTrigger * areaTrigger, uint32 diffTime) {};
            inline void CreateAreaTrigger(AreaTrigger * areaTrigger) {};
        }
    }
}

#endif
#endif
