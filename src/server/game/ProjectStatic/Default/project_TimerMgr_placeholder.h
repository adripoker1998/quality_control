#ifndef PROJECT_TIMERS_MGR_PLACEHOLDER_H
#define PROJECT_TIMERS_MGR_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_TimerMgr.h"
#else

namespace PROJECT
{
    namespace Timers
    {
        enum Repeats
        {
            UNLIMITED = 0,
            ONCE = 1,
        };

        class TC_GAME_API Manager
        {
            public:
                static Manager* instance()
                {
                    static Manager instance;
                    return &instance;
                };
                
                Manager() {};

                void RemoveTimersForEntity(void * entityPointer) {};
                template <typename T>
                uint32 AddTimer(T entityPointer, std::string const& timerName, uint32 interval, uint32 repeats, std::function<void()> callbackFunction = nullptr) { return 0; };

                void RestartTimer(void * entityPointer, uint32 timerId) {};
                template <typename T>
                void RestartTimer(T entityPointer, std::string const& timerName) {};
        };

        #define PROJECT_TimerManager PROJECT::Timers::Manager::instance()
    }
}

#endif
#endif
