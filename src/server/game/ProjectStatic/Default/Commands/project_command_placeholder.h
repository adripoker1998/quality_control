#ifndef PROJECT_COMMAND_PLACEHOLDER_H
#define PROJECT_COMMAND_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "project_command.h"
#else

#include "project.h"
#include "project_language.h"

class WorldSession;

namespace PROJECT
{
    namespace Commands
    {

        enum eProjectCommandScope
        {
            SCOPE_CONSOLE = 0x1,
            SCOPE_WORLD   = 0x2,
        };

        inline bool ChatCommand(char const* text, WorldSession * m_session) { return true; };
    }
}

#endif
#endif
