#ifndef PROJECT_DATABASE_IMPLEMENTATIONS_H
#define PROJECT_DATABASE_IMPLEMENTATIONS_H

#define PROJECT_DATABASE_CHARACTER_PREPARED_STATEMENTS_IMPLEMENTATION \
    /* azerite */ \
    PrepareStatement(PROJECT_CHAR_SEL_ITEM_INSTANCE_AZERITE, "SELECT a.itemGuid, a.level, a.xp FROM project_item_instance_azerite a INNER JOIN character_inventory ci ON ci.item = a.itemGuid WHERE ci.guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ITEM_INSTANCE_AZERITE, "INSERT INTO project_item_instance_azerite (level, xp, itemGuid) VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_ITEM_INSTANCE_AZERITE, "UPDATE project_item_instance_azerite SET level = ?, xp = ? WHERE itemGuid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ITEM_INSTANCE_AZERITE, "DELETE FROM project_item_instance_azerite WHERE itemGuid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_ITEM_INSTANCE_AZERITE_EMPOWERED, "SELECT a.itemGuid, power0, power1, power2, power3 FROM project_item_instance_azerite_empowered a INNER JOIN character_inventory ci ON ci.item = a.itemGuid WHERE ci.guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ITEM_INSTANCE_AZERITE_EMPOWERED, "INSERT INTO project_item_instance_azerite_empowered (power0, power1, power2, power3, itemGuid) VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_ITEM_INSTANCE_AZERITE_EMPOWERED, "UPDATE project_item_instance_azerite_empowered SET power0=?,power1=?,power2=?,power3=? WHERE itemGuid=?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ITEM_INSTANCE_AZERITE_EMPOWERED, "DELETE FROM project_item_instance_azerite_empowered WHERE itemGuid = ?", CONNECTION_ASYNC); \
    /* misc */ \
    /* Account wide mounts */ \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_MOUNTS, "SELECT mountSpellId, flags FROM project_character_mounts WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_REP_CHARACTER_MOUNTS, "REPLACE INTO project_character_mounts (guid, mountSpellId, flags) VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    /*Transmog collection*/ \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_ITEM_APPEARANCES, "SELECT blobIndex, appearanceMask FROM project_character_item_appearances WHERE guid = ? ORDER BY blobIndex DESC", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_ITEM_APPEARANCES, "INSERT INTO project_character_item_appearances (guid, blobIndex, appearanceMask) VALUES (?, ?, ?) " \
    "ON DUPLICATE KEY UPDATE appearanceMask = appearanceMask | VALUES(appearanceMask)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_ITEM_FAVORITE_APPEARANCES, "SELECT itemModifiedAppearanceId FROM project_character_item_favorite_appearances WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_ITEM_FAVORITE_APPEARANCE, "INSERT INTO project_character_item_favorite_appearances (guid, itemModifiedAppearanceId) VALUES (?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_ITEM_FAVORITE_APPEARANCE, "DELETE FROM project_character_item_favorite_appearances WHERE guid = ? AND itemModifiedAppearanceId = ?", CONNECTION_ASYNC); \
    /*toys*/ \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_HEIRLOOMS, "SELECT itemId, flags FROM project_character_heirlooms WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_REP_CHARACTER_HEIRLOOMS, "REPLACE INTO project_character_heirlooms (guid, itemId, flags) VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    /* Account wide toys*/  \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_TOYS, "SELECT itemId, isFavourite FROM project_character_toys WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_REP_CHARACTER_TOYS, "REPLACE INTO project_character_toys (guid, itemId, isFavourite) VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    /* Cache*/ \
    PrepareStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_TOYS, "SELECT itemId, isFavourite, guid FROM project_character_toys ORDER BY guid ASC", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_HEIRLOOMS, "SELECT itemId, flags, guid FROM project_character_heirlooms ORDER BY guid ASC", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_MOUNTS, "SELECT mountSpellId, flags, guid FROM project_character_mounts ORDER BY guid ASC", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_ITEM_APPEARANCES, "SELECT blobIndex, appearanceMask, guid FROM project_character_item_appearances ORDER BY guid ASC, blobIndex DESC", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_ITEM_FAVORITE_APPEARANCES, "SELECT itemModifiedAppearanceId, guid FROM project_character_item_favorite_appearances ORDER BY guid ASC", CONNECTION_SYNCH); \
    \
    PrepareStatement(PROJECT_CHAR_SEL_EXTERNAL_MAILS, "SELECT id, receiver, subject, message, money FROM project_mail_external WHERE NOT sent AND receiver = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_ITEMS_MODIFIERS, "SELECT itemGuid, ModifierIndex, Value FROM item_instance_modifiers im INNER JOIN item_instance i ON im.itemGuid = i.guid WHERE i.owner_guid = ? ORDER BY itemGuid ASC", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHECK_NAME_DELETED, "SELECT 1 FROM characters WHERE name = ? OR (name = ? AND death_expire_time > ?) UNION SELECT 1 FROM reserve_char WHERE name = ? AND userId <> ?", CONNECTION_BOTH); \
    PrepareStatement(PROJECT_CHAR_REP_PLAYER_VARIABLE, "REPLACE INTO project_player_variables_int VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_PLAYER_VARIABLE, "DELETE FROM project_player_variables_int WHERE guid = ? AND variable = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_REP_ITEM_LEVEL, "REPLACE INTO project_character_itemlevel (guid, itemlevel) VALUES (?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_REP_RATED_BG_STATS, "REPLACE INTO project_rated_bg_stats VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    /* guilds */ \
    PrepareStatement(PROJECT_CHAR_UPD_GUILD_RENAME, "UPDATE guild SET `rename` = ? WHERE guildid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_GUILD_MAIN_PROFESSIONS, "DELETE FROM project_main_professions WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_GUILD_MAIN_PROFESSIONS, "REPLACE INTO project_main_professions VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_GUILD_RESERVED_NAME, "SELECT 1 FROM reserve_guild WHERE name = ? AND userId <> ?", CONNECTION_SYNCH); \
    /* garrisons */ \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON, "INSERT INTO project_character_garrison(id, guid, level, talentTree, followerActivationsRemaining, followerActivationsTimestamp) VALUES (?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON, "SELECT id, level, talentTree, followerActivationsRemaining, followerActivationsTimestamp FROM project_character_garrison WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_CHARACTER_GARRISON, "UPDATE project_character_garrison SET level = ?, talentTree = ?, followerActivationsRemaining = ?, followerActivationsTimestamp = ? WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON, "DELETE FROM project_character_garrison WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON_MISSION, "INSERT INTO project_character_garrison_mission(id, garrison_id, mission_id, offer_time, offer_max_duration, start_time, state) VALUES (?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON_MISSIONS, "SELECT id, garrison_id, mission_id, offer_time, offer_max_duration, start_time, state FROM project_character_garrison_mission WHERE garrison_id IN (SELECT b.id FROM project_character_garrison b WHERE b.guid=?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_CHARACTER_GARRISON_MISSION, "UPDATE project_character_garrison_mission SET mission_id = ?, offer_time = ?, offer_max_duration = ?, start_time = ?, state = ? WHERE id = ? AND garrison_id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_MISSION, "DELETE FROM project_character_garrison_mission WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_MISSIONS, "DELETE FROM project_character_garrison_mission WHERE garrison_id = (SELECT b.id FROM project_character_garrison b WHERE b.guid = ? LIMIT 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON_FOLLOWER, "INSERT INTO project_character_garrison_follower(id, garrison_id, follower_id, level, xp, quality, durability, item_level_armor, item_level_weapon, current_mission_id, current_building_id, abilities, zone_support_spell, flags, ship_name) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON_FOLLOWERS, "SELECT id, garrison_id, follower_id, level, xp, quality, durability, item_level_armor, item_level_weapon, current_mission_id, current_building_id, abilities, zone_support_spell, flags, ship_name FROM project_character_garrison_follower WHERE garrison_id IN (SELECT b.id FROM project_character_garrison b WHERE b.guid=?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_CHARACTER_GARRISON_FOLLOWER, "UPDATE project_character_garrison_follower SET follower_id = ?, level = ?, xp = ?, quality = ?, durability = ?, item_level_armor = ?, item_level_weapon = ?, current_mission_id = ?, current_building_id = ?, abilities = ?, zone_support_spell = ?, flags = ?, ship_name = ? WHERE id = ? AND garrison_id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_FOLLOWER, "DELETE FROM project_character_garrison_follower WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_FOLLOWERS, "DELETE FROM project_character_garrison_follower WHERE garrison_id = (SELECT b.id FROM project_character_garrison b WHERE b.guid = ? LIMIT 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON_WORKORDER, "INSERT INTO project_character_garrison_work_order(id, garrison_id, plot_instance_id, shipment_id, creation_time, complete_time) VALUES (?,?,?,?,?,?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON_WORKORDER, "SELECT id, garrison_id, plot_instance_id, shipment_id, creation_time, complete_time FROM project_character_garrison_work_order WHERE garrison_id IN (SELECT b.id FROM project_character_garrison b WHERE b.guid = ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_WORKORDER, "DELETE FROM project_character_garrison_work_order WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_WORKORDERS, "DELETE FROM project_character_garrison_work_order WHERE garrison_id = (SELECT b.id FROM project_character_garrison b WHERE b.guid = ? LIMIT 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON_CATEGORY, "INSERT INTO project_character_garrison_category(garrison_id, spec, cond) VALUES (?,?,?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON_CATEGORY, "SELECT garrison_id, spec, cond FROM project_character_garrison_category WHERE garrison_id IN (SELECT b.id FROM project_character_garrison b WHERE b.guid = ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_CATEGORY, "DELETE FROM project_character_garrison_category WHERE garrison_id = (SELECT b.id FROM project_character_garrison b WHERE b.guid = ? LIMIT 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_CHARACTER_GARRISON_TALENT, "INSERT INTO project_character_garrison_talent(garrison_id, TalentID, ResearchStartTime, Flags) VALUES (?,?,?,?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_CHARACTER_GARRISON_TALENT, "SELECT garrison_id, TalentID, ResearchStartTime, Flags FROM project_character_garrison_talent WHERE garrison_id IN (SELECT b.id FROM project_character_garrison b WHERE b.guid = ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_TALENTS, "DELETE FROM project_character_garrison_talent WHERE garrison_id = (SELECT b.id FROM project_character_garrison b WHERE b.guid = ? LIMIT 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_CHARACTER_GARRISON_TALENT, "DELETE FROM project_character_garrison_talent WHERE garrison_id = ? AND TalentID = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_CHARACTER_GARRISON_TALENT, "UPDATE project_character_garrison_talent SET ResearchStartTime = ?, Flags = ? WHERE garrison_id = ? AND TalentID = ?", CONNECTION_ASYNC); \
    /* tickets */ \
    PrepareStatement(PROJECT_CHAR_SEL_GM_REQUESTS, "SELECT id, playerGuid, note, createTime, mapId, posX, posY, posZ, facing, closedBy, assignedTo, comment FROM project_gm_request", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_REP_GM_REQUEST, "REPLACE INTO project_gm_request (id, playerGuid, note, createTime, mapId, posX, posY, posZ, facing, closedBy, assignedTo, comment) VALUES (?, ?, ?, UNIX_TIMESTAMP(NOW()), ?, ?, ?, ?, ?, ? ,? ,?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_GM_REQUEST, "DELETE FROM project_gm_request WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ALL_GM_REQUESTS, "TRUNCATE TABLE project_gm_request", CONNECTION_ASYNC); \
    /* achievements */ \
    PrepareStatement(PROJECT_CHAR_DEL_ACCOUNT_ACHIEVEMENT_PROGRESS_BY_CRITERIA, "DELETE FROM account_achievement_progress WHERE account = ? AND criteria = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ACCOUNT_ACHIEVEMENT, "INSERT INTO account_achievement (account, achievement, date) VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ACCOUNT_ACHIEVEMENT_PROGRESS, "INSERT INTO account_achievement_progress (account, criteria, counter, date) VALUES (?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ACCOUNT_ACHIEVEMENT_BY_ACHIEVEMENT, "DELETE FROM account_achievement WHERE achievement = ? AND account = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ACCOUNT_ACHIEVEMENTS, "DELETE FROM account_achievement WHERE account = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ACCOUNT_ACHIEVEMENTS_PROGRESS, "DELETE FROM account_achievement_progress WHERE account = ?", CONNECTION_ASYNC); \
    /* black market */ \
    PrepareStatement(PROJECT_CHAR_SEL_BLACK_MARKET_AUCTIONS, "SELECT id, templateId, startTime, highestBidder, highestBid, totalBids, duration FROM project_black_market", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_INS_BLACK_MARKET_AUCTION, "INSERT INTO project_black_market (id, templateId, startTime, highestBidder, highestBid, totalBids, duration) VALUES (?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_BLACK_MARKET_AUCTION, "DELETE FROM project_black_market WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_BLACK_MARKET_AUCTION, "UPDATE project_black_market SET highestBidder = ?, highestBid = ?, totalBids = ?, duration = ? WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_BLACK_MARKET_AUCTION_LOG, "INSERT INTO project_black_market_log (templateId, itemId, startTime, highestBidder, highestBid, totalBids, duration) VALUES (?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    /* battle pets */ \
    PrepareStatement(PROJECT_CHAR_SEL_ACCOUNT_BATTLE_PET_MAX_GUID, "SELECT MAX(id) from project_account_battle_pet", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_ACCOUNT_BATTLE_PETS, "SELECT id, species, nickname, timestamp, level, xp, health, maxHealth, power, speed, quality, breed, flags FROM project_account_battle_pet WHERE accountId = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_DEL_ACCOUNT_BATTLE_PET, "DELETE FROM project_account_battle_pet WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ACCOUNT_BATTLE_PET, "REPLACE INTO project_account_battle_pet (id, accountId, species, nickname, timestamp, level, xp, health, maxHealth, power, speed, quality, breed, flags) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_ACCOUNT_BATTLE_PET_SLOTS, "SELECT slot1, slot2, slot3, flags FROM project_account_battle_pet_slots WHERE accountId = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_INS_ACCOUNT_BATTLE_PET_SLOTS, "REPLACE INTO project_account_battle_pet_slots (accountId, slot1, slot2, slot3, flags) VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    /* loot */ \
    PrepareStatement(PROJECT_CHAR_INS_LOOT_COOLDOWN_PERSONAL, "INSERT IGNORE INTO project_loot_cooldown_personal VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_LOOT_COOLDOWN_BONUS, "INSERT IGNORE INTO project_loot_cooldown_bonus VALUES (?, ?)", CONNECTION_ASYNC); \
    /* archaeology */ \
    PrepareStatement(PROJECT_CHAR_SEL_ARCHAEOLOGY_SITES, "SELECT sites, site_positions FROM project_archaeology_sites WHERE guid = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_INS_ARCHAEOLOGY_SITES, "REPLACE INTO project_archaeology_sites VALUES (?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ARCHAEOLOGY_SITES, "DELETE FROM project_archaeology_sites WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_ARCHAEOLOGY_PROJECTS_ACTIVE, "SELECT project FROM project_archaeology_projects_active WHERE guid = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_INS_ARCHAEOLOGY_PROJECTS_ACTIVE, "INSERT INTO project_archaeology_projects_active VALUES (?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ARCHAEOLOGY_PROJECTS_ACTIVE, "DELETE FROM project_archaeology_projects_active WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_INS_ARCHAEOLOGY_PROJECTS_COMPLETED, "INSERT IGNORE INTO project_archaeology_projects_completed VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE count = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_ARCHAEOLOGY_PROJECTS_COMPLETED, "SELECT project, count, first_time FROM project_archaeology_projects_completed WHERE guid = ?", CONNECTION_SYNCH); \
    /* pvp */ \
    PrepareStatement(PROJECT_CHAR_SEL_PVP_FIRST_WINS, "SELECT SkirmishArena, RatedBg, RatedArena2v2, RatedArena3v3 FROM project_pvp_first_win WHERE guid = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_REP_PVP_FIRST_WINS, "REPLACE INTO project_pvp_first_win VALUES (?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_PVP_FIRST_WINS, "DELETE FROM project_pvp_first_win WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_PVP_FIRST_WINS_ALL, "TRUNCATE TABLE project_pvp_first_win", CONNECTION_ASYNC); \
    /* arena */ \
    PrepareStatement(PROJECT_CHAR_SEL_ARENA_RATINGS_BY_SLOT, "SELECT mmr, rating FROM project_arena_stats WHERE guid = ? AND slot = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_ARENA_BEST_RATINGS_BY_SLOT, "SELECT weekBestRating, seasonBestRating FROM project_arena_stats WHERE guid = ? AND slot = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_REP_ARENA_STATS, "REPLACE INTO project_arena_stats VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_ARENA_STATS, "UPDATE project_arena_stats SET weekPlayed = weekPlayed + 1, seasonPlayed = seasonPlayed + 1, weekWon = weekWon + ?, seasonWon = seasonWon + ?, mmr = ?, rating = ?, weekBestRating = ?, seasonBestRating = ? WHERE guid = ? AND slot = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_ARENA_STATS, "DELETE FROM project_arena_stats WHERE guid = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_SEL_ARENA_STATS, "SELECT slot, rating, mmr, weekPlayed, weekWon, weekBestRating, seasonPlayed, seasonWon, seasonBestRating FROM project_arena_stats WHERE guid = ?", CONNECTION_SYNCH); \
    /* boss timers */ \
    PrepareStatement(PROJECT_CHAR_SEL_BOSS_TIMERS_RECORD, "SELECT MIN(FinishTime - StartTime) FROM project_boss_timers WHERE BossID = ? AND BossMode = ? AND BossState = 2", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_SEL_BOSS_TIMERS_MAX_ID, "SELECT MAX(GUID) FROM project_boss_timers", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_CHAR_INS_BOSS_TIMERS_TIMER, "INSERT INTO project_boss_timers (GUID, BossID, BossMode, BossState, StartTime, FinishTime, GuildName, Players) VALUES (?, ?, ?, ?, ?, 0, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_DEL_BOSS_TIMERS_TIMER, "DELETE FROM project_boss_timers WHERE GUID = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_BOSS_TIMERS_STATE, "UPDATE project_boss_timers SET BossState = ?, FinishTime = ? WHERE GUID = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_CHAR_UPD_BOSS_TIMERS_STATE_CLEANUP, "UPDATE project_boss_timers SET BossState = ? WHERE BossState = ?", CONNECTION_ASYNC); \
    /* world quests */ \
    PrepareStatement(CUSTOM_CHAR_SEL_WORLD_QUEST, "SELECT id, starttime, remaining FROM custom_world_quest", CONNECTION_SYNCH); \
    PrepareStatement(CUSTOM_CHAR_UPD_WORLD_QUEST, "UPDATE custom_world_quest SET remaining = ? WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(CUSTOM_CHAR_DEL_WORLD_QUEST, "DELETE FROM custom_world_quest WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(CUSTOM_CHAR_REP_WORLD_QUEST, "REPLACE INTO custom_world_quest (id, starttime, remaining) VALUES (?, ?, ?)", CONNECTION_ASYNC); \


#define PROJECT_DATABASE_HOTFIX_PREPARED_STATEMENTS_IMPLEMENTATION \
    /* AdventureMapPOI.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_ADVENTURE_MAP_POI, "SELECT ID, POI0, POI1, Unk1, Title, Description, Unk2, Unk3, QuestID, Unk4, Unk5, Unk6, Unk7, Unk8, Unk9 " \
    "FROM project_adventure_map_poi ORDER BY ID", CONNECTION_SYNCH); \
    /* AdventureJournal.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_ADVENTURE_JOURNAL, "SELECT ID, Title, Description, ButtonText, field10, field14, field18, ContinueText, field20, field22, QuestID, field261, field262, field2A, field2C, field2E, field2F, Type, field31, field32, field33, field341, field342, field36, field37 " \
    "FROM project_adventure_journal ORDER BY ID", CONNECTION_SYNCH); \
    /* AzeriteEmpoweredItem.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_AZERITE_EMPOWERED_ITEM, "SELECT ID, ItemID, AzeriteTierUnlockSetID, AzeritePowerSetID " \
    "FROM project_azerite_empowered_item ORDER BY ID", CONNECTION_SYNCH); \
    /* AzeriteItem.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_AZERITE_ITEM, "SELECT ID, ItemID FROM project_azerite_item ORDER BY ID", CONNECTION_SYNCH); \
    /* AzeriteTierUnlock.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_AZERITE_TIER_UNLOCK, "SELECT ID, ItemCreationContext, Tier, AzeriteLevel, AzeriteTierUnlockSetId FROM project_azerite_tier_unlock ORDER BY ID", CONNECTION_SYNCH); \
    /* AzeritePower.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_AZERITE_POWER, "SELECT ID, SpellID, ItemBonusListID, SpecSet, Unk0 FROM project_azerite_power ORDER BY ID", CONNECTION_SYNCH); \
    /* AzeritePower.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_AZERITE_POWER_SET_MEMBER, "SELECT ID, AzeritePowerID, Class, Tier, OrderIndex, AzeritePowerSetID FROM project_azerite_power_set_member ORDER BY ID", CONNECTION_SYNCH); \
    /* BattlePetAbility.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY, "SELECT ID, IconId, Name, Description, Unk1, FamilyId, Unk2, Cooldown FROM project_battle_pet_ability", CONNECTION_SYNCH); \
    PREPARE_LOCALE_STMT(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY, "SELECT ID, Name_lang, Description_lang FROM project_battle_pet_ability_locale WHERE locale = ?", CONNECTION_SYNCH); \
    /* BattlePetAbilityState.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY_STATE, "SELECT ID, Value, AbilityId, StateId FROM project_battle_pet_ability_state ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* BattlePetAbilityEffect.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY_EFFECT, "SELECT TurnID, Unk1, Aura, Effect, Values1, Values2, Values3, Values4, Values5, Values6, TurnInstanceID, ID " \
        "FROM project_battle_pet_ability_effect ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* BattlePetAbilityTurn.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY_TURN, "SELECT AbilityID, Unk1, TurnDelay, Unk2, ProcType, ID FROM project_battle_pet_ability_turn" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* BattlePetEffectProperties.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_EFFECT_PROPERTIES, "SELECT ID, Values1, Values2, Values3, Values4, Values5, Values6, EffectType, Misc1, Misc2, Misc3, Misc4, " \
        "Misc5, Misc6 FROM project_battle_pet_effect_properties ORDER BY ID DESC", CONNECTION_SYNCH); \
    PREPARE_LOCALE_STMT(PROJECT_HOTFIX_SEL_BATTLE_PET_EFFECT_PROPERTIES, "SELECT ID, Values1_lang, Values2_lang, Values3_lang, Values4_lang, Values5_lang, Values6_lang " \
        "FROM project_battle_pet_effect_properties_locale WHERE locale = ?", CONNECTION_SYNCH); \
    /* BattlePetSpeciesXAbility.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_BATTLE_PET_ABILITY_SPECIES_X_ABILITY, "SELECT ID, SpeciesId, AbilityId, RequiredLevel, SlotId FROM project_battle_pet_ability_x_species " \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* CharShipmentContainer.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_CHAR_SHIPMENT_CONTAINER, "SELECT ID, Name, Description, OverrideDisplayID1, Unk1, OverrideIfAmountMet1, OverrideIfAmountMet2, " \
        "ShipmentAmountNeeded1, ShipmentAmountNeeded2, Unk2, Unk3, Unk4, GarrisonType, Unk5, Unk6, Unk7, Unk8" \
        " FROM project_char_shipment_container ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* CharShipment.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_CHAR_SHIPMENT, "SELECT ID, Unk, Duration, SpellReward, Icon, SpellTutorial, ShipmentContainerID, FollowerID, " \
        "MaxCount, Unk2 FROM project_char_shipment ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrAbilityEffect.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_ABILITY_EFFECT, "SELECT ModMin, ModMax, Amount, MiscValueA, AbilityID, EffectType, TargetMask, " \
        "CounterMechanicTypeID, Unk1, Unk2, Unk3, ID FROM project_garr_ability_effect ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrClassSpecPlayerCond.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_CLASS_SPEC_PLAYER_COND, "SELECT ID, IconID, Name, `Index`, ClassSpec, Unk1, GarrStringID" \
        " FROM project_garr_class_spec_player_cond ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrEncounterXMechanic.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_ENCOUNTER_X_MECHANIC, "SELECT ID, EncounterID, MechanicID, ClassID FROM project_garr_encounter_x_mechanic" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrFollowerLevelXP.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_FOLLOWER_LEVEL_X_P, "SELECT ID, RequiredExperience, UnkExp, Level, Class FROM project_garr_follower_level_x_p" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrFollowerType.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_FOLLOWER_TYPE, "SELECT ID, MaxItemLevel, Flags, Unk1, Unk2, Unk3, Unk4, Unk5 FROM project_garr_follower_type" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrFollowerXAbility.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_FOLLOWER_X_ABILITY, "SELECT ID, GarrAbilityID, FactionIndex, GarrFollowerID FROM project_garr_follower_x_ability" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrFollSupportSpell.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_FOLL_SUPPORT_SPELL, "SELECT ID, AllianceSpellID, HordeSpellID, Unk, GarrFollowerID FROM project_garr_foll_support_spell" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrMechanic.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_MECHANIC, "SELECT ID, Unk1, MechanicTypeID, Unk2 FROM project_garr_mechanic ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrMechanicType.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_MECHANIC_TYPE, "SELECT Environment, EnvironmentDesc, EnvironmentTextureID, Type, ID FROM project_garr_mechanic_type" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrMission.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_MISSION, "SELECT Duration, OfferTime, Name, Description, Location, MapX, MapY, PosX, PosY, RequiredItemLevel, " \
        "LocPrefixID, CurrencyID, RequiredLevel, GarrMechanicTypeRecID, RequiredFollowersCount, Category, " \
        "MissionType, FollowerType, BaseBonusChance, LostChance, GarrTypeID, ID, TravelTime, SubCategory2, SubCategory1, CurrencyCost, Flags, RewardFollowerExperience, AreaID, " \
        "Unk3, Unk FROM project_garr_mission ORDER BY ID DESC", CONNECTION_SYNCH); \
    PREPARE_LOCALE_STMT(PROJECT_HOTFIX_SEL_GARR_MISSION, "SELECT ID, Name_lang, Description_lang, Location_lang FROM project_garr_mission_locale WHERE locale = ?", CONNECTION_SYNCH); \
    /* GarrMissionXEncounter.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_MISSION_X_ENCOUNTER, "SELECT Unk1, ID, MissionID, EncounterID, Unk2 FROM project_garr_mission_x_encounter" \
        " ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrTalentTree.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_TALENT_TREE, "SELECT ID, Unk1, Unk2, ClassID, GarrTypeID FROM project_garr_talent_tree ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrTalent.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_TALENT, "SELECT Icon, Name, Description, InitialCooldown, TierLevel, `Index`, Unk1, ID, GarrTalentTree, Unk2, " \
        "Unk3, InitialCurrencyAmount, InitialCurrencyID, Unk4, SpellID, Unk5, ChangeCurrencyAmount, ChangeCurrencyID, ChangeCooldown, Unk6" \
        " FROM project_garr_talent ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GarrType.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GARR_TYPE, "SELECT ID, Unk1, CurrencyResource1, CurrencyResource2, Expansion, MapID FROM project_garr_type ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GroupFinderActivity.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GROUP_FINDER_ACTIVITY, "SELECT ID, Name, DifficultyString, RequiredILvl, MapID, ZoneID, CategoryID, ActivityGroupID, Unk1, MinLevel, " \
        "MaxLevel, Difficulty, Type, Unk2, MaxPlayers FROM project_group_finder_activity ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* GroupFinderCategory.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_GROUP_FINDER_CATEGORY, "SELECT ID, Name, Unk1, Unk2 FROM project_group_finder_category ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* ItemNameDescription.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_ITEM_NAME_DESCRIPTION, "SELECT ID, Description_lang, Color FROM project_item_name_description ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* SpellKeyboundOverride.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_SPELL_KEYBOUND_OVERRIDE, "SELECT ID, Function, Data, Type FROM project_spell_keybound_override ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* MapChallengeMode.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_MAP_CHALLENGE_MODE, "SELECT ID, Name, MapID, TimeBronze, TimeSilver, TimeGold, Unk FROM project_map_challenge_mode ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* QuestV2CliTask.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_QUEST_V2_CLI_TASK, "SELECT FiltRaces, QuestTitle, BulletText, StartItem, UniqueBitFlag, ConditionID, FiltClasses, FiltCompletedQuest1, " \
    "FiltCompletedQuest2, FiltCompletedQuest3, FiltMinSkillID, WorldStateExpressionID, FiltActiveQuest, FiltCompletedQuestLogic, FiltMaxFactionID, FiltMaxFactionValue, FiltMaxLevel, "\
    "FiltMinFactionID, FiltMinFactionValue, FiltMinLevel, FiltNonActiveQuest, FiltMinSkillValue, ID, BreadCrumbID, QuestInfoID, SandboxScalingID " \
        "FROM project_quest_v2_cli_task ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* QuestPOIBlob.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_QUEST_POI_BLOB, "SELECT ID, MapID, WorldMapAreaID, NumPoints, Unk1, Unk2, Unk3, Unk4 FROM project_quest_poi_blob ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* QuestPOIPoint.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_QUEST_POI_POINT, "SELECT POI, X, Y, ID FROM project_quest_poi_point ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* ResearchBranch.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_RESEARCH_BRANCH, "SELECT ID, RaceName_lang, Icon_lang, KeystoneItemId, Currency, Unk FROM project_research_branch ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* ResearchSite.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_RESEARCH_SITE, "SELECT ID, POI, Name_lang, MapID, Icon FROM project_research_site ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* ResearchProject.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_RESEARCH_PROJECT, "SELECT Name_lang, Description_lang, RewardSpell, Icon_lang, Race, Rare, MaxKeystonesAllowed, ID, RequiredCurrency " \
        "FROM project_research_project ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* SceneScriptPackageMember.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_SCENE_SCRIPT_PACKAGE_MEMBER, "SELECT ID, PackageID, ScriptID, FrameworkPackageID, `Order` FROM project_scene_script_package_member ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* SpecSetMember.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_SPEC_SET_MEMBER, "SELECT ID, CharSpecialization, SpecSetMemberID FROM project_spec_set_member ORDER BY ID", CONNECTION_SYNCH); \
    /* SpellReagentsCurrency.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_SPELL_REAGENTS_CURRENCY, "SELECT ID, SpellID, CurrencyID, CurrencyCount FROM project_spell_reagents_currency ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* UiCamera.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_UI_CAMERA, "SELECT ID, Pos1_1, Pos1_2, Pos1_3, Pos2_1, Pos2_2, Pos2_3, Pos3_1, Pos3_2, Pos3_3, Unk1, CameraType, Unk2, Unk3, Unk4 FROM project_ui_camera ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* Vignette.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_VIGNETTE, "SELECT ID, Name_lang, X, Y, QuestFeedbackEffectId, Flags, PlayerCondition, QuestID FROM project_vignette ORDER BY ID DESC", CONNECTION_SYNCH); \
    /* WorldStateUI.db2 */ \
    PrepareStatement(PROJECT_HOTFIX_SEL_WORLD_STATE_UI, "SELECT Texture, TextureFlag, FlagText, Text, Description, MapID, ZoneID, PhaseID, PhaseGroupID, FlagWorldState, "\
    "ExtendedUIStateVariable_1, ExtendedUIStateVariable_2, ExtendedUIStateVariable_3, OrderIndex, PhaseUseFlags, Type, ID, DynamicIconFileID, DynamicFlashIconFileID FROM project_world_state_ui ORDER BY ID DESC", CONNECTION_SYNCH); \

#define PROJECT_DATABASE_WORLD_PREPARED_STATEMENTS_IMPLEMENTATION \
    PrepareStatement(PROJECT_WORLD_SEL_BLACK_MARKET_TEMPLATE, "SELECT templateId, sellerNpc, itemId, minBid, chance FROM project_black_market_template", CONNECTION_SYNCH); \

#define PROJECT_DATABASE_SITE_PREPARED_STATEMENTS_IMPLEMENTATION \
    /* donations */ \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_CONFIG, "SELECT name, value FROM config WHERE name IN ('DONATIONS_SALES_0', 'DONATIONS_SALES_1', 'DONATIONS_SALES_2', 'DONATIONS_SALES_FORCE_FOR_ACCOUNTS', 'DONATIONS_SALES_START', 'DONATIONS_SALES_DURATION')", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_EXCHANGE_RATE, "SELECT in_ron FROM donations_exchange_rates WHERE currency = 'usd'", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_PRICES, "SELECT name, price, sales_type FROM donations_prices", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_POINTS, "SELECT remaining FROM donations_points WHERE user_id = ?", CONNECTION_BOTH); \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_COOLDOWN, "SELECT last_donation FROM donations_bonuses_cooldowns WHERE user_id = ? AND guid = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_SITE_SEL_DONATIONS_DUE, "SELECT timeLimit FROM donations_dues WHERE userId = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_UPD_DONATIONS_POINTS, "UPDATE donations_points SET remaining = remaining - ?, spent = spent + ? WHERE user_id = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_SITE_INS_DONATIONS_REWARD_LOG, "INSERT INTO donations_rewards_log (id, user_id, `guid`, `char`, realm, `type`, time, info1, bonus, ingame) VALUES (0, ?, ?, ?, ?, ?, ?, ?, 0, 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_INS_DONATIONS_REWARD_LOG_BY_TYPE, "INSERT INTO donations_rewards_by_type_log VALUES (0, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_INS_DONATIONS_BONUS, "INSERT INTO donations_bonuses VALUES (?, ?, ?, ?, ?, 1)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_REP_DONATIONS_BONUS_COOLDOWN, "REPLACE INTO donations_bonuses_cooldowns VALUES (?, ?, ?)", CONNECTION_SYNCH); \
    /* staff messages */ \
    PrepareStatement(PROJECT_SITE_SEL_STAFF_MESSAGES_READ, "SELECT title, text, FROM_UNIXTIME(time), id FROM staff_messages WHERE receiver_account = ? AND id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_STAFF_MESSAGES_LIST, "SELECT id, title FROM staff_messages WHERE receiver_account = ? AND status = 1", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_STAFF_MESSAGES_UNREAD, "SELECT COUNT(id) FROM staff_messages WHERE status = 1 AND receiver_account = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_UPD_STAFF_MESSAGES_MARK_READ, "UPDATE staff_messages SET status = 0 WHERE id = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_DEL_STAFF_MESSAGES_DELETE, "DELETE FROM staff_messages WHERE id = ? AND receiver_account = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_DEL_STAFF_MESSAGES_DELETE_ALL, "DELETE FROM staff_messages WHERE receiver_account = ?", CONNECTION_ASYNC); \
    /* votes */ \
    PrepareStatement(PROJECT_SITE_INS_VOTES_ADD, "INSERT INTO votes VALUES (?, ?, 0, 0, 0, '', 0, 0, 0) ON DUPLICATE KEY UPDATE points = points + ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_INS_VOTES_SET, "INSERT INTO votes VALUES (?, ?, 0, 0, 0, '', 0, 0, 0) ON DUPLICATE KEY UPDATE points = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_VOTES, "SELECT points FROM votes WHERE userid = ?", CONNECTION_SYNCH); \
    /* streams */ \
    PrepareStatement(PROJECT_SITE_SEL_STREAMS_LIST, "SELECT votes, viewers, online FROM streams WHERE guid = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_SITE_SEL_STREAMS_CONFIG, "SELECT value FROM ingame_settings WHERE name = 'last_stream_message_id'", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_STREAMS_COMMENTS, "SELECT id, guid, voter, REPLACE(REPLACE(comment, '\n', ' '), '\r', ' ') AS comment FROM streams_comments WHERE id > ? ORDER BY id ASC", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_STREAMS_ONLINE, "SELECT guid, votes, viewers FROM streams WHERE online", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_INS_STREAMS_COMMENT, "INSERT INTO streams_comments VALUES (0, ?, ?, ?, ?, ?)", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_UPD_STREAMS_LAST_MESSAGE, "UPDATE ingame_settings SET value = ? WHERE name = 'last_stream_message_id'", CONNECTION_ASYNC); \
    /* misc */ \
    PrepareStatement(PROJECT_SITE_SEL_MISC_LATEST_FIXES, "SELECT value FROM variables WHERE name = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_MISC_STATS_QUESTS_COMPLETED, "SELECT count FROM stats_quest_complete WHERE quest = ? AND realm = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_SEL_MISC_STATS_ACHIEVEMENTS_COMPLETED, "SELECT count, last FROM stats_achievement_complete WHERE achievement = ? AND realm = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_DEL_MISC_SESSION, "DELETE FROM sessions WHERE user_id = ?", CONNECTION_ASYNC); \
    /*stas*/ \
    PrepareStatement(PROJECT_SITE_INS_ACCOUNT_HISTORY, "INSERT INTO project_account_history (accountId, characterGuid, realmId, startdate, enddate) VALUES(?,?,?,?,?) ", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_UPD_ACCOUNT_HISTORY, "UPDATE project_account_history SET enddate = ? WHERE accountId = ? AND characterGuid = ? AND realmId = ? AND startdate = ?", CONNECTION_ASYNC); \
    PrepareStatement(PROJECT_SITE_INS_GM_COMMANDS, "INSERT INTO project_character_gm_commands (accountId, realmId, characterGuid, message) VALUES(?,?,?,?) ", CONNECTION_ASYNC);

#define PROJECT_DATABASE_LOGIN_PREPARED_STATEMENTS_IMPLEMENTATION \
    /*shop*/ \
    PrepareStatement(PROJECT_LOGIN_SEL_WEBSITE_SHOP, "SELECT id, accountID, characterGUID, objectType, objectID, objectParam0, objectParam1, objectParam2, objectParam3 FROM project_website_shop WHERE status = 0 AND realmID = ?", CONNECTION_SYNCH); \
    PrepareStatement(PROJECT_LOGIN_UPD_WEBSITE_SHOP, "UPDATE project_website_shop SET status = 1 WHERE id = ?", CONNECTION_ASYNC);
    
#endif
