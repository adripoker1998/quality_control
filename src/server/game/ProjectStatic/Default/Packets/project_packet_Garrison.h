#ifndef PROJECT_PACKET_GARRISON_H
#define PROJECT_PACKET_GARRISON_H

#include "Packet.h"
#include "GarrisonPackets.h"

namespace PROJECT
{
    namespace Packets
    {
        namespace Garrison
        {
            class GarrisonStartMissionResult final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonStartMissionResult() : WorldPackets::ServerPacket(SMSG_GARRISON_START_MISSION_RESULT, 5) { }

                WorldPacket const* Write() override;

                uint32 Result = 0;
                uint16 Unk = 1;
                WorldPackets::Garrison::GarrisonMission mission;
                std::vector<uint64> followerIDs;

            };

            class GarrisonAddMissionResult final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonAddMissionResult() : WorldPackets::ServerPacket(SMSG_GARRISON_ADD_MISSION_RESULT, 5) { }

                WorldPacket const* Write() override;

                uint32 GarrType = 0; // 3
                uint32 Unk1 = 0;
                uint8 Unk2 = 0;
                WorldPackets::Garrison::GarrisonMission mission;
                std::vector<WorldPackets::Garrison::GarrisonMissionReward> reward1;
                std::vector<WorldPackets::Garrison::GarrisonMissionReward> reward2;
                bool Unk3 = true;

            };

            class GarrisonMissionUpdateCanStart final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonMissionUpdateCanStart() : WorldPackets::ServerPacket(SMSG_GARRISON_MISSION_UPDATE_CAN_START, 5) { }

                WorldPacket const* Write() override;

                std::vector<uint32> missions;
                std::vector<bool> states;
            };

            class GarrisonCompleteMissionResult final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonCompleteMissionResult() : WorldPackets::ServerPacket(SMSG_GARRISON_COMPLETE_MISSION_RESULT, 5) { }

                WorldPacket const* Write() override;

                struct Follower
                {
                    uint64 DbID = 0;
                    uint32 Unk = 0;
                };

                uint32 CanComplete = 0;
                WorldPackets::Garrison::GarrisonMission mission;
                uint32 MissionID = 0;
                std::vector<Follower> followers;
                bool succeeded = false;
            };

            class GarrisonFollowerChangedXp final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonFollowerChangedXp() : WorldPackets::ServerPacket(SMSG_GARRISON_FOLLOWER_CHANGED_XP, 5) { }

                WorldPacket const* Write() override;

                uint32 Xp = 0;
                uint32 Result = 0;
                WorldPackets::Garrison::GarrisonFollower follower1;
                WorldPackets::Garrison::GarrisonFollower follower2;
            };

            class GarrisonFollowerChangedDurability final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonFollowerChangedDurability() : WorldPackets::ServerPacket(SMSG_GARRISON_FOLLOWER_CHANGED_DURABILITY, 5) { }

                WorldPacket const* Write() override;

                uint32 Result = 0;
                WorldPackets::Garrison::GarrisonFollower follower;
            };

            class GarrisonFollowerChangedItemLevel final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonFollowerChangedItemLevel() : WorldPackets::ServerPacket(SMSG_GARRISON_FOLLOWER_CHANGED_ITEM_LEVEL, 5) { }

                WorldPacket const* Write() override;

                WorldPackets::Garrison::GarrisonFollower follower;
            };

            class GarrisonMissionBonusRoll final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonMissionBonusRoll(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_MISSION_BONUS_ROLL, std::move(packet)) { }

                void Read() override;

                ObjectGuid Npc;
                uint32 MissionID;;
            };

            class GarrisonNumFollowerActivationsRemaining final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonNumFollowerActivationsRemaining() : WorldPackets::ServerPacket(SMSG_GARRISON_NUM_FOLLOWER_ACTIVATIONS_REMAINING, 4 + 4) { }

                WorldPacket const* Write() override;

                uint32 NumFollowerActivationsRemaining = 0;
                uint32 Unk = 0;
            };

            class GarrisonMissionBonusRollResult final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonMissionBonusRollResult() : WorldPackets::ServerPacket(SMSG_GARRISON_MISSION_BONUS_ROLL_RESULT, 5) { }

                WorldPacket const* Write() override;

                WorldPackets::Garrison::GarrisonMission mission;
                uint32 MissionID = 0;
                uint32 Result = 0;
            };

            class GarrisonCheckUpgradeable final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonCheckUpgradeable(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_CHECK_UPGRADEABLE, std::move(packet)) { }

                void Read() override { };
            };

            class GarrisonIsUpgradeableResult final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonIsUpgradeableResult() : WorldPackets::ServerPacket(SMSG_GARRISON_IS_UPGRADEABLE_RESULT, 5) { }

                WorldPacket const* Write() override;

                uint32 Result = 0;
            };

            class GarrisonStartMission final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonStartMission(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_START_MISSION, std::move(packet)) { }

                void Read() override;

                ObjectGuid Npc;
                uint32 MissionID = 0;
                std::vector<uint64> DbIds;
            };

            class GarrisonCompleteMission final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonCompleteMission(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_COMPLETE_MISSION, std::move(packet)) { }

                void Read() override;

                ObjectGuid Npc;
                uint32 MissionID = 0;
            };

            class OpenMissionNpc final : public WorldPackets::ClientPacket
            {
            public:
                OpenMissionNpc(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_OPEN_MISSION_NPC, std::move(packet)) { }

                void Read() override;

                ObjectGuid Unit;
                uint32 Unk;
            };

            class ShowAdventureMap final : public WorldPackets::ServerPacket
            {
            public:
                ShowAdventureMap(ObjectGuid guid) : WorldPackets::ServerPacket(SMSG_SHOW_ADVENTURE_MAP, 4), Unit(guid) { }

                WorldPacket const* Write() override;

                ObjectGuid Unit;
            };

            class TC_GAME_API AdventureJournalStartQuest final : public WorldPackets::ClientPacket
            {
            public:
                AdventureJournalStartQuest(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_ADVENTURE_JOURNAL_START_QUEST, std::move(packet)) { }

                void Read() override;

                uint32 QuestID;

            };

            class CreateShipment final : public WorldPackets::ClientPacket
            {
            public:
                CreateShipment(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_CREATE_SHIPMENT, std::move(packet)) { }

                void Read() override;

                ObjectGuid Unit;
                uint32 Count;
            };

            class CreateShipmentResponse final : public WorldPackets::ServerPacket
            {
            public:
                CreateShipmentResponse() : WorldPackets::ServerPacket(SMSG_CREATE_SHIPMENT_RESPONSE, 8 + 4 * 2) { }

                WorldPacket const* Write() override;

                uint64 DatabaseID = 0;
                uint32 ShipmentID = 0;
                uint32 Result = 0;
            };

            class OpenShipmentNpc final : public WorldPackets::ClientPacket
            {
            public:
                OpenShipmentNpc(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_OPEN_SHIPMENT_NPC, std::move(packet)) { }

                void Read() override;

                ObjectGuid Unit;
            };

            class OpenShipmentNpcFromGossip final : public WorldPackets::ServerPacket
            {
            public:
                OpenShipmentNpcFromGossip() : WorldPackets::ServerPacket(SMSG_OPEN_SHIPMENT_NPC_FROM_GOSSIP, 8 + 4 * 2) { }

                WorldPacket const* Write() override;

                ObjectGuid Unit;
                uint32 ShipmentContainerID;
            };

            class GarrisonRequestShipmentInfo final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonRequestShipmentInfo(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_REQUEST_SHIPMENT_INFO, std::move(packet)) { }

                void Read() override;

                ObjectGuid Unit;
            };

            class GetShipmentInfoResponse final : public WorldPackets::ServerPacket
            {
            public:
                GetShipmentInfoResponse() : WorldPackets::ServerPacket(SMSG_GET_SHIPMENT_INFO_RESPONSE, 8 + 4 * 2) { }

                WorldPacket const* Write() override;

                struct CharShipment
                {
                    uint32 ShipmentRecID = 0;
                    uint64 ShipmentDatabaseID = 0;
                    uint64 ShipmentFollowerDatabaseID = 0;
                    uint32 CreationTime = time(nullptr);
                    uint32 ShipmentDuration = 0;
                    uint32 ShipmentXP = 0;
                };

                bool Success = true;
                uint32 ShipmentID = 0;
                uint32 MaxShipment = 0;
                uint32 Plot = 0;
                std::vector<CharShipment> CharShipments;
            };

            class GarrisonRequestLandingPageShipmentInfo final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonRequestLandingPageShipmentInfo(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_REQUEST_LANDING_PAGE_SHIPMENT_INFO, std::move(packet)) { }

                void Read() override { };

            };

            class GarrisonLandingPageShipmentInfo final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonLandingPageShipmentInfo() : WorldPackets::ServerPacket(SMSG_GARRISON_LANDING_PAGE_SHIPMENT_INFO, 1) { }

                WorldPacket const* Write() override;

                struct Shipment
                {
                    uint32 ShipmentID = 0;
                    uint64 ShipmentDB = 0;
                    uint64 ShipmentFollowerDB = 0;
                    uint32 StartTime = 0;
                    uint32 Duration = 0;
                    uint32 RewardXP = 0;
                };

                std::vector<Shipment> Shipments;
            };

            class GarrisonRequestClassSpecCategoryInfo final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonRequestClassSpecCategoryInfo(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_REQUEST_CLASS_SPEC_CATEGORY_INFO, std::move(packet)) { }

                void Read() override;

                uint32 FollowerType = 0;
            };

            class GarrisonResearchTalent final : public WorldPackets::ClientPacket
            {
            public:
                GarrisonResearchTalent(WorldPacket&& packet) : WorldPackets::ClientPacket(CMSG_GARRISON_RESEARCH_TALENT, std::move(packet)) { }

                void Read() override;

                uint32 TalentID;
            };

            class OpenTalentNpc final : public WorldPackets::ServerPacket
            {
            public:
                OpenTalentNpc() : WorldPackets::ServerPacket(SMSG_GARRISON_OPEN_TALENT_NPC, 8 + 4 * 2) { }

                WorldPacket const* Write() override;

                ObjectGuid Unit;
            };

            /*class GarrisonMissionInterface final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonMissionInterface() : WorldPackets::ServerPacket(SMSG_GARRISON_MISSION_INTERFACE, 11) { }

                WorldPacket const* Write() override;

                struct UnkStructure
                {
                    uint32 Unk = 0;
                };

                uint32 GarrisonTypeID = 0;
                uint32 Unk1 = 0;
                std::vector<UnkStructure> UnkStruct; // maybe missions?
                bool Unk2 = true;
                bool Unk3 = false;
            };*/

            class GarrisonOpenMissionNpc final : public WorldPackets::ServerPacket
            {
            public:
                GarrisonOpenMissionNpc(ObjectGuid guid) : WorldPackets::ServerPacket(SMSG_GARRISON_OPEN_MISSION_NPC, 16 + 1), Guid(guid) { }

                WorldPacket const* Write() override;

                ObjectGuid Guid;
                uint32 Unk = 1;
            };
        }
    }
}

#endif
