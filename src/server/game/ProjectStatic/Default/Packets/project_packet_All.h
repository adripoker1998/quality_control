#ifndef PROJECT_PACKET_ALL_H
#define PROJECT_PACKET_ALL_H

#include "AllPackets.h"
#include "project_packet_Archaeology.h"
#include "project_packet_BattlePay.h"
#include "project_packet_BattlePet.h"
#include "project_packet_Character.h"
#include "project_packet_Garrison.h"
#include "project_packet_Premade.h"
#include "project_packet_Misc.h"
#include "project_packet_Launcher.h"

#endif
