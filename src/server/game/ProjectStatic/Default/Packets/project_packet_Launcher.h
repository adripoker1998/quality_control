#ifndef PROJECT_PACKETS_LAUNCHER_H
#define PROJECT_PACKETS_LAUNCHER_H

#include "project.h"
#include "WorldPacket.h"
#include "WorldSession.h"

namespace PROJECT
{
    namespace Opcodes
    {
        enum ClientOpcodes
        {
            CMSG_PROJECT_LAUNCHER_GET_ENCRIPTION_KEY     = 0x0001,
            CMSG_PROJECT_LAUNCHER_UPDATE                 = 0x0002,
            CMSG_PROJECT_LAUNCHER_LOADED_DLLS            = 0x0003,
        };
        enum ServerOpcodes
        {
            SMSG_PROJECT_LAUNCHER_ENCRIPTION_KEY         = 0x0001,
            SMSG_PROJECT_LAUNCHER_NEW_MODULE             = 0x0002,
            SMSG_PROJECT_LAUNCHER_NEW_BINARY             = 0x0003,
        };
    }
    namespace Packets
    {
        namespace Launcher
        {
            namespace Defines
            {
                enum launcherDefines
                {
                    DATA_HASH_LENGTH                    = 16,
                };
            }

            bool DecryptData(WorldPacket* packet, uint8* dataHashField, WorldSession* session);
            bool ParseData(WorldPacket* packet, uint8* dataHashField, WorldSession* session);
            bool CheckEncryption(WorldSession* session);
            void IntegrityCheckFailed(WorldSession* session);

            class Update final : public WorldPackets::ClientPacket
            {
            	public:
            	    Update(WorldPacket&& packet) : WorldPackets::ClientPacket((OpcodeClient)PROJECT::Opcodes::CMSG_PROJECT_LAUNCHER_UPDATE, std::move(packet)) { }
            	
                    void Read() override { }
                    bool Read(WorldSession* session);

                    uint8 DataHash[Defines::DATA_HASH_LENGTH];
                    bool Is64;
                    bool ModuleDownloaded;
                    bool BinaryDownloaded;
                    uint8 HashModule[Defines::DATA_HASH_LENGTH];
                    uint8 HashBinary[Defines::DATA_HASH_LENGTH];
                    uint32 WowBuild;
                    std::string WowExeHash;
            };

            class LoadedDlls final : public WorldPackets::ClientPacket
            {
            	public:
            	    LoadedDlls(WorldPacket&& packet) : WorldPackets::ClientPacket((OpcodeClient)PROJECT::Opcodes::CMSG_PROJECT_LAUNCHER_LOADED_DLLS, std::move(packet)) { }
            	
                    void Read() override { }
                    bool Read(WorldSession* session);

                    uint8 DataHash[Defines::DATA_HASH_LENGTH];
                    PROJECT::Defines::StringList LoadedModules;
            };

            class GetEncryptionKey final : public WorldPackets::ClientPacket
            {
            	public:
            	    GetEncryptionKey(WorldPacket&& packet) : WorldPackets::ClientPacket((OpcodeClient)PROJECT::Opcodes::CMSG_PROJECT_LAUNCHER_GET_ENCRIPTION_KEY, std::move(packet)) { }
            	
                    void Read() override { }
            };

            // we are using SMSG_WARDEN_DATA (an unused opcode) to send custom data to the client, otherwise we would also have to implement a client-side handler for custom opcodes
            class ServerPkt final : public WorldPackets::ServerPacket
            {
                public:
                    ServerPkt(uint32 opcode) : WorldPackets::ServerPacket(SMSG_WARDEN_DATA), Opcode(opcode) { }

                    WorldPacket const* Write() override { return &_worldPacket; }
                    WorldPacket const* Write(WorldSession* session);
                    void GenerateChecksum();
                    void EncryptData(WorldSession* session);

                    uint32 Opcode;
                    uint32 Length;
                    uint8 DataHash[Defines::DATA_HASH_LENGTH];
                    ByteBuffer Data;
            };
        }
    }
}

#endif
