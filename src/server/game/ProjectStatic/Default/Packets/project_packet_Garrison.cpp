#include "project_packet_Garrison.h"

WorldPacket const* PROJECT::Packets::Garrison::GarrisonStartMissionResult::Write()
{
    _worldPacket << Result;
    _worldPacket << Unk;
    _worldPacket << mission;
    _worldPacket << uint32(followerIDs.size());

    for (uint64 follower : followerIDs)
        _worldPacket << follower;

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonAddMissionResult::Write()
{
    _worldPacket << GarrType;
    _worldPacket << Unk1;
    _worldPacket << Unk2;
    _worldPacket << mission;
    _worldPacket << uint32(reward1.size());
    _worldPacket << uint32(reward2.size());

    for (WorldPackets::Garrison::GarrisonMissionReward reward : reward1)
        _worldPacket << reward;

    for (WorldPackets::Garrison::GarrisonMissionReward reward : reward2)
        _worldPacket << reward;

    _worldPacket.WriteBit(Unk3);
    _worldPacket.FlushBits();

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonMissionUpdateCanStart::Write()
{
    _worldPacket << uint32(missions.size());
    _worldPacket << uint32(states.size());

    for (uint32 mission : missions)
        _worldPacket << mission;

    for (uint32 state : states)
        _worldPacket << state;

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonCompleteMissionResult::Write()
{
    _worldPacket << CanComplete;
    _worldPacket << mission;
    _worldPacket << MissionID;
    _worldPacket << uint32(followers.size());

    for (Follower follower : followers)
    {
        _worldPacket << follower.DbID;
        _worldPacket << follower.Unk;
    }

    _worldPacket.WriteBit(succeeded);
    _worldPacket.FlushBits();

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::GarrisonMissionBonusRoll::Read()
{
    _worldPacket >> Npc;
    _worldPacket >> MissionID;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonMissionBonusRollResult::Write()
{
    _worldPacket << mission;
    _worldPacket << MissionID;
    _worldPacket << Result;

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonNumFollowerActivationsRemaining::Write()
{
    _worldPacket << NumFollowerActivationsRemaining;
    _worldPacket << Unk;

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonIsUpgradeableResult::Write()
{
    _worldPacket << Result;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::GarrisonStartMission::Read()
{
    uint32 size = 0;
    _worldPacket >> Npc;
    _worldPacket >> size;
    DbIds.reserve(size);
    _worldPacket >> MissionID;

    for (uint32 i = 0; i < size; ++i)
        DbIds.push_back(_worldPacket.read<uint64>());
}

void PROJECT::Packets::Garrison::GarrisonCompleteMission::Read()
{
    _worldPacket >> Npc;
    _worldPacket >> MissionID;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonFollowerChangedXp::Write()
{
    _worldPacket << Xp;
    _worldPacket << Result;
    _worldPacket << follower1;
    _worldPacket << follower2;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::OpenMissionNpc::Read()
{
    _worldPacket >> Unit;
    _worldPacket >> Unk;
}

WorldPacket const* PROJECT::Packets::Garrison::ShowAdventureMap::Write()
{
    _worldPacket << Unit;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::AdventureJournalStartQuest::Read()
{
    _worldPacket >> QuestID;
}

void PROJECT::Packets::Garrison::CreateShipment::Read()
{
    _worldPacket >> Unit;
    _worldPacket >> Count;
}

WorldPacket const* PROJECT::Packets::Garrison::CreateShipmentResponse::Write()
{
    _worldPacket << DatabaseID;
    _worldPacket << ShipmentID;
    _worldPacket << Result;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::OpenShipmentNpc::Read()
{
    _worldPacket >> Unit;
}

WorldPacket const* PROJECT::Packets::Garrison::OpenShipmentNpcFromGossip::Write()
{
    _worldPacket << Unit;
    _worldPacket << ShipmentContainerID;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::GarrisonRequestShipmentInfo::Read()
{
    _worldPacket >> Unit;
}

WorldPacket const* PROJECT::Packets::Garrison::GetShipmentInfoResponse::Write()
{
    _worldPacket.WriteBit(Success);
    _worldPacket.FlushBits();

    _worldPacket << ShipmentID;
    _worldPacket << MaxShipment;
    _worldPacket << uint32(CharShipments.size());
    _worldPacket << Plot;

    for (auto ship : CharShipments)
    {
        _worldPacket << ship.ShipmentRecID;
        _worldPacket << ship.ShipmentDatabaseID;
        _worldPacket << ship.ShipmentFollowerDatabaseID;
        _worldPacket << ship.CreationTime;
        _worldPacket << ship.ShipmentDuration;
        _worldPacket << ship.ShipmentXP;
    }

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonLandingPageShipmentInfo::Write()
{
    _worldPacket << uint32(Shipments.size());

    for (auto ship : Shipments)
    {
        _worldPacket << ship.ShipmentID;
        _worldPacket << ship.ShipmentDB;
        _worldPacket << ship.ShipmentFollowerDB;
        _worldPacket << ship.StartTime;
        _worldPacket << ship.Duration;
        _worldPacket << ship.RewardXP;
    }

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::GarrisonRequestClassSpecCategoryInfo::Read()
{
    _worldPacket >> FollowerType;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonFollowerChangedDurability::Write()
{
    _worldPacket << Result;
    _worldPacket << follower;

    return &_worldPacket;
}

WorldPacket const* PROJECT::Packets::Garrison::GarrisonFollowerChangedItemLevel::Write()
{
    _worldPacket << follower;

    return &_worldPacket;
}

void PROJECT::Packets::Garrison::GarrisonResearchTalent::Read()
{
    _worldPacket >> TalentID;
}

WorldPacket const* PROJECT::Packets::Garrison::OpenTalentNpc::Write()
{
    _worldPacket << Unit;

    return &_worldPacket;
}

/*WorldPacket const* PROJECT::Packets::Garrison::GarrisonMissionInterface::Write()
{
    _worldPacket << GarrisonTypeID;
    _worldPacket << Unk1;
    _worldPacket << uint32(UnkStruct.size());

    for (auto const& unk : UnkStruct)
        _worldPacket << unk.Unk;

    _worldPacket.WriteBit(Unk2);
    _worldPacket.WriteBit(Unk3);
    _worldPacket.FlushBits();

    return &_worldPacket;
}*/

WorldPacket const* PROJECT::Packets::Garrison::GarrisonOpenMissionNpc::Write()
{
    _worldPacket << Guid;
    _worldPacket << Unk;

    return &_worldPacket;
}
