#include "project_packet_Launcher.h"
#include "project_handler_placeholder.h"
#include <openssl/md5.h>

bool PROJECT::Packets::Launcher::Update::Read(WorldSession* session)
{
    if (!PROJECT::Packets::Launcher::ParseData(&_worldPacket, &DataHash[0], session))
        return false;

    _worldPacket >> Is64;
    _worldPacket >> ModuleDownloaded;
    _worldPacket >> BinaryDownloaded;

    for (int i = 0; i < Defines::DATA_HASH_LENGTH; i++)
        HashModule[i] = _worldPacket.read<uint8>();
    for (int i = 0; i < Defines::DATA_HASH_LENGTH; i++)
        HashBinary[i] = _worldPacket.read<uint8>();
    
    _worldPacket >> WowBuild;
    WowExeHash = _worldPacket.ReadString(32);

    return true;
}

bool PROJECT::Packets::Launcher::LoadedDlls::Read(WorldSession* session)
{
    if (!PROJECT::Packets::Launcher::ParseData(&_worldPacket, &DataHash[0], session))
        return false;

    uint32 modulesCount;
    uint8 nameLength;
    _worldPacket >> modulesCount;

    for (uint32 i = 0; i < modulesCount; i++)
    {
        _worldPacket >> nameLength;
        std::string moduleName = _worldPacket.ReadString(nameLength);
        LoadedModules.push_back(moduleName);
    }

    return true;
}

WorldPacket const* PROJECT::Packets::Launcher::ServerPkt::Write(WorldSession* session)
{
    GenerateChecksum();

    if (session)
        EncryptData(session);

    _worldPacket << Opcode;
    _worldPacket << Length;
    for (int i = 0; i < Defines::DATA_HASH_LENGTH; i++)
        _worldPacket << DataHash[i];
    
    _worldPacket.append(Data);

    return &_worldPacket;
}


void PROJECT::Packets::Launcher::ServerPkt::GenerateChecksum()
{
    Length = Data.size();

    // md5 hash of data
    if (Length)
    {
        MD5_CTX ctx;
        MD5_Init(&ctx);
        MD5_Update(&ctx, Data.contents(), Length);
        MD5_Final((uint8*)&DataHash[0], &ctx);
    }
    else
        for (int i = 0; i < Defines::DATA_HASH_LENGTH; i++)
            DataHash[i] = 0;
}

void PROJECT::Packets::Launcher::ServerPkt::EncryptData(WorldSession* session)
{
    if (Data.empty())
        return;
    uint8* storage = Data.contents();
    uint8 encryptionKey = session->Variables.GetValue<uint8>("Launcher.EncryptionKey");
    for (uint32 i = 0; i < Data.size(); i++)
        storage[i] ^= encryptionKey;
}

bool PROJECT::Packets::Launcher::DecryptData(WorldPacket* packet, uint8* dataHashField, WorldSession* session)
{
    uint8* storage = packet->contents();
    uint8 encryptionKey = session->Variables.GetValue<uint8>("Launcher.EncryptionKey");

    // first 16 bytes are data hash sent by client, apply encryption only for the data after it
    for (uint32 i = Defines::DATA_HASH_LENGTH; i < packet->size(); i++)
        storage[i] ^= encryptionKey;

    // generate md5 for data
    uint8 calculatedHash[Defines::DATA_HASH_LENGTH];
    MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx, (void*)(storage + Defines::DATA_HASH_LENGTH), packet->size() - Defines::DATA_HASH_LENGTH);
    MD5_Final((uint8*)&calculatedHash[0], &ctx);

    // compare client provided hash with server calculated hash for the read data
    return memcmp(dataHashField, &calculatedHash[0], Defines::DATA_HASH_LENGTH) == 0;
}

bool PROJECT::Packets::Launcher::ParseData(WorldPacket* packet, uint8* dataHashField, WorldSession* session)
{
    for (int i = 0; i < Defines::DATA_HASH_LENGTH; i++)
        dataHashField[i] = packet->read<uint8>();

    if (!DecryptData(packet, dataHashField, session))
        return false;

    return true;
}

bool PROJECT::Packets::Launcher::CheckEncryption(WorldSession* session)
{
    if (!session->Variables.IsSet("Launcher.EncryptionKey"))
    {
        // force encryption key calculation
        PacketHandler::Launcher::HandleLauncherGetEncryptionKey(session);

        //TC_LOG_ERROR("network.opcode", "LAUNCHER: Received CMSG_PROJECT_LAUNCHER_UPDATE before requesting encryption key");
        return false;
    }

    return true;
}

void PROJECT::Packets::Launcher::IntegrityCheckFailed(WorldSession* session)
{
    // force sending encryption key to client
    PacketHandler::Launcher::HandleLauncherGetEncryptionKey(session);

    //TC_LOG_ERROR("network.opcode", "LAUNCHER: Data integrity check failed");
}
