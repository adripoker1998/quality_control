#ifndef PROJECT_LOG_PLACEHOLDER_H
#define PROJECT_LOG_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_log.h"
#else

namespace PROJECT
{
    namespace LogCustom
    {
        inline void Write(const char * str, ...) {};
    }
    
    namespace LogCritical
    {
        inline void Write(const char * str, ...) {};
    }

    namespace LogDatabase
    {
        namespace Types
        {
            enum PlayerLogTypes
            {
                ITEM_BOUGHT        = 0,    // item entry, npc entry
                ITEM_SOLD          = 1,    // item entry, npc entry
                ITEM_REFUNDED      = 2,    // item entry, unused
                ITEM_TRADED        = 3,    // item entry, player guid
                CURRENCY_BOUGHT    = 4,    // currency id, npc entry
                OPCODES            = 5,    // has sepparate table
                OPCODES_SMSG       = 6,    // has sepparate table
            };
        }

        inline void Write(Player * player, Types::PlayerLogTypes type, uint32 info1, uint32 info2) {};
        inline void WriteOpcode(Player * player, WorldPacket & packet, bool serverToClient) {};
    }
}

#endif
#endif