#ifndef PROJECT_GARRISON_PLACEHOLDER_H
#define PROJECT_GARRISON_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "project_garrison.h"
#else

namespace PROJECT
{
    namespace Garrisons
    {
        class TC_GAME_API Garrison
        {
        public:
            Garrison(Player* owner) {};

        };
        
        inline void UseGarrisonShipmentObject(GameObject* gameObject, Unit* user) {};
        inline void BuildValuesUpdateShipmentObject(GameObject* gameObject, Player* target, uint16& dynFlags) {};
    }
}

#endif
#endif
