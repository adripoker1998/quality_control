// this file is included in Common.h which is included by all libraries, will cause FULL recompile

#ifndef PROJECT_COMMON_H
#define PROJECT_COMMON_H

#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>
#include <mutex>
#include <unordered_map>
#include <map>
#include <regex>
#include <list>
#include "Errors.h"
#include "StringFormat.h"

namespace PROJECT
{
    TC_GAME_API class Variables
    {
        friend class VariablesSafe;

        public:
            Variables() : variables(100), ensureAlreadyExists(false) { }
            virtual ~Variables() { }

            typedef std::unordered_map<std::string, boost::any> variablesMap;

            template<typename T>
            T * Get(std::string const& key) const
            {
                _ensureVariable(key);
                auto itr = variables.find(key);
                if (itr != variables.end())
                    return const_cast<T*>(boost::any_cast<T>(&itr->second));
                return nullptr;
            }
            template<typename T>
            T * GetOrCreate(std::string const& key)
            {
                static_assert(std::is_fundamental<T>::value, "GetOrCreate() must be used only for fundamental types, use GetAuto() instead");
                if (!IsSet(key))
                    Reset<T>(key);
                return Get<T>(key);
            }
            template<typename T>
            T GetValue(std::string const& key) const
            {
                _ensureVariable(key);
                auto itr = variables.find(key);
                if (itr != variables.end())
                    return boost::any_cast<T>(itr->second);
                return T();
            }
            template<typename T>
            Variables * Set(std::string const& key, T&& value)
            {
                _ensureVariable(key);
                variables[key] = (boost::any)(value);
                return this;
            }
            template<typename T>
            T SetAndReturnValue(std::string const& key, T&& value)
            {
                _ensureVariable(key);
                variables[key] = (boost::any)(value);
                return value;
            }
            template<typename T>
            Variables * Reset(std::string const& key)
            {
                Set<T>(key, T());
                return this;
            }
            Variables * ToggleBool(std::string const& key)
            {
                if (IsSet(key))
                    Remove(key);
                else
                    Set(key, true);
                return this;
            }
            template<typename T>
            T * GetAuto(std::string const& key, bool createIfInexistent = true)
            {
                static_assert(!std::is_fundamental<T>::value, "GetAuto() must not be used for fundamental types, use GetOrCreate() instead");
                _ensureVariable(key);
                if (IsSet(key))
                    return *Get<T*>(key);
                else if (!createIfInexistent)
                    return nullptr;
                T * value = new T;
                _destructors[key].p = value;
                _destructors[key].destroy = [](const void* x) { static_cast<const T*>(x)->~T(); };
                variables[key] = (boost::any)(value);
                return value;
            }
            Variables * Remove(std::string const& key)
            {
                std::list<std::string> keys;
                if (key.find("*") != std::string::npos)
                {
                    std::string expr = key;
                    std::replace(expr.begin(), expr.end(), '.', '|');
                    while (expr.find("|") != std::string::npos)
                        expr.replace(expr.find("|"), 1, std::string(R"(\.)"));
                    std::replace(expr.begin(), expr.end(), '*', '|');
                    while (expr.find("|") != std::string::npos)
                        expr.replace(expr.find("|"), 1, std::string(".*"));
                    expr = "^" + expr + "$";
                    for (auto var : variables)
                        if (std::regex_match(var.first, std::regex(expr)))
                            keys.push_back(var.first);
                }
                else
                    keys.push_back(key);
                for (auto k : keys)
                {
                    _destructors.erase(k);
                    variables.erase(k);
                }
                return this;
            }

            inline bool IsSet(std::string const& key) const { return variables.find(key) != variables.end(); }
            Variables * Clear() { _destructors.clear(); variables.clear(); return this; }

            void EnsureVariablesAlreadyExist(bool state) { ensureAlreadyExists = state; }

            variablesMap const * GetVariables() const { return &variables; }

            // format helpers
            template<typename T, typename... Args> T * Get(std::string const& key, Args&&... args) const { return Get<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> T * GetOrCreate(std::string const& key, Args&&... args) { static_assert(std::is_fundamental<T>::value, "GetOrCreate() must be used only for fundamental types, use GetAuto() instead"); return GetOrCreate<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> T GetValue(std::string const& key, Args&&... args) const { return GetValue<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> Variables * Set(std::string const& key, T&& value, Args&&... args) { return Set(Trinity::StringFormat(key, std::forward<Args>(args)...), (T&&)value); }
            template<typename T, typename... Args> T SetAndReturnValue(std::string const& key, T&& value, Args&&... args) { return SetAndReturnValue<T>(Trinity::StringFormat(key, std::forward<Args>(args)...), (T&&)value); }
            template<typename T, typename... Args> Variables * Reset(std::string const& key, Args&&... args) { return Reset<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename... Args> Variables * ToggleBool(std::string const& key, Args&&... args) { return ToggleBool(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> typename std::enable_if<(sizeof...(Args) != 0), T*>::type GetAuto(std::string const& key, bool createIfInexistent, Args&&... args) { static_assert(!std::is_fundamental<T>::value, "GetAuto() must not be used for fundamental types, use GetOrCreate() instead"); return GetAuto<T>(Trinity::StringFormat(key, std::forward<Args>(args)...), createIfInexistent); }
            template<typename... Args> Variables * Remove(std::string const& key, Args&&... args) { return Remove(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename... Args> bool IsSet(std::string const& key, Args&&... args) const { return IsSet(Trinity::StringFormat(key, std::forward<Args>(args)...)); }

        protected:
            variablesMap variables;
            bool ensureAlreadyExists;

        private:
            struct destructor
            {
                const void* p;
                void(*destroy)(const void*);
                ~destructor() { destroy(p); delete (char*)p; }
            };
            std::map<std::string, destructor> _destructors;
            void _ensureVariable(std::string const& key) const
            {
                if (!ensureAlreadyExists)
                    return;
                ASSERT(IsSet(key), "Invalid variable '%s'", key.c_str());
            }
    };

    #define __LOCK std::lock_guard<std::recursive_mutex> lock(_lock)
    TC_GAME_API class VariablesSafe : public Variables
    {
        public:
            template<typename T> T * Get(std::string const& key) const { __LOCK; return Variables::Get<T>(key); }
            template<typename T> T * GetOrCreate(std::string const& key) { static_assert(std::is_fundamental<T>::value, "GetOrCreate() must be used only for fundamental types, use GetAuto() instead"); __LOCK; return Variables::GetOrCreate<T>(key); }
            template<typename T> T GetValue(std::string const& key) const { __LOCK; return Variables::GetValue<T>(key); }
            template<typename T> VariablesSafe * Set(std::string const& key, T&& value) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Set<T>(key, (T&&)value)); }
            template<typename T> T SetAndReturnValue(std::string const& key, T&& value) { __LOCK; return Variables::SetAndReturnValue<T>(key, (T&&)value); }
            template<typename T> VariablesSafe * Reset(std::string const& key) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Reset<T>(key)); }
            VariablesSafe * ToggleBool(std::string const& key) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::ToggleBool(key)); }
            template<typename T> T * GetAuto(std::string const& key, bool createIfInexistent = true) { static_assert(!std::is_fundamental<T>::value, "GetAuto() must not be used for fundamental types, use GetOrCreate() instead"); __LOCK; return Variables::GetAuto<T>(key, createIfInexistent); }
            VariablesSafe * Remove(std::string const& key) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Remove(key)); }
            inline bool IsSet(std::string const& key) const { __LOCK; return Variables::IsSet(key); }
            VariablesSafe * Clear() { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Clear()); }

            // format helpers
            template<typename T, typename... Args> T * Get(std::string const& key, Args&&... args) const { __LOCK; return Variables::Get<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> T * GetOrCreate(std::string const& key, Args&&... args) { static_assert(std::is_fundamental<T>::value, "GetOrCreate() must be used only for fundamental types, use GetAuto() instead"); __LOCK; return Variables::GetOrCreate<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> T GetValue(std::string const& key, Args&&... args) const { __LOCK; return Variables::GetValue<T>(Trinity::StringFormat(key, std::forward<Args>(args)...)); }
            template<typename T, typename... Args> VariablesSafe * Set(std::string const& key, T&& value, Args&&... args) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Set<T>(Trinity::StringFormat(key, std::forward<Args>(args)...), (T&&)value)); }
            template<typename T, typename... Args> T SetAndReturnValue(std::string const& key, T&& value, Args&&... args) { __LOCK; return Variables::SetAndReturnValue<T>(Trinity::StringFormat(key, std::forward<Args>(args)...), (T&&)value); }
            template<typename T, typename... Args> VariablesSafe * Reset(std::string const& key, Args&&... args) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Reset<T>(Trinity::StringFormat(key, std::forward<Args>(args)...))); }
            template<typename... Args> VariablesSafe * ToggleBool(std::string const& key, Args&&... args) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::ToggleBool(Trinity::StringFormat(key, std::forward<Args>(args)...))); }
            template<typename T, typename... Args> typename std::enable_if<(sizeof...(Args) != 0), T*>::type GetAuto(std::string const& key, bool createIfInexistent, Args&&... args) { static_assert(!std::is_fundamental<T>::value, "GetAuto() must not be used for fundamental types, use GetOrCreate() instead"); __LOCK; return Variables::GetAuto<T>(Trinity::StringFormat(key, std::forward<Args>(args)...), createIfInexistent); }
            template<typename... Args> VariablesSafe * Remove(std::string const& key, Args&&... args) { __LOCK; return dynamic_cast<VariablesSafe*>(Variables::Remove(Trinity::StringFormat(key, std::forward<Args>(args)...))); }
            template<typename... Args> bool IsSet(std::string const& key, Args&&... args) const { __LOCK; return Variables::IsSet(Trinity::StringFormat(key, std::forward<Args>(args)...)); }

            void Lock() { _lock.lock(); }
            void Unlock() { _lock.unlock(); }

        private:
            mutable std::recursive_mutex _lock;
    };
}

#endif
