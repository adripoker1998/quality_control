#ifndef PROJECT_CHAT_FILTER_PLACEHOLDER_H
#define PROJECT_CHAT_FILTER_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_chat_filter.h"
#else

namespace PROJECT
{
    namespace ChatFilter
    {
        TC_GAME_API inline std::string Check(std::string text, Player * player, uint32 type, uint32 lang, std::string & channel, bool & chatHandlerCanContinue) { return text; };
    }
}

#endif
#endif
