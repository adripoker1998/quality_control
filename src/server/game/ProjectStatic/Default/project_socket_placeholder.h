#ifndef PROJECT_SOCKET_PLACEHOLDER_H
#define PROJECT_SOCKET_PLACEHOLDER_H

#include "WorldSession.h"

#ifdef PROJECT_CUSTOM
#include "project_socket.h"
#else

namespace PROJECT
{
    namespace Socket
    {
        TC_GAME_API inline bool HandleSocketClosed(WorldSession* session) { return false; };
        TC_GAME_API inline bool HandleContinuePlayerLogin(WorldSession* session) { return false; };
        TC_GAME_API inline void SetShouldSetOfflineInDB(WorldSession* session, bool val) {};
        TC_GAME_API inline bool GetShouldSetOfflineInDB(WorldSession* session) { return true; };
        TC_GAME_API inline void SetOfflineTime(WorldSession* session, uint32 time) {};
        TC_GAME_API inline uint32 GetOfflineTime(WorldSession* session) { return session->_offlineTime; };
        TC_GAME_API inline bool IsKicked(WorldSession* session) { return session->_kicked; };
        TC_GAME_API inline void SetKicked(WorldSession* session, bool val) { session->_kicked = val; };
    }
}
#endif
#endif
