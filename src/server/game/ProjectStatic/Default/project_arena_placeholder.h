#ifndef PROJECT_ARENA_PLACEHOLDER
#define PROJECT_ARENA_PLACEHOLDER

#include "project.h"
#include "project_util_placeholder.h"
#include "ArenaTeam.h"
#include "Battleground.h"
#include "Player.h"

#ifdef PROJECT_CUSTOM
#include "project_arena.h"
#else

namespace PROJECT
{
    namespace Arenas
    {
        TC_GAME_API inline void Ended(Battleground * bg, uint32 winner) {};
        TC_GAME_API inline void StartBattleground(Battleground * bg) {};
        TC_GAME_API inline uint32 GetAverageRating(Group * group, int32 typeOrSlot) { return 0; };
        TC_GAME_API inline uint32 GetAverageMMR(Group * group, int32 typeOrSlot) { return 0; };

        namespace SoloQueue
        {
            enum Type
            {
                TYPE_NONE            = 0,
                TYPE_2v2_SKIRMISH    = 2,
                TYPE_3v3_RATED       = 3,
            };

            class TC_GAME_API Manager
            {
                private:

            public:
                static Manager* instance()
                {
                    static Manager instance;
                    return &instance;
                };

                Manager() {};
            
                Type IsInQueue(Player * player, Type type = Type::TYPE_NONE) { return TYPE_2v2_SKIRMISH; };

                bool AddPlayer(Player * player, Type type) { return false; };
                void RemovePlayer(Player * player) {};
                void PenalizePlayerInviteNotAccepted(Player * player, bool showMessage = true, bool removeInvitation = false) {};
            };
        }
    }
}

#define PROJECT_ArenaSoloQueueManager PROJECT::Arenas::SoloQueue::Manager::instance()

#endif
#endif
