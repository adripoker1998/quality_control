#ifndef PROJECT_CONFIG_H
#define PROJECT_CONFIG_H

#include "project_common_placeholder.h"

#define _CONFIG_LOAD_BOOL(name, value) PROJECT::Config->Set<bool>(name, sConfigMgr->GetBoolDefault(std::string("PROJECT.") + std::string(name), value))
#define _CONFIG_LOAD_INT32(name, value) PROJECT::Config->Set<int32>(name, sConfigMgr->GetIntDefault(std::string("PROJECT.") + std::string(name), value))
#define _CONFIG_LOAD_FLOAT(name, value) PROJECT::Config->Set<float>(name, sConfigMgr->GetFloatDefault(std::string("PROJECT.") + std::string(name), value))
#define _CONFIG_LOAD_STRING(name, value) PROJECT::Config->Set<std::string>(name, sConfigMgr->GetStringDefault(std::string("PROJECT.") + std::string(name), value))

namespace PROJECT
{
    namespace StaticConfig
    {
        extern uint32 MaxPacketsPerSessionUpdate;
    }

    class TC_GAME_API _Config : public PROJECT::Variables
    {
        public:
            static _Config* instance();

            void Reload(bool full = true);
            int32 GetInt(std::string const& key) { return GetValue<int32>(key); }
            float GetFloat(std::string const& key) { return GetValue<float>(key); }
            bool GetBool(std::string const& key) { return GetValue<bool>(key); }
            std::string GetString(std::string const& key) { return GetValue<std::string>(key); }
        
            void SetInt(std::string const& key, int32 value) { Set(key, int32(value)); }
            void SetFloat(std::string const& key, float value) { Set(key, float(value)); }
            void SetBool(std::string const& key, bool value) { Set(key, bool(value)); }
            void SetString(std::string const& key, std::string value) { Set(key, std::string(value)); }
    };
    TC_GAME_API extern _Config * Config;
}

#endif
