#ifndef PROJECT_LOOT_PLACEHOLDER_H
#define PROJECT_LOOT_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_loot.h"
#else

class Map;
struct Loot;
class LootStore;

namespace PROJECT
{
    namespace Loots
    {
        enum ToastType
        {
            TOAST_ITEM           = 0,
            TOAST_CURRENCY       = 1,
            TOAST_GOLD           = 2,
        };

        // loot allow
        inline bool CanLootEntityDirectCheck(Player * player, ObjectGuid guid) { return false; };
        inline bool CanLootEntity(Player * player, ObjectGuid guid) { return true; };
        inline bool CheckIfCustomAllow(Map * map, ObjectGuid guid) { return false; };

        // misc
        TC_GAME_API inline bool HandleLoot(Player * player, Object * lootedObject) { return true; };

        // shared loot
        inline bool FillSharedLoot(Loot* loot, uint32 lootId, LootStore const& store, std::list<ObjectGuid> looters, bool noEmptyError = false, uint16 lootMode = LOOT_MODE_DEFAULT) { return false; };
        inline bool IsAllowedToSharedLoot(Unit* killer, Creature* killed) { return false; };
    }
}
#endif
#endif
