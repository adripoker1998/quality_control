#ifndef PROJECT_WARDEN_PLACEHOLDER_H
#define PROJECT_WARDEN_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_warden.h"
#else

namespace PROJECT
{
    namespace Warden
    {
        class WardenClass
        {
            public:
                WardenClass(Player * player) {};
                void Disable(int32 duration = -1) {};
                void HandleMovementPacket(MovementInfo * movementInfo, uint32 opcode) {};
        };

        class Manager
        {
            public:
                static Manager* instance()
                {
                    static Manager instance;
                    return &instance;
                };

                Manager() {};

                bool IsEnabled() { return false; }
                void LoadConfig() {};
        };

        #define PROJECT_WardenManager PROJECT::Warden::Manager::instance()
    }
}

#endif
#endif
