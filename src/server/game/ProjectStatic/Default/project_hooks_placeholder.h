#ifndef PROJECT_HOOKS_PLACEHOLDER_H
#define PROJECT_HOOKS_PLACEHOLDER_H

#pragma once
#include "project.h"
#include "LFGMgr.h"
#include "DisableMgr.h"
#include "Player.h"
#include "World.h"
#include "SpellInfo.h"
#include "SpellPackets.h"
#include "TalentPackets.h"


#ifdef PROJECT_CUSTOM
#include "project_hooks.h"
#else


enum SpellEffectHandleMode;
struct AreaTriggerStruct;

namespace PROJECT
{
    namespace Hooks
    {
        namespace Units
        {
            TC_GAME_API inline bool Kill(Unit * killer, Unit * killed) { return true; };
            inline bool HandleProcTriggerSpell(Unit * caster, Unit *victim, SpellInfo const* auraSpellInfo, uint32 damage, AuraEffect* triggeredByAura, SpellInfo const *procSpell, uint32 & triggeredSpellId, int32 & basepoints0, Unit* & target, uint32 procFlag, uint32 procEx, uint32 cooldown, bool & takeCharges) { return true; };
            inline bool EnterVehicle(Unit * unit, Vehicle *vehicle) { return true; };
            inline bool ExitVehicle(Unit * unit, Vehicle *vehicle) { return true; };
            inline bool HandleDummyAuraProc(Unit * proccingUnit, Unit *victim, uint32 damage, AuraEffect* triggeredByAura, SpellInfo const * procSpell, uint32 cooldown, bool & skipDefaultSwitch, Unit* & target, int32 & basepoints0, uint32 & triggeredSpellId, bool & returnValue, uint32 procFlag, uint32 procEx) { return true; };
            inline bool isSpellCrit(Unit const * caster, Unit * victim, SpellInfo const * spellProto, bool & dontContinue) { dontContinue = false; return false; };
            inline void GetUnitSpellCriticalChance(Unit const * caster, Unit * victim, SpellInfo const * spellProto, float & currentCritChance) {};
            inline bool AddThreat(Unit * caster, Unit * target, float & threat) { return true; };
            inline bool RemoveAurasWithMechanic(Unit * target, Aura const * aura, uint32 removingSpell) { return true; };
            inline void RemoveAurasDueToSpellByDispel(Unit * target, uint32 auraId, ObjectGuid casterGUID, Unit * dispeller) {};
            inline int32 GetModelForForm(Unit * target, ShapeshiftForm form) { return 0; };
            inline bool ApplyAura(Unit* target, Aura* aura) { return true; };
            inline bool ApplyAuraEffect(Unit* unit, Aura * aura, uint8 effIndex) { return true; };
            inline bool RemoveArenaAuras(Unit * unit, Aura const* aura) { return true; };
            inline bool ProcDamageAndSpellFor_DropCharge(Unit * target, Aura * aura, uint32 damage, SpellInfo const * procSpell, Unit * victim) { return true; };
            inline int32 SpellDamageBonus(Unit * caster, Unit *victim, SpellInfo const *spellProto, uint32 pdamage, int32 & absoluteDamage, float & DoneTotalMod, DamageEffectType damageType) { return 0; };
            inline int32 SpellHealingBonus(Unit * caster, Unit *victim, SpellInfo const *spellProto, uint32 healamount, int32 & absoluteDamage, float & DoneTotalMod, DamageEffectType damagetype, SpellEffectInfo const* spellEffect) { return 0; };
            inline int32 HealBySpell(Player * caster, Unit * victim, SpellInfo const * spellProto, uint32 addHealth, bool critical) { return addHealth; };
            inline void DealHeal(Unit * caster, Unit * victim, uint32 & addHealth) {};
            inline void AfterDealHeal(Unit* caster, Unit* victim, uint32 addHealth, uint32 gain) {};
            inline int32 CalculateSpellDamageTaken(Unit * caster, Unit * victim, int32 damage, SpellInfo const *spellProto, bool crit) { return damage; };
            inline int32 CalculateSpellDamageTakenAfterAborbCalc(Unit * caster, Unit * victim, int32 damage, SpellInfo const *spellProto, bool crit) { return damage; };
            inline int32 CalculateMeleeDamage(Unit * caster, Unit * victim, int32 damage) { return damage; };
            inline int32 AnyDamageTaken(Unit * caster, Unit * victim, int32 damage, SpellInfo const *spellProto) { return damage; };
            inline void UnmitigatedDamageTaken(Unit * caster, Unit * victim, int32 damage, SpellInfo const * spellProto) {};
            inline void DealDamage(Unit *caster, Unit *victim, uint32 damage, CleanDamage const* cleanDamage, DamageEffectType damagetype, SpellSchoolMask damageSchoolMask, SpellInfo const *spellProto, bool durabilityLoss) {};
            inline void SetPower(Unit * unit, Powers power, int32 & val) {};
            inline void Update(Unit * unit, uint32 p_time) {};
            inline void SpellDamageBonusDone_spellPower(Unit * caster, SpellInfo const* spellProto, int32 & spellPower) {};
            inline void CalcHealAbsorb(Unit * caster, Unit * victim, int32 currentAbsorb, AuraEffect * absorbAuraEffect) {};
            inline SpellMissInfo SpellHitResult(Unit * caster, Unit * victim, SpellInfo const* spell, bool & dontContinue) { return SPELL_MISS_NONE; };
            inline bool SetInCombatState(Unit * source, Unit * enemy, bool PvP) { return true; };
            inline void ClearInCombat(Unit * source) {};
            inline void ModifyPower(Unit* unit, Powers powerType, int32 amount) {};
            inline bool HasAuraState(Unit const* unit, AuraStateType flag, SpellInfo const* spellProto, Unit const* caster, bool& dontContinue) { return false; };
        }

        namespace Players
        {
            TC_GAME_API inline void PlayerLoggedIn(Player * player, SQLQueryHolder* holder) {};
            inline bool EnvironmentalDamage(Player * plr, EnviromentalDamage type, uint32& damage) { return true; };
            inline bool RemoveSpellMods(Aura * ownerAura, Spell * procSpell) { return true; };
            inline void EquipItem(Player * plr, Item * pItem, uint8 bag, uint8 slot) {};
            inline void RemoveItem(Player * plr, Item * pItem, uint8 bag, uint8 slot) {};
            inline void ReputationChanged(Player * plr, FactionEntry const* factionEntry) {};
            inline bool TeleportTo(Player * plr, uint32 mapid, float x, float y, float z, float orientation) { return true; };
            inline void Update(Player * player, uint32 p_time) {};
            inline bool LoadAuras(Unit * playerOrPet, uint32 spell_id) { return true; };
            inline void UpdateZone(Player * player, uint32& newZone, uint32& newArea, uint32 oldZone) {};
            inline void UpdateArea(Player * player, uint32& newArea, uint32 oldArea) {};
            inline bool CompleteQuest(Player * player, uint32 quest_id) { return true; };
            inline void StoreItem(Player * plr, Item* item) {};
            inline void DestroyItem(Player * plr, Item* item) {};
            inline bool CanTakeQuest(Player * player, Quest const *pQuest, bool msg) { return true; };
            inline bool SaveToDB(Player * player, SQLTransaction& trans) { return true; };
            inline void DeleteFromDB_Queries(ObjectGuid::LowType guid, SQLTransaction trans) {};
            inline bool isHonorOrXPTarget(Unit * victim, bool & result) { return true; };
            inline bool RemoveArenaSpellCooldowns(Player * player, bool removeActivePetCooldowns) { return true; };
            inline bool SendLoot(Player * player, ObjectGuid guid, LootType loot_type) { return true; };
            inline void RewardQuest(Player * player, Quest const* quest, uint32 reward, Object* questGiver) {};
            inline void UpdateSkillPro(Player * player, uint16 skillId) {};
            inline void ResurrectPlayer(Player * player) {};
            inline void PlayerContructor(Player * player) {};
            inline void PlayerDestructor(Player * player) {};
            inline void removeSpell(Player * player, uint32 spellId) {};
            inline void learnSpell(Player * player, uint32 spellId) {};
            inline void SetSkill(Player * player, uint16 skillId, uint16 newVal) {};
            inline void SendInitialPacketsAfterAddToMap(Player * player) {};
            inline InventoryResult CanEquipItem(Player * player, Item* pItem) { return EQUIP_ERR_OK; };
            inline void ActivateTalentGroup(Player* player, ChrSpecializationEntry const* spec) {};
            inline void UpdatePosition(Player* player, float x, float y, float z, float orientation, bool teleport) {};
            inline void SendQuestUpdate(Player* player) {};
            inline void Create(Player* player, ObjectGuid::LowType GUID) {};
            inline bool IsActionButtonDataValid(Player const* player, uint8 button, uint32 action, uint8 type, bool & dontContinue) { return true; };
            inline TC_GAME_API void AddItem(Player* player, Item* item, uint32 count) {};
            inline void BuyItemFromVendorSlot(Player* player, Item* item) {};
        }

        namespace Accounts
        {
            inline void LoggedIn(WorldSession * session, Field * loginFields) {};
        }

        namespace Spells
        {
            inline SpellMissInfo DoSpellHitOnUnit(SpellInfo const* spellProto, Unit* m_caster, Spell* m_spell, Unit* unit) { return (SpellMissInfo)999; };
            inline SpellCastResult cast(Spell * spell, bool before_cast) { return SPELL_CAST_OK; };
            inline SpellCastResult CheckCast(Spell * spell, Unit * target, Unit * m_caster, bool & dontContinue) { return SPELL_CAST_OK; };
            inline void SearchAreaTarget(Spell * spell, PROJECT::Defines::WorldObjectList & targets, int32 effIndex = -1) {};
            inline bool SelectEffectImplicitTargets(Spell * spell, uint32 effectIndex, SpellImplicitTargetInfo const& currentTargetType) { return true; };
            inline bool prepare(Spell * spell, SpellCastTargets const * targets) { return true; };
            inline void finish(Spell * spell, bool ok) {};
            inline void SpellConstructor(Spell * spell) {};
            inline uint32 GetSpellCastTime(Unit * caster, SpellInfo const * spellProto, Spell * spell) { return spellProto->CalcCastTime(caster->getLevel(), spell); };
            inline void DoAllEffectOnTarget(Spell * spell, Unit * caster, Unit * target) {};
            inline void HandleEffects(Spell * spell, uint32 effectId, uint32 effectIndex) {};
            inline bool CheckRange(Spell * spell, bool strict, SpellCastResult & castResult) { return true; };
            inline DiminishingGroup GetDiminishingReturnsGroupForSpell(SpellInfo const* spellproto, uint32 effectMask = 0) { return DIMINISHING_NONE; };
            inline int32 GetDiminishingReturnsLimitDuration(SpellInfo const* spellproto) { return 0; };
            inline void LoadSpellInfoStore() {};

            inline void EffectSummonType_SUMMON_TYPE_TOTEM(Spell * spell, TempSummon * & summon, Position * pos, SpellEffIndex effIndex, Unit * m_caster, Unit * m_originalCaster, uint32 damage) {};
            inline void EffectInterruptCast(Spell * m_spell, Unit* target, SpellInfo const * lastInterruptedSpell) {};

            TC_GAME_API inline void PostEffectActions(Spell * spell, uint32 effectId, uint32 effectIndex) {};
            TC_GAME_API inline void CheckSpellTargetZoneBehindAndRelocatePositionIfNecessary(Spell * spell) {};
            inline void PerSpellMakeChanges(Spell * spell) {};
            inline bool CanSpellPutCasterInCombat(uint32 spellId) { return true; };
        }

        namespace Auras
        {
            inline bool Create(WorldObject * owner_worldobj, Unit * caster, SpellInfo const* spellproto, bool onlyAuraReapplying = false) { return true; };
            inline bool HandleAuraSpecificMods(Aura * aura, AuraApplication const * aurApp, Unit * caster, bool apply, bool reapply) { return true; };
            inline void FillTargetMap(Aura * aura, PROJECT::Defines::UnitList & targetList, AuraObjectType auraType, uint8 effIndex) {};
        }

        namespace AuraEffects
        {
            inline void CalculateSpellMod(AuraEffect * aurEff, SpellModifier * m_spellmod) {};
            inline bool PeriodicTick(const AuraEffect * aurEff, AuraApplication * aurApp, Unit * target, Unit * caster) { return true; };
            inline void PeriodicTick_Periodic_Damage(Unit * caster, Unit * target, const AuraEffect * aurEff, uint32 & damage, bool crit) {};
            inline void PeriodicTick_Periodic_Heal(Unit * caster, Unit * target, const AuraEffect * aurEff, uint32 & heal) {};
            inline bool CalculateAmount(AuraEffect * aurEff, float & amount) { return true; };
            inline void HandleEffects(AuraEffect * aurEff, AuraApplication * aurApp, uint8 mode, bool apply) {};
            inline void HandleEffects_after(AuraEffect * aurEff, AuraApplication * aurApp, uint8 mode, bool apply) {};
        }

        namespace AuraApplications
        {
            inline void InitFlags(Unit* caster, AuraApplication* aurApp, uint32 effMask, uint8& flags) {};
            inline void BuildUpdatePacket(AuraApplication* aurApp, WorldPackets::Spells::AuraInfo& auraInfo, bool remove) {};
        }

        namespace Creatures
        {
            inline void UpdateEntry(Creature * creature) {};
            inline void OnCreatureCreate(Creature* creature) {};
        }

        namespace Handlers
        {
            inline bool HandleQuestgiverAcceptQuestOpcode(Player * plr, Quest const* qInfo) { return true; };
            inline bool HandleGroupUninviteGuidOpcode(Player * plr, ObjectGuid uninvitedGuid) { return true; };
            inline void HandleMovementOpcodes(Player * plMover, MovementInfo movementInfo, Player * _player) {};
            inline void HandleMailTakeItem(Player * player, Mail * mail, Item * item) {};
            inline void HandleLogoutRequestOpcode(Player * player, uint8 & reason, uint8 logoutLocationInCode) {};
            inline bool HandleAreaTriggerOpcode(Player * player, AreaTriggerStruct const* at) { return true; };
            inline bool HandleUseItemOpcode(Player * player, Item * item) { return true; };
            inline bool Update_PacketsHandler(WorldPacket & packet, WorldSession* session, bool serverToClient = false) { return true; };
            inline bool HandleCastSpellOpcode_HasSpell(Unit * caster, uint32 spellId) { return true; };
            TC_GAME_API inline void HandleCastSpellOpcode(Unit * caster, SpellInfo const* & spellInfo, WorldPackets::Spells::CastSpell& cast) {};
            inline void HandleMoveWorldportAck(WorldSession* session) {};
        }

        namespace Events
        {
            inline void StartEvent(uint32 event_id) {};
            inline void StopEvent(uint32 event_id) {};
        }

        namespace GameObjects
        {
            inline bool Use(GameObject * gObject, Unit * user) { return true; };
            inline void OnGameObjectCreate(GameObject* go) {};
        }

        namespace Worlds
        {
            inline void Update(World * world, uint32 & diff) {};
            inline void UpdateEnd(World * world, uint32 & diff) {};
            inline void ResetDailyQuests() {};
        }

        namespace SpellInfos
        {
            inline bool IsPositiveSpell(uint32 spell_id, bool & positive, int32 effectIndex = -1) { return true; };
            inline void SpellEffectInfo_CalcValue(SpellInfo const * spellProto, uint8 effIndex, Unit const * caster, float & value, Unit const* target) {};
            inline bool IsAffectedBySpellMod(SpellInfo const * spellProto, SpellModifier const* mod) { return true; };
            inline bool IsAffectedBySpellModForce(SpellInfo const * spellProto, SpellModifier const* mod) { return false; };
        }

        namespace Pets
        {
            inline void InitStatsForLevel(Creature * pet, uint8 petlevel) {};
            inline void LoadPetFromDB(Pet * pet) {};
            inline void InitStatsForLevel(Guardian * pet, float & value) {};
            inline void UpdateAttackPowerAndDamage(Guardian * pet) {};
        }

        namespace LootItems
        {
            inline bool AllowedForPlayer(Player * player, uint32 itemId, bool & canLoot) { return true; };
        }

        namespace Channels
        {
            inline bool Say(Channel * channel, std::string const& originalMsg, ObjectGuid playerGUID, uint32 forcedTeam) { return true; };
        }

        namespace Objects
        {
            inline bool CanSeeOrDetect(WorldObject const * seerObj, WorldObject const * seenObj, bool & returnValue) { return true; };
        }

        namespace Battlegrounds
        {
            inline bool EndBattleground(Battleground * bg, uint32 winner) { return true; };
            inline void ProcessJoin_SetupBattleground(Battleground * bg) {};
            inline void Update(Battleground * bg, uint32 diff) {};
            inline void AddPlayer(Player* player) {};
            inline void ProcessJoin_AboutToStart(Battleground * bg) {};
            inline void ProcessJoin_Started(Battleground * bg) {};
        }

        namespace Maps
        {
            inline bool GetHeight(const Map* const map, float x, float y, float z, float & gridHeight, float& maxSearchDist) { return false; };
            inline void InstanceMap_Destructor(InstanceMap * map) {};
            inline void InstanceMap_Constructor(InstanceMap* map, Player* player) {};
            inline void CreateInstance(InstanceMap * map) {};
            inline Map::EnterState CanPlayerEnter(Player * player, uint32 mapId, bool atLogin) { return Map::CAN_ENTER; };
            inline void Update_Before(Map* map, uint32 diff) {};
            inline void Update_After(Map* map, uint32 diff) {};
        }

        namespace LFGs
        {
            inline void InitializeLockedDungeons(Player * player, lfg::LfgLockMap & lock) {};
        }

        namespace On
        {
            TC_GAME_API inline void Summon(Unit * owner, Unit * summon) {};
            inline void CurrencyUpdate(Player * player, uint32 currencyId, bool atLogin = false) {};
            inline void QuestAdd(Player * player, Quest const* quest) {};
        }

        namespace WorldSessions
        {
            inline void Update(WorldSession* session, uint32 diff) {};
            inline void HandlePlayerLogin(WorldSession* session, Player* player) {};
        }

        namespace Items
        {
            inline void GetItemLevel(const ItemTemplate* itemTemplate, Player const* owner, uint32 & itemLevel) {};
        }

        namespace ObjectManage
        {
            inline void LoadCreatureTemplate(CreatureTemplate& creatureTemplate, std::string scriptName) {};
        }

        namespace Disables
        {
            inline bool IsDisabledFor(DisableType type, uint32 entry, Unit const* unit) { return false; };
        }
    }
}

#endif
#endif
