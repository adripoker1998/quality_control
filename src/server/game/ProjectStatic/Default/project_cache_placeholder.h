#ifndef PROJECT_CACHE_PLACEHOLDER_H
#define PROJECT_CACHE_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_cache.h"
#else

namespace PROJECT
{
    namespace Cache
    {
        namespace EntityPositions
        {
            inline void Add(uint32 guid, uint32 map, float x, float y, float z, bool isCreature) {};
            inline void Remove(uint32 guid, bool isCreature) {};
        }
    }
}

#endif
#endif
