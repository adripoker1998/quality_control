#ifndef _WORLDQUESTMGR_PLACEHOLDER_H
#define _WORLDQUESTMGR_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "custom_worldquest.h"
#else

#include "QuestDef.h"
#include "ObjectMgr.h"
#include "World.h"
#include "Player.h"
#include "CriteriaHandler.h"
#include "QuestPackets.h"
#include "WorldStatePackets.h"


namespace PROJECT
{
namespace WorldQuest
{

class TC_GAME_API WorldQuestMgr
{
public:
    WorldQuestMgr() {};
    ~WorldQuestMgr() {};

    static WorldQuestMgr* instance()
    {
        static WorldQuestMgr instance;
        return &instance;
    };

    void BuildPacket(Player* player, WorldPackets::Quest::WorldQuestUpdate& packet) {};
    void FillInitWorldStates(Player* player, WorldPackets::WorldState::InitWorldStates& packet) {};
};

#define sWorldQuestMgr WorldQuestMgr::instance()

} // WorldQuest
} // PROJECT
#endif
#endif
