#ifndef PROJECT_SPELL_QUEUE_PLACEHOLDER_H
#define PROJECT_SPELL_QUEUE_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "project_spell_queue.h"
#else

namespace PROJECT
{
    namespace Spells
    {
        namespace Queue
        {
        inline void handle(Player * player, bool instantSpells, SpellInfo const* spellProto) {};
        }
    }
}

#endif
#endif
