#ifndef PROJECT_SPELLS_PLACEHOLDER_H
#define PROJECT_SPELLS_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "project_spells.h"
#else

#include "project.h"
#include "Spell.h"
#include "SpellInfo.h"
#include "SpellAuras.h"
#include "SpellMgr.h"

namespace PROJECT
{
    namespace Spells
    {

        namespace Constants
        {
            enum SpellsEnum
            {
                SPELL_ARENA_PREPARATION = 32727,
                SPELL_ROOT = 42716,
                SPELL_DESERTER = 26013,
                SPELL_BG_IDLE = 43680,
                SPELL_BERSERK = 26662,
                SPELL_SLEEP = 9454,
                SPELL_CLONE = 45204,
                SPELL_DISPEL_MAGIC = 15090,
                SPELL_SHAMAN_HEX = 51514,
                SPELL_ARENA_DAMPENING = 110310,
                SPELL_WEAKENED_ARMOR = 113746,
                SPELL_BATTLE_FATIGUE = 134735,
                SPELL_PET_STATS_SCALING = 34902,
                SPELL_LFR_DETERMINATION = 139068,
                SPELL_WAR_GAMES = 96357,
                SPELL_HONORLESS_TARGET = 2479,
                SPELL_THE_LAST_STANDING = 26549,
                SPELL_COMBO_POINT = 139546,
                SPELL_COMBO_POINT_DELAYED = 139569,
                SPELL_HONORABLE_MEDALLION = 195710,
                SPELL_NO_FALL_DAMAGE = 999011,
                SPELL_RAID_FINDER_KICK_CD_AURA = 999016,
                SPELL_ICC_VALKYR_EXPLOIT_CHECK_AURA = 999029,
                SPELL_CLOAK_OF_SHADOWS_IMMUNITY_AURA = 999034,
                SPELL_DS_ARENA_KNOCKBACK_AURA = 999036,
                SPELL_LOOT_AURA = 999037,
                SPELL_STEALTH_TRACKING_AURA = 999039,
                SPELL_TEMPORARY_AURA_MARKER = 999040,
                SPELL_MOD_HEALING_TAKEN = 999047,
                SPELL_DUMMY_WARLOCK_FORCE_MASTERY_UPDATE = 999049,
            };
        }
        enum TriggerFlags
        {
            IGNORE_MOVEMENT         = 0x80000000,
        };

        enum SpellTriggerFlags : uint32
        {
            CHECK_CASTER_FOR_AURA           = 0x01,
            CHECK_TARGET_FOR_AURA           = 0x02,
            CAST_ON_CASTER                  = 0x04,
            CAST_ON_TARGET                  = 0x08,
            TRIGGER_ON_CAST                 = 0x10,
            TRIGGER_ON_HIT                  = 0x20,
        };
        
        TC_GAME_API inline void HandleSpellTriggers(Unit* caster, Unit* target, uint32 spellId, uint32 triggerMode) {};
        TC_GAME_API inline bool IsCrowdControlSpell(SpellInfo const * spellProto, bool includeSnare = false) { return false; };
        TC_GAME_API inline void CheckArmorSpecializations(Player * plr) {};
        TC_GAME_API inline int32 GetAuraEffectAmount(Unit * unit, AuraType auraType, SpellFamilyNames spellFamilyName, uint32 IconFileDataId, uint8 effIndex) { return 0; };
        TC_GAME_API inline int32 GetAuraEffectAmount(Unit * unit, uint32 spellId, uint8 effIndex, ObjectGuid casterGuid = ObjectGuid::Empty) { return 0; };
        TC_GAME_API inline void RemoveAurasByType(Unit * unit, AuraType type) {};
        TC_GAME_API inline bool CanSpellIgnoreLoS(Unit const * caster, Unit const * target, const SpellInfo * spellInfo = NULL) { return true; };
        TC_GAME_API inline uint32 CanDodgeBlockParryDeflect(Unit const* caster, Unit *victim, SpellInfo const * spellProto, uint8 codeLocation) { return PROJECT::Constants::CanHit::CanHitTypes::TYPE_ALL; };
        TC_GAME_API inline float GetMagicSpellHitChance(Unit const* caster, Unit * victim = NULL, SpellInfo const* spellProto = NULL) { return 0.0f; };
        TC_GAME_API inline float GetTargetResistanceModifier(Unit * caster, Unit * victim, SpellSchoolMask schoolMask) { return 0.0f; };
        TC_GAME_API inline void LossOfControl_Add(Player * player, SpellInfo const* attackerSpell, uint32 duration, uint32 type = 0, uint32 lockoutMask = 0) {};
        TC_GAME_API inline bool Roll_RealPPM_Chance(Unit * unit, Aura * aura) { return true; };
        TC_GAME_API inline std::string GetSpellText(uint32 spellId)
        {
            std::string str;
            char tmp[255];
            if (!spellId)
                sprintf(tmp, "|cff00ffff0|r");
            else if (auto spellInfo = GET_SPELL(spellId))
                sprintf(tmp, "|cff00ffff|Hspell:%u|h[%u][%s]|h|r", spellId, spellId, spellInfo->SpellName->Str[LOCALE_enUS]);
            else
                sprintf(tmp, "|cff00ffff|Hspell:%u|h[%s]|h|r", spellId, "UNKNOWN");
            str = tmp;
            return str;
        }
        TC_GAME_API inline float GetSpellPowerCostModifier(Powers power, SpellInfo const* spellInfo, std::vector<SpellPowerCost> finalCosts) { return 0; };
        TC_GAME_API inline int32 CalcMaxAuraStacks(Unit* unit, int32 spellId)
        {
            if (SpellInfo* spellInfo = GET_SPELL(spellId))
                return spellInfo->StackAmount;

            return 0;
        };
        TC_GAME_API inline void HandleAutoattackOverrides(Unit* attacker, Unit* target, WeaponAttackType attackType) {};
        TC_GAME_API inline void HandleCooldownMods(Player* player, int32& cooldown, int32& categoryCooldown, SpellInfo const* spellInfo) {};
        TC_GAME_API inline bool HandleDelayedSpellTriggerEffect(Unit* caster, SpellCastTargets targets, SpellEffectInfo const* effectInfo, CustomSpellValues values, ObjectGuid originalCasterGuid) { return true; };
        TC_GAME_API inline bool CanCastTimeReductionAuraProc(Aura* aura, Spell* spell) { return true; };
        TC_GAME_API inline void HandleCriticalBlockProc(Unit* caster) {};
    }

    namespace Mastery
    {
        TC_GAME_API inline void ScheduleAurasUpdate(Player * player, bool forceAmountUpdateAndRecalculate = false) {};
    }
}
#endif
#endif
