#ifndef PROJECT_BATTLE_PET_DEFINES_H
#define PROJECT_BATTLE_PET_DEFINES_H

namespace PROJECT
{
    namespace BattlePets
    {
        namespace Defines
        {
            typedef std::map<uint32, std::list<uint32>> TamerPetsMap;

            extern TamerPetsMap TamerPets;
        }

        namespace Constants
        {
            enum Slots
            {
                SLOT_1      = 0,
                SLOT_2      = 1,
                SLOT_3      = 2,
                SLOT_MAX,
            };

            enum SlotFlags
            {
                SLOT_FLAG_NONE           = 0x0,
                SLOT_FLAG_SLOT_1         = 0x1,
                SLOT_FLAG_SLOT_2         = 0x2,
                SLOT_FLAG_SLOT_3         = 0x4,
            };

            // used in project_account_battle_pet db table
            enum JournalFlags
            {
                JOURNAL_FLAG_NONE                = 0x00,
                JOURNAL_FLAG_FAVORITES           = 0x01,
                JOURNAL_FLAG_COLLECTED           = 0x02, // name dumped from client, use unknown
                JOURNAL_FLAG_NOT_COLLECTED       = 0x04,
                JOURNAL_FLAG_UNKNOWN_1           = 0x08,
                JOURNAL_FLAG_ABILITY_1           = 0x10, // ability flags are set if the second ability for that slot is selected
                JOURNAL_FLAG_ABILITY_2           = 0x20,
                JOURNAL_FLAG_ABILITY_3           = 0x40,
            };

            enum Miscs
            {
                TOTAL_CHALLENGERS                       = 2,
                TOTAL_ENVIROS                           = 3,
                MAX_ACTIVE_ABILITIES                    = 3,
                REQUEST_TIMEOUT                         = 15 * IN_MILLISECONDS,
                MAX_TOTAL_PETS                          = TOTAL_CHALLENGERS * Slots::SLOT_MAX,
                QUEUED_MATCH_FIRST_ROUND_TIME_BONUS     = 15,
                TRAP_PET_HP                             = 35,
                MAX_BATTLE_PETS_PER_SPECIES             = 3,
                CAGE_ITEM_ID                            = 82800,
                MAX_PET_LEVEL                           = 25,
                MAX_NAME_LENGTH                         = 16,
                MAX_JOURNAL_PETS                        = 1000,
            };

            enum Spells
            {
                SPELL_DEFAULT_SUMMON_BATTLE_PET_SPELL       = 118301,
                SPELL_BATTLE_PET_TRAINING_PASSIVE           = 119467,
                SPELL_TRACK_PETS                            = 122026,
                SPELL_REVIVE_BATTLE_PETS                    = 125439,
                SPELL_LEVEL_3_QUEST_KILLCREDIT              = 129584,
                SPELL_GENERIC_WIN_BATTLE_QUEST_KKILLCREDIT  = 127966,
                SPELL_GENERIC_TRAP_QUEST_KILLCREDIT         = 128010,
            };

            enum DatabaseState
            {
                DB_STATE_NONE   = 0,
                DB_STATE_DELETE = 1,
                DB_STATE_SAVE   = 2,
            };

            enum QueueStatus
            {
                QUEUE_JOINED     = 1,
                QUEUE_UPDATE     = 2,
                QUEUE_LEAVE      = 8,
            };


            enum FlagsControlType
            {
                FLAGS_CONTROL_TYPE_APPLY        = 1,
                FLAGS_CONTROL_TYPE_REMOVE       = 2
            };

            enum Errors
            {
                BATTLEPETRESULT_CANT_HAVE_MORE_PETS_OF_THAT_TYPE = 3,
                BATTLEPETRESULT_CANT_HAVE_MORE_PETS              = 4,
                BATTLEPETRESULT_TOO_HIGH_LEVEL_TO_UNCAGE         = 7,

                // TODO: find correct values if possible and needed (also wrong order)
                BATTLEPETRESULT_DUPLICATE_CONVERTED_PET,
                BATTLEPETRESULT_NEED_TO_UNLOCK,
                BATTLEPETRESULT_BAD_PARAM,
                BATTLEPETRESULT_LOCKED_PET_ALREADY_EXISTS,
                BATTLEPETRESULT_OK,
                BATTLEPETRESULT_UNCAPTURABLE,
                BATTLEPETRESULT_CANT_INVALID_CHARACTER_GUID
            };

            enum SpeciesFlags
            {
                SPECIES_FLAG_NONE                = 0x0000,
                SPECIES_FLAG_LIMITED_ABILITIES   = 0x0001, // battle pets with less than 6 abilites have this flag
                SPECIES_FLAG_NOT_TRADABLE        = 0x0002,
                SPECIES_FLAG_NOT_ACCOUNT_BOUND   = 0x0004,
                SPECIES_FLAG_RELEASABLE          = 0x0008,
                SPECIES_FLAG_NOT_CAGEABLE        = 0x0010,
                SPECIES_FLAG_NOT_TAMEABLE        = 0x0020,
                SPECIES_FLAG_UNIQUE              = 0x0040,
                SPECIES_FLAG_COMPANION           = 0x0080,
                SPECIES_FLAG_UNKNOWN_2           = 0x0100,
                SPECIES_FLAG_UNKNOWN_3           = 0x0200,
                SPECIES_FLAG_ELITE               = 0x0400,
                SPECIES_FLAG_UNKNOWN_4           = 0x0800, // battle pets 89, 169 and 218, these also have not tradable flag but don't display message in game
                SPECIES_FLAG_UNKNOWN_5           = 0x1000  // only seen on battle pet 316 (Cenarion Hatchling)
            };

            enum RequestFailReasons
            {
                FAIL_SUCCESS                         = -1,
                FAIL_FAILED                          = 0,
                FAIL_NOT_HERE                        = 1, // NYI
                FAIL_NOT_ON_TRANSPORT                = 2,
                FAIL_GROUND_TOO_UNEVEN               = 3, // NYI
                FAIL_AREA_OBSTRUCTED                 = 4, // NYI
                FAIL_IN_COMBAT                       = 5,
                FAIL_DEAD                            = 6,
                FAIL_MUST_BE_STANDING                = 7, // NYI
                FAIL_PLAYER_INVALID_TARGET           = 8,
                FAIL_OUT_OF_RANGE                    = 9,
                FAIL_CREATURE_INVALID_TARGET         = 10,
                FAIL_MUST_BE_BATTLE_PET_TRAINER      = 11, // NYI
                FAIL_INVITATION_DECLINED             = 12,
                FAIL_ALREADY_IN_PROGRESS             = 13,
                FAIL_MUST_EQUIP_PET_IN_SLOT          = 14,
                FAIL_ALL_EQUIPPED_PETS_DEAD          = 15,
                FAIL_MUST_HAVE_PET_IN_SLOT           = 16,
                FAIL_PET_JOURNAL_LOCKED              = 17, // NYI
                FAIL_PET_BATTLED_BY_ANOTHER_PLAYER   = 18, // NYI
            };

            enum EffectTargetType
            {
                EFFECT_TARGET_TYPE_NONE             = 0,
                EFFECT_TARGET_TYPE_AURA             = 1,
                EFFECT_TARGET_TYPE_STATE            = 2,
                EFFECT_TARGET_TYPE_HEALTH           = 3,
                EFFECT_TARGET_TYPE_NEW_STAT_VALUE   = 4,
                EFFECT_TARGET_TYPE_TRIGGER_ABILITY  = 5,
                EFFECT_TARGET_TYPE_CHANGED_ABILITY  = 6,
                EFFECT_TARGET_TYPE_BROADCAST_TEXT   = 7,
            };

            enum InputMoveType
            {
                INPUT_MOVE_TYPE_FORFEIT          = 0,
                INPUT_MOVE_TYPE_CAST_ABILITY     = 1,
                INPUT_MOVE_TYPE_SWAP_PASS        = 2,
                INPUT_MOVE_TYPE_TRAP             = 3,
                INPUT_MOVE_TYPE_ANIMATIONS_DONE  = 4, // sent by client automatically when you won/pet gained level/xp/etc animations are completed
            };

            enum EffectType
            {
                EFFECT_TYPE_NONE                 = 0,
                EFFECT_TYPE_AURA_ADD             = 1,
                EFFECT_TYPE_AURA_REMOVE          = 2, // (?) causes aura to be removed
                EFFECT_TYPE_AURA_UPDATE          = 3,
                EFFECT_TYPE_SWAP_PET             = 4,
                EFFECT_TYPE_TRAPPED              = 5,
                EFFECT_TYPE_STATE                = 6,
                EFFECT_TYPE_MAX_HEALTH           = 7,
                EFFECT_TYPE_SPEED                = 8,
                EFFECT_TYPE_POWER                = 9,
                EFFECT_TYPE_UNK_13               = 13,
                EFFECT_TYPE_UNK_14               = 14,
            };

            enum EffectPropertyType
            {
                PROPERTY_TYPE_NONE                       = 0,
                PROPERTY_TYPE_HEAL                       = 37,
                PROPERTY_TYPE_DAMAGE                     = 38,
                PROPERTY_TYPE_TARGET_AS_CASTER           = 50,
            };

            enum AbilityTargetType
            {
                ABILITY_TARGET_TYPE_ENEMY               = 1,
                ABILITY_TARGET_TYPE_SELF                = 2,
                ABILITY_TARGET_TYPE_ENEMY_TEAM          = 3,
                ABILITY_TARGET_TYPE_SELF_TEAM           = 4,
            };

            enum Effects
            {
                ABILITY_EFFECT_NONE                             = 22,
                ABILITY_EFFECT_HEAL                             = 23,
                ABILITY_EFFECT_DAMAGE                           = 24,
                ABILITY_EFFECT_TRAP                             = 25, // NYI
                ABILITY_EFFECT_APPLY_AURA_SELF                  = 26,
                ABILITY_EFFECT_DAMAGE_INCREASE_PER_HIT          = 27,
                ABILITY_EFFECT_APPLY_AURA_SELF_2                = 28,
                ABILITY_EFFECT_DAMAGE_IF_STATE_ACTIVE           = 29,
                ABILITY_EFFECT_SET_STATE_VALUE                  = 31,
                ABILITY_EFFECT_HEALTH_LEECH_PCT                 = 32,
                ABILITY_EFFECT_UNK_33                           = 33, // NYI
                ABILITY_EFFECT_HEAL_LAST_DAMAGE_PCT             = 44,
                ABILITY_EFFECT_REMOVE_AURA_SELF                 = 49,
                ABILITY_EFFECT_APPLY_AURA_ENEMY                 = 50,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_CHANCE          = 52,
                ABILITY_EFFECT_HEAL_PCT                         = 53,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_3               = 54, // some are persistent area/target auras with visual objects, client sends "WARNING: Invalid auraInstanceID passed to FindObject: 0" if invalid SourceAuraInstanceID sent
                ABILITY_EFFECT_DAMAGE_IF_LOWER_HP               = 59,
                ABILITY_EFFECT_CONSUME_CORPSE                   = 61, // NYI
                ABILITY_EFFECT_DAMAGE_MAX_HEALTH_PCT            = 62,
                ABILITY_EFFECT_APPLY_AURA_SELF_TEAM             = 63, // maybe
                ABILITY_EFFECT_DAMAGE_MORE_IF_STATE_ACTIVE      = 65,
                ABILITY_EFFECT_DAMAGE_MORE_PCT_IF_BELOW_25_HP   = 66,
                ABILITY_EFFECT_HP_SWAP                          = 67, // NYI
                ABILITY_EFFECT_DAMAGE_MAX_HEALTH_PCT_CASTER     = 68,
                ABILITY_EFFECT_APPLY_TEAM_AURA_OR_DEAL_DAMAGE   = 75, // first use applies aura, second use deals damage
                ABILITY_EFFECT_APPLY_AURA_OR_DEAL_DAMAGE        = 76, // first use applies aura, second use deals damage
                ABILITY_EFFECT_MOD_STATE_VALUE                  = 79,
                ABILITY_EFFECT_SET_WEATHER                      = 80,
                ABILITY_EFFECT_DEAL_DAMAGE_AND_APPLY_AURA       = 86,
                ABILITY_EFFECT_UNK_96                           = 96, // NYI
                ABILITY_EFFECT_DAMAGE_ALWAYS_HIT_IF_STATE_ACTIVE= 96,
                ABILITY_EFFECT_UNK_99                           = 99, // NYI
                ABILITY_EFFECT_HEAL_IF_PET_FAMILY               = 100,
                ABILITY_EFFECT_DAMAGE_IF_FIRST                  = 103,
                ABILITY_EFFECT_HEAL_IF_STATE_ACTIVE             = 104,
                ABILITY_EFFECT_SWAP_PET_RANDOM                  = 107,
                ABILITY_EFFECT_RESURRECT_WITH_PCT_HEALTH        = 111,
                ABILITY_EFFECT_UNK_116                          = 116, // all abilities which should "always go first" have this effect
                ABILITY_EFFECT_SET_HEALTH_PCT                   = 128,
                ABILITY_EFFECT_LOCK_ABILITY                     = 129,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_IF_FIRST        = 131,
                ABILITY_EFFECT_KILL                             = 135,
                ABILITY_EFFECT_REMOVE_ALL_AURAS                 = 136,
                ABILITY_EFFECT_MOD_CASTER_STATE_IF_TARGET_STATE = 138,
                ABILITY_EFFECT_SWAP_PET_LOWEST_HP               = 139,
                ABILITY_EFFECT_ADD_OR_REFRESH_AURA_SELF         = 140,
                ABILITY_EFFECT_DAMAGE_ENEMY_TEAM_SPLIT          = 141,
                ABILITY_EFFECT_SWAP_PET_HIGHEST_HP              = 145,
                ABILITY_EFFECT_DAMAGE_CANT_KILL                 = 149,
                ABILITY_EFFECT_APPLY_SHARED_AURA_BOTH_TEAMS     = 150,
                ABILITY_EFFECT_STOP_IF_MISSED                   = 158, // maybe?
                ABILITY_EFFECT_DAMAGE_IF_LAST                   = 160,
                ABILITY_EFFECT_DAMAGE_CHANCE                    = 164,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_2               = 168,
                ABILITY_EFFECT_DAMAGE_2                         = 169,
                ABILITY_EFFECT_DAMAGE_IF_WEATHER                = 170,
                ABILITY_EFFECT_HEAL_IF_WEATHER                  = 171,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_CHANCE_STATE    = 172,
                ABILITY_EFFECT_APPLY_AURA_ENEMY_IF_NOT_STATE    = 178,
                ABILITY_EFFECT_DAMAGE_SHIELD                    = 179,
                ABILITY_EFFECT_STOP_IF_NOT_MISSED               = 194, // maybe?
                ABILITY_EFFECT_DAMAGE_LAST_DAMAGE_PCT_2         = 197,
                ABILITY_EFFECT_DAMAGE_LAST_DAMAGE_PCT           = 204,
                ABILITY_EFFECT_DAMAGE_3                         = 222,
            };

            enum States
            {
                STATE_DIED_THIS_ROUND               = 1,
                STATE_MOD_HEALTH                    = 2,
                STATE_MOD_ACCURACY                  = 4,    // NYI ?
                STATE_INTERNAL_INITIAL_LEVEL        = 17,
                STATE_STAT_POWER                    = 18,
                STATE_STAT_STAMINA                  = 19,
                STATE_MOD_SPEED                     = 20,
                STATE_POISONED                      = 21,
                STATE_STUNNED                       = 22,
                STATE_MOD_DAMAGE_DONE_PCT           = 23,
                STATE_MOD_DAMAGE_TAKEN_PCT          = 24,
                STATE_MOD_SPEED_PCT                 = 25,
                STATE_WAS_HIT_THIS_ROUND            = 28,
                STATE_UNATTACKABLE                  = 29,
                STATE_UNDERGROUND                   = 30,
                STATE_FLYING                        = 33,
                STATE_BURNING                       = 34,
                STATE_CANT_CONTROL                  = 35,
                STATE_ROOT                          = 36,
                STATE_MOD_CRIT_CHANCE_PCT           = 40,
                STATE_MOD_HIT_CHANCE_PCT            = 41,
                STATE_CHILLED                       = 52,
                // weather states
                STATE_SCORCHED_EARTH                = 53,
                STATE_ARCANE_WINDS                  = 54,
                STATE_MOONLIGHT                     = 55,
                STATE_DARKNESS                      = 56,
                STATE_SANDSTORM                     = 57,
                STATE_BLIZZARD                      = 58,
                STATE_MUDSLIDE                      = 59,
                STATE_CLEANSING_RAIN                = 60,
                STATE_SUNNY_DAY                     = 61,
                STATE_LIGHTNING_STORM               = 62,
                // end weather states
                STATE_WEBBED                        = 64,
                STATE_MOD_HEAL_DONE_PCT             = 65,
                STATE_MOD_HEAL_TAKEN_PCT            = 66,
                STATE_INVISIBLE                     = 67,
                STATE_UNKILLABLE                    = 68,
                STATE_TEAM_AURA                     = 69, // ?
                STATE_MOD_DAMAGE_TAKEN              = 71,
                STATE_MOD_DODGE_PCT                 = 73,
                STATE_BLOCK_ATTACKS                 = 74,
                STATE_BLEEDING                      = 77,
                STATE_GENDER                        = 78,   // 1 - male, 2 - female
                STATE_BLINDED                       = 82,
                STATE_COSMETIC_WATER_BUBBLED        = 85,
                STATE_MOD_SCHOOL_DAMAGE_DONE_PCT    = 87,
                STATE_UNK_91                        = 91,
                STATE_SPECIAL_IS_COCKROACH          = 93,
                STATE_BANISHED                      = 98,   // NYI
                STATE_MOD_HEALTH_PCT                = 99,
                STATE_SHARED_AURA                   = 126,  // ?
                STATE_COSMETIC_FLY_TIER             = 128,
                STATE_COSMETIC_BIGGLESWORTH         = 144,
                STATE_IMMUNE_STUN_SLEEP_POLYMORPH   = 149,
                STATE_PASSIVE_ELITE                 = 153,
                STATE_PASSIVE_BOSS                  = 162,
                STATE_COSMETIC_TREASURE_GOBLIN      = 176,
                STATE_COSMETIC_SPECTRAL_BLUE        = 196,
                // these are not in BattlePetState.db2 but are used in BattlePetSpeciesState.db2
                STATE_START_WITH_BUFF               = 183,  // NYI
                STATE_START_WITH_BUFF_2             = 184,  // NYI
            };

            enum ProcType
            {
                PROC_ON_TRIGGERED           = -2,
                PROC_ON_DIRECT_CAST         = -1,

                PROC_ON_DAMAGE_TAKEN        = 1,
                PROC_ON_DAMAGE_DEALT        = 2,
                PROC_ON_HEAL_TAKEN          = 3,
                PROC_ON_HEAL_DEALT          = 4,
                PROC_ON_AURA_REMOVED        = 5,
                PROC_ON_ROUND_START         = 6,
                PROC_ON_ROUND_END           = 7,
                PROC_ON_TURN                = 8,  // NYI
                PROC_ON_ABILITY             = 9,
                PROC_ON_SWAP_IN             = 10,
                PROC_ON_SWAP_OUT            = 11,
            };

            enum EffectFlags
            {
                EFFECT_FLAG_MISS           = 0x0002,
                EFFECT_FLAG_CRITICAL       = 0x0004,
                EFFECT_FLAG_BLOCK          = 0x0008,
                EFFECT_FLAG_DODGE          = 0x0010,
                EFFECT_FLAG_NEGATIVE       = 0x0020, // shows amount negative in pet combat log
                EFFECT_FLAG_REFLECT        = 0x0080,
                EFFECT_FLAG_BLANK_LOG      = 0x0100, // shows a blank log in combat log
                EFFECT_FLAG_IMMUNE         = 0x0200,
                EFFECT_FLAG_STRONG         = 0x0400,
                EFFECT_FLAG_WEAK           = 0x0800,
            };

            enum AbilitySchool
            {
                ABILITY_SCHOOL_AURA         = -1,
                ABILITY_SCHOOL_HUMANOID     = 0,
                ABILITY_SCHOOL_DRAGONKIN    = 1,
                ABILITY_SCHOOL_FLYING       = 2,
                ABILITY_SCHOOL_UNDEAD       = 3,
                ABILITY_SCHOOL_CRITTER      = 4,
                ABILITY_SCHOOL_MAGIC        = 5,
                ABILITY_SCHOOL_ELEMENTAL    = 6,
                ABILITY_SCHOOL_BEAST        = 7,
                ABILITY_SCHOOL_AQUATIC      = 8,
                ABILITY_SCHOOL_MECHANICAL   = 9,
                MAX_ABILITY_SCHOOL          = 10,
            };

            enum PassiveAbilities
            {
                ABILITY_PASSIVE_HUMANOID     = 726,
                ABILITY_PASSIVE_DRAGONKIN    = 245,
                ABILITY_PASSIVE_FLYING       = 239,
                ABILITY_PASSIVE_UNDEAD       = 724,
                ABILITY_PASSIVE_CRITTER      = 236,
                ABILITY_PASSIVE_MAGIC        = 243,
                ABILITY_PASSIVE_ELEMENTAL    = 241, // NYI
                ABILITY_PASSIVE_BEAST        = 237,
                ABILITY_PASSIVE_AQUATIC      = 240,
                ABILITY_PASSIVE_MECHANICAL   = 723,
            };

            enum TrapStatus
            {
                TRAP_STATUS_NONE                 = 0,
                TRAP_STATUS_CAN_TRAP             = 1,
                TRAP_STATUS_CAN_TRAP_AT_LEVEL_3  = 2, // Can only trap when one of your pets reaches level 3
                TRAP_STATUS_CANT_TRAP_DEAD       = 3, // Can't trap dead pets
                TRAP_STATUS_ENEMY_HP_TOO_HIGH    = 4, // Enemy pet not low enough
                TRAP_STATUS_JOURNAL_FULL         = 5, // You already have 3 of this pet or your journal is full
                TRAP_STATUS_NOT_CAPTURABLE       = 6, // Enemy pet is not capturable
                TRAP_STATUS_CANT_TRAP_NPC_PETS   = 7, // You can't trap NPC pet
                TRAP_STATUS_NOT_TWICE            = 8, // Can't trap twice in same battle
            };

            enum InputFlags
            {
                // these are probably flags but can't figure out how they are combined
                INPUT_DISABLE_CAST_AND_PASS  = 1,
                INPUT_DISABLE_CAST           = 2,
                INPUT_DISABLE_SWAP           = 4,
                INPUT_DISABLE_CAST_AND_SWAP  = 6,
                INPUT_DISABLE_ALL            = 7,
            };

            enum BattleState
            {
                STATE_NORMAL                             = 2,
                STATE_MUST_SWAP_PET                      = 3,
                STATE_MUST_SWAP_PET_NO_ROUND_TIMER_BAR   = 5,
                STATE_MUST_SWAP_PET_NO_TIMER_COUNTER     = 7,
            };

            enum Target
            {
                TARGET_PET_NONE  = -1,
                TARGET_PET_0     = 0,
                TARGET_PET_1     = 1,
                TARGET_PET_2     = 2,
                TARGET_PET_3     = 3,
                TARGET_PET_4     = 4,
                TARGET_PET_5     = 5,
                TARGET_TEAM_0    = 6,
                TARGET_TEAM_1    = 7,
                TARGET_WEATHER   = 8,
            };
        }
    }
}

#endif
