#ifndef PROJECT_BATTLE_PET_MANAGER_PLACEHOLDER_H
#define PROJECT_BATTLE_PET_MANAGER_PLACEHOLDER_H

#ifdef PROJECT_CUSTOM
#include "project_battle_pet_manager.h"
#else

namespace PROJECT
{
    namespace BattlePets
    {
        class Manager
        {
            public:
                Manager(Player* owner) {};
                ~Manager() {};
                void JustSummoned(TempSummon* summon) {};
        };
    }
}

#endif
#endif
