#ifndef PROJECT_BATTLE_PET_BATTLES_PLACEHOLDER_H
#define PROJECT_BATTLE_PET_BATTLES_PLACEHOLDER_H

#include "project_battle_pet_placeholder.h"

#ifdef PROJECT_CUSTOM
#include "project_battle_pet_battle.h"
#else

namespace PROJECT
{
    namespace Packets { namespace PetBattles { struct Locations; } }
    namespace BattlePets
    {
        namespace Battles
        {
        
            class EffectTarget
            {
                public:
                    Constants::EffectTargetType Type;
                    int8 Petx = -1;
                    uint32 AuraInstanceID = 0;
                    uint32 AuraAbilityID = 0;
                    int32 RoundsRemaining = 0;
                    int32 CurrentRound = 0;
                    uint32 StateID = 0;
                    int32 StateValue = 0;
                    int32 Health = 0;
                    int32 NewStatValue = 0;
                    int32 TriggerAbilityID = 0;
                    int32 ChangedAbilityID = 0;
                    int32 CooldownRemaining = 0;
                    int32 LockdownRemaining = 0;
                    int32 BroadcastTextID = 0;
            };

            class BattleEffect
            {
                public:
                    uint32 AbilityEffectID = 0;
                    uint16 Flags = 0;
                    uint16 SourceAuraInstanceID = 0;
                    uint16 TurnInstanceID = 0;
                    Constants::EffectType PetBattleEffectType = Constants::EffectType::EFFECT_TYPE_NONE;
                    int8 CasterPBOID = -1;
                    uint8 StackDepth = 0;
                    std::vector<EffectTarget> Targets;
            };

            class ActiveAbility
            {
                public:
                    int32 AbilityID = 0;
                    int16 CooldownRemaining = 0;
                    int16 LockdownRemaining = 0;
                    int8 AbilityIndex = 0;
                    uint8 Pboid = 0;
            };

            struct ActiveAura
            {
                uint32 InstanceID;
                uint32 AbilityID;
                int32 RoundsRemaining;
                uint32 CurrentRound;
                uint8 CasterPBOID;
            };

            struct ActiveState
            {
                uint32 StateID;
                int32 StateValue;

                // internal
                uint8 target;

                ActiveState(uint8 _target, uint32 _StateId, int32 _StateValue) : target(_target), StateID(_StateId), StateValue(_StateValue) { }
            };

            class RoundData
            {
                public:

                    int32 CurRound;
                    int8 NextPetBattleState;
                    std::vector<BattleEffect> Effects;
                    std::vector<int8> PetXDied;
                    std::vector<ActiveAbility> Cooldowns;
                    uint8 NextInputFlags[Constants::TOTAL_CHALLENGERS];
                    int8 NextTrapStatus[Constants::TOTAL_CHALLENGERS];
                    uint16 RoundTimeSecs[Constants::TOTAL_CHALLENGERS];
            };

            class Battle
            {
                public:
                    Battle(Player * player1, Unit * player2) {};
            };
        }
    }
}

#endif
#endif
