#ifndef PROJECT_VIGNETTE_MGR_PLACEHOLDER_H
#define PROJECT_VIGNETTE_MGR_PLACEHOLDER_H


#ifdef PROJECT_CUSTOM
#include "project_vignette_mgr.h"
#else

class WorldObject;
class GameObject;
class Creature;
class Player;

namespace PROJECT
{
    class VignetteMgr
    {
        public:

            /**
            * Constructor of the vignette manager
            * @param player : Owner of the manager
            */
            VignetteMgr(Player const* player) {};

            /**
            * Destructor of the vignette manager
            */
            ~VignetteMgr() {};

            template<class T>
            void OnWorldObjectAppear(T const* target) {};

            template<class T>
            void OnWorldObjectDisappear(T const* target) {};
    };
}
#endif

#endif
