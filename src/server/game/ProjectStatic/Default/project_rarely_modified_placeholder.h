#ifndef PROJECT_RARELY_MODIFIED_H
#define PROJECT_RARELY_MODIFIED_H

#include "project_db2_container.h"

namespace PROJECT
{
    namespace ZoneInstancing
    {
        enum Flags
        {
            FORCED_INSTANCE     = 0x80000000,
        };
    }

    namespace Spells
    {
        enum Attributes
        {
            AOE_SPELL                          = 0x00000001,   // mark spell as AoE, currently only for Avoidance
            FORCE_DIRECTION_BACK               = 0x00000002,   // used for SPELL_EFFECT_LEAP_BACK
            IGNORE_PVP_POWER_AND_FATIGUE       = 0x00000004,   // ignore PvP Power and Battle Fatigue
            NO_HEALING_MODIFIERS               = 0x00000008,   // no healing modifiers
            DONT_SEND_TO_CLIENT                = 0x00000010,   // don't send aura and SMSG_SPELLLOGEXECUTE to client
            ALWAYS_CRIT                        = 0x00000020,   // always critically hit
            SPELLMOD_USE_STACKS_FOR_CHARGES    = 0x00000040,   // use stacks as charges for spell mods
            DELAY_PER_TARGET                   = 0x00000080,   // delay spell effect per target, not all of them at once
            HALF_CRIT_DAMAGE_BONUS             = 0x00000100,   // half critical strike damage bonus
            IGNORE_ABSORB_RESIST               = 0x00000200,   // ignore CalcAbsorbResist()
            IGNORE_RESILIENCE                  = 0x00000400,   // ignore resilience
            SEND_SPELL_START_EVEN_IF_TRIGGERED = 0x00000800,   // send SMSG_SPELL_START even if spell is cast triggered
            SEND_TO_CLIENT                     = 0x00001000,   // always return true in Spell::IsNeedSendToClient()
            DONT_ENABLE_PVP                    = 0x00002000,   // don't enable PvP on flag
            FORCE_COOLDOWN_EVEN_IF_TRIGGERED   = 0x00004000,   // removes TRIGGERED_IGNORE_SPELL_AND_CATEGORY_CD from spell->_triggeredCastFlags
            FORCE_SPELL_CD_IGNORE_ITEM_CD      = 0x00008000,   // ignore item cooldown and take spell cooldown in consideration
            FORCE_OWNER_SPELLPOWER_SCALING     = 0x00010000,   // force scaling with owner's spellpower (usually for pet spells)
            FORCE_REFLECTABLE                  = 0x00020000,   // force spell to be reflectable
            DONT_LOAD_AT_LOGIN                 = 0x00040000,   // don't load aura from character_aura/pet_aura database at login/summon
            REMOVE_AT_SPEC_CHANGE_OR_RESET     = 0x00080000,   // remove aura on spec change or talents reset
            DONT_RESET_CD_ON_DUEL_END          = 0x00100000,   // don't reset cooldown on duel end
            IGNORE_DAMAGE_HACK_CHECK           = 0x00200000,   // ignores damage limit hack check
            FORCE_LOS_CHECK                    = 0x00400000,   // force LoS check even if triggered
            IGNORE_DISPEL_AURA_ON_IMMUNITY     = 0x00800000,   // skips SPELL_ATTR1_DISPEL_AURAS_ON_IMMUNITY aura removal
            DELAYED_FORCE_HIT_FRIENDLY         = 0x01000000,   // allow a delayed negative spell to hit a friendly unit (prevented in DoSpellHitOnUnit)
        };

        // effect attributes
        namespace Effects
        {
            enum Attributes
            {
                DONT_STACK                   = 0x00000040,   // aura will not change amount depending on stacks
            };
        }
    }

    namespace Players
    {
        enum PlayerAvgItemLevelOffsets
        {
            PLAYER_AVG_ITEM_LEVEL_BAG           = 0,
            PLAYER_AVG_ITEM_LEVEL_EQUIPPED      = 1,
            PLAYER_AVG_ITEM_LEVEL_UNK           = 2,
            PLAYER_AVG_ITEM_LEVEL_PVP           = 3,
        };

        enum HonorDefines
        {
            PVP_TALENT_LEVEL = 110,
        };
    }

    namespace Quests
    {
        enum QuestObjectiveFlags
        {
            OBJECTIVE_FLAG_PROGRESS_BAR         = 0x40,
        };
    }

    namespace DataStores
    {
        namespace Flags
        {
            enum FlagsSummonProperties
            {
                SUMMON_PROPERTIES_PERSONAL_CREATURE         = 0x00000010,
                SUMMON_PROPERTIES_GUARDIAN                  = 0x00000200,
            };
        }
    }

    namespace Units
    {
        namespace Flags
        {
            // Value masks for UNIT_FIELD_FLAGS_3
            enum UnitFlags3
            {
                UNIT_FLAG3_ALLOW_CASTING_WHILE_MOUNTED      = 0x00000004,
            };
        }
    }
}

#endif
