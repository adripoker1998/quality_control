#ifndef PROJECT_SCENARIO_PLACEHOLDER_H
#define PROJECT_SCENARIO_PLACEHOLDER_H

#include "project_db2.h"
#include "LFGMgr.h"
#include "Group.h"

#ifdef PROJECT_CUSTOM
#include "project_scenario.h"
#else

class Scenario;

namespace PROJECT
{
    namespace Scenarios
    {
        enum Defines
        {
            SCENARIO_MAX_GROUP_SIZE             = 3,
            SCENARIO_GROUP_KICK_VOTES_NEEDED    = 2,
        };
        
        inline void Completed(Scenario* scenario) {};
        inline void OnPlayerEnter(Scenario* scenario, Player* player) {};
        inline uint32 GetMaxGroupSize() { return MAX_GROUP_SIZE; };
    }
}

#endif
#endif
