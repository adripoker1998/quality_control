#ifndef PROJECT_GUILD_PLACEHOLDER_H
#define PROJECT_GUILD_PLACEHOLDER_H

#include "project.h"
#include "Guild.h"

#ifdef PROJECT_CUSTOM
#include "project_guild.h"
#else

namespace PROJECT
{
    namespace Guilds
    {
        namespace Constants
        {
            enum ConstantsEnum
            {
                REPUTATION_XP_MULTIPLIER = 470,
            };
        }

        inline void RenameGuild(Guild* guild, bool isRenamable) {};
    }
}

#endif
#endif