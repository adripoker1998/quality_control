#ifndef PROJECT_ACHIEVEMENT_PLACEHOLDER_H
#define PROJECT_ACHIEVEMENT_PLACEHOLDER_H

#include "project.h"
#include "CriteriaHandler.h"

#ifdef PROJECT_CUSTOM
#include "project_achievement.h"
#else

namespace PROJECT
{
    namespace Hooks
    {
        namespace Achievements
        {
            inline void CompletedAchievement(Player * player, AchievementEntry const* achievement, bool isGuild) {};
            inline void UpdateCriteria(CriteriaHandler * criteriaHandler, Criteria const* criteria, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) {};
            inline bool IsCompletedCriteria(CriteriaHandler * criteriaHandler, Criteria const* criteria, CriteriaProgress const* progress, uint64 requiredAmount) { return false; };
            inline bool AdditionalRequirementsSatisfied(CriteriaHandler * criteriaHandler, Criteria const* criteria, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer, ModifierTreeEntry const* tree) { return false; };
            inline bool RequirementsSatisfied(CriteriaHandler * criteriaHandler, Criteria const* criteria, uint64 miscValue1, uint64 miscValue2, uint64 miscValue3, Unit const* unit, Player* referencePlayer) { return false; };
        }
    }
}

#endif
#endif
