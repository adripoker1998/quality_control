#ifndef PROJECT_BATTLE_PAY_PLACEHOLDER_H
#define PROJECT_BATTLE_PAY_PLACEHOLDER_H

#include "project.h"
#include "project_language.h"

#ifdef PROJECT_CUSTOM
#include "project_battle_pay.h"
#else

namespace PROJECT
{
    namespace BattlePay
    {
        struct Purchase
        {
            uint64 PurchaseID;
            uint32 Status;
            uint32 ResultCode = 0;
            uint32 ProductId;
            std::string WalletName;
        };

        struct UnkBfaProduct
        {
            uint32 Unk1;
            uint32 Unk2;
            std::string Unk3;
        };

        struct DisplayInfoStruct
        {
            bool HasDisplayInfo() { return false; }

            uint32 CreatureDisplayInfoID;
            uint32 ItemDisplayInfoID;
            uint32 FileDataID;
            uint32 Flags;
            uint32 Flags2;
            uint32 Flags3;
            uint32 Flags4;
            std::string Name1;
            std::string Description; // Name2
            std::string Name3;
            std::string Description2;
            std::vector<UnkBfaProduct> UnkBfa;
        };

        struct Product
        {
            bool HasProduct() { return false; }

            uint32 ID;
            uint32 NormalPrice;
            uint32 CurrentPrice;
            uint32 ItemID;
            uint32 Quantity;
            uint8 Type = 0;
            uint32 Flags;
            uint8 PetResult = 0;
            DisplayInfoStruct DisplayInfo;
        };


        struct Group
        {
            uint32 GroupID;
            int32 Ordering;
            int32 UnkBfa = 0;
            std::string Name;
            std::string Description = "";
            uint32 IconFileDataID;
            uint8 DisplayType;
        };

        struct ShopEntry
        {
            uint32 EntryID;
            int32 Ordering;
            uint32 GroupID;
            uint32 ProductID;
            uint32 Flags;
            uint8 BannerType;
            DisplayInfoStruct DisplayInfo;
        };

        class Manager
        {
            public:
                static Manager* instance()
                {
                    static Manager instance;
                    return &instance;
                };

                Manager() {};

                void LoadConfig() {};
        };

        #define PROJECT_BattlePayManager PROJECT::BattlePay::Manager::instance()
    }
}
#endif
#endif
