#ifndef PROJECT_PREMADE_PLACEHOLDER_H
#define PROJECT_PREMADE_PLACEHOLDER_H

#include "project.h"
#include "project_packet_Premade.h"

#ifdef PROJECT_CUSTOM
#include "project_premade.h"
#else

namespace PROJECT
{
    class PremadeMgr
    {
    public:

        PremadeMgr() {};
        static PremadeMgr* instance()
        {
            static PremadeMgr instance;
            return &instance;
        };

        inline bool Remove(ObjectGuid guid, Player* player = nullptr, bool disband = true) { return true; };
    };
}
#define sPremadeMgr PROJECT::PremadeMgr::instance()

#endif
#endif
