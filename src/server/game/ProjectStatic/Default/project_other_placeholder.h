#ifndef PROJECT_OTHER_PLACEHOLDER_H
#define PROJECT_OTHER_PLACEHOLDER_H

#include "project.h"

#ifdef PROJECT_CUSTOM
#include "project_other.h"
#else

namespace PROJECT
{
    namespace Loaders
    {
        inline void SmallDBCOverrides() {};
        inline void AchievementsCorrections() {};
        inline void StaticScripts() {};
        inline void SpellDBCoverrides(uint32 onlyForSpell = 0) {};
        inline void SpellTriggers() {};
    }

    namespace Armory
    {
        enum Actions
        {
            ITEM_LOOTED_BONUS  = 1,    // looted @ bonus roll
            ITEM_LOOTED        = 2,    // all item sources if not otherwise specified
            BOSS_KILLED        = 3,    // boss killed
            ITEM_DESTROYED     = 4,    // item deleted
            ITEM_REFUNDED      = 5,    // item refunded @ vendor
            ITEM_DISENCHANTED  = 6,    // item disenchanted
            ITEM_TRADED_BOP    = 7,    // traded BoP item
            ITEM_VOID_DEPOSIT  = 8,    // void storage deposit
            ITEM_VOID_WITHDRAW = 9,    // void storage withdraw
            ITEM_GOT_FROM_MAIL = 10,   // item received from mail
            ITEM_BOUGHT        = 11,   // item bought from vendor
            ITEM_RECEIVED_BOP  = 12,   // received BoP item @ trade
            ITEM_RECEIVED_GM   = 13,   // received from gamemaster
            ITEM_LOOTED_GO     = 14,   // item looted from gameobject
            ITEM_LOOTED_ITEM   = 15,   // item looted from item
        };

        TC_GAME_API inline void WriteDatabaseLog(Player * player, Actions type, void * entityPointer, uint32 data2 = 0) {};
    }

    namespace Misc
    {
        inline void HandleRightAfterServerStartActions() {};
        inline void HandleRightBeforeServerStartActions() {};
    }
}
#endif
#endif
