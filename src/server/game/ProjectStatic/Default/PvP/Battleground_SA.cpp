#include "Battleground_SA.h"
#include "Language.h"
#include "UpdateData.h"
#include "Random.h"
#include "WorldStatePackets.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Log.h"
#include "Creature.h"
#include "GameObject.h"

Battleground_SA::Battleground_SA()
{
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_BG_SA_START_TWO_MINUTES;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_BG_SA_START_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_BG_SA_START_HALF_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_BG_SA_HAS_BEGUN;
    BgObjects.resize(BG_SA_PROJECT_MAXOBJ);
    BgCreatures.resize(BG_SA_PROJECT_MAXNPC + BG_SA_PROJECT_MAX_GY);
    TimerEnabled = false;
    UpdateWaitTimer = 0;
    SignaledRoundTwo = false;
    SignaledRoundTwoHalfMin = false;
    InitSecondRound = false;

    //! This is here to prevent an uninitialised variable warning
    //! The warning only occurs when SetUpBattleGround fails though.
    //! In the future this function should be called BEFORE sending initial worldstates.
    memset(&GraveyardStatus, 0, sizeof(GraveyardStatus));
}

Battleground_SA::~Battleground_SA() { }

void Battleground_SA::Reset()
{
    TotalTime = 0;
    timerTimeUpdate = 0;
    Attackers = ((urand(0, 1)) ? TEAM_ALLIANCE : TEAM_HORDE);
    for (uint8 i = 0; i <= 5; i++)
        GateStatus[i] = BG_SA_PROJECT_GATE_OK;
    ShipsStarted = false;
    gateDestroyed = false;
    _allVehiclesAlive[TEAM_ALLIANCE] = true;
    _allVehiclesAlive[TEAM_HORDE] = true;
    Status = BG_SA_PROJECT_WARMUP;
}

bool Battleground_SA::SetupBattleground()
{
    return ResetObjs();
}

bool Battleground_SA::ResetObjs()
{
    gateDestroyed = false;

    for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            SendTransportsRemove(player);

    uint32 atF = BG_SA_PROJECT_Factions[Attackers];
    uint32 defF = BG_SA_PROJECT_Factions[Attackers ? TEAM_ALLIANCE : TEAM_HORDE];

    for (uint8 i = 0; i < BG_SA_PROJECT_MAXOBJ; i++)
        DelObject(i);

    for (uint8 i = 0; i < BG_SA_PROJECT_MAXNPC; i++)
        DelCreature(i);

    for (uint8 i = BG_SA_PROJECT_MAXNPC; i < BG_SA_PROJECT_MAXNPC + BG_SA_PROJECT_MAX_GY; i++)
        DelCreature(i);

    for (uint8 i = 0; i < 6; i++)
        GateStatus[i] = BG_SA_PROJECT_GATE_OK;

    for (uint8 i = 0; i <= BG_SA_PROJECT_PORTAL_DEFFENDER_RED; i++)
    {
        if (!AddObject(i, BG_SA_PROJECT_ObjEntries[i], BG_SA_PROJECT_ObjSpawnlocs[i], 0, 0, 0, 0, RESPAWN_ONE_DAY))
            return false;
    }

    for (uint8 i = BG_SA_PROJECT_BOAT_ONE; i <= BG_SA_PROJECT_BOAT_TWO; i++)
    {
        uint32 boatid = 0;
        switch (i)
        {
            case BG_SA_PROJECT_BOAT_ONE:
                boatid = Attackers ? BG_SA_PROJECT_BOAT_ONE_H : BG_SA_PROJECT_BOAT_ONE_A;
                break;
            case BG_SA_PROJECT_BOAT_TWO:
                boatid = Attackers ? BG_SA_PROJECT_BOAT_TWO_H : BG_SA_PROJECT_BOAT_TWO_A;
                break;
            default:
                break;
        }
        if (!AddObject(i, boatid, BG_SA_PROJECT_ObjSpawnlocs[i].GetPositionX(),
          BG_SA_PROJECT_ObjSpawnlocs[i].GetPositionY(),
          BG_SA_PROJECT_ObjSpawnlocs[i].GetPositionZ() + (Attackers ? -3.750f: 0),
          BG_SA_PROJECT_ObjSpawnlocs[i].GetOrientation(), 0, 0, 0, 0, RESPAWN_ONE_DAY))
            return false;
    }

    for (uint8 i = BG_SA_PROJECT_SIGIL_1; i <= BG_SA_PROJECT_LEFT_FLAGPOLE; i++)
    {
        if (!AddObject(i, BG_SA_PROJECT_ObjEntries[i],
            BG_SA_PROJECT_ObjSpawnlocs[i], 0, 0, 0, 0, RESPAWN_ONE_DAY))
        return false;
    }

    // MAD props for Kiper for discovering those values - 4 hours of his work.
    //GetBGObject(BG_SA_PROJECT_BOAT_ONE)->UpdateRotationFields(1.0f, 0.0002f);
    //GetBGObject(BG_SA_PROJECT_BOAT_TWO)->UpdateRotationFields(1.0f, 0.00001f);
    SpawnBGObject(BG_SA_PROJECT_BOAT_ONE, RESPAWN_IMMEDIATELY);
    SpawnBGObject(BG_SA_PROJECT_BOAT_TWO, RESPAWN_IMMEDIATELY);

    //Cannons and demolishers - NPCs are spawned
    //By capturing GYs.
    for (uint8 i = 0; i < BG_SA_PROJECT_DEMOLISHER_5; i++)
    {
        if (!AddCreature(BG_SA_PROJECT_NpcEntries[i], i, BG_SA_PROJECT_NpcSpawnlocs[i], Attackers, 600))
        return false;
    }

    OverrideGunFaction();
    DemolisherStartState(true);

    for (uint8 i = 0; i <= BG_SA_PROJECT_PORTAL_DEFFENDER_RED; i++)
    {
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
        GetBGObject(i)->SetUInt32Value(GAMEOBJECT_FACTION, defF);
    }

    GetBGObject(BG_SA_PROJECT_TITAN_RELIC)->SetUInt32Value(GAMEOBJECT_FACTION, atF);
    GetBGObject(BG_SA_PROJECT_TITAN_RELIC)->Refresh();

    for (uint8 i = 0; i <= 5; i++)
        GateStatus[i] = BG_SA_PROJECT_GATE_OK;

    TotalTime = 0;
    ShipsStarted = false;

    //Graveyards
    for (uint8 i = 0; i < BG_SA_PROJECT_MAX_GY; i++)
    {
        WorldSafeLocsEntry const* sg = NULL;
        sg = sWorldSafeLocsStore.LookupEntry(BG_SA_PROJECT_GYEntries[i]);

        if (!sg)
        {
            TC_LOG_ERROR("bg.battleground", "SOTA: Can't find GY entry %u", BG_SA_PROJECT_GYEntries[i]);
            return false;
        }

        if (i == BG_SA_PROJECT_BEACH_GY)
        {
            GraveyardStatus[i] = Attackers;
            AddSpiritGuide(i + BG_SA_PROJECT_MAXNPC, sg->Loc.X, sg->Loc.Y, sg->Loc.Z, BG_SA_PROJECT_GYOrientation[i], Attackers);
        }
        else
        {
            GraveyardStatus[i] = Attackers;
            if (!AddSpiritGuide(i + BG_SA_PROJECT_MAXNPC, sg->Loc.X, sg->Loc.Y, sg->Loc.Z, BG_SA_PROJECT_GYOrientation[i], Attackers))
                TC_LOG_ERROR("bg.battleground", "SOTA: couldn't spawn GY: %u", i);
        }
    }

    //GY capture points
    for (uint8 i = BG_SA_PROJECT_CENTRAL_FLAG; i <= BG_SA_PROJECT_LEFT_FLAG; i++)
    {
        AddObject(i, (BG_SA_PROJECT_ObjEntries[i] - (Attackers == TEAM_ALLIANCE ? 1 : 0)),
            BG_SA_PROJECT_ObjSpawnlocs[i], 0, 0, 0, 0, RESPAWN_ONE_DAY);
        GetBGObject(i)->SetUInt32Value(GAMEOBJECT_FACTION, atF);
    }

    for (uint8 i = BG_SA_PROJECT_BOMB; i < BG_SA_PROJECT_MAXOBJ; i++)
    {
        AddObject(i, BG_SA_PROJECT_ObjEntries[BG_SA_PROJECT_BOMB],
            BG_SA_PROJECT_ObjSpawnlocs[i], 0, 0, 0, 0, RESPAWN_ONE_DAY);
        GetBGObject(i)->SetUInt32Value(GAMEOBJECT_FACTION, atF);
    }

    //Player may enter BEFORE we set up BG - lets update his worldstates anyway...
    UpdateWorldState(BG_SA_PROJECT_RIGHT_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_RIGHT_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);
    UpdateWorldState(BG_SA_PROJECT_LEFT_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_LEFT_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);
    UpdateWorldState(BG_SA_PROJECT_CENTER_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);

    UpdateWorldState(BG_SA_PROJECT_RIGHT_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_RIGHT_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);
    UpdateWorldState(BG_SA_PROJECT_LEFT_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_LEFT_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);
    UpdateWorldState(BG_SA_PROJECT_CENTER_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);

    if (Attackers == TEAM_ALLIANCE)
    {
        UpdateWorldState(BG_SA_PROJECT_ALLY_ATTACKS, 1);
        UpdateWorldState(BG_SA_PROJECT_HORDE_ATTACKS, 0);

        UpdateWorldState(BG_SA_PROJECT_RIGHT_ATT_TOKEN_ALL, 1);
        UpdateWorldState(BG_SA_PROJECT_LEFT_ATT_TOKEN_ALL, 1);
        UpdateWorldState(BG_SA_PROJECT_RIGHT_ATT_TOKEN_HRD, 0);
        UpdateWorldState(BG_SA_PROJECT_LEFT_ATT_TOKEN_HRD, 0);

        UpdateWorldState(BG_SA_PROJECT_HORDE_DEFENCE_TOKEN, 1);
        UpdateWorldState(BG_SA_PROJECT_ALLIANCE_DEFENCE_TOKEN, 0);
    }
    else
    {
        UpdateWorldState(BG_SA_PROJECT_HORDE_ATTACKS, 1);
        UpdateWorldState(BG_SA_PROJECT_ALLY_ATTACKS, 0);

        UpdateWorldState(BG_SA_PROJECT_RIGHT_ATT_TOKEN_ALL, 0);
        UpdateWorldState(BG_SA_PROJECT_LEFT_ATT_TOKEN_ALL, 0);
        UpdateWorldState(BG_SA_PROJECT_RIGHT_ATT_TOKEN_HRD, 1);
        UpdateWorldState(BG_SA_PROJECT_LEFT_ATT_TOKEN_HRD, 1);

        UpdateWorldState(BG_SA_PROJECT_HORDE_DEFENCE_TOKEN, 0);
        UpdateWorldState(BG_SA_PROJECT_ALLIANCE_DEFENCE_TOKEN, 1);
    }

    UpdateWorldState(BG_SA_PROJECT_PURPLE_GATEWS, 1);
    UpdateWorldState(BG_SA_PROJECT_RED_GATEWS, 1);
    UpdateWorldState(BG_SA_PROJECT_BLUE_GATEWS, 1);
    UpdateWorldState(BG_SA_PROJECT_GREEN_GATEWS, 1);
    UpdateWorldState(BG_SA_PROJECT_YELLOW_GATEWS, 1);
    UpdateWorldState(BG_SA_PROJECT_ANCIENT_GATEWS, 1);

    // set status manually so preparation is cast correctly in 2nd round too
    SetStatus(STATUS_WAIT_JOIN);

    TeleportPlayers();
    return true;
}

void Battleground_SA::StartShips()
{
    if (ShipsStarted)
        return;

    // teleport attackers to beach
    for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
    {
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
        {
            // manually check teamId through GetBGTeam() because in case of crossfaction bgs the correct teamId wasn't set yet
            uint32 teamId = player->GetBGTeam() == ALLIANCE ? TEAM_ALLIANCE : TEAM_HORDE;
            if (teamId == Attackers)
            {
                if (urand(0, 1))
                    player->TeleportTo(607, 1598.73f, -105.58f, 8.87f, 4.10f, 0);
                else
                    player->TeleportTo(607, 1605.75f, 51.0833f, 7.64863f, 2.38651f, 0);
            }
        }
    }
    ShipsStarted = true;
    return;

    DoorOpen(BG_SA_PROJECT_BOAT_ONE);
    DoorOpen(BG_SA_PROJECT_BOAT_TWO);

    for (int i = BG_SA_PROJECT_BOAT_ONE; i <= BG_SA_PROJECT_BOAT_TWO; i++)
    {
        for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
        {
            if (Player* p = ObjectAccessor::FindPlayer(itr->first))
            {
                UpdateData data(p->GetMapId());
                WorldPacket pkt;
                GetBGObject(i)->BuildValuesUpdateBlockForPlayer(&data, p);
                data.BuildPacket(&pkt);
                p->SendDirectMessage(&pkt);
            }
        }
    }
    ShipsStarted = true;
}

void Battleground_SA::PostUpdateImpl(uint32 diff)
{
    if (InitSecondRound)
    {
        if (UpdateWaitTimer < diff)
        {
            if (!SignaledRoundTwo)
            {
                SignaledRoundTwo = true;
                InitSecondRound = false;
                SendMessageToAll(LANG_BG_SA_ROUND_TWO_ONE_MINUTE, CHAT_MSG_BG_SYSTEM_NEUTRAL);
            }
        }
        else
        {
            UpdateWaitTimer -= diff;
            return;
        }
    }
    TotalTime += diff;
    timerTimeUpdate += diff;

    if (Status == BG_SA_PROJECT_WARMUP )
    {
        EndRoundTimer = BG_SA_PROJECT_ROUNDLENGTH;
        if (TotalTime >= BG_SA_PROJECT_WARMUPLENGTH)
        {
            TotalTime = 0;
            ToggleTimer();
            DemolisherStartState(false);
            Status = BG_SA_PROJECT_ROUND_ONE;
            StartCriteriaTimer(CRITERIA_TIMED_TYPE_EVENT, (Attackers == TEAM_ALLIANCE) ? 23748 : 21702);
            StartShips();
            SendTime();
        }
        //if (TotalTime >= BG_SA_PROJECT_BOAT_START)
        //    StartShips();
        return;
    }
    else if (Status == BG_SA_PROJECT_SECOND_WARMUP)
    {
        if (RoundScores[0].time<BG_SA_PROJECT_ROUNDLENGTH)
            EndRoundTimer = RoundScores[0].time;
        else
            EndRoundTimer = BG_SA_PROJECT_ROUNDLENGTH;

        if (TotalTime >= 60000)
        {
            SendMessageToAll(LANG_BG_SA_HAS_BEGUN, CHAT_MSG_SYSTEM);
            TotalTime = 0;
            ToggleTimer();
            DemolisherStartState(false);
            Status = BG_SA_PROJECT_ROUND_TWO;
            StartCriteriaTimer((CriteriaTimedTypes)13, (Attackers == TEAM_ALLIANCE) ? 23748 : 21702);
            // status was set to STATUS_WAIT_JOIN manually for Preparation, set it back now
            SetStatus(STATUS_IN_PROGRESS);
            for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                if (Player* p = ObjectAccessor::FindPlayer(itr->first))
                    p->RemoveAurasDueToSpell(SPELL_ARENA_PREPARATION);
            StartShips();
            SendTime();
        }
        if (TotalTime >= 30000)
        {
            if (!SignaledRoundTwoHalfMin)
            {
                SignaledRoundTwoHalfMin = true;
                SendMessageToAll(LANG_BG_SA_ROUND_TWO_START_HALF_MINUTE, CHAT_MSG_BG_SYSTEM_NEUTRAL);
            }
        }
        //StartShips();
        return;
    }
    else if (GetStatus() == STATUS_IN_PROGRESS)
    {
        if (Status == BG_SA_PROJECT_ROUND_ONE)
        {
            if (TotalTime >= BG_SA_PROJECT_ROUNDLENGTH)
            {
                TriggerAchievementDefenseOfTheAncients();

                RoundScores[0].winner = Attackers;
                RoundScores[0].time = BG_SA_PROJECT_ROUNDLENGTH;
                TotalTime = 0;
                Status = BG_SA_PROJECT_SECOND_WARMUP;
                Attackers = (Attackers == TEAM_ALLIANCE) ? TEAM_HORDE : TEAM_ALLIANCE;
                UpdateWaitTimer = 5000;
                SignaledRoundTwo = false;
                SignaledRoundTwoHalfMin = false;
                InitSecondRound = true;
                ToggleTimer();
                ResetObjs();
                return;
            }
        }
        else if (Status == BG_SA_PROJECT_ROUND_TWO)
        {
            if (TotalTime >= EndRoundTimer)
            {
                TriggerAchievementDefenseOfTheAncients();

                RoundScores[1].time = BG_SA_PROJECT_ROUNDLENGTH;
                RoundScores[1].winner = (Attackers == TEAM_ALLIANCE) ? TEAM_HORDE : TEAM_ALLIANCE;
                if (RoundScores[0].time == RoundScores[1].time)
                    EndBattleground(0);
                else if (RoundScores[0].time < RoundScores[1].time)
                    EndBattleground(RoundScores[0].winner == TEAM_ALLIANCE ? ALLIANCE : HORDE);
                else
                    EndBattleground(RoundScores[1].winner == TEAM_ALLIANCE ? ALLIANCE : HORDE);
                return;
            }
        }
        if (Status == BG_SA_PROJECT_ROUND_ONE || Status == BG_SA_PROJECT_ROUND_TWO)
        {
            UpdateDemolisherSpawns();
            if (timerTimeUpdate >= BG_SA_PROJECT_TIMER_UPDATE)
            {
                timerTimeUpdate = 0;
                SendTime();
            }
        }
    }
}

void Battleground_SA::StartingEventCloseDoors() { }

void Battleground_SA::StartingEventOpenDoors() { }

void Battleground_SA::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& builder)
{
  uint32 ally_attacks = uint32(Attackers == TEAM_ALLIANCE ? 1 : 0);
  uint32 horde_attacks = uint32(Attackers == TEAM_HORDE ? 1 : 0);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_ANCIENT_GATEWS, GateStatus[BG_SA_PROJECT_ANCIENT_GATE]);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_YELLOW_GATEWS, GateStatus[BG_SA_PROJECT_YELLOW_GATE]);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_GREEN_GATEWS, GateStatus[BG_SA_PROJECT_GREEN_GATE]);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_BLUE_GATEWS, GateStatus[BG_SA_PROJECT_BLUE_GATE]);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_RED_GATEWS, GateStatus[BG_SA_PROJECT_RED_GATE]);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_PURPLE_GATEWS, GateStatus[BG_SA_PROJECT_PURPLE_GATE]);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_BONUS_TIMER, 0);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_HORDE_ATTACKS, horde_attacks);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_ALLY_ATTACKS, ally_attacks);

  //Time will be sent on first update...
  builder.Worldstates.emplace_back(BG_SA_PROJECT_ENABLE_TIMER, TimerEnabled ? 1 : 0);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_TIMER, 0);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_RIGHT_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_RIGHT_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_LEFT_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_LEFT_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_CENTER_GY_HORDE, GraveyardStatus[BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY] == TEAM_HORDE ? 1 : 0);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_RIGHT_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_RIGHT_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_LEFT_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_LEFT_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_CENTER_GY_ALLIANCE, GraveyardStatus[BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY] == TEAM_ALLIANCE ? 1 : 0);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_HORDE_DEFENCE_TOKEN, ally_attacks);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_ALLIANCE_DEFENCE_TOKEN, horde_attacks);

  builder.Worldstates.emplace_back(BG_SA_PROJECT_LEFT_ATT_TOKEN_HRD, horde_attacks);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_RIGHT_ATT_TOKEN_HRD, horde_attacks);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_RIGHT_ATT_TOKEN_ALL, ally_attacks);
  builder.Worldstates.emplace_back(BG_SA_PROJECT_LEFT_ATT_TOKEN_ALL, ally_attacks);
}

void Battleground_SA::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    //create score and add it to map, default values are set in constructor
    Battleground_SAScore* sc = new Battleground_SAScore(player->GetGUID(), player->GetBGTeam());

    // manually check teamId through GetBGTeam() because in case of crossfaction bgs the correct teamId wasn't set yet
    uint32 teamId = player->GetBGTeam() == ALLIANCE ? TEAM_ALLIANCE : TEAM_HORDE;
    if (!ShipsStarted)
    {
        if (teamId == Attackers)
        {
            player->CastSpell(player, 12438, true); // Safe Fall

            player->TeleportTo(607, 2688.936f, -830.368f, 20.0f, 2.895f, 0);
        }
        else
            player->TeleportTo(607, 1209.7f, -65.16f, 70.1f, 0.0f, 0);
    }
    else
    {
        if (teamId == Attackers)
        {
            if (urand(0, 1))
                player->TeleportTo(607, 1598.73f, -105.58f, 8.87f, 4.10f, 0);
            else
                player->TeleportTo(607, 1605.75f, 51.0833f, 7.64863f, 2.38651f, 0);
        }
        else
            player->TeleportTo(607, 1209.7f, -65.16f, 70.1f, 0.0f, 0);
    }
    timerTimeUpdate = BG_SA_PROJECT_TIMER_UPDATE - 1000; // schedule time update
    PlayerScores[player->GetGUID()] = sc;
}

void Battleground_SA::RemovePlayer(Player* /*player*/, ObjectGuid /*guid*/, uint32 /*team*/) { }

void Battleground_SA::HandleAreaTrigger(Player* /*Source*/, uint32 /*Trigger*/, bool /*entered*/)
{
    // this is wrong way to implement these things. On official it done by gameobject spell cast.
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;
}

bool Battleground_SA::UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor)
{
    if (!Battleground::UpdatePlayerScore(Source, type, value, doAddHonor))
        return false;

    return true;
}

void Battleground_SA::TeleportPlayers()
{
    for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
    {
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
        {
            // should remove spirit of redemption
            if (player->HasAuraType(SPELL_AURA_SPIRIT_OF_REDEMPTION))
                player->RemoveAurasByType(SPELL_AURA_MOD_SHAPESHIFT);

            player->RemoveAura(BG_SA_PROJECT_SPELL_SEAFORIUM_CHARGE_1);
            player->RemoveAura(BG_SA_PROJECT_SPELL_SEAFORIUM_CHARGE_2);

            if (!player->IsAlive())
            {
                player->ResurrectPlayer(1.0f);
                player->SpawnCorpseBones();
            }

            player->ResetAllPowers();
            player->CombatStopWithPets(true);

            player->CastSpell(player, SPELL_ARENA_PREPARATION, true);

            // manually check teamId through GetBGTeam() because in case of crossfaction bgs the correct teamId wasn't set yet
            uint32 teamId = player->GetBGTeam() == ALLIANCE ? TEAM_ALLIANCE : TEAM_HORDE;
            if (teamId == Attackers)
            {
                player->CastSpell(player, 12438, true); // Safe Fall

                player->TeleportTo(607, 2688.936f, -830.368f, 20.0f, 2.895f, 0);
            }
            else
                player->TeleportTo(607, 1209.7f, -65.16f, 70.1f, 0.0f, 0);
        }
    }
}

void Battleground_SA::EventPlayerDamagedGO(Player* /*player*/, GameObject* go, uint32 eventType)
{
    if (!go || !go->GetGOInfo())
        return;

    if (eventType == go->GetGOInfo()->destructibleBuilding.DamagedEvent)
    {
        uint32 i = getGateIdFromDamagedOrDestroyEventId(eventType);
        GateStatus[i] = BG_SA_PROJECT_GATE_DAMAGED;
        uint32 uws = getWorldStateFromGateId(i);
        if (uws)
            UpdateWorldState(uws, GateStatus[i]);
    }

    if (eventType == go->GetGOInfo()->destructibleBuilding.DestroyedEvent)
    {
        if (go->GetGOInfo()->destructibleBuilding.DestroyedEvent == BG_SA_PROJECT_EVENT_ANCIENT_GATE_DESTROYED)
            SendMessageToAll(LANG_BG_SA_CHAMBER_BREACHED, CHAT_MSG_SYSTEM);
        /*else
            SendMessageToAll(LANG_BG_SA_WAS_DESTROYED, go->GetGOInfo()->name.c_str());*/
    }

    /*if (eventType == go->GetGOInfo()->destructibleBuilding.DamageEvent)
        SendWarningToAll(LANG_BG_SA_IS_UNDER_ATTACK, go->GetGOInfo()->name.c_str());*/
}

void Battleground_SA::HandleKillUnit(Creature* creature, Player* killer)
{
    if (creature->GetEntry() == NPC_PROJECT_DEMOLISHER_SA)
    {
        UpdatePlayerScore(killer, SCORE_DESTROYED_DEMOLISHER, 1);
        _allVehiclesAlive[Attackers] = false;
    }
}

/*
  You may ask what the fuck does it do?
  Prevents owner overwriting guns faction with own.
 */
void Battleground_SA::OverrideGunFaction()
{
    if (!BgCreatures[0])
        return;

    for (uint8 i = BG_SA_PROJECT_GUN_1; i <= BG_SA_PROJECT_GUN_10; i++)
    {
        if (Creature* gun = GetBGCreature(i))
            gun->setFaction(BG_SA_PROJECT_Factions[Attackers ? TEAM_ALLIANCE : TEAM_HORDE]);
    }

    for (uint8 i = BG_SA_PROJECT_DEMOLISHER_1; i <= BG_SA_PROJECT_DEMOLISHER_4; i++)
    {
        if (Creature* dem = GetBGCreature(i))
            dem->setFaction(BG_SA_PROJECT_Factions[Attackers]);
    }
}

void Battleground_SA::DemolisherStartState(bool start)
{
    if (!BgCreatures[0])
        return;

    // set flags only for the demolishers on the beach, factory ones dont need it
    for (uint8 i = BG_SA_PROJECT_DEMOLISHER_1; i <= BG_SA_PROJECT_DEMOLISHER_4; i++)
    {
        if (Creature* dem = GetBGCreature(i))
        {
            if (start)
                dem->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            else
                dem->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
        }
    }
}

void Battleground_SA::DestroyGate(Player* player, GameObject* go)
{
    uint32 i = getGateIdFromDamagedOrDestroyEventId(go->GetGOInfo()->destructibleBuilding.DestroyedEvent);
    if (!GateStatus[i] || GateStatus[i] == BG_SA_PROJECT_GATE_DESTROYED)
        return;

    if (GameObject* g = GetBGObject(i))
    {
        if (g->GetGOValue()->Building.Health == 0)
        {
            GateStatus[i] = BG_SA_PROJECT_GATE_DESTROYED;
            uint32 uws = getWorldStateFromGateId(i);
            if (uws)
                UpdateWorldState(uws, GateStatus[i]);
            bool rewardHonor = true;
            gateDestroyed = true;
            switch (i)
            {
                case BG_SA_PROJECT_GREEN_GATE:
                    if (GateStatus[BG_SA_PROJECT_BLUE_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                        rewardHonor = false;
                    break;
                case BG_SA_PROJECT_BLUE_GATE:
                    if (GateStatus[BG_SA_PROJECT_GREEN_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                        rewardHonor = false;
                    break;
                case BG_SA_PROJECT_RED_GATE:
                    if (GateStatus[BG_SA_PROJECT_PURPLE_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                        rewardHonor = false;
                    break;
                case BG_SA_PROJECT_PURPLE_GATE:
                    if (GateStatus[BG_SA_PROJECT_RED_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                        rewardHonor = false;
                    break;
                default:
                    break;
            }

            if (i < 5)
                DelObject(i + 14);
            UpdatePlayerScore(player, SCORE_DESTROYED_WALL, 1);
            if (rewardHonor)
                UpdatePlayerScore(player, SCORE_BONUS_HONOR, GetBonusHonorFromKill(1));
        }
    }
}

WorldSafeLocsEntry const* Battleground_SA::GetClosestGraveYard(Player* player)
{
    uint32 safeloc = 0;
    WorldSafeLocsEntry const* ret;
    WorldSafeLocsEntry const* closest;
    float dist, nearest;
    float x, y, z;

    player->GetPosition(x, y, z);

    if (player->GetTeamId() == Attackers)
        safeloc = BG_SA_PROJECT_GYEntries[BG_SA_PROJECT_BEACH_GY];
    else
        safeloc = BG_SA_PROJECT_GYEntries[BG_SA_PROJECT_DEFENDER_LAST_GY];

    closest = sWorldSafeLocsStore.LookupEntry(safeloc);
    nearest = sqrt((closest->Loc.X - x)*(closest->Loc.X - x) + (closest->Loc.Y - y)*(closest->Loc.Y - y) + (closest->Loc.Z - z)*(closest->Loc.Z - z));

    for (uint8 i = BG_SA_PROJECT_RIGHT_CAPTURABLE_GY; i < BG_SA_PROJECT_MAX_GY; i++)
    {
        if (GraveyardStatus[i] != player->GetTeamId())
            continue;

        ret = sWorldSafeLocsStore.LookupEntry(BG_SA_PROJECT_GYEntries[i]);
        dist = sqrt((ret->Loc.X - x)*(ret->Loc.X - x) + (ret->Loc.Y - y)*(ret->Loc.Y - y) + (ret->Loc.Z - z)*(ret->Loc.Z - z));
        if (dist < nearest)
        {
            closest = ret;
            nearest = dist;
        }
    }

    return closest;
}

void Battleground_SA::SendTime()
{
    uint32 end_of_round = (EndRoundTimer - TotalTime);
    UpdateWorldState(BG_SA_PROJECT_TIMER, uint32(time(NULL) + end_of_round / 1000));
}

void Battleground_SA::EventPlayerClickedOnFlag(Player* Source, GameObject* target_obj)
{
    switch (target_obj->GetEntry())
    {
        case 191307:
        case 191308:
            if (GateStatus[BG_SA_PROJECT_GREEN_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_BLUE_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                CaptureGraveyard(BG_SA_PROJECT_LEFT_CAPTURABLE_GY, Source);
            break;
        case 191305:
        case 191306:
            if (GateStatus[BG_SA_PROJECT_GREEN_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_BLUE_GATE] == BG_SA_PROJECT_GATE_DESTROYED)
                CaptureGraveyard(BG_SA_PROJECT_RIGHT_CAPTURABLE_GY, Source);
            break;
        case 191310:
        case 191309:
            if ((GateStatus[BG_SA_PROJECT_GREEN_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_BLUE_GATE] == BG_SA_PROJECT_GATE_DESTROYED) && (GateStatus[BG_SA_PROJECT_RED_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_PURPLE_GATE] == BG_SA_PROJECT_GATE_DESTROYED))
                CaptureGraveyard(BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY, Source);
            break;
        default:
            return;
    };
}

void Battleground_SA::CaptureGraveyard(BG_SA_PROJECT_Graveyards i, Player* Source)
{
    if (GraveyardStatus[i] == Attackers)
        return;

    DelCreature(BG_SA_PROJECT_MAXNPC + i);
    GraveyardStatus[i] = Source->GetTeamId();
    WorldSafeLocsEntry const* sg = sWorldSafeLocsStore.LookupEntry(BG_SA_PROJECT_GYEntries[i]);
    if (!sg)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground_SA::CaptureGraveyard: non-existant GY entry: %u", BG_SA_PROJECT_GYEntries[i]);
        return;
    }

    AddSpiritGuide(i + BG_SA_PROJECT_MAXNPC, sg->Loc.X, sg->Loc.Y, sg->Loc.Z, BG_SA_PROJECT_GYOrientation[i], GraveyardStatus[i]);
    uint32 npc = 0;
    uint32 flag = 0;

    switch (i)
    {
        case BG_SA_PROJECT_LEFT_CAPTURABLE_GY:
            flag = BG_SA_PROJECT_LEFT_FLAG;
            DelObject(flag);
            AddObject(flag, (BG_SA_PROJECT_ObjEntries[flag] - (Source->GetTeamId() == TEAM_ALLIANCE ? 0 : 1)),
            BG_SA_PROJECT_ObjSpawnlocs[flag], 0, 0, 0, 0, RESPAWN_ONE_DAY);

            npc = BG_SA_PROJECT_NPC_RIGSPARK;
            AddCreature(BG_SA_PROJECT_NpcEntries[npc], npc, BG_SA_PROJECT_NpcSpawnlocs[npc], Attackers);

            for (uint8 j = BG_SA_PROJECT_DEMOLISHER_7; j <= BG_SA_PROJECT_DEMOLISHER_8; j++)
            {
                AddCreature(BG_SA_PROJECT_NpcEntries[j], j, BG_SA_PROJECT_NpcSpawnlocs[j], Attackers, 600);

                if (Creature* dem = GetBGCreature(j))
                    dem->setFaction(BG_SA_PROJECT_Factions[Attackers]);
            }

            UpdateWorldState(BG_SA_PROJECT_LEFT_GY_ALLIANCE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 1 : 0));
            UpdateWorldState(BG_SA_PROJECT_LEFT_GY_HORDE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 0 : 1));
            if (Source->GetTeamId() == TEAM_ALLIANCE)
                SendMessageToAll(LANG_BG_SA_A_GY_WEST, CHAT_MSG_SYSTEM);
            else
                SendMessageToAll(LANG_BG_SA_H_GY_WEST, CHAT_MSG_SYSTEM);
            break;
        case BG_SA_PROJECT_RIGHT_CAPTURABLE_GY:
            flag = BG_SA_PROJECT_RIGHT_FLAG;
            DelObject(flag);
            AddObject(flag, (BG_SA_PROJECT_ObjEntries[flag] - (Source->GetTeamId() == TEAM_ALLIANCE ? 0 : 1)),
              BG_SA_PROJECT_ObjSpawnlocs[flag], 0, 0, 0, 0, RESPAWN_ONE_DAY);

            npc = BG_SA_PROJECT_NPC_SPARKLIGHT;
            AddCreature(BG_SA_PROJECT_NpcEntries[npc], npc, BG_SA_PROJECT_NpcSpawnlocs[npc], Attackers);

            for (uint8 j = BG_SA_PROJECT_DEMOLISHER_5; j <= BG_SA_PROJECT_DEMOLISHER_6; j++)
            {
                AddCreature(BG_SA_PROJECT_NpcEntries[j], j, BG_SA_PROJECT_NpcSpawnlocs[j], Attackers, 600);

                if (Creature* dem = GetBGCreature(j))
                    dem->setFaction(BG_SA_PROJECT_Factions[Attackers]);
            }

            UpdateWorldState(BG_SA_PROJECT_RIGHT_GY_ALLIANCE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 1 : 0));
            UpdateWorldState(BG_SA_PROJECT_RIGHT_GY_HORDE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 0 : 1));
            if (Source->GetTeamId() == TEAM_ALLIANCE)
                SendMessageToAll(LANG_BG_SA_A_GY_EAST, CHAT_MSG_SYSTEM);
            else
                SendMessageToAll(LANG_BG_SA_H_GY_EAST, CHAT_MSG_SYSTEM);
            break;
        case BG_SA_PROJECT_CENTRAL_CAPTURABLE_GY:
            flag = BG_SA_PROJECT_CENTRAL_FLAG;
            DelObject(flag);
            AddObject(flag, (BG_SA_PROJECT_ObjEntries[flag] - (Source->GetTeamId() == TEAM_ALLIANCE ? 0 : 1)),
              BG_SA_PROJECT_ObjSpawnlocs[flag], 0, 0, 0, 0, RESPAWN_ONE_DAY);

            UpdateWorldState(BG_SA_PROJECT_CENTER_GY_ALLIANCE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 1 : 0));
            UpdateWorldState(BG_SA_PROJECT_CENTER_GY_HORDE, (GraveyardStatus[i] == TEAM_ALLIANCE ? 0 : 1));
            if (Source->GetTeamId() == TEAM_ALLIANCE)
                SendMessageToAll(LANG_BG_SA_A_GY_SOUTH, CHAT_MSG_SYSTEM);
            else
                SendMessageToAll(LANG_BG_SA_H_GY_SOUTH, CHAT_MSG_SYSTEM);
            break;
        default:
            ASSERT(false);
            break;
    };
}

void Battleground_SA::EventPlayerUsedGO(Player* Source, GameObject* object)
{
    if (object->GetEntry() == BG_SA_PROJECT_ObjEntries[BG_SA_PROJECT_TITAN_RELIC] &&
        GateStatus[BG_SA_PROJECT_ANCIENT_GATE] == BG_SA_PROJECT_GATE_DESTROYED &&
        GateStatus[BG_SA_PROJECT_YELLOW_GATE] == BG_SA_PROJECT_GATE_DESTROYED &&
        (GateStatus[BG_SA_PROJECT_PURPLE_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_RED_GATE] == BG_SA_PROJECT_GATE_DESTROYED) &&
        (GateStatus[BG_SA_PROJECT_GREEN_GATE] == BG_SA_PROJECT_GATE_DESTROYED || GateStatus[BG_SA_PROJECT_BLUE_GATE] == BG_SA_PROJECT_GATE_DESTROYED))
    {
        if (Source->GetTeamId() == Attackers)
        {
            if (Source->GetTeamId() == TEAM_ALLIANCE)
                SendMessageToAll(LANG_BG_SA_ALLIANCE_CAPTURED_RELIC, CHAT_MSG_BG_SYSTEM_NEUTRAL);
            else SendMessageToAll(LANG_BG_SA_HORDE_CAPTURED_RELIC, CHAT_MSG_BG_SYSTEM_NEUTRAL);

            if (Status == BG_SA_PROJECT_ROUND_ONE)
            {
                RoundScores[0].winner = Attackers;
                RoundScores[0].time = TotalTime;
                //Achievement Storm the Beach (1310)
                for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                {
                    if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                        if (player->GetTeamId() == Attackers)
                            player->UpdateCriteria(CRITERIA_TYPE_BE_SPELL_TARGET, 65246);
                }

                Attackers = (Attackers == TEAM_ALLIANCE) ? TEAM_HORDE : TEAM_ALLIANCE;
                Status = BG_SA_PROJECT_SECOND_WARMUP;
                TotalTime = 0;
                ToggleTimer();
                SendMessageToAll(LANG_BG_SA_ROUND_ONE_END, CHAT_MSG_SYSTEM);
                UpdateWaitTimer = 5000;
                SignaledRoundTwo = false;
                SignaledRoundTwoHalfMin = false;
                InitSecondRound = true;
                ResetObjs();
            }
            else if (Status == BG_SA_PROJECT_ROUND_TWO)
            {
                RoundScores[1].winner = Attackers;
                RoundScores[1].time = TotalTime;
                ToggleTimer();
                //Achievement Storm the Beach (1310)
                for (BattlegroundPlayerMap::const_iterator itr = GetPlayers().begin(); itr != GetPlayers().end(); ++itr)
                {
                    if (Player* player = ObjectAccessor::FindPlayer(itr->first))
                        if (player->GetTeamId() == Attackers && RoundScores[1].winner == Attackers)
                            player->UpdateCriteria(CRITERIA_TYPE_BE_SPELL_TARGET, 65246);
                }

                if (RoundScores[0].time == RoundScores[1].time)
                    EndBattleground(0);
                else if (RoundScores[0].time < RoundScores[1].time)
                    EndBattleground(RoundScores[0].winner == TEAM_ALLIANCE ? ALLIANCE : HORDE);
                else
                    EndBattleground(RoundScores[1].winner == TEAM_ALLIANCE ? ALLIANCE : HORDE);
            }
        }
    }
}

void Battleground_SA::ToggleTimer()
{
    TimerEnabled = !TimerEnabled;
    UpdateWorldState(BG_SA_PROJECT_ENABLE_TIMER, (TimerEnabled) ? 1 : 0);
}

void Battleground_SA::EndBattleground(uint32 winner)
{
    //honor reward for winning
    if (winner == ALLIANCE)
        RewardHonorToTeam(GetBonusHonorFromKill(1), ALLIANCE);
    else if (winner == HORDE)
        RewardHonorToTeam(GetBonusHonorFromKill(1), HORDE);

    //complete map_end rewards (even if no team wins)
    RewardHonorToTeam(GetBonusHonorFromKill(2), ALLIANCE);
    RewardHonorToTeam(GetBonusHonorFromKill(2), HORDE);

    Battleground::EndBattleground(winner);
}

void Battleground_SA::UpdateDemolisherSpawns()
{
    for (uint8 i = BG_SA_PROJECT_DEMOLISHER_1; i <= BG_SA_PROJECT_DEMOLISHER_8; i++)
    {
        if (!BgCreatures[i].IsEmpty())
        {
            if (Creature* Demolisher = GetBGCreature(i))
            {
                if (Demolisher->isDead())
                {
                    // Demolisher is not in list
                    if (DemoliserRespawnList.find(i) == DemoliserRespawnList.end())
                    {
                        DemoliserRespawnList[i] = getMSTime()+30000;
                    }
                    else
                    {
                        if (DemoliserRespawnList[i] < getMSTime())
                        {
                            Demolisher->Relocate(BG_SA_PROJECT_NpcSpawnlocs[i]);

                            Demolisher->Respawn();
                            DemoliserRespawnList.erase(i);
                        }
                    }
                }
            }
        }
    }
}

void Battleground_SA::SendTransportInit(Player* player)
{
    if (!BgObjects[BG_SA_PROJECT_BOAT_ONE].IsEmpty() || !BgObjects[BG_SA_PROJECT_BOAT_TWO].IsEmpty())
    {
        UpdateData transData(player->GetMapId());
        if (!BgObjects[BG_SA_PROJECT_BOAT_ONE].IsEmpty())

            GetBGObject(BG_SA_PROJECT_BOAT_ONE)->BuildCreateUpdateBlockForPlayer(&transData, player);
        if (!BgObjects[BG_SA_PROJECT_BOAT_TWO].IsEmpty())
            GetBGObject(BG_SA_PROJECT_BOAT_TWO)->BuildCreateUpdateBlockForPlayer(&transData, player);
        WorldPacket packet;
        transData.BuildPacket(&packet);
        player->SendDirectMessage(&packet);
    }
}

void Battleground_SA::SendTransportsRemove(Player* player)
{
    if (!BgObjects[BG_SA_PROJECT_BOAT_ONE].IsEmpty() || !BgObjects[BG_SA_PROJECT_BOAT_TWO].IsEmpty())
    {
        UpdateData transData(player->GetMapId());
        if (!BgObjects[BG_SA_PROJECT_BOAT_ONE].IsEmpty())
            GetBGObject(BG_SA_PROJECT_BOAT_ONE)->BuildOutOfRangeUpdateBlock(&transData);
        if (!BgObjects[BG_SA_PROJECT_BOAT_TWO].IsEmpty())
            GetBGObject(BG_SA_PROJECT_BOAT_TWO)->BuildOutOfRangeUpdateBlock(&transData);
        WorldPacket packet;
        transData.BuildPacket(&packet);
        player->SendDirectMessage(&packet);
    }
}

bool Battleground_SA::CheckAchievementCriteriaMeet(uint32 criteriaId, Player const* source, Unit const* target, uint32 miscValue)
{
    switch (criteriaId)
    {
        case BG_CRITERIA_CHECK_NOT_EVEN_A_SCRATCH:
            return _allVehiclesAlive[GetTeamIndexByTeamId(source->GetTeam(true))];
        case BG_CRITERIA_CHECK_DEFENSE_OF_THE_ANCIENTS:
            return source->GetTeamId() != Attackers && !gateDestroyed;
    }

    return Battleground::CheckAchievementCriteriaMeet(criteriaId, source, target, miscValue);
}

void Battleground_SA::TriggerAchievementDefenseOfTheAncients()
{
    // End of Round @ Defense of the Ancients achievement
    CastSpellOnTeam(BG_SA_PROJECT_SPELL_END_ROUND, Attackers == TEAM_ALLIANCE ? HORDE : ALLIANCE);
}
