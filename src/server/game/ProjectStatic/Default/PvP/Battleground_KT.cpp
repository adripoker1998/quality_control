#include "Battleground.h"
#include "Battleground_KT.h"
#include "BattlegroundMgr.h"
#include "Language.h"
#include "Random.h"
#include "WorldStatePackets.h"
#include "BattlegroundPackets.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Log.h"
#include "Creature.h"
#include "GameObject.h"

Battleground_KT::Battleground_KT()
{
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_BG_KT_START_TWO_MINUTES;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_BG_KT_START_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_BG_KT_START_HALF_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_BG_KT_HAS_BEGUN;

    m_ReputationCapture = 0;
    m_HonorWinKills = 0;
    m_HonorEndKills = 0;

    m_EndTimer = 0;
    m_UpdatePointsTimer = BG_KT_POINTS_UPDATE_TIME;
    m_LastCapturedOrbTeam = TEAM_OTHER;
}

Battleground_KT::~Battleground_KT()
{
}

void Battleground_KT::PostUpdateImpl(uint32 diff)
{
    if (GetStatus() == STATUS_IN_PROGRESS)
    {
        if (m_EndTimer <= diff)
        {
            uint32 allianceScore = GetTeamScore(ALLIANCE);
            uint32 hordeScore    = GetTeamScore(HORDE);

            if (allianceScore > hordeScore)
                EndBattleground(ALLIANCE);
            else if (allianceScore < hordeScore)
                EndBattleground(HORDE);
            else
            {
                // if 0 => tie
                EndBattleground(m_LastCapturedOrbTeam);
            }
        }
        else
        {
            uint32 minutesLeftPrev = GetRemainingTimeInMinutes();
            m_EndTimer -= diff;
            uint32 minutesLeft = GetRemainingTimeInMinutes();

            if (minutesLeft != minutesLeftPrev)
                UpdateWorldState(BG_KT_TIME_REMAINING, minutesLeft);
        }

        if (m_UpdatePointsTimer <= diff)
        {
            for (uint8 i = 0; i < MAX_ORBS; ++i)
            {
                ObjectGuid guid = m_OrbKeepers[i];
                if (!guid.IsEmpty())
                    if (m_playersZone.find(guid) != m_playersZone.end())
                        if (Player* player = ObjectAccessor::FindPlayer(guid))
                        {
                            AccumulateScore(player, m_playersZone[guid]);
                            player->CastSpell(player, BG_KT_TickVisual[m_playersZone[guid]], true);
                            UpdatePlayerScore(player, SCORE_ORB_SCORE, m_playersZone[guid]);
                        }
            }

            // Test win condition
            if (m_TeamScores[TEAM_ALLIANCE] >= BG_KT_MAX_TEAM_SCORE)
                EndBattleground(ALLIANCE);
            if (m_TeamScores[TEAM_HORDE] >= BG_KT_MAX_TEAM_SCORE)
                EndBattleground(HORDE);

            m_UpdatePointsTimer = BG_KT_POINTS_UPDATE_TIME;
        }
        else
            m_UpdatePointsTimer -= diff;

        // Powerball achievement
        for (uint8 i = 0; i < MAX_ORBS; ++i)
        {
            ObjectGuid guid = m_OrbKeepers[i];
            if (!guid.IsEmpty())
            {
                if (m_playersZone.find(guid) != m_playersZone.end())
                {
                    if (m_playersZone[guid] == KT_ZONE_MIDDLE)
                        if (Player* player = ObjectAccessor::FindPlayer(guid))
                        {
                            orbKeeperDurations[guid] += diff;
                            if (orbKeeperDurations[guid] >= BG_KT_POWERBALL_LIMIT)
                                player->CastSpell(player, BG_KT_POWERBALL_CREDIT, true);
                        }
                }
            }
        }
    }
}

void Battleground_KT::StartingEventCloseDoors()
{
    SpawnBGObject(BG_KT_OBJECT_A_DOOR, RESPAWN_IMMEDIATELY);
    SpawnBGObject(BG_KT_OBJECT_H_DOOR, RESPAWN_IMMEDIATELY);

    DoorClose(BG_KT_OBJECT_A_DOOR);
    DoorClose(BG_KT_OBJECT_H_DOOR);

    for (uint8 i = 0; i < BG_KT_ORBS_COUNT; ++i)
        SpawnBGObject(BG_KT_OBJECT_ORB_1 + i, RESPAWN_ONE_DAY);
}

void Battleground_KT::StartingEventOpenDoors()
{
    DoorOpen(BG_KT_OBJECT_A_DOOR);
    DoorOpen(BG_KT_OBJECT_H_DOOR);

    for (uint8 i = 0; i < BG_KT_ORBS_COUNT; ++i)
        SpawnBGObject(BG_KT_OBJECT_ORB_1 + i, RESPAWN_IMMEDIATELY);

    // Players that join battleground after start are not eligible to get achievement.
    StartCriteriaTimer(CRITERIA_TIMED_TYPE_EVENT, BG_KT_EVENT_START_BATTLE);
}

void Battleground_KT::AddPlayer(Player *plr)
{
    Battleground::AddPlayer(plr);
    //create score and add it to map, default values are set in constructor
    Battleground_KTScore* sc = new Battleground_KTScore(plr->GetGUID(), plr->GetBGTeam());

    PlayerScores[plr->GetGUID()] = sc;
    m_playersZone[plr->GetGUID()] = KT_ZONE_OUT;
}

void Battleground_KT::EventPlayerClickedOnOrb(Player* source, GameObject* target_obj)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    if (!source->IsWithinDistInMap(target_obj, 10))
        return;

    uint32 index = target_obj->GetEntry() - BG_KT_OBJECT_ORB_1_ENTRY;

    // If this orb is already keeped by a player, there is a problem
    if (index > MAX_ORBS || !m_OrbKeepers[index].IsEmpty())
        return;

    // Check if the player already have an orb
    for (uint8 i = 0; i < MAX_ORBS; ++i)
        if (m_OrbKeepers[i] == source->GetGUID())
            return;

    PlaySoundToAll(source->GetTeamId() == TEAM_ALLIANCE ? BG_KT_SOUND_A_ORB_PICKED_UP: BG_KT_SOUND_H_ORB_PICKED_UP);
    source->CastSpell(source, BG_KT_ORBS_SPELLS[index], true);
    source->CastSpell(source, source->GetTeamId() == TEAM_ALLIANCE ? BG_KT_ALLIANCE_INSIGNIA: BG_KT_HORDE_INSIGNIA, true);

    UpdatePlayerScore(source, SCORE_ORB_HANDLES, 1);
    UpdateWorldState(GetOrbWorldState(index), BG_KT_ORB_STATE_PICKED_UP);

    m_OrbKeepers[index] = source->GetGUID();
    orbKeeperDurations[source->GetGUID()] = 0;
    SpawnBGObject(BG_KT_OBJECT_ORB_1 + index, RESPAWN_ONE_DAY);

    if (Creature* aura = GetBGCreature(BG_KT_CREATURE_ORB_AURA_1 + index))
        aura->RemoveAllAuras();

    SendMessageToAll(LANG_BG_KT_PICKEDUP_1 + index, source->GetTeamId() == TEAM_ALLIANCE ? CHAT_MSG_BG_SYSTEM_ALLIANCE: CHAT_MSG_BG_SYSTEM_HORDE, source);
    source->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_ENTER_PVP_COMBAT);
}

void Battleground_KT::EventPlayerDroppedOrb(Player* source, ObjectGuid guid)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    uint8 index = 0;

    for (; index <= MAX_ORBS; ++index)
    {
        if (index == MAX_ORBS)
            return;

        if (m_OrbKeepers[index] == guid)
            break;
    }

    if (source)
    {
        PlaySoundToAll(source->GetTeamId() == TEAM_ALLIANCE ? BG_KT_SOUND_A_ORB_PICKED_UP : BG_KT_SOUND_H_ORB_PICKED_UP);
        source->RemoveAurasDueToSpell(BG_KT_ORBS_SPELLS[index]);
        source->RemoveAurasDueToSpell(BG_KT_ALLIANCE_INSIGNIA);
        source->RemoveAurasDueToSpell(BG_KT_HORDE_INSIGNIA);

        source->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_ENTER_PVP_COMBAT);
    }
    SendMessageToAll(LANG_BG_KT_DROPPED_1 + index, CHAT_MSG_RAID_BOSS_EMOTE);

    UpdateWorldState(GetOrbWorldState(index), BG_KT_ORB_STATE_ON_GROUND);

    m_OrbKeepers[index].Clear();
    orbKeeperDurations[guid] = 0;
    SpawnBGObject(BG_KT_OBJECT_ORB_1 + index, RESPAWN_IMMEDIATELY);

    if (Creature* aura = GetBGCreature(BG_KT_CREATURE_ORB_AURA_1 + index))
        aura->AddAura(BG_KT_ORBS_AURA[index], aura);
}

void Battleground_KT::RemovePlayer(Player* player, ObjectGuid guid, uint32 team)
{
    EventPlayerDroppedOrb(player, guid);
    m_playersZone.erase(guid);
}

void Battleground_KT::UpdateTeamScore(Team team)
{
    if (team == ALLIANCE)
        UpdateWorldState(BG_KT_ORB_POINTS_A, GetTeamScore(team));
    else
        UpdateWorldState(BG_KT_ORB_POINTS_H, GetTeamScore(team));
}

void Battleground_KT::HandleAreaTrigger(Player* source, uint32 trigger, bool entered)
{
    // this is wrong way to implement these things. On official it done by gameobject spell cast.
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    ObjectGuid sourceGuid = source->GetGUID();
    switch(trigger)
    {
        case 7734: // Out-In trigger
        {
            if (m_playersZone.find(sourceGuid) == m_playersZone.end())
                return;

            if (m_playersZone[sourceGuid] == KT_ZONE_OUT)
                m_playersZone[sourceGuid] = KT_ZONE_IN;
            else
                m_playersZone[sourceGuid] = KT_ZONE_OUT;
            break;
        }
        case 7735: // Middle-In trigger
        {
            if (m_playersZone.find(sourceGuid) == m_playersZone.end())
                return;

            if (m_playersZone[sourceGuid] == KT_ZONE_IN)
                m_playersZone[sourceGuid] = KT_ZONE_MIDDLE;
            else
                m_playersZone[sourceGuid] = KT_ZONE_IN;
            break;
        }
        default:
            TC_LOG_DEBUG("bg.battleground", "WARNING: Unhandled AreaTrigger in Battleground: %u", trigger);
            //source->GetSession()->SendAreaTriggerMessage("Warning: Unhandled AreaTrigger in Battleground: %u", trigger);
            break;
    }
}

bool Battleground_KT::SetupBattleground()
{
    // Doors
    if (   !AddObject(BG_KT_OBJECT_A_DOOR, BG_KT_OBJECT_DOOR_ENTRY, BG_KT_DoorPositions[0], 0, 0, sin(BG_KT_DoorPositions[0].GetOrientation()/2), cos(BG_KT_DoorPositions[0].GetOrientation()/2), RESPAWN_IMMEDIATELY)
        || !AddObject(BG_KT_OBJECT_H_DOOR, BG_KT_OBJECT_DOOR_ENTRY, BG_KT_DoorPositions[1], 0, 0, sin(BG_KT_DoorPositions[1].GetOrientation()/2), cos(BG_KT_DoorPositions[1].GetOrientation()/2), RESPAWN_IMMEDIATELY))
        return false;

    // spirit guides
    if (   !AddSpiritGuide(BG_KT_CREATURE_SPIRIT_1, BG_KT_SpiritPositions[0], TEAM_ALLIANCE)
        || !AddSpiritGuide(BG_KT_CREATURE_SPIRIT_2, BG_KT_SpiritPositions[1], TEAM_HORDE))
        return false;

    // buffs
    if (!AddObject(BG_KT_OBJECT_BERSERKBUFF_1, BG_OBJECTID_BERSERKERBUFF_ENTRY, 1710.95f, 1333.47f, 10.5556f, 0.0186885f, 0, 0, 0.5591929f, 0.8290376f, BUFF_RESPAWN_TIME)
        || !AddObject(BG_KT_OBJECT_BERSERKBUFF_2, BG_OBJECTID_BERSERKERBUFF_ENTRY, 1855.5f, 1333.37f, 10.5556f, 3.15075f, 0, 0, 0.9396926f, -0.3420201f, BUFF_RESPAWN_TIME))
        return false;

    // Orbs
    for (uint8 i = 0; i < MAX_ORBS; ++i)
    {
        if (!AddObject(BG_KT_OBJECT_ORB_1 + i, BG_KT_OBJECT_ORB_1_ENTRY + i, BG_KT_OrbPositions[i], 0, 0, sin(BG_KT_OrbPositions[i].GetOrientation()/2), cos(BG_KT_OrbPositions[i].GetOrientation()/2), RESPAWN_ONE_DAY))
            return false;

        if (Creature* trigger = AddCreature(WORLD_TRIGGER, BG_KT_CREATURE_ORB_AURA_1 + i, BG_KT_OrbPositions[i], TEAM_NEUTRAL, RESPAWN_IMMEDIATELY))
            trigger->AddAura(BG_KT_ORBS_AURA[i], trigger);
    }

    return true;
}

void Battleground_KT::Reset()
{
    //call parent's class reset
    Battleground::Reset();
    BgObjects.resize(BG_KT_OBJECT_MAX);
    BgCreatures.resize(BG_KT_CREATURE_MAX);

    for(uint32 i = 0; i < MAX_ORBS; ++i)
        m_OrbKeepers[i].Clear();

    bool isBGWeekend = BattlegroundMgr::IsBGWeekend(GetTypeID());
    m_ReputationCapture = (isBGWeekend) ? 45 : 35;
    m_HonorWinKills = (isBGWeekend) ? 3 : 1;
    m_HonorEndKills = (isBGWeekend) ? 4 : 2;

    m_EndTimer = BG_KT_TIME_LIMIT;
    m_LastCapturedOrbTeam = TEAM_OTHER;
}

void Battleground_KT::EndBattleground(uint32 winner)
{
    //win reward
    if (winner == ALLIANCE)
        RewardHonorToTeam(GetBonusHonorFromKill(m_HonorWinKills), ALLIANCE);
    if (winner == HORDE)
        RewardHonorToTeam(GetBonusHonorFromKill(m_HonorWinKills), HORDE);
    //complete map_end rewards (even if no team wins)
    RewardHonorToTeam(GetBonusHonorFromKill(m_HonorEndKills), ALLIANCE);
    RewardHonorToTeam(GetBonusHonorFromKill(m_HonorEndKills), HORDE);

    Battleground::EndBattleground(winner);
}

void Battleground_KT::HandleKillPlayer(Player *player, Player *killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    EventPlayerDroppedOrb(player, player->GetGUID());

    Battleground::HandleKillPlayer(player, killer);
}


bool Battleground_KT::UpdatePlayerScore(Player* Source, uint32 type, uint32 value, bool doAddHonor)
{
    if (!Battleground::UpdatePlayerScore(Source, type, value, doAddHonor))
        return false;

    return true;
}

WorldSafeLocsEntry const* Battleground_KT::GetClosestGraveYard(Player* player)
{
    //if status in progress, it returns main graveyards with spiritguides
    //else it will return the graveyard in the flagroom - this is especially good
    //if a player dies in preparation phase - then the player can't cheat
    //and teleport to the graveyard outside the flagroom
    //and start running around, while the doors are still closed
    if (player->GetTeam(true) == ALLIANCE)
    {
        if (GetStatus() == STATUS_IN_PROGRESS)
            return sWorldSafeLocsStore.LookupEntry(KT_GRAVEYARD_RECTANGLEA1);
        else
            return sWorldSafeLocsStore.LookupEntry(KT_GRAVEYARD_RECTANGLEA2);
    }
    else
    {
        if (GetStatus() == STATUS_IN_PROGRESS)
            return sWorldSafeLocsStore.LookupEntry(KT_GRAVEYARD_RECTANGLEH1);
        else
            return sWorldSafeLocsStore.LookupEntry(KT_GRAVEYARD_RECTANGLEH2);
    }
}

void Battleground_KT::AccumulateScore(Player * source, BG_KT_ZONE zone)
{
    uint32 team = source->GetTeamId();

    if (zone > KT_ZONE_MAX)
        return;

    if (team >= TEAM_NEUTRAL)
        return;

    m_TeamScores[team] += BG_KT_TickPoints[zone];

    // ACHIEVEMENT_CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE is increasing with 1. do this hack instead of changing that all over the core
    for (uint32 i = 0; i < BG_KT_TickPoints[zone]; i++)
        source->UpdateCriteria(CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE, KT_OBJECTIVE_SCORE_POINTS);

    if (m_TeamScores[team] > BG_KT_MAX_TEAM_SCORE)
        m_TeamScores[team] = BG_KT_MAX_TEAM_SCORE;

    if (team == TEAM_ALLIANCE)
        UpdateWorldState(BG_KT_ORB_POINTS_A, m_TeamScores[team]);
    if (team == TEAM_HORDE)
        UpdateWorldState(BG_KT_ORB_POINTS_H, m_TeamScores[team]);
}

uint32 Battleground_KT::GetOrbWorldState(uint8 index)
{
    switch (index)
    {
        case 0: return BG_KT_ORB_STATE_GREEN;
        case 1: return BG_KT_ORB_STATE_PURPLE;
        case 2: return BG_KT_ORB_STATE_ORANGE;
        case 3: return BG_KT_ORB_STATE_BLUE;
    }
    return 0;
}        

void Battleground_KT::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& builder)
{
    builder.Worldstates.emplace_back(BG_KT_ORB_POINTS_A, GetTeamScore(ALLIANCE));
    builder.Worldstates.emplace_back(BG_KT_ORB_POINTS_H, GetTeamScore(HORDE));

    for (int i = 0; i < MAX_ORBS; i++)
        builder.Worldstates.emplace_back(GetOrbWorldState(i), m_OrbKeepers[i] == ObjectGuid::Empty ? BG_KT_ORB_STATE_ON_GROUND : BG_KT_ORB_STATE_PICKED_UP);

    builder.Worldstates.emplace_back(BG_KT_ORB_POINTS_MAX, BG_KT_MAX_TEAM_SCORE);

    builder.Worldstates.emplace_back(BG_KT_TIME_ENABLED, 1);
    builder.Worldstates.emplace_back(BG_KT_TIME_REMAINING, GetRemainingTimeInMinutes());
}

void Battleground_KT::GetPlayerPositionData(std::vector<WorldPackets::Battleground::BattlegroundPlayerPosition>* positions) const
{
    // orb carriers
    for (int i = 0; i < BG_KT_ORBS_COUNT; i++)
    {
        ObjectGuid guid = GetFlagPickerGUID(i);
        if (guid.IsEmpty())
            continue;
        if (Player * player = ObjectAccessor::FindPlayer(guid))
        {
            WorldPackets::Battleground::BattlegroundPlayerPosition position;
            position.Guid = player->GetGUID();
            position.Pos = Position(player->GetPositionX(), player->GetPositionY());
            position.IconID = player->GetTeam(true) == HORDE ? PLAYER_POSITION_ICON_HORDE_FLAG : PLAYER_POSITION_ICON_ALLIANCE_FLAG;
            position.ArenaSlot = i + 2;
            positions->push_back(position);
        }
    }
}

bool Battleground_KT::CheckAchievementCriteriaMeet(uint32 criteriaId, Player const* player, Unit const* target, uint32 miscvalue)
{
    switch (criteriaId)
    {
        case BG_CRITERIA_I_VE_GOT_THE_POWER_A:
        case BG_CRITERIA_I_VE_GOT_THE_POWER_H:
            uint32 orbsHeld = 0;
            uint32 expectedTeam = criteriaId == BG_CRITERIA_I_VE_GOT_THE_POWER_A ? ALLIANCE : HORDE;
            for (uint8 i = 0; i < MAX_ORBS; ++i)
            {
                ObjectGuid guid = m_OrbKeepers[i];
                if (!guid.IsEmpty())
                    if (Player* player = ObjectAccessor::FindPlayer(guid))
                        if (player->GetTeam(true) == expectedTeam)
                            orbsHeld++;
            }
            return orbsHeld == MAX_ORBS;
    }

    return Battleground::CheckAchievementCriteriaMeet(criteriaId, player, target, miscvalue);
}
