#ifndef __BATTLEGROUND_TTP_H
#define __BATTLEGROUND_TTP_H

#include "Arena.h"

enum BattlegroundTTPObjectTypes
{
    BG_TTP_OBJECT_DOOR_1         = 0,
    BG_TTP_OBJECT_DOOR_2         = 1,
    BG_TTP_OBJECT_BUFF_1         = 2,
    BG_TTP_OBJECT_BUFF_2         = 3,
    BG_TTP_OBJECT_MAX            = 4
};

enum BattlegroundTTPObjects
{
    BG_TTP_OBJECT_TYPE_DOOR_1    = 219395,
    BG_TTP_OBJECT_TYPE_DOOR_2    = 219396,
    BG_TTP_OBJECT_TYPE_BUFF_1    = 184663,
    BG_TTP_OBJECT_TYPE_BUFF_2    = 184664
};

struct Battleground_TTPScore : public BattlegroundScore
{
    Battleground_TTPScore(ObjectGuid guid, uint32 team) : BattlegroundScore(guid, team) { }
    void UpdateScore(uint32 type, uint32 value) override
    {
        BattlegroundScore::UpdateScore(type, value);
    }

    void BuildObjectivesBlock(std::vector<int32>& stats) override
    {

    }
};

class Battleground_TTP : public Arena
{
    public:
        Battleground_TTP();
        ~Battleground_TTP();

        /* inherited from BattlegroundClass */
        void AddPlayer(Player* player);
        void StartingEventCloseDoors();
        void StartingEventOpenDoors();

        void RemovePlayer(Player* player, ObjectGuid guid, uint32 team);
        void HandleAreaTrigger(Player* Source, uint32 Trigger, bool entered);
        bool SetupBattleground();
        void Reset();
        void FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates &builder);
        void HandleKillPlayer(Player* player, Player* killer);
};
#endif
