/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __BATTLEGROUNDNAL_H
#define __BATTLEGROUNDNAL_H

#include "Arena.h"

enum BattlegroundNALObjectTypes
{
    BG_NAL_OBJECT_DOOR_1         = 0,
    BG_NAL_OBJECT_DOOR_2         = 1,
    BG_NAL_OBJECT_MAX            = 2
};

enum BattlegroundNALGameObjects
{
    BG_NAL_OBJECT_TYPE_DOOR_1    = 260528,
    BG_NAL_OBJECT_TYPE_DOOR_2    = 260527,
};

class Battleground_NAL : public Arena
{
    public:
        Battleground_NAL();

        /* inherited from BattlegroundClass */
        void StartingEventCloseDoors() override;
        void StartingEventOpenDoors() override;

        void HandleAreaTrigger(Player* source, uint32 trigger, bool entered) override;
        bool SetupBattleground() override;
        void FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& packet) override;
};
#endif
