/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Battleground_NAL.h"
#include "Log.h"
#include "Player.h"
#include "WorldPacket.h"
#include "WorldStatePackets.h"

Battleground_NAL::Battleground_NAL()
{
    BgObjects.resize(BG_NAL_OBJECT_MAX);
}

void Battleground_NAL::StartingEventCloseDoors()
{
    for (uint32 i = BG_NAL_OBJECT_DOOR_1; i <= BG_NAL_OBJECT_DOOR_2; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void Battleground_NAL::StartingEventOpenDoors()
{
    for (uint32 i = BG_NAL_OBJECT_DOOR_1; i <= BG_NAL_OBJECT_DOOR_2; ++i)
        DoorOpen(i);
}

void Battleground_NAL::HandleAreaTrigger(Player* player, uint32 trigger, bool entered)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        case 4536:                                          // buff trigger?
        case 4537:                                          // buff trigger?
            break;
        default:
            Battleground::HandleAreaTrigger(player, trigger, entered);
            break;
    }
}

void Battleground_NAL::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& packet)
{
    packet.Worldstates.emplace_back(0xa11, 1);
    Arena::FillInitialWorldStates(packet);
}

bool Battleground_NAL::SetupBattleground()
{
    // gates
    if (!AddObject(BG_NAL_OBJECT_DOOR_1, BG_NAL_OBJECT_TYPE_DOOR_1, -2019.238f, 6609.098f, 11.9068f, 5.205194f, 0, 0, -0.5132742f, 0.8582247f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_NAL_OBJECT_DOOR_2, BG_NAL_OBJECT_TYPE_DOOR_2, -2067.722f, 6699.386f, 11.9068f, 2.063599f, 0, 0, 0.8582239f, 0.5132754f, RESPAWN_IMMEDIATELY))
    // buffs
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundNA: Failed to spawn some object!");
        return false;
    }

    return true;
}
