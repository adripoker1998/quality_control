#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "Battleground_DG.h"
#include "PassiveAI.h"
#include "ScriptMgr.h"
#include "Language.h"
#include "Random.h"
#include "WorldStatePackets.h"
#include "BattlegroundPackets.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Log.h"
#include "Creature.h"
#include "GameObject.h"

uint32 BG_DG_OBJECTID_NODE_BANNER[BG_DG_DYNAMIC_NODES_COUNT] = { BG_DG_OBJECTID_NODE_PANDAREN, BG_DG_OBJECTID_NODE_CENTER, BG_DG_OBJECTID_NODE_GOBLIN };
uint32 BG_DG_WS_ID_CARTS[BG_TEAMS_COUNT] = { BG_DG_WS_CART_STATE_HORDE, BG_DG_WS_CART_STATE_ALLY };
uint32 BG_DG_SPELL_ID_CART_VISUAL[BG_TEAMS_COUNT] = { BG_DG_SPELL_ALLIANCE_CART_VISUAL, BG_DG_SPELL_HORDE_CART_VISUAL };
uint32 BG_DG_SPELL_ID_VISUAL[BG_TEAMS_COUNT] = { BG_DG_SPELL_ALLIANCE_VISUAL, BG_DG_SPELL_HORDE_VISUAL };
uint32 BG_DG_SPELL_ID_SUMMON_CART[BG_TEAMS_COUNT] = { BG_DG_SPELL_SUMMON_CART_ALLIANCE, BG_DG_SPELL_SUMMON_CART_HORDE };
uint32 BG_DG_CREATURE_ID_CART[BG_TEAMS_COUNT] = { BG_DG_CART_ALLIANCE, BG_DG_CART_HORDE };
uint32 BG_DG_PEASANT_ID_SACK[BG_TEAMS_COUNT] = { BG_DG_PEASANT_ALLIANCE_SACK, BG_DG_PEASANT_HORDE_SACK };
uint32 BG_DG_PEASANT_MODEL_ID_EMPTY[BG_TEAMS_COUNT] = { BG_DG_PEASANT_MODEL_ALLIANCE_EMPTY, BG_DG_PEASANT_MODEL_HORDE_EMPTY };

Battleground_DG::Battleground_DG()
{
    BgObjects.resize(BG_DG_OBJECT_MAX);
    BgCreatures.resize(BG_DG_CREATURE_MAX_CREATURES);

    StartMessageIds[BG_STARTING_EVENT_FIRST] = LANG_BG_DG_START_TWO_MINUTES;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_BG_DG_START_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_THIRD] = LANG_BG_DG_START_HALF_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_BG_DG_HAS_BEGUN;

    for (int i = 0; i < BG_TEAMS_COUNT; i++)
    {
        cartStates[i] = BG_DG_CART_STATE_AT_BASE;
        cartPickers[i].Clear();
        stolenGold[i] = 0;
        cartDropTimer[i] = 0;
    }

    for (int i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; i++)
        peasantsSpawnTimer[i] = 0;
}

Battleground_DG::~Battleground_DG()
{
}

void Battleground_DG::PostUpdateImpl(uint32 diff)
{
    if (GetStatus() == STATUS_IN_PROGRESS)
    {
        int teamGold[BG_TEAMS_COUNT] = { 0, 0 };

        for (int node = 0; node < BG_DG_DYNAMIC_NODES_COUNT; ++node)
        {
            // 3 sec delay to spawn new banner instead previous despawned one
            if (m_BannerTimers[node].timer)
            {
                if (m_BannerTimers[node].timer > diff)
                    m_BannerTimers[node].timer -= diff;
                else
                {
                    m_BannerTimers[node].timer = 0;
                    _CreateBanner(node, m_BannerTimers[node].type, m_BannerTimers[node].teamIndex, false);
                }
            }

            // 1-minute to occupy a node from contested state
            if (m_NodeTimers[node])
            {
                if (m_NodeTimers[node] > diff)
                    m_NodeTimers[node] -= diff;
                else
                {
                    m_NodeTimers[node] = 0;
                    // Change from contested to occupied !
                    uint8 teamIndex = m_Nodes[node] - 1;
                    m_prevNodes[node] = m_Nodes[node];
                    m_Nodes[node] += 2;
                    // burn current contested banner
                    _DelBanner(node, BG_DG_NODE_TYPE_CONTESTED, teamIndex);
                    // create new occupied banner
                    _CreateBanner(node, BG_DG_NODE_TYPE_OCCUPIED, teamIndex, true);
                    _SendNodeUpdate(node);
                    _NodeOccupied(node, (teamIndex == 0) ? ALLIANCE : HORDE);
                    // Message to chatlog

                    if (teamIndex == 0)
                    {
                        // FIXME: team and node names not localized
                        SendMessage2ToAll(LANG_BG_DG_NODE_TAKEN, CHAT_MSG_BG_SYSTEM_ALLIANCE, 0, LANG_BG_DG_ALLY, _GetNodeNameId(node));
                        PlaySoundToAll(BG_DG_SOUND_NODE_CAPTURED_ALLIANCE);
                    }
                    else
                    {
                        // FIXME: team and node names not localized
                        SendMessage2ToAll(LANG_BG_DG_NODE_TAKEN, CHAT_MSG_BG_SYSTEM_HORDE, 0, LANG_BG_DG_HORDE, _GetNodeNameId(node));
                        PlaySoundToAll(BG_DG_SOUND_NODE_CAPTURED_HORDE);
                    }
                }
            }

            for (int team = 0; team < BG_TEAMS_COUNT; ++team)
                if (m_Nodes[node] == team + BG_DG_NODE_TYPE_OCCUPIED)
                    ++teamGold[team];

            // peasants
            for (int team = 0; team < BG_TEAMS_COUNT; ++team)
            {
                // initial
                if (m_Nodes[node] == team + BG_DG_NODE_TYPE_OCCUPIED)
                {
                    if (diff >= peasantsSpawnTimer[node])
                    {
                        peasantsSpawnTimer[node] = BG_DG_PEASANTS_SPAWN_TIMER;
                        uint32 creatureIndex = BG_DG_CREATURE_PEASANT_A + (BG_DG_DYNAMIC_NODES_COUNT * team) + node;

                        // return old peasant
                        if (Creature * peasant = GetBGCreature(creatureIndex))
                        {
                            peasant->SetDisplayId(BG_DG_PEASANT_MODEL_ID_EMPTY[team]);
                            peasant->GetMotionMaster()->MovePoint(0, BG_DG_PeasantsSpawnPos[node], false);
                        }

                        // spawn new peasant
                        if (Creature * peasant = AddCreature(BG_DG_PEASANT_ID_SACK[team], creatureIndex, BG_DG_PeasantsSpawnPos[node]))
                        {
                            peasant->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1);
                            peasant->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                            peasant->DespawnOrUnsummon(BG_DG_PEASANTS_SPAWN_TIMER * 2);
                            peasant->SetSpeed(MOVE_WALK, 10.f);
                            peasant->SetSpeed(MOVE_RUN, 10.f);
                            peasant->GetMotionMaster()->MovePoint(0, BG_DG_PeasantsEndPos[node], false);
                        }
                    }
                    else
                        peasantsSpawnTimer[node] -= diff;
                }
            }
        }

        // Accumulate points
        for (int team = 0; team < BG_TEAMS_COUNT; ++team)
        {
            int points = teamGold[team];
            if (!points)
                continue;
            m_lastTick[team] += diff;
            if (m_lastTick[team] > BG_DG_SCORE_TICK_INTERVAL)
            {
                m_lastTick[team] -= BG_DG_SCORE_TICK_INTERVAL;
                m_TeamScores[team] += BG_DG_TickPoints[points];
                m_HonorScoreTics[team] += BG_DG_TickPoints[points];
                if (!m_IsInformedNearVictory && m_TeamScores[team] > BG_DG_WARNING_NEAR_VICTORY_SCORE)
                {
                    if (team == TEAM_ALLIANCE)
                        SendMessageToAll(LANG_BG_DG_A_NEAR_VICTORY, CHAT_MSG_BG_SYSTEM_NEUTRAL);
                    else
                        SendMessageToAll(LANG_BG_DG_H_NEAR_VICTORY, CHAT_MSG_BG_SYSTEM_NEUTRAL);
                    PlaySoundToAll(BG_DG_SOUND_NEAR_VICTORY);
                    m_IsInformedNearVictory = true;
                }

                if (m_TeamScores[team] > BG_DG_MAX_TEAM_SCORE)
                    m_TeamScores[team] = BG_DG_MAX_TEAM_SCORE;
                if (team == TEAM_ALLIANCE)
                    UpdateWorldState(BG_DG_WS_RESOURCES_ALLY, m_TeamScores[team]);
                if (team == TEAM_HORDE)
                    UpdateWorldState(BG_DG_WS_RESOURCES_HORDE, m_TeamScores[team]);
            }
        }

        // carts
        for (int teamIndex = 0; teamIndex < BG_TEAMS_COUNT; teamIndex++)
        {
            if (cartStates[teamIndex] == BG_DG_CART_STATE_DROPPED)
            {
                cartDropTimer[teamIndex] -= diff;

                if (cartDropTimer[teamIndex] < 0)
                {
                    cartDropTimer[teamIndex] = 0;
                    UpdateCartState(teamIndex, BG_DG_CART_STATE_AT_BASE);
                    
                    // remove dropped cart
                    if (GameObject * obj = GetBGObject(BG_DG_OBJECT_CART_A_DROPPED + teamIndex * 2))
                        obj->Delete();
                    
                    // respawn base cart
                    SpawnBGObject(BG_DG_OBJECT_CART_A + teamIndex * 2, RESPAWN_IMMEDIATELY);
                    
                    // send message
                    SendMessage2ToAll(LANG_BG_DG_CART_RETURNED, teamIndex == TEAM_ALLIANCE ? CHAT_MSG_BG_SYSTEM_ALLIANCE : CHAT_MSG_BG_SYSTEM_HORDE, NULL, teamIndex == TEAM_ALLIANCE ? LANG_BG_DG_ALLY : LANG_BG_DG_HORDE);
                }
            }
        }

        // Test win condition
        if (m_TeamScores[TEAM_ALLIANCE] >= BG_DG_MAX_TEAM_SCORE)
            EndBattleground(ALLIANCE);
        else if (m_TeamScores[TEAM_HORDE] >= BG_DG_MAX_TEAM_SCORE)
            EndBattleground(HORDE);
    }
}

void Battleground_DG::StartingEventCloseDoors()
{
    // despawn banners, auras and buffs
    for (int obj = BG_DG_OBJECT_BANNER_NEUTRAL; obj < BG_DG_DYNAMIC_NODES_COUNT * 8; ++obj)
        SpawnBGObject(obj, RESPAWN_ONE_DAY);
    for (int i = 0; i < BG_DG_BUFFS_COUNT; ++i)
        SpawnBGObject(BG_DG_OBJECT_REGENBUFF_UP + i, RESPAWN_ONE_DAY);

    // Starting doors
    DoorClose(BG_DG_OBJECT_GATE_A_1);
    DoorClose(BG_DG_OBJECT_GATE_A_2);
    DoorClose(BG_DG_OBJECT_GATE_H_1);
    DoorClose(BG_DG_OBJECT_GATE_H_2);
    SpawnBGObject(BG_DG_OBJECT_GATE_A_1, RESPAWN_IMMEDIATELY);
    SpawnBGObject(BG_DG_OBJECT_GATE_A_2, RESPAWN_IMMEDIATELY);
    SpawnBGObject(BG_DG_OBJECT_GATE_H_1, RESPAWN_IMMEDIATELY);
    SpawnBGObject(BG_DG_OBJECT_GATE_H_2, RESPAWN_IMMEDIATELY);

    // Starting base spirit guides
    _NodeOccupied(BG_DG_SPIRIT_ALIANCE_N, ALLIANCE);
    _NodeOccupied(BG_DG_SPIRIT_ALIANCE_S, ALLIANCE);
    _NodeOccupied(BG_DG_SPIRIT_HORDE_N, HORDE);
    _NodeOccupied(BG_DG_SPIRIT_HORDE_S, HORDE);
}

void Battleground_DG::StartingEventOpenDoors()
{
    // spawn neutral banners
    for (int banner = BG_DG_OBJECT_BANNER_NEUTRAL, i = 0; i < 5; banner += 8, ++i)
        SpawnBGObject(banner, RESPAWN_IMMEDIATELY);
    for (int i = 0; i < BG_DG_BUFFS_COUNT; ++i)
        SpawnBGObject(BG_DG_OBJECT_REGENBUFF_UP + i, RESPAWN_IMMEDIATELY);
    DoorOpen(BG_DG_OBJECT_GATE_A_1);
    DoorOpen(BG_DG_OBJECT_GATE_A_2);
    DoorOpen(BG_DG_OBJECT_GATE_H_1);
    DoorOpen(BG_DG_OBJECT_GATE_H_2);
}

void Battleground_DG::AddPlayer(Player *plr)
{
    Battleground::AddPlayer(plr);
    //create score and add it to map, default values are set in the constructor
    Battleground_DGScore* sc = new Battleground_DGScore(plr->GetGUID(), plr->GetBGTeam());

    PlayerScores[plr->GetGUID()] = sc;
}

void Battleground_DG::RemovePlayer(Player* /*player*/, ObjectGuid /*guid*/, uint32 /*team*/)
{

}

/*  type: 0-neutral, 1-contested, 3-occupied
    teamIndex: 0-ally, 1-horde                        */
void Battleground_DG::_CreateBanner(uint8 node, uint8 type, uint8 teamIndex, bool delay)
{
    // Just put it into the queue
    if (delay)
    {
        m_BannerTimers[node].timer = 2000;
        m_BannerTimers[node].type = type;
        m_BannerTimers[node].teamIndex = teamIndex;
        return;
    }

    uint8 obj = node * BG_DG_OBJECTS_PER_NODE + type + teamIndex;

    SpawnBGObject(obj, RESPAWN_IMMEDIATELY);

    // handle aura with banner
    if (!type)
        return;
    obj = node * BG_DG_OBJECTS_PER_NODE + ((type == BG_DG_NODE_TYPE_OCCUPIED) ? (BG_DG_OBJECT_AURA_ALLY + teamIndex) : BG_DG_OBJECT_AURA_CONTESTED);
    SpawnBGObject(obj, RESPAWN_IMMEDIATELY);
}

void Battleground_DG::_DelBanner(uint8 node, uint8 type, uint8 teamIndex)
{
    uint8 obj = node * BG_DG_OBJECTS_PER_NODE + type + teamIndex;
    SpawnBGObject(obj, RESPAWN_ONE_DAY);

    // handle aura with banner
    if (!type)
        return;
    obj = node * BG_DG_OBJECTS_PER_NODE + ((type == BG_DG_NODE_TYPE_OCCUPIED) ? (BG_DG_OBJECT_AURA_ALLY + teamIndex) : BG_DG_OBJECT_AURA_CONTESTED);
    SpawnBGObject(obj, RESPAWN_ONE_DAY);
}

int32 Battleground_DG::_GetNodeNameId(uint8 node)
{
    switch (node)
    {
        case BG_DG_NODE_PANDAREN:   return LANG_BG_DG_NODE_PANDAREN;
        case BG_DG_NODE_CENTER:     return LANG_BG_DG_NODE_CENTER;
        case BG_DG_NODE_GOBLIN:     return LANG_BG_DG_NODE_GOBLIN;
        default:
            ASSERT(false);
    }

    return 0;
}

void Battleground_DG::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& packet)
{
    // Node icons
    for (uint8 node = 0; node < BG_DG_DYNAMIC_NODES_COUNT; ++node)
        packet.Worldstates.emplace_back(BG_DG_WS_NODEICONS[node], (m_Nodes[node] == 0) ? 1 : 0);

    // Node occupied states
    for (uint8 node = 0; node < BG_DG_DYNAMIC_NODES_COUNT; ++node)
    {
        BG_DG_NodeStatus nodeStatus = (BG_DG_NodeStatus)m_Nodes[node];
        std::map<uint32, uint32> worldStates;
        for (auto ws : BG_DG_WS_NODESTATES[node])
            worldStates[ws] = 0;
        switch (nodeStatus)
        {
            case BG_DG_NODE_STATUS_ALLY_OCCUPIED:
            case BG_DG_NODE_STATUS_HORDE_OCCUPIED:
                worldStates[BG_DG_WS_NODESTATES[node][0]] = nodeStatus == BG_DG_NODE_STATUS_HORDE_OCCUPIED ? 1 : 2;
                break;
            case BG_DG_NODE_STATUS_ALLY_CONTESTED:
                worldStates[BG_DG_WS_NODESTATES[node][1]] = 1;
                break;
            case BG_DG_NODE_STATUS_HORDE_CONTESTED:
                worldStates[BG_DG_WS_NODESTATES[node][2]] = 1;
                break;
        }
        for (auto ws : worldStates)
            packet.Worldstates.emplace_back(ws.first, ws.second);
    }

    // How many bases each team owns
    uint8 ally = 0, horde = 0;
    for (uint8 node = 0; node < BG_DG_DYNAMIC_NODES_COUNT; ++node)
        if (m_Nodes[node] == BG_DG_NODE_STATUS_ALLY_OCCUPIED)
            ++ally;
        else if (m_Nodes[node] == BG_DG_NODE_STATUS_HORDE_OCCUPIED)
            ++horde;

    packet.Worldstates.emplace_back(BG_DG_WS_OCCUPIED_BASES_ALLY, ally);
    packet.Worldstates.emplace_back(BG_DG_WS_OCCUPIED_BASES_HORDE, horde);

    // Team scores
    packet.Worldstates.emplace_back(BG_DG_WS_RESOURCES_ALLY, m_TeamScores[TEAM_ALLIANCE]);
    packet.Worldstates.emplace_back(BG_DG_WS_RESOURCES_HORDE, m_TeamScores[TEAM_HORDE]);

    // show scores
    packet.Worldstates.emplace_back(BG_DG_WS_CART_STATE_HORDE, GetCartState(ALLIANCE));
    packet.Worldstates.emplace_back(BG_DG_WS_CART_STATE_ALLY, GetCartState(HORDE));
}

void Battleground_DG::_SendNodeUpdate(uint8 node)
{
    // Send node owner state update to refresh map icons on client
    if (!m_prevNodes[node])
        UpdateWorldState(BG_DG_WS_NODEICONS[node], 0);

    BG_DG_NodeStatus nodeStatus = (BG_DG_NodeStatus)m_Nodes[node];
    std::map<uint32, uint32> worldStates;
    for (auto ws : BG_DG_WS_NODESTATES[node])
        worldStates[ws] = 0;
    switch (nodeStatus)
    {
        case BG_DG_NODE_STATUS_ALLY_OCCUPIED:
        case BG_DG_NODE_STATUS_HORDE_OCCUPIED:
            worldStates[BG_DG_WS_NODESTATES[node][0]] = nodeStatus == BG_DG_NODE_STATUS_HORDE_OCCUPIED ? 1 : 2;
            break;
        case BG_DG_NODE_STATUS_ALLY_CONTESTED:
            worldStates[BG_DG_WS_NODESTATES[node][1]] = 1;
            break;
        case BG_DG_NODE_STATUS_HORDE_CONTESTED:
            worldStates[BG_DG_WS_NODESTATES[node][2]] = 1;
            break;
    }
    for (auto ws : worldStates)
        UpdateWorldState(ws.first, ws.second);

    // How many bases each team owns
    uint8 ally = 0, horde = 0;
    for (uint8 i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
        if (m_Nodes[i] == BG_DG_NODE_STATUS_ALLY_OCCUPIED)
            ++ally;
        else if (m_Nodes[i] == BG_DG_NODE_STATUS_HORDE_OCCUPIED)
            ++horde;

    UpdateWorldState(BG_DG_WS_OCCUPIED_BASES_ALLY, ally);
    UpdateWorldState(BG_DG_WS_OCCUPIED_BASES_HORDE, horde);
}

void Battleground_DG::_NodeOccupied(uint8 node, Team team)
{
    if (node != BG_DG_NODE_CENTER)
        if (!AddSpiritGuide(node, BG_DG_SpiritGuidePos[node], GetTeamIndexByTeamId(team)))
            TC_LOG_DEBUG("bg.battleground", "Failed to spawn spirit guide! point: %u, team: %u,", node, team);

    uint8 capturedNodes = 0;
    for (uint8 i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
        if (m_Nodes[node] == GetTeamIndexByTeamId(team) + BG_DG_NODE_TYPE_OCCUPIED && !m_NodeTimers[i])
            ++capturedNodes;

    // only dynamic nodes, no start spirit guides points
    if (node >= BG_DG_DYNAMIC_NODES_COUNT)
        return;

    Creature* trigger = GetBGCreature(node + BG_DG_ALL_NODES_COUNT); // 0-6 spirit guides
    if (!trigger)
        trigger = AddCreature(WORLD_TRIGGER, node + BG_DG_ALL_NODES_COUNT, BG_DG_NodePositions[node], GetTeamIndexByTeamId(team));

    // add bonus honor aura trigger creature when node is accupied
    // cast bonus aura (+50% honor in 25yards)
    // aura should only apply to players who have accupied the node, set correct faction for trigger
    if (trigger)
    {
        trigger->setFaction(team == ALLIANCE ? 84 : 83);
        trigger->CastSpell(trigger, SPELL_HONORABLE_DEFENDER_25Y, false);
    }
}

void Battleground_DG::_NodeDeOccupied(uint8 node)
{
    if (node >= BG_DG_DYNAMIC_NODES_COUNT)
        return;

    // remove bonus honor aura trigger creature when node is lost
    // only dynamic nodes, no start spirit guides points
    if (node < BG_DG_DYNAMIC_NODES_COUNT)
        DelCreature(node + BG_DG_ALL_NODES_COUNT); // 0-6 spirit guides

    // Those who are waiting to resurrect at this node are taken to the closest own node's graveyard
    std::vector<ObjectGuid> ghost_list = m_ReviveQueue[BgCreatures[node]];
    if (!ghost_list.empty())
    {
        WorldSafeLocsEntry const *ClosestGrave = NULL;
        for (std::vector<ObjectGuid>::const_iterator itr = ghost_list.begin(); itr != ghost_list.end(); ++itr)
        {
            Player* plr = ObjectAccessor::FindPlayer(*itr);
            if (!plr)
                continue;

            if (!ClosestGrave)
                ClosestGrave = GetClosestGraveYard(plr);

            if (ClosestGrave)
                plr->TeleportTo(GetMapId(), ClosestGrave->Loc.X, ClosestGrave->Loc.Y, ClosestGrave->Loc.Z, plr->GetOrientation());
        }
    }

    if (!BgCreatures[node].IsEmpty())
        DelCreature(node);

    // buff object isn't despawned
}

void Battleground_DG::HandleAreaTrigger(Player * player, uint32 trigger, bool entered)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        case 9302: // Pandaren Mine inside
        case 9303: // Goblin Mine inside
        {
            if (player->isDead())
                break;
            WorldSafeLocsEntry const* grave = GetClosestGraveYard(player);
            if (!grave)
            {
                player->Kill(player);
                break;
            }
            else
                player->TeleportTo(grave->MapID, grave->Loc.X, grave->Loc.Y, grave->Loc.Z, player->GetOrientation());
            break;
        }
        case 9012: // alliance base
        case 9013: // horde base
        {
            TeamId teamIndex = GetTeamIndexByTeamId(player->GetTeam(true));
            bool ally = teamIndex == TEAM_ALLIANCE;
            TeamId enemyTeamIndex = ally ? TEAM_HORDE : TEAM_ALLIANCE;

            // only when going to own base with enemy cart
            if (ally && trigger == 9013 || !ally && trigger == 9012)
                return;

            // only if having enemy cart
            if (!player->HasAura(BG_DG_SPELL_ID_CART_VISUAL[enemyTeamIndex]))
                return;

            // only if cart is nearby
            Creature * cart = player->FindNearestCreature(BG_DG_CREATURE_ID_CART[enemyTeamIndex], 10.0f);
            if (!cart)
                return;

            // respawn enemy cart at base
            SpawnBGObject(BG_DG_OBJECT_CART_A + enemyTeamIndex * 2, RESPAWN_IMMEDIATELY);

            // update cart state
            UpdateCartState(enemyTeamIndex, BG_DG_CART_STATE_CAPTURED);

            // send message
            SendMessage2ToAll(LANG_BG_DG_STOLEN_GOLD, ally ? CHAT_MSG_BG_SYSTEM_ALLIANCE : CHAT_MSG_BG_SYSTEM_HORDE, NULL, ally ? LANG_BG_DG_ALLY : LANG_BG_DG_HORDE, !ally ? LANG_BG_DG_ALLY : LANG_BG_DG_HORDE);

            // score
            UpdatePlayerScore(player, SCORE_CART_CAPTURED, 1);

            // remove cart/visuals
            cart->DespawnOrUnsummon();
            player->RemoveAura(BG_DG_SPELL_ID_CART_VISUAL[enemyTeamIndex]);
            break;
        }
        default:
            Battleground::HandleAreaTrigger(player, trigger, entered);
            break;
    }
}

void Battleground_DG::UpdateCartState(uint32 teamIndex, BG_DG_CartState newState, Player * player)
{
    BG_DG_CartState oldState = cartStates[teamIndex];
    cartPickers[teamIndex] = ObjectGuid::Empty;

    // stolen gold
    switch (newState)
    {
        case BG_DG_CART_STATE_TAKEN:
            if (player)
                cartPickers[teamIndex] = player->GetGUID();
            if (oldState != BG_DG_CART_STATE_AT_BASE)
                break;
            stolenGold[teamIndex] = std::min<int32>(BG_DG_MAX_GOLD_STEAL, m_TeamScores[teamIndex]);
            m_TeamScores[teamIndex] -= stolenGold[teamIndex];
            break;
        case BG_DG_CART_STATE_AT_BASE:
            m_TeamScores[teamIndex] += stolenGold[teamIndex];
            stolenGold[teamIndex] = 0;
            break;
        case BG_DG_CART_STATE_CAPTURED:
            m_TeamScores[teamIndex == TEAM_ALLIANCE ? TEAM_HORDE : TEAM_ALLIANCE] += stolenGold[teamIndex];
            stolenGold[teamIndex] = 0;
            newState = BG_DG_CART_STATE_AT_BASE;
            break;
    }

    UpdateWorldState(BG_DG_WS_ID_CARTS[teamIndex], newState);
    cartStates[teamIndex] = newState;

    UpdateWorldState(BG_DG_WS_RESOURCES_ALLY, m_TeamScores[TEAM_ALLIANCE]);
    UpdateWorldState(BG_DG_WS_RESOURCES_HORDE, m_TeamScores[TEAM_HORDE]);
}

void Battleground_DG::CartDropped(uint32 teamIndex, float x, float y, float z, float o)
{
    bool ally = teamIndex == TEAM_ALLIANCE;
    uint32 objectType = ally ? BG_DG_OBJECT_CART_A_DROPPED : BG_DG_OBJECT_CART_H_DROPPED;
    uint32 objectId = ally ? BG_DG_OBJECTID_CART_A_DROPPED : BG_DG_OBJECTID_CART_H_DROPPED;
    AddObject(objectType, objectId, x, y, z, o, 0.0f, 0.0f, 0.0f, 0.0f);

    SendMessage2ToAll(LANG_BG_DG_CART_DROPPED, ally ? CHAT_MSG_BG_SYSTEM_ALLIANCE : CHAT_MSG_BG_SYSTEM_HORDE, NULL, ally ? LANG_BG_DG_ALLY : LANG_BG_DG_HORDE);

    // update cart state
    UpdateCartState(teamIndex, BG_DG_CART_STATE_DROPPED);
    cartDropTimer[teamIndex] = BG_DG_CART_RETURN_TIMER;
}

void Battleground_DG::SummonCart(Player * source)
{
    TeamId teamIndex = GetTeamIndexByTeamId(source->GetTeam(true));
    TeamId enemyTeamIndex = teamIndex == TEAM_HORDE ? TEAM_ALLIANCE : TEAM_HORDE;

    // summon cart
    source->CastSpell(source, BG_DG_SPELL_ID_SUMMON_CART[enemyTeamIndex], true);

    Creature * cart = source->FindNearestCreature(BG_DG_CREATURE_ID_CART[enemyTeamIndex], 20.0f);
    // something went terribly wrong
    if (!cart)
        return;

    // visuals
    source->CastSpell(source, BG_DG_SPELL_ID_CART_VISUAL[enemyTeamIndex], true);
    cart->CastSpell(cart, BG_DG_SPELL_ID_VISUAL[enemyTeamIndex], true);
    source->CastSpell(cart, BG_DG_SPELL_CHAINS_VISUAL, true);
}

void Battleground_DG::EventPlayerClickedOnCart(Player * source, GameObject * targetObject)
{
    TeamId teamIndex = GetTeamIndexByTeamId(source->GetTeam(true));
    std::map<uint32, TeamId> expectedTeamIndexes;
    expectedTeamIndexes[BG_DG_OBJECTID_CART_A] = TEAM_HORDE;
    expectedTeamIndexes[BG_DG_OBJECTID_CART_H] = TEAM_ALLIANCE;
    // check if player really could click on this cart
    if (expectedTeamIndexes.find(targetObject->GetEntry()) != expectedTeamIndexes.end())
        if (expectedTeamIndexes[targetObject->GetEntry()] != teamIndex)
            return;

    uint32 sound = (teamIndex == TEAM_ALLIANCE) ? BG_DG_SOUND_NODE_ASSAULTED_ALLIANCE : BG_DG_SOUND_NODE_ASSAULTED_HORDE;
    uint32 objectType = 0;
    bool canDelete = false;
    switch (targetObject->GetEntry())
    {
        case BG_DG_OBJECTID_CART_A:
            objectType = BG_DG_OBJECT_CART_A;
            SendMessage2ToAll(LANG_BG_DG_CART_STOLEN, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, LANG_BG_DG_ALLY);

            // update cart state
            UpdateCartState(TEAM_ALLIANCE, BG_DG_CART_STATE_TAKEN, source);

            // dismount
            source->Dismount();
            source->RemoveAurasByType(SPELL_AURA_MOUNTED);

            // summon cart
            SummonCart(source);
            break;
        case BG_DG_OBJECTID_CART_A_DROPPED:
            objectType = BG_DG_OBJECT_CART_A_DROPPED;
            canDelete = true;

            // return to base
            if (teamIndex == TEAM_ALLIANCE)
            {
                // respawn cart at base
                SpawnBGObject(BG_DG_OBJECT_CART_A, RESPAWN_IMMEDIATELY);

                // send message
                SendMessage2ToAll(LANG_BG_DG_CART_RETURNED_BY, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, LANG_BG_DG_ALLY);

                // update cart state
                UpdateCartState(TEAM_ALLIANCE, BG_DG_CART_STATE_AT_BASE);

                // score
                UpdatePlayerScore(source, SCORE_CART_RETURNED, 1);
            }
            // recapture
            else
            {
                // send message
                SendMessage2ToAll(LANG_BG_DG_CART_STOLEN, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, LANG_BG_DG_ALLY);

                // update cart state
                UpdateCartState(TEAM_ALLIANCE, BG_DG_CART_STATE_TAKEN, source);

                // dismount
                source->Dismount();
                source->RemoveAurasByType(SPELL_AURA_MOUNTED);

                // summon cart
                SummonCart(source);
            }
            break;
        case BG_DG_OBJECTID_CART_H:
            objectType = BG_DG_OBJECT_CART_H;
            SendMessage2ToAll(LANG_BG_DG_CART_STOLEN, CHAT_MSG_BG_SYSTEM_HORDE, source, LANG_BG_DG_HORDE);

            // update cart state
            UpdateCartState(TEAM_HORDE, BG_DG_CART_STATE_TAKEN, source);

            // dismount
            source->Dismount();
            source->RemoveAurasByType(SPELL_AURA_MOUNTED);

            // summon cart
            SummonCart(source);
            break;
        case BG_DG_OBJECTID_CART_H_DROPPED:
            objectType = BG_DG_OBJECT_CART_H_DROPPED;
            canDelete = true;

            // return to base
            if (teamIndex == TEAM_HORDE)
            {
                // respawn cart at base
                SpawnBGObject(BG_DG_OBJECT_CART_H, RESPAWN_IMMEDIATELY);

                // send message
                SendMessage2ToAll(LANG_BG_DG_CART_RETURNED_BY, CHAT_MSG_BG_SYSTEM_HORDE, source, LANG_BG_DG_HORDE);

                // update cart state
                UpdateCartState(TEAM_HORDE, BG_DG_CART_STATE_AT_BASE);

                // score
                UpdatePlayerScore(source, SCORE_CART_RETURNED, 1);
            }
            // recapture
            else
            {
                // send message
                SendMessage2ToAll(LANG_BG_DG_CART_STOLEN, CHAT_MSG_BG_SYSTEM_HORDE, source, LANG_BG_DG_HORDE);

                // update cart state
                UpdateCartState(TEAM_HORDE, BG_DG_CART_STATE_TAKEN, source);

                // dismount
                source->Dismount();
                source->RemoveAurasByType(SPELL_AURA_MOUNTED);

                // summon cart
                SummonCart(source);
            }
            break;
        default:
            return;
    }


    // permanently despawn (dropped carts)
    if (canDelete)
    {
        targetObject->Delete();
        BgObjects[objectType].Clear();
    }
    else
        SpawnBGObject(objectType, RESPAWN_ONE_DAY);

    if (sound)
        PlaySoundToAll(sound);
}

/* Invoked if a player used a banner as a gameobject */
void Battleground_DG::EventPlayerClickedOnFlag(Player * source, GameObject * targetObject)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    // carts handling
    switch (targetObject->GetEntry())
    {
        case BG_DG_OBJECTID_CART_A:
        case BG_DG_OBJECTID_CART_A_DROPPED:
        case BG_DG_OBJECTID_CART_H:
        case BG_DG_OBJECTID_CART_H_DROPPED:
            EventPlayerClickedOnCart(source, targetObject);
            return;
    }

    uint8 node = BG_DG_NODE_PANDAREN;
    GameObject* obj = GetBgMap()->GetGameObject(BgObjects[node * BG_DG_OBJECTS_PER_NODE + BG_DG_OBJECT_AURA_CONTESTED]);
    while ((node < BG_DG_DYNAMIC_NODES_COUNT) && ((!obj) || (!source->IsWithinDistInMap(obj, 10))))
    {
        ++node;
        obj = GetBgMap()->GetGameObject(BgObjects[node * BG_DG_OBJECTS_PER_NODE + BG_DG_OBJECT_AURA_CONTESTED]);
    }

    if (node == BG_DG_DYNAMIC_NODES_COUNT)
    {
        TC_LOG_DEBUG("bg.battleground", "Player %s (GUID: %u) in Deepwind Gorge fired EventPlayerClickedOnFlag() but isnt near of any flag", source->GetName().c_str(), source->GetGUIDLow());
        // this means our player isn't close to any of banners - maybe cheater ??
        return;
    }

    TeamId teamIndex = GetTeamIndexByTeamId(source->GetTeam(true));

    // Check if player really could use this banner, not cheated
    if (!(m_Nodes[node] == 0 || teamIndex == m_Nodes[node] % 2))
        return;

    source->RemoveAurasWithInterruptFlags(AURA_INTERRUPT_FLAG_ENTER_PVP_COMBAT);
    uint32 sound = 0;

    // If node is neutral, change to contested
    if (m_Nodes[node] == BG_DG_NODE_TYPE_NEUTRAL)
    {
        UpdatePlayerScore(source, SCORE_MINE_ASSAULTED, 1);
        m_prevNodes[node] = m_Nodes[node];
        m_Nodes[node] = teamIndex + 1;
        // burn current neutral banner
        _DelBanner(node, BG_DG_NODE_TYPE_NEUTRAL, 0);
        // create new contested banner
        _CreateBanner(node, BG_DG_NODE_TYPE_CONTESTED, teamIndex, true);
        _SendNodeUpdate(node);
        m_NodeTimers[node] = BG_DG_FLAG_CAPTURING_TIME;

        // FIXME: team and node names not localized
        if (teamIndex == 0)
            SendMessage2ToAll(LANG_BG_DG_NODE_CLAIMED, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, _GetNodeNameId(node), LANG_BG_DG_ALLY);
        else
            SendMessage2ToAll(LANG_BG_DG_NODE_CLAIMED, CHAT_MSG_BG_SYSTEM_HORDE, source, _GetNodeNameId(node), LANG_BG_DG_HORDE);

        sound = BG_DG_SOUND_NODE_CLAIMED;
    }
    // If node is contested
    else if ((m_Nodes[node] == BG_DG_NODE_STATUS_ALLY_CONTESTED) || (m_Nodes[node] == BG_DG_NODE_STATUS_HORDE_CONTESTED))
    {
        // If last state is NOT occupied, change node to enemy-contested
        if (m_prevNodes[node] < BG_DG_NODE_TYPE_OCCUPIED)
        {
            UpdatePlayerScore(source, SCORE_MINE_ASSAULTED, 1);
            m_prevNodes[node] = m_Nodes[node];
            m_Nodes[node] = teamIndex + BG_DG_NODE_TYPE_CONTESTED;
            // burn current contested banner
            _DelBanner(node, BG_DG_NODE_TYPE_CONTESTED, !teamIndex);
            // create new contested banner
            _CreateBanner(node, BG_DG_NODE_TYPE_CONTESTED, teamIndex, true);
            _SendNodeUpdate(node);
            m_NodeTimers[node] = BG_DG_FLAG_CAPTURING_TIME;

            // FIXME: node names not localized
            if (teamIndex == TEAM_ALLIANCE)
                SendMessage2ToAll(LANG_BG_DG_NODE_ASSAULTED, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, _GetNodeNameId(node));
            else
                SendMessage2ToAll(LANG_BG_DG_NODE_ASSAULTED, CHAT_MSG_BG_SYSTEM_HORDE, source, _GetNodeNameId(node));
        }
        // If contested, change back to occupied
        else
        {
            UpdatePlayerScore(source, SCORE_MINE_DEFENDED, 1);
            m_prevNodes[node] = m_Nodes[node];
            m_Nodes[node] = teamIndex + BG_DG_NODE_TYPE_OCCUPIED;
            // burn current contested banner
            _DelBanner(node, BG_DG_NODE_TYPE_CONTESTED, !teamIndex);
            // create new occupied banner
            _CreateBanner(node, BG_DG_NODE_TYPE_OCCUPIED, teamIndex, true);
            _SendNodeUpdate(node);
            m_NodeTimers[node] = 0;
            _NodeOccupied(node, (teamIndex == TEAM_ALLIANCE) ? ALLIANCE : HORDE);

            // FIXME: node names not localized
            if (teamIndex == TEAM_ALLIANCE)
                SendMessage2ToAll(LANG_BG_DG_NODE_DEFENDED, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, _GetNodeNameId(node));
            else
                SendMessage2ToAll(LANG_BG_DG_NODE_DEFENDED, CHAT_MSG_BG_SYSTEM_HORDE, source, _GetNodeNameId(node));
        }
        sound = (teamIndex == TEAM_ALLIANCE) ? BG_DG_SOUND_NODE_ASSAULTED_ALLIANCE : BG_DG_SOUND_NODE_ASSAULTED_HORDE;
    }
    // If node is occupied, change to enemy-contested
    else
    {
        UpdatePlayerScore(source, SCORE_MINE_ASSAULTED, 1);
        m_prevNodes[node] = m_Nodes[node];
        m_Nodes[node] = teamIndex + BG_DG_NODE_TYPE_CONTESTED;
        // burn current occupied banner
        _DelBanner(node, BG_DG_NODE_TYPE_OCCUPIED, !teamIndex);
        // create new contested banner
        _CreateBanner(node, BG_DG_NODE_TYPE_CONTESTED, teamIndex, true);
        _SendNodeUpdate(node);
        _NodeDeOccupied(node);
        m_NodeTimers[node] = BG_DG_FLAG_CAPTURING_TIME;

        // FIXME: node names not localized
        if (teamIndex == TEAM_ALLIANCE)
            SendMessage2ToAll(LANG_BG_DG_NODE_ASSAULTED, CHAT_MSG_BG_SYSTEM_ALLIANCE, source, _GetNodeNameId(node));
        else
            SendMessage2ToAll(LANG_BG_DG_NODE_ASSAULTED, CHAT_MSG_BG_SYSTEM_HORDE, source, _GetNodeNameId(node));

        sound = (teamIndex == TEAM_ALLIANCE) ? BG_DG_SOUND_NODE_ASSAULTED_ALLIANCE : BG_DG_SOUND_NODE_ASSAULTED_HORDE;
    }

    // If node is occupied again, send "X has taken the Y" msg.
    if (m_Nodes[node] >= BG_DG_NODE_TYPE_OCCUPIED)
    {
        // FIXME: team and node names not localized
        if (teamIndex == TEAM_ALLIANCE)
            SendMessage2ToAll(LANG_BG_DG_NODE_TAKEN, CHAT_MSG_BG_SYSTEM_ALLIANCE, NULL, LANG_BG_DG_ALLY, _GetNodeNameId(node));
        else
            SendMessage2ToAll(LANG_BG_DG_NODE_TAKEN, CHAT_MSG_BG_SYSTEM_HORDE, NULL, LANG_BG_DG_HORDE, _GetNodeNameId(node));
    }

    PlaySoundToAll(sound);
}

bool Battleground_DG::SetupBattleground()
{
    // banners
    for (int i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
    {
        if (   !AddObject(BG_DG_OBJECT_BANNER_NEUTRAL + 8 * i, BG_DG_OBJECTID_NODE_BANNER[i], BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_BANNER_CONT_A + 8 * i, BG_DG_OBJECTID_BANNER_CONT_A, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_BANNER_CONT_H + 8 * i, BG_DG_OBJECTID_BANNER_CONT_H, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_BANNER_ALLY + 8 * i, BG_DG_OBJECTID_BANNER_A, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_BANNER_HORDE + 8 * i, BG_DG_OBJECTID_BANNER_H, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_AURA_ALLY + 8 * i, BG_DG_OBJECTID_AURA_A, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_AURA_HORDE + 8 * i, BG_DG_OBJECTID_AURA_H, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            || !AddObject(BG_DG_OBJECT_AURA_CONTESTED + 8 * i, BG_DG_OBJECTID_AURA_C, BG_DG_NodePositions[i], 0, 0, sin(BG_DG_NodePositions[i].GetOrientation()), cos(BG_DG_NodePositions[i].GetOrientation()), RESPAWN_ONE_DAY)
            )
        {
            TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn some object Battleground not created!");
            return false;
        }
    }

    // visual capture points
    for (int i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
        if (!AddCreature(BG_DG_CREATURE_ID_CAPTURE_POINT, BG_DG_CREATURE_NODES_CAPTURE + i, BG_DG_NodePositions[i]))
        {
            TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn some creature Battleground not created!");
            return false;
        }

    // gates
    if (   !AddObject(BG_DG_OBJECT_GATE_A_1, BG_DG_OBJECTID_GATE_A_1, BG_DG_DoorPositions[0][0], BG_DG_DoorPositions[0][1], BG_DG_DoorPositions[0][2], BG_DG_DoorPositions[0][3], BG_DG_DoorPositions[0][4], BG_DG_DoorPositions[0][5], BG_DG_DoorPositions[0][6], BG_DG_DoorPositions[0][7], RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_GATE_A_2, BG_DG_OBJECTID_GATE_A_2, BG_DG_DoorPositions[1][0], BG_DG_DoorPositions[1][1], BG_DG_DoorPositions[1][2], BG_DG_DoorPositions[1][3], BG_DG_DoorPositions[1][4], BG_DG_DoorPositions[1][5], BG_DG_DoorPositions[1][6], BG_DG_DoorPositions[1][7], RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_GATE_H_1, BG_DG_OBJECTID_GATE_H_1, BG_DG_DoorPositions[2][0], BG_DG_DoorPositions[2][1], BG_DG_DoorPositions[2][2], BG_DG_DoorPositions[2][3], BG_DG_DoorPositions[2][4], BG_DG_DoorPositions[2][5], BG_DG_DoorPositions[2][6], BG_DG_DoorPositions[2][7], RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_GATE_H_2, BG_DG_OBJECTID_GATE_H_2, BG_DG_DoorPositions[3][0], BG_DG_DoorPositions[3][1], BG_DG_DoorPositions[3][2], BG_DG_DoorPositions[3][3], BG_DG_DoorPositions[3][4], BG_DG_DoorPositions[3][5], BG_DG_DoorPositions[3][6], BG_DG_DoorPositions[3][7], RESPAWN_IMMEDIATELY)
        )
    {
        TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn door object Battleground not created!");
        return false;
    }

    // mines doors
    if (   !AddObject(BG_DG_OBJECT_COLLISION_GATE_1, BG_DG_OBJECTID_MINE_GATE, BG_DG_MineDoorsPos[0], 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_COLLISION_GATE_2, BG_DG_OBJECTID_MINE_GATE, BG_DG_MineDoorsPos[1], 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_COLLISION_WALL_1, BG_DG_OBJECTID_MINE_GATE_COLLISION, BG_DG_MineDoorsPos[0], 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_DG_OBJECT_COLLISION_WALL_2, BG_DG_OBJECTID_MINE_GATE_COLLISION, BG_DG_MineDoorsPos[1], 0, 0, 0, 0, RESPAWN_IMMEDIATELY)
        )
    {
        TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn mines door object Battleground not created!");
        return false;
    }

    // carts
    for (int teamIndex = 0; teamIndex < 2; teamIndex++)
        if (!AddObject(BG_DG_OBJECT_CART_A + teamIndex * 2, BG_DG_OBJECTID_CART_A + teamIndex * 2, BG_DG_CartPos[teamIndex], 0, 0, 0, 0, RESPAWN_ONE_DAY))
    {
        TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn cart object!");
        return false;
    }

    // buffs
    if (   !AddObject(BG_DG_OBJECT_REGENBUFF_UP, Buff_Entries[1], BG_DG_BuffPositions[0], 0, 0, sin(BG_DG_BuffPositions[0].GetOrientation()), cos(BG_DG_BuffPositions[0].GetOrientation()), RESPAWN_ONE_DAY)
        || !AddObject(BG_DG_OBJECT_REGENBUFF_DOWN, Buff_Entries[1], BG_DG_BuffPositions[1], 0, 0, sin(BG_DG_BuffPositions[1].GetOrientation()), cos(BG_DG_BuffPositions[1].GetOrientation()), RESPAWN_ONE_DAY)
        || !AddObject(BG_DG_OBJECT_BERSERKBUFF_UP, Buff_Entries[2], BG_DG_BuffPositions[2], 0, 0, sin(BG_DG_BuffPositions[2].GetOrientation()), cos(BG_DG_BuffPositions[2].GetOrientation()), RESPAWN_ONE_DAY)
        || !AddObject(BG_DG_OBJECT_BERSERKBUFF_DOWN, Buff_Entries[2], BG_DG_BuffPositions[3], 0, 0, sin(BG_DG_BuffPositions[3].GetOrientation()), cos(BG_DG_BuffPositions[3].GetOrientation()), RESPAWN_ONE_DAY)
        )
    {
        TC_LOG_DEBUG("bg.battleground", "BatteGroundDG: Failed to spawn buff object!");
        return false;
    }

    return true;
}

void Battleground_DG::Reset()
{
    // call parent's class reset
    Battleground::Reset();

    for (int teamIndex = 0; teamIndex < BG_TEAMS_COUNT; teamIndex++)
    {
        m_TeamScores[teamIndex] = 0;
        m_lastTick[teamIndex] = 0;
        m_HonorScoreTics[teamIndex] = 0;
    }

    m_IsInformedNearVictory = false;
    bool isBGWeekend = sBattlegroundMgr->IsBGWeekend(GetTypeID());
    m_HonorTics = (isBGWeekend) ? BG_DG_BGBGWeekendHonorTicks : BG_DG_NotBGBGWeekendHonorTicks;

    for (uint8 i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
    {
        m_Nodes[i] = 0;
        m_prevNodes[i] = 0;
        m_NodeTimers[i] = 0;
        m_BannerTimers[i].timer = 0;
    }

    for (uint8 i = 0; i < BG_DG_CREATURE_MAX_CREATURES; ++i)
        if (!BgCreatures[i].IsEmpty())
            DelCreature(i);
}

void Battleground_DG::EndBattleground(uint32 winner)
{
    // win reward
    if (winner == ALLIANCE)
        RewardHonorToTeam(GetBonusHonorFromKill(1), ALLIANCE);
    if (winner == HORDE)
        RewardHonorToTeam(GetBonusHonorFromKill(1), HORDE);

    // complete map_end rewards (even if no team wins)
    RewardHonorToTeam(GetBonusHonorFromKill(1), HORDE);
    RewardHonorToTeam(GetBonusHonorFromKill(1), ALLIANCE);

    Battleground::EndBattleground(winner);
}

WorldSafeLocsEntry const* Battleground_DG::GetClosestGraveYard(Player* player)
{
    TeamId teamIndex = GetTeamIndexByTeamId(player->GetTeam(true));

    // Is there any occupied node for this team?
    std::vector<uint8> nodes;
    for (uint8 i = 0; i < BG_DG_DYNAMIC_NODES_COUNT; ++i)
        if (m_Nodes[i] == teamIndex + 3)
            nodes.push_back(i);

    WorldSafeLocsEntry const* good_entry = NULL;
    // If so, select the closest node to place ghost on
    if (!nodes.empty())
    {
        float plr_x = player->GetPositionX();
        float plr_y = player->GetPositionY();

        float mindist = 999999.0f;
        for (uint8 i = 0; i < nodes.size(); ++i)
        {
            WorldSafeLocsEntry const*entry = sWorldSafeLocsStore.LookupEntry(BG_DG_GraveyardIds[nodes[i]]);
            if (!entry)
                continue;
            float dist = (entry->Loc.X - plr_x)*(entry->Loc.X - plr_x) + (entry->Loc.Y - plr_y)*(entry->Loc.Y - plr_y);
            if (mindist > dist)
            {
                mindist = dist;
                good_entry = entry;
            }
        }
        nodes.clear();
    }
    // If not, place ghost on starting location
    if (!good_entry)
    {
        nodes.push_back(BG_DG_SPIRIT_ALIANCE_N + teamIndex * 2);
        nodes.push_back(BG_DG_SPIRIT_ALIANCE_N + 1 + teamIndex * 2);

        float plr_x = player->GetPositionX();
        float plr_y = player->GetPositionY();

        float mindist = 999999.0f;
        for (uint8 i = 0; i < nodes.size(); ++i)
        {
            WorldSafeLocsEntry const*entry = sWorldSafeLocsStore.LookupEntry(BG_DG_GraveyardIds[nodes[i]]);
            if (!entry)
                continue;
            float dist = (entry->Loc.X - plr_x)*(entry->Loc.X - plr_x) + (entry->Loc.Y - plr_y)*(entry->Loc.Y - plr_y);
            if (mindist > dist)
            {
                mindist = dist;
                good_entry = entry;
            }
        }
        nodes.clear();
    }

    return good_entry;
}

bool Battleground_DG::UpdatePlayerScore(Player *Source, uint32 type, uint32 value, bool doAddHonor)
{
    if (!Battleground::UpdatePlayerScore(Source, type, value, doAddHonor))
        return false;

    switch (type)
    {
        case SCORE_MINE_ASSAULTED:
            Source->UpdateCriteria(CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE, BG_DG_OBJECTIVE_ASSAULT_MINE);
            break;
        case SCORE_MINE_DEFENDED:
            Source->UpdateCriteria(CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE, BG_DG_OBJECTIVE_DEFEND_MINE);
            break;
        case SCORE_CART_CAPTURED:
            Source->UpdateCriteria(CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE, BG_DG_OBJECTIVE_CAPTURE_CART);
            break;
        case SCORE_CART_RETURNED:
            Source->UpdateCriteria(CRITERIA_TYPE_BG_OBJECTIVE_CAPTURE, BG_DG_OBJECTIVE_RESTORE_CART);
            break;
        default:
            break;
    }

    return true;
}

// 71073 / 71071 - Horde / Alliance Mine Cart
class project_npc_battleground_deepwind_gorge_mine_cart : public CreatureScript
{
    public:
        project_npc_battleground_deepwind_gorge_mine_cart() : CreatureScript("project_npc_battleground_deepwind_gorge_mine_cart") { }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new project_npc_battleground_deepwind_gorge_mine_cartAI(creature);
        }

        struct project_npc_battleground_deepwind_gorge_mine_cartAI : public PassiveAI
        {
            project_npc_battleground_deepwind_gorge_mine_cartAI(Creature* creature) : PassiveAI(creature) { }

            EventMap events;
            int32 teamIndex;
            Battleground * bg;

            void Reset()
            {
                bg = NULL;
                teamIndex = -1;
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                me->SetHealth(99999999);

                events.Reset();
                events.ScheduleEvent(1, 200);

                me->SetSpeed(MOVE_WALK, 7.0f);
                me->SetSpeed(MOVE_RUN, 7.0f);
            }

            void UpdateAI(uint32 diff)
            {
                events.Update(diff);

                if (teamIndex == -1)
                    if (Unit * owner = me->GetOwner())
                        if (Player * player = owner->ToPlayer())
                        {
                            bg = player->GetBattleground();
                            if (!bg)
                                return;
                            me->GetMotionMaster()->MoveChase(player, 2.0f);
                            teamIndex = bg->GetTeamIndexByTeamId(player->GetTeam(true)) == TEAM_HORDE ? TEAM_ALLIANCE : TEAM_HORDE;
                        }

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case 1:
                            if (!bg || teamIndex == -1)
                            {
                                events.ScheduleEvent(eventId, 200);
                                break;
                            }

                            Unit * owner = me->GetOwner();
                            bool dropCart = false;
                            if (!owner || owner->isDead())
                                dropCart = true;
                            if (!dropCart)
                            {
                            std::vector<uint32> restrictedAuraTypes;
                            restrictedAuraTypes.push_back(SPELL_AURA_MOD_STEALTH);
                            restrictedAuraTypes.push_back(SPELL_AURA_MOD_INVISIBILITY);
                            restrictedAuraTypes.push_back(SPELL_AURA_MOUNTED);
                            for (auto auraType : restrictedAuraTypes)
                                if (owner->HasAuraType((AuraType)auraType))
                                {
                                    dropCart = true;
                                    break;
                                }
                            }
                            if (dropCart)
                            {
                                ((Battleground_DG*)bg)->CartDropped(teamIndex, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), me->GetOrientation());
                                me->DespawnOrUnsummon(0);
                                if (owner)
                                {
                                    owner->RemoveAura(BG_DG_SPELL_ALLIANCE_CART_VISUAL);
                                    owner->RemoveAura(BG_DG_SPELL_HORDE_CART_VISUAL);
                                }
                                return;
                            }
                            events.ScheduleEvent(eventId, 200);
                            break;
                    }
                }
            }
        };
};

ObjectGuid Battleground_DG::GetFlagPickerGUID(int32 teamIndex) const
{
    if (teamIndex >= 0 && teamIndex < BG_TEAMS_COUNT)
        return cartPickers[teamIndex];

    return ObjectGuid::Empty;
}

void Battleground_DG::GetPlayerPositionData(std::vector<WorldPackets::Battleground::BattlegroundPlayerPosition>* positions) const
{
    for (int teamIndex = 0; teamIndex < BG_TEAMS_COUNT; teamIndex++)
    {
        ObjectGuid guid = GetFlagPickerGUID(teamIndex);
        if (guid.IsEmpty())
            continue;
        if (Player * player = ObjectAccessor::FindPlayer(guid))
        {
            WorldPackets::Battleground::BattlegroundPlayerPosition position;
            position.Guid = player->GetGUID();
            position.Pos = Position(player->GetPositionX(), player->GetPositionY());
            position.IconID = player->GetTeam(true) == HORDE ? PLAYER_POSITION_ICON_ALLIANCE_FLAG : PLAYER_POSITION_ICON_HORDE_FLAG;
            position.ArenaSlot = teamIndex + 1;
            positions->push_back(position);
        }
    }
}

TC_GAME_API void AddSC_project_scripts_battleground_deepwind_gorge()
{
    new project_npc_battleground_deepwind_gorge_mine_cart;
}
