#ifndef __Battleground_DG_H
#define __Battleground_DG_H

#include "Battleground.h"

#define BG_DG_MAX_GOLD_STEAL 200

enum BG_DG_WorldStates
{
    BG_DG_WS_GOLD_ALLIANCE              = 7880,
    BG_DG_WS_GOLD_HORDE                 = 7881,

    BG_DG_WS_OCCUPIED_BASES_HORDE       = 8231,
    BG_DG_WS_OCCUPIED_BASES_ALLY        = 8230,
    BG_DG_WS_RESOURCES_ALLY             = 7880,
    BG_DG_WS_RESOURCES_HORDE            = 7881,
    BG_DG_WS_CART_STATE_HORDE           = 7887,
    BG_DG_WS_CART_STATE_ALLY            = 7904,
};

enum BG_DG_CartState
{
    BG_DG_CART_STATE_AT_BASE    = 1,
    BG_DG_CART_STATE_TAKEN      = 2,
    BG_DG_CART_STATE_DROPPED    = 3,
    BG_DG_CART_STATE_CAPTURED   = 4,
};

enum BG_DG_CreatureTypes
{
    BG_DG_CREATURE_ID_CAPTURE_POINT     = 53194,
    BG_DG_CART_HORDE                    = 71073,
    BG_DG_CART_ALLIANCE                 = 71071,
    BG_DG_PEASANT_HORDE_SACK            = 71049,
    BG_DG_PEASANT_HORDE_EMPTY           = 71597,
    BG_DG_PEASANT_ALLIANCE_SACK         = 71050,
    BG_DG_PEASANT_ALLIANCE_EMPTY        = 71091,
};

enum BG_DG_CreatureModels
{
    BG_DG_PEASANT_MODEL_HORDE_EMPTY     = 49054,
    BG_DG_PEASANT_MODEL_ALLIANCE_EMPTY  = 11354,
};

#define BG_DG_OBJECTS_PER_NODE 8
enum BG_DG_ObjectType
{
    // for all 3 node points 8 * 3 = 24 objects
    BG_DG_OBJECT_BANNER_NEUTRAL          = 0,
    BG_DG_OBJECT_BANNER_CONT_A           = 1,
    BG_DG_OBJECT_BANNER_CONT_H           = 2,
    BG_DG_OBJECT_BANNER_ALLY             = 3,
    BG_DG_OBJECT_BANNER_HORDE            = 4,
    BG_DG_OBJECT_AURA_ALLY               = 5,
    BG_DG_OBJECT_AURA_HORDE              = 6,
    BG_DG_OBJECT_AURA_CONTESTED          = 7,
    //gates
    BG_DG_OBJECT_GATE_A_1                = 24,
    BG_DG_OBJECT_GATE_A_2,
    BG_DG_OBJECT_GATE_H_1,
    BG_DG_OBJECT_GATE_H_2,
    // carts
    BG_DG_OBJECT_CART_A,
    BG_DG_OBJECT_CART_A_DROPPED,
    BG_DG_OBJECT_CART_H,
    BG_DG_OBJECT_CART_H_DROPPED,
    //buffs
    BG_DG_OBJECT_REGENBUFF_UP,
    BG_DG_OBJECT_REGENBUFF_DOWN,
    BG_DG_OBJECT_BERSERKBUFF_UP,
    BG_DG_OBJECT_BERSERKBUFF_DOWN,
    // misc
    BG_DG_OBJECT_COLLISION_WALL_1,
    BG_DG_OBJECT_COLLISION_WALL_2,
    BG_DG_OBJECT_COLLISION_GATE_1,
    BG_DG_OBJECT_COLLISION_GATE_2,
    BG_DG_OBJECT_MAX,
};

/* Object id templates from DB */
enum BG_DG_ObjectTypes
{
    BG_DG_OBJECTID_BANNER_A             = 180058,
    BG_DG_OBJECTID_BANNER_CONT_A        = 180059,
    BG_DG_OBJECTID_BANNER_H             = 180060,
    BG_DG_OBJECTID_BANNER_CONT_H        = 180061,

    BG_DG_OBJECTID_NODE_PANDAREN        = 208782,
    BG_DG_OBJECTID_NODE_CENTER          = 208783,
    BG_DG_OBJECTID_NODE_GOBLIN          = 208784,

    BG_DG_OBJECTID_AURA_A               = 180100,
    BG_DG_OBJECTID_AURA_H               = 180101,
    BG_DG_OBJECTID_AURA_C               = 180102,

    BG_DG_OBJECTID_GATE_A_1             = 220160,
    BG_DG_OBJECTID_GATE_A_2             = 220159,
    BG_DG_OBJECTID_GATE_H_1             = 220161,
    BG_DG_OBJECTID_GATE_H_2             = 220366,

    BG_DG_OBJECTID_CART_A               = 220164,
    BG_DG_OBJECTID_CART_A_DROPPED       = 220165,
    BG_DG_OBJECTID_CART_H               = 220166,
    BG_DG_OBJECTID_CART_H_DROPPED       = 220174,

    BG_DG_OBJECTID_MINE_GATE            = 211981,
    BG_DG_OBJECTID_MINE_GATE_COLLISION  = 194323,
};

enum BG_DG_Timers
{
    BG_DG_FLAG_CAPTURING_TIME           = 60 * IN_MILLISECONDS,
    BG_DG_PEASANTS_SPAWN_TIMER          = 3 * IN_MILLISECONDS,
    BG_DG_CART_RETURN_TIMER             = 10 * IN_MILLISECONDS,
};

enum BG_DG_Score
{
    BG_DG_WARNING_NEAR_VICTORY_SCORE    = 1400,
    BG_DG_MAX_TEAM_SCORE                = 1500
};

/* do NOT change the order, else wrong behaviour */
enum BG_DG_BattlegroundNodes
{
    BG_DG_NODE_PANDAREN         = 0,
    BG_DG_NODE_CENTER           = 1,
    BG_DG_NODE_GOBLIN           = 2,

    BG_DG_DYNAMIC_NODES_COUNT   = 3,                        // dynamic nodes that can be captured

    BG_DG_SPIRIT_ALIANCE_N      = 3,
    BG_DG_SPIRIT_ALIANCE_S      = 4,
    BG_DG_SPIRIT_HORDE_N        = 5,
    BG_DG_SPIRIT_HORDE_S        = 6,

    BG_DG_ALL_NODES_COUNT       = 7,                        // all nodes (dynamic and static)
};

enum BG_DG_CreatureType
{
    BG_DG_CREATURE_NODES_START      = 0, // 3-6 spirit guides
    BG_DG_CREATURE_NODES_AURAS      = BG_DG_ALL_NODES_COUNT, // nodes visual auras
    BG_DG_CREATURE_NODES_CAPTURE    = BG_DG_CREATURE_NODES_AURAS + BG_DG_ALL_NODES_COUNT, // capture auras
    BG_DG_CREATURE_PEASANT_A        = BG_DG_CREATURE_NODES_CAPTURE + BG_DG_DYNAMIC_NODES_COUNT,
    BG_DG_CREATURE_PEASANT_H        = BG_DG_CREATURE_PEASANT_A + BG_DG_DYNAMIC_NODES_COUNT,

    BG_DG_DUMMY                     = BG_DG_CREATURE_PEASANT_H + BG_DG_DYNAMIC_NODES_COUNT,

    BG_DG_CREATURE_MAX_CREATURES,
};


const uint32 BG_DG_WS_NODESTATES[BG_DG_DYNAMIC_NODES_COUNT][3] = { 
    { 7855, 7857, 7861 }, // Pandaren
    { 7932, 7934, 7936 }, // Center
    { 7856, 7864, 7865 }, // Goblin
};
const uint32 BG_DG_WS_NODEICONS[BG_DG_DYNAMIC_NODES_COUNT] = { 7935, 7939, 7938 };

extern uint32 BG_DG_OBJECTID_NODE_BANNER[BG_DG_DYNAMIC_NODES_COUNT];

enum BG_DG_Spells
{
    BG_DG_SPELL_ALLIANCE_CART_VISUAL    = 140876,
    BG_DG_SPELL_HORDE_CART_VISUAL       = 141210,
    BG_DG_SPELL_ALLIANCE_VISUAL         = 141551,
    BG_DG_SPELL_HORDE_VISUAL            = 141555,
    BG_DG_SPELL_CHAINS_VISUAL           = 141553,
    BG_DG_SPELL_SUMMON_CART_ALLIANCE    = 141554,
    BG_DG_SPELL_SUMMON_CART_HORDE       = 141550,
};

enum BG_DG_NodeStatus
{
    BG_DG_NODE_TYPE_NEUTRAL             = 0,
    BG_DG_NODE_TYPE_CONTESTED           = 1,
    BG_DG_NODE_STATUS_ALLY_CONTESTED    = 1,
    BG_DG_NODE_STATUS_HORDE_CONTESTED   = 2,
    BG_DG_NODE_TYPE_OCCUPIED            = 3,
    BG_DG_NODE_STATUS_ALLY_OCCUPIED     = 3,
    BG_DG_NODE_STATUS_HORDE_OCCUPIED    = 4
};

enum BG_DG_Sounds
{
    BG_DG_SOUND_NODE_CLAIMED            = 8192,
    BG_DG_SOUND_NODE_CAPTURED_ALLIANCE  = 8173,
    BG_DG_SOUND_NODE_CAPTURED_HORDE     = 8213,
    BG_DG_SOUND_NODE_ASSAULTED_ALLIANCE = 8212,
    BG_DG_SOUND_NODE_ASSAULTED_HORDE    = 8174,
    BG_DG_SOUND_NEAR_VICTORY            = 8456
};

enum BG_DG_Objectives
{
    BG_DG_OBJECTIVE_ASSAULT_MINE        = 459,
    BG_DG_OBJECTIVE_DEFEND_MINE         = 460,
    BG_DG_OBJECTIVE_CAPTURE_CART        = 457,
    BG_DG_OBJECTIVE_RESTORE_CART        = 458,
};

enum DGScoreType
{
    SCORE_CART_CAPTURED     = 30,
    SCORE_CART_RETURNED     = 31,
    SCORE_MINE_ASSAULTED    = 32,
    SCORE_MINE_DEFENDED     = 33,
};

#define BG_DG_NotBGBGWeekendHonorTicks      330
#define BG_DG_BGBGWeekendHonorTicks         200

// x, y, z, o
const Position BG_DG_NodePositions[BG_DG_DYNAMIC_NODES_COUNT] =
{
    { 68.3924f, 431.177f, 111.493f, 1.22925f },    // Pandaren
    { -167.503f, 499.059f, 92.629f, 1.24955f },    // Center
    { -397.773f, 574.368f, 111.053f, 1.43787f }    // Goblin
};

// x, y, z, o, rot0, rot1, rot2, rot3
const float BG_DG_DoorPositions[4][8] =
{
    // alliance
    { -263.088f, 217.673f, 132.31f, 4.83817f, 0.0f, 0.0f, 0.688257f, 0.725467f },
    { -213.574f, 200.611f, 132.413f, 4.030561f, 0.0f, 0.0f, 0.412576f, 0.910923f },
    // horde
    { -69.8785f, 781.837f, 132.43f, 1.58825f, 0.0f, 0.0f, 0.0f, 0.0f },
    { -119.621f, 798.957f, 132.488f, 0.820303f, 0.0f, 0.0f, 0.0f, 0.0f },
};

const Position BG_DG_MineDoorsPos[2] =
{
    { 112.421f, 421.165f, 113.372f, 2.90617f },
    { -447.782f, 583.8f, 112.336f, 6.10847f },
};

const Position BG_DG_PeasantsSpawnPos[BG_DG_DYNAMIC_NODES_COUNT] =
{
    { 114.074959f, 421.700653f, 112.973793f, 2.844127f }, // Pandaren Mine
    { 0.0f, 0.0f, 0.0f, 0.0f },                           // Center Mine
    { -449.355164f, 583.359070f, 112.304710f, 6.11396f }, // Goblin Mine
};

const Position BG_DG_PeasantsEndPos[BG_DG_DYNAMIC_NODES_COUNT] =
{
    { 89.324351f, 428.514526f, 111.372017f, 2.859835f },    // Pandaren Mine
    { 0.0f, 0.0f, 0.0f, 0.0f },                             // Center Mine
    { -418.67883f, 579.910400f, 111.017326f, 6.22785f }, // Goblin Mine
};

// Tick intervals and given points: case 0,1,2,3 captured nodes
#define BG_DG_SCORE_TICK_INTERVAL 5 * IN_MILLISECONDS
const uint32 BG_DG_TickPoints[BG_DG_DYNAMIC_NODES_COUNT + 1] = { 0, 8, 16, 32 };

// WorldSafeLocs ids for 3 nodes, and for ally, and horde starting location
const uint32 BG_DG_GraveyardIds[BG_DG_ALL_NODES_COUNT] = { 4613, 0, 4614, 4546, 4488, 4489, 4545 };

// x, y, z, o
#define BG_DG_BUFFS_COUNT 4
const Position BG_DG_BuffPositions[BG_DG_BUFFS_COUNT] =
{
    { 96.0521f, 426.092f, 111.186f, 4.05951f },   // Food up
    { -428.229f, 581.012f, 110.958f, 4.40984f },  // Food down
    { -93.5556f, 375.34f, 135.581f, 5.51675f },   // Berserk up
    {-239.436f, 624.611f, 135.625f, 2.56232f }    // Berserk down
};

// x, y, z, o
const Position BG_DG_SpiritGuidePos[BG_DG_ALL_NODES_COUNT] =
{
    { 104.08f, 423.483f, 112.371f },   // Pandaren
    { 0.0f, 0.0f, 0.0f, 0.0f },        // Center
    { -438.592f, 582.325f, 111.57f },  // Goblin
    { -113.682f, 195.135f, 137.454f }, // alliance N
    { -331.54f, 242.712f, 132.569f },  // alliance S
    { -0.0087f, 758.708f, 132.569f },  // horde N
    { -222.743f, 802.188f, 137.451f }  // horde S
};

const Position BG_DG_CartPos[BG_TEAMS_COUNT]
{
    {-241.741f, 208.611f, 133.747f, 0.84278f }, // Alliance
    { -91.616f, 791.361f, 133.747f, 4.02356f }, // Horde
};

struct BG_DG_BannerTimer
{
    uint32      timer;
    uint8       type;
    uint8       teamIndex;
};

class Battleground_DGScore : public BattlegroundScore
{
    public:
        Battleground_DGScore(ObjectGuid guid, uint32 team) : BattlegroundScore(guid, team), MinesAssaulted(0), MinesDefended(0), CartsCaptures(0), CartsReturned(0) { };
        
        void UpdateScore(uint32 type, uint32 value) override
        {
            switch (type)
            {
            case SCORE_MINE_ASSAULTED:
                MinesAssaulted += value;
                break;
            case SCORE_MINE_DEFENDED:
                MinesDefended += value;
                break;
            case SCORE_CART_CAPTURED:
                CartsCaptures += value;
                break;
            case SCORE_CART_RETURNED:
                CartsReturned += value;
                break;
            default:
                BattlegroundScore::UpdateScore(type, value);
                break;
            }
        }

        void BuildObjectivesBlock(std::vector<int32>& stats) override
        {
            stats.push_back(MinesAssaulted);
            stats.push_back(MinesDefended);
            stats.push_back(CartsCaptures);
            stats.push_back(CartsReturned);
        }

        uint32 CartsCaptures;
        uint32 CartsReturned;
        uint32 MinesAssaulted;
        uint32 MinesDefended;
};

class Battleground_DG : public Battleground
{
    friend class BattlegroundMgr;

    public:
        Battleground_DG();
        ~Battleground_DG();

        void PostUpdateImpl(uint32 diff);
        void AddPlayer(Player *plr);
        virtual void StartingEventCloseDoors();
        virtual void StartingEventOpenDoors();
        void RemovePlayer(Player* /*player*/, ObjectGuid /*guid*/, uint32 /*team*/);
        void HandleAreaTrigger(Player *Source, uint32 Trigger, bool entered);
        virtual bool SetupBattleground();
        virtual void Reset();
        void EndBattleground(uint32 winner);
        virtual WorldSafeLocsEntry const* GetClosestGraveYard(Player* player);
        uint8 GetCartState(uint32 team)             { return cartStates[GetTeamIndexByTeamId(team)]; }
        void SummonCart(Player * source);
        void CartDropped(uint32 teamIndex, float x, float y, float z, float o);
        ObjectGuid GetFlagPickerGUID(int32 teamIndex) const;

        /* Scorekeeping */
        virtual bool UpdatePlayerScore(Player *Source, uint32 type, uint32 value, bool doAddHonor = true);
        void UpdateCartState(uint32 teamIndex, BG_DG_CartState newState, Player * player = NULL);

        virtual void FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& builder);

        /* Nodes occupying */
        virtual void EventPlayerClickedOnFlag(Player * source, GameObject* targetObject);
        /* Carts */
        void EventPlayerClickedOnCart(Player * source, GameObject * targetObject);

    protected:
        void GetPlayerPositionData(std::vector<WorldPackets::Battleground::BattlegroundPlayerPosition>* positions) const override;

    private:
        /* Gameobject spawning/despawning */
        void _CreateBanner(uint8 node, uint8 type, uint8 teamIndex, bool delay);
        void _DelBanner(uint8 node, uint8 type, uint8 teamIndex);
        void _SendNodeUpdate(uint8 node);

        /* Creature spawning/despawning */
        // TODO: working, scripted peons spawning
        void _NodeOccupied(uint8 node,Team team);
        void _NodeDeOccupied(uint8 node);

        int32 _GetNodeNameId(uint8 node);

        /* Nodes info:
            0: neutral
            1: ally contested
            2: horde contested
            3: ally occupied
            4: horde occupied     */
        uint8               m_Nodes[BG_DG_DYNAMIC_NODES_COUNT];
        uint8               m_prevNodes[BG_DG_DYNAMIC_NODES_COUNT];
        BG_DG_CartState     cartStates[BG_TEAMS_COUNT];
        ObjectGuid          cartPickers[BG_TEAMS_COUNT];
        uint32              stolenGold[BG_TEAMS_COUNT];
        int32               cartDropTimer[BG_TEAMS_COUNT];
        uint32              peasantsSpawnTimer[BG_DG_DYNAMIC_NODES_COUNT];
        BG_DG_BannerTimer   m_BannerTimers[BG_DG_DYNAMIC_NODES_COUNT];
        uint32              m_NodeTimers[BG_DG_DYNAMIC_NODES_COUNT];
        uint32              m_lastTick[BG_TEAMS_COUNT];
        uint32              m_HonorScoreTics[BG_TEAMS_COUNT];
        uint32              m_ReputationScoreTics[BG_TEAMS_COUNT];
        bool                m_IsInformedNearVictory;
        uint32              m_HonorTics;
        uint32              m_ReputationTics;
        // need for achievements
        bool                m_TeamScores500Disadvantage[BG_TEAMS_COUNT];
        bool                m_TeamScores100Disadvantage[BG_TEAMS_COUNT];
};
#endif

