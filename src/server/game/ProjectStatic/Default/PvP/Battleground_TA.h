#ifndef __BATTLEGROUND_TA_H
#define __BATTLEGROUND_TA_H

#include "Arena.h"

enum BattlegroundTAObjectTypes
{
    BG_TA_OBJECT_DOOR_1         = 0,
    BG_TA_OBJECT_DOOR_2         = 1,
    BG_TA_OBJECT_BUFF_1         = 2,
    BG_TA_OBJECT_BUFF_2         = 3,
    BG_TA_OBJECT_MAX            = 4
};

enum BattlegroundTAObjects
{
    BG_TA_OBJECT_TYPE_DOOR_1    = 213196,
    BG_TA_OBJECT_TYPE_DOOR_2    = 213197,
    BG_TA_OBJECT_TYPE_BUFF_1    = 184663,
    BG_TA_OBJECT_TYPE_BUFF_2    = 184664
};

struct Battleground_TAScore : public BattlegroundScore
{
    Battleground_TAScore(ObjectGuid guid, uint32 team) : BattlegroundScore(guid, team) { }
    void UpdateScore(uint32 type, uint32 value) override
    {
        BattlegroundScore::UpdateScore(type, value);
    }

    void BuildObjectivesBlock(std::vector<int32>& stats) override
    {

    }
};

class Battleground_TA : public Arena
{
    public:
        Battleground_TA();
        ~Battleground_TA();

        /* inherited from BattlegroundClass */
        void AddPlayer(Player* player);
        void StartingEventCloseDoors();
        void StartingEventOpenDoors();

        void RemovePlayer(Player* player, ObjectGuid guid, uint32 team);
        void HandleAreaTrigger(Player* Source, uint32 Trigger, bool entered);
        bool SetupBattleground();
        void Reset();
        void FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates &builder);
        void HandleKillPlayer(Player* player, Player* killer);
};
#endif
