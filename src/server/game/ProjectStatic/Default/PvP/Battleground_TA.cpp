#include "Battleground.h"
#include "Battleground_TA.h"
#include "BattlegroundMgr.h"
#include "Language.h"
#include "Random.h"
#include "WorldStatePackets.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "Log.h"
#include "Creature.h"
#include "GameObject.h"

Battleground_TA::Battleground_TA()
{
    BgObjects.resize(BG_TA_OBJECT_MAX);

    StartDelayTimes[BG_STARTING_EVENT_FIRST]  = BG_START_DELAY_1M;
    StartDelayTimes[BG_STARTING_EVENT_SECOND] = BG_START_DELAY_30S;
    StartDelayTimes[BG_STARTING_EVENT_THIRD]  = BG_START_DELAY_15S;
    StartDelayTimes[BG_STARTING_EVENT_FOURTH] = BG_START_DELAY_NONE;
    //we must set messageIds
    StartMessageIds[BG_STARTING_EVENT_FIRST]  = LANG_ARENA_ONE_MINUTE;
    StartMessageIds[BG_STARTING_EVENT_SECOND] = LANG_ARENA_THIRTY_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_THIRD]  = LANG_ARENA_FIFTEEN_SECONDS;
    StartMessageIds[BG_STARTING_EVENT_FOURTH] = LANG_ARENA_HAS_BEGUN;
}

Battleground_TA::~Battleground_TA()
{

}

void Battleground_TA::StartingEventCloseDoors()
{
    for (uint32 i = BG_TA_OBJECT_DOOR_1; i <= BG_TA_OBJECT_DOOR_2; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void Battleground_TA::StartingEventOpenDoors()
{
    for (uint32 i = BG_TA_OBJECT_DOOR_1; i <= BG_TA_OBJECT_DOOR_2; ++i)
        DoorOpen(i);

    for (uint32 i = BG_TA_OBJECT_BUFF_1; i <= BG_TA_OBJECT_BUFF_2; ++i)
        SpawnBGObject(i, 60);
}

void Battleground_TA::AddPlayer(Player* player)
{
    Battleground::AddPlayer(player);
    PlayerScores[player->GetGUID()] = new Battleground_TAScore(player->GetGUID(), player->GetBGTeam());
    UpdateArenaWorldState();
}

void Battleground_TA::RemovePlayer(Player* /*player*/, ObjectGuid /*guid*/, uint32 /*team*/)
{
    if (GetStatus() == STATUS_WAIT_LEAVE)
        return;

    UpdateArenaWorldState();
    CheckWinConditions();
}

void Battleground_TA::HandleKillPlayer(Player* player, Player* killer)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    if (!killer)
    {
        TC_LOG_ERROR("bg.battleground", "Battleground_TA: Killer player not found");
        return;
    }

    Battleground::HandleKillPlayer(player, killer);

    UpdateArenaWorldState();
    CheckWinConditions();
}

void Battleground_TA::HandleAreaTrigger(Player* player, uint32 trigger, bool entered)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        // buff triggers
        case 8005:
        case 8006:
        // entrance into arena from door
        case 8451:
        case 8452:
            break;
        default:
            Battleground::HandleAreaTrigger(player, trigger, entered);
            break;
    }
}

void Battleground_TA::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates &builder)
{
    builder.Worldstates.emplace_back(0xa11, 1);           // 9
    UpdateArenaWorldState();
}

void Battleground_TA::Reset()
{
    //call parent's class reset
    Battleground::Reset();
}

bool Battleground_TA::SetupBattleground()
{
    // gates
    if (!AddObject(BG_TA_OBJECT_DOOR_1, BG_TA_OBJECT_TYPE_DOOR_1, -10767.8f, 431.39f, 23.5428f, 0.0f, 0.0f, 0.0f, 1.0f, -0.0000000437114f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_TA_OBJECT_DOOR_2, BG_TA_OBJECT_TYPE_DOOR_2, -10654.3f, 428.305f, 23.5428f, 3.14159f, 0.0f, 0.0f, 1.0f, -0.0000000437114f, RESPAWN_IMMEDIATELY)
    // buffs
        || !AddObject(BG_TA_OBJECT_BUFF_1, BG_TA_OBJECT_TYPE_BUFF_1, -10715.5f, 381.071f, 24.4125f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 120)
        || !AddObject(BG_TA_OBJECT_BUFF_2, BG_TA_OBJECT_TYPE_BUFF_2, -10714.5f, 480.647f, 24.4128f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 120))
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundNA: Failed to spawn some object!");
        return false;
    }

    return true;
}
