/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Battleground_AF.h"
#include "Log.h"
#include "Player.h"
#include "WorldPacket.h"
#include "WorldStatePackets.h"

Battleground_AF::Battleground_AF()
{
    BgObjects.resize(BG_AF_OBJECT_MAX);
}

void Battleground_AF::StartingEventCloseDoors()
{
    for (uint32 i = BG_AF_OBJECT_DOOR_1; i <= BG_AF_OBJECT_DOOR_2; ++i)
        SpawnBGObject(i, RESPAWN_IMMEDIATELY);
}

void Battleground_AF::StartingEventOpenDoors()
{
    for (uint32 i = BG_AF_OBJECT_DOOR_1; i <= BG_AF_OBJECT_DOOR_2; ++i)
        DoorOpen(i);
}

void Battleground_AF::HandleAreaTrigger(Player* player, uint32 trigger, bool entered)
{
    if (GetStatus() != STATUS_IN_PROGRESS)
        return;

    switch (trigger)
    {
        default:
            Battleground::HandleAreaTrigger(player, trigger, entered);
            break;
    }
}

void Battleground_AF::FillInitialWorldStates(WorldPackets::WorldState::InitWorldStates& packet)
{
    packet.Worldstates.emplace_back(0xa11, 1);
    Arena::FillInitialWorldStates(packet);
}

bool Battleground_AF::SetupBattleground()
{
    // gates
    if (!AddObject(BG_AF_OBJECT_DOOR_1, BG_AF_OBJECT_TYPE_DOOR_1, 3539.87f, 5488.701f, 323.5819, 1.553341f, 0, 0, 0.7009087f, 0.7132511f, RESPAWN_IMMEDIATELY)
        || !AddObject(BG_AF_OBJECT_DOOR_2, BG_AF_OBJECT_TYPE_DOOR_2, 3548.342f, 5584.779f, 323.6123f, 1.544616f, 0, 0, 0.6977901f, 0.7163023f, RESPAWN_IMMEDIATELY))
    // buffs
    {
        TC_LOG_ERROR("sql.sql", "BatteGroundNA: Failed to spawn some object!");
        return false;
    }

    return true;
}
