/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "LFGPackets.h"

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::LFG::ShortageReward const& shortage)
{
    data << shortage.Mask;
    data << shortage.RewardMoney;
    data << shortage.RewardXP;
    data << uint32(shortage.Items.size());
    data << uint32(shortage.Currencies.size());
    data << uint32(shortage.BonusCurrency.size());

    for (auto item : shortage.Items)
    {
        data << item.ItemID;
        data << item.Quantiy;
    }

    for (auto currency : shortage.Currencies)
    {
        data << currency.CurrencyID;
        data << currency.Quantiy;
    }

    for (auto currency : shortage.BonusCurrency)
    {
        data << currency.CurrencyID;
        data << currency.Quantiy;
    }

    data.WriteBit(shortage.SpellCast.is_initialized());
    data.WriteBit(shortage.Unk2.is_initialized());
    data.WriteBit(shortage.Unk3.is_initialized());
    data.WriteBit(shortage.Honor.is_initialized());
    data.FlushBits();

    if (shortage.SpellCast.is_initialized())
        data << *shortage.SpellCast;

    if (shortage.Unk2.is_initialized())
        data << *shortage.Unk2;

    if (shortage.Unk3.is_initialized())
        data << *shortage.Unk3;

    if (shortage.Honor.is_initialized())
        data << *shortage.Honor;

    return data;
}

WorldPacket const* WorldPackets::LFG::LfgRoleChosen::Write()
{
    _worldPacket << Guid;
    _worldPacket << Roles;
    _worldPacket.WriteBit(Roles > 0 ? true : false);
    _worldPacket.FlushBits();

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgOfferContinue::Write()
{
    _worldPacket << DungeonEntry;

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgBootPlayer::Write()
{
    _worldPacket.WriteBit(Boot.VoteInProgress);
    _worldPacket.WriteBit(Boot.VotePassed);
    _worldPacket.WriteBit(Boot.MyVoteCompleted);
    _worldPacket.WriteBit(Boot.MyVote);
    _worldPacket.WriteBits(Boot.Reason.length(), 8);
    _worldPacket.FlushBits();

    _worldPacket << Boot.Target;
    _worldPacket << Boot.TotalVotes;
    _worldPacket << Boot.BootVotes;
    _worldPacket << Boot.TimeLeft;
    _worldPacket << Boot.VotesNeeded;
    _worldPacket.WriteString(Boot.Reason);

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgPartyInfo::Write()
{
    _worldPacket << uint32(BlackList.size());

    for (auto& blackList : BlackList)
    {
        bool hasPlayerGuid = blackList.PlayerGuid != ObjectGuid::Empty;
        _worldPacket.WriteBit(hasPlayerGuid);
        _worldPacket << uint32(blackList.Info.size());

        if (hasPlayerGuid)
            _worldPacket << blackList.PlayerGuid;

        for (auto& info : blackList.Info)
            _worldPacket << info;
    }

    return &_worldPacket;
}

ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::LFG::LFGBlackListInfo const& entry)
{
    data << entry.Slot;
    data << entry.Reason;
    data << entry.SubReason1;
    data << entry.SubReason2;

    return data;
}

WorldPacket const* WorldPackets::LFG::LfgPlayerInfo::Write()
{
    _worldPacket << uint32(Dungeons.size());

    _worldPacket.WriteBit(BlackList.PlayerGuid != ObjectGuid::Empty);
    _worldPacket.FlushBits();

    _worldPacket << uint32(BlackList.Info.size());

    if (BlackList.PlayerGuid != ObjectGuid::Empty)
        _worldPacket << BlackList.PlayerGuid;

    for (auto& info : BlackList.Info)
        _worldPacket << info;

    for (auto& dungeon : Dungeons)
    {
        _worldPacket << dungeon.Slot;
        _worldPacket << dungeon.CompletionQuantity;
        _worldPacket << dungeon.CompletionLimit;
        _worldPacket << dungeon.CompletionCurrencyID;
        _worldPacket << dungeon.SpecificQuantity;
        _worldPacket << dungeon.SpecificLimit;
        _worldPacket << dungeon.OverallQuantity;
        _worldPacket << dungeon.OverallLimit;
        _worldPacket << dungeon.PurseWeeklyQuantity;
        _worldPacket << dungeon.PurseWeeklyLimit;
        _worldPacket << dungeon.PurseQuantity;
        _worldPacket << dungeon.PurseLimit;
        _worldPacket << dungeon.Quantity;
        _worldPacket << dungeon.CompletedMask;
        _worldPacket << dungeon.Unk;
        _worldPacket << uint32(dungeon.Rewards.size());
        _worldPacket.WriteBit(dungeon.FirstReward);
        _worldPacket.WriteBit(dungeon.ShortageEligible);
        _worldPacket.FlushBits();

        _worldPacket << dungeon.Reward;

        for (auto& reward : dungeon.Rewards)
            _worldPacket << reward;

    }

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgJoinResult::Write()
{
    _worldPacket << Ride.RequesterGuid;
    _worldPacket << Ride.Id;
    _worldPacket << Ride.Type;
    _worldPacket << Ride.Time;
    _worldPacket << Result;
    _worldPacket << ResultDetails;
    _worldPacket << uint32(Blacklists.size());
    for (auto& blacklist : Blacklists)
    {
        _worldPacket << blacklist.PlayerGuid;
        _worldPacket << uint32(blacklist.Info.size());
        for (auto& slot : blacklist.Info)
            _worldPacket << slot;
    }

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgUpdateStatus::Write()
{
    _worldPacket << Ride.RequesterGuid;
    _worldPacket << Ride.Id;
    _worldPacket << Ride.Type;
    _worldPacket << Ride.Time;
    _worldPacket << SubType;
    _worldPacket << Reason;
    _worldPacket << uint32(Slots.size());
    _worldPacket << RequestedRoles;
    _worldPacket << uint32(Suspended.size());

    for (auto& slot : Slots)
        _worldPacket << slot;

    for (auto& suspended : Suspended)
        _worldPacket << suspended;

    _worldPacket.WriteBit(IsParty);
    _worldPacket.WriteBit(NotifyUI);
    _worldPacket.WriteBit(Joined);
    _worldPacket.WriteBit(LfgJoined);
    _worldPacket.WriteBit(Queued);

    _worldPacket.FlushBits();

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgQueueStatus::Write()
{
    _worldPacket << Ride.RequesterGuid;
    _worldPacket << Ride.Id;
    _worldPacket << Ride.Type;
    _worldPacket << Ride.Time;
    _worldPacket << Slot;
    _worldPacket << AverageWaitTime;
    _worldPacket << QueuedTime;

    _worldPacket << Options[0].AverageWaitTimeByRole;
    _worldPacket << Options[0].LastNeeded;
    _worldPacket << Options[1].AverageWaitTimeByRole;
    _worldPacket << Options[1].LastNeeded;
    _worldPacket << Options[2].AverageWaitTimeByRole;
    _worldPacket << Options[2].LastNeeded;

    _worldPacket << AverageWaitTimeSelf;

    return &_worldPacket;
}

void WorldPackets::LFG::DfGetSystemInfo::Read()
{
    isPlayer = _worldPacket.ReadBit();
    _worldPacket >> PartyIndex;
}

void WorldPackets::LFG::DfJoin::Read()
{
    AsGroup = _worldPacket.ReadBit();
    Unk = _worldPacket.ReadBit();
    _worldPacket >> PartyIndex;
    _worldPacket >> Roles;

    Slots.resize(_worldPacket.read<uint32>());

    for (uint32& slot : Slots)
        _worldPacket >> slot;
}

void WorldPackets::LFG::DfLeave::Read()
{
    _worldPacket >> Ride.RequesterGuid;
    _worldPacket >> Ride.Id;
    _worldPacket >> Ride.Type;
    _worldPacket >> Ride.Time;
}

void WorldPackets::LFG::DfSetRoles::Read()
{
    _worldPacket >> RolesDesired;
    _worldPacket >> PartyIndex;
}

void WorldPackets::LFG::DfProposalResponse::Read()
{
    _worldPacket >> Ride;
    _worldPacket >> InstanceID;
    _worldPacket >> ProposalID;
    Accepted = _worldPacket.ReadBit();
}

void WorldPackets::LFG::DfBootPlayerVote::Read()
{
    Vote = _worldPacket.ReadBit();
}

void WorldPackets::LFG::DfTeleport::Read()
{
    TeleportOut = _worldPacket.ReadBit();
}

WorldPacket const* WorldPackets::LFG::LfgProposalUpdate::Write()
{
	_worldPacket << Ride;
	_worldPacket << InstanceID;
	_worldPacket << ProposalID;
	_worldPacket << Slot;
	_worldPacket << State;
	_worldPacket << CompletedMask;
	_worldPacket << uint32(Players.size());
    _worldPacket << uint8(Unused);
    _worldPacket.WriteBit(ValidCompletedMask);
    _worldPacket.WriteBit(ProposalSilent);
    _worldPacket.WriteBit(IsRequeue);
    _worldPacket.FlushBits();

    for (auto player : Players)
    {
        _worldPacket << player.Roles;
        _worldPacket.WriteBit(player.Me);
        _worldPacket.WriteBit(player.SameParty);
        _worldPacket.WriteBit(player.MyParty);
        _worldPacket.WriteBit(player.Responded);
        _worldPacket.WriteBit(player.Accepted);
        _worldPacket.FlushBits();
    }

    return &_worldPacket;
}

WorldPacket const* WorldPackets::LFG::LfgRoleCheckUpdate::Write()
{
    _worldPacket << PartyIndex;
    _worldPacket << RoleCheckStatus;
    _worldPacket << uint32(Slots.size());
    _worldPacket << QueueID;
    _worldPacket << ActivityID;
    _worldPacket << uint32(MemberUpdate.size());

    for (auto slot : Slots)
        _worldPacket << slot;

    _worldPacket.WriteBit(IsBeginning);
    _worldPacket.WriteBit(ShowRoleCheck);
    _worldPacket.FlushBits();

    for (auto member : MemberUpdate)
    {
        _worldPacket << member.guid;
        _worldPacket << member.RolesDesired;
        _worldPacket << member.Level;
        _worldPacket.WriteBit(member.Complete);
        _worldPacket.FlushBits();
    }

    return &_worldPacket;
}
