/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LFGPackets_h__
#define LFGPackets_h__

#include "Packet.h"
#include "PacketUtilities.h"
#include "ItemPacketsCommon.h"
#include "LFGPacketsCommon.h"
#include <Optional.h>

namespace WorldPackets
{
    namespace LFG
    {
        struct Item
        {
            uint32 ItemID = 0;
            uint32 Quantiy = 0;
        };

        struct Currency
        {
            uint32 CurrencyID = 0;
            uint32 Quantiy = 0;
        };

        struct ShortageReward
        {
            uint32 Mask = 0;
            uint32 RewardMoney = 0;
            uint32 RewardXP = 0;
            std::vector<Item> Items;
            std::vector<Currency> Currencies;
            std::vector<Currency> BonusCurrency;
            Optional<uint32> SpellCast;
            Optional<uint32> Unk2;
            Optional<uint32> Unk3;
            Optional<uint32> Honor;

        };

        class LfgDisabled final : public ServerPacket
        {
        public:
            LfgDisabled() : ServerPacket(SMSG_LFG_DISABLED, 0) { }

            WorldPacket const* Write() override { return &_worldPacket; }
        };

        class LfgRoleChosen final : public ServerPacket
        {
        public:
            LfgRoleChosen() : ServerPacket(SMSG_ROLE_CHOSEN, 21) { }

            WorldPacket const* Write() override;

            ObjectGuid Guid;
            uint32 Roles = 0;
        };

        class LfgOfferContinue final : public ServerPacket
        {
        public:
            LfgOfferContinue() : ServerPacket(SMSG_LFG_OFFER_CONTINUE, 4) { }

            WorldPacket const* Write() override;

            uint32 DungeonEntry = 0;
        };

        struct LFGBootInfo
        {
            bool VoteInProgress = false;
            bool VotePassed = false;
            bool MyVoteCompleted = false;
            bool MyVote = false;
            ObjectGuid Target;
            uint32 TotalVotes = 0;
            uint32 BootVotes = 0;
            uint32 TimeLeft = 0;
            uint32 VotesNeeded = 0;
            std::string Reason;
        };

        class LfgBootPlayer final : public ServerPacket
        {
        public:
            LfgBootPlayer() : ServerPacket(SMSG_LFG_BOOT_PLAYER, 28) { }

            WorldPacket const* Write() override;

            LFGBootInfo Boot;
        };

        struct LFGBlackListInfo
        {
            uint32 Slot = 0;
            uint32 Reason = 0;
            uint32 SubReason1 = 0;
            uint32 SubReason2 = 0;
        };

        struct LFGBlackList
        {
            ObjectGuid PlayerGuid;
            std::vector<LFGBlackListInfo> Info;

        };

        class LfgPartyInfo final : public ServerPacket
        {
        public:
            LfgPartyInfo() : ServerPacket(SMSG_LFG_PARTY_INFO, 28) { }

            WorldPacket const* Write() override;

            std::vector<LFGBlackList> BlackList;
        };

        struct DungeonInfo
        {
            uint32 Slot = 0;
            uint32 CompletionQuantity = 0;
            uint32 CompletionLimit = 0;
            uint32 CompletionCurrencyID = 0;
            uint32 SpecificQuantity = 0;
            uint32 SpecificLimit = 0;
            uint32 OverallQuantity = 0;
            uint32 OverallLimit = 0;
            uint32 PurseWeeklyQuantity = 0;
            uint32 PurseWeeklyLimit = 0;
            uint32 PurseQuantity = 0;
            uint32 PurseLimit = 0;
            uint32 Quantity = 0;
            uint32 CompletedMask = 0;
            uint32 Unk = 0;
            ShortageReward Reward;
            std::vector<ShortageReward> Rewards;
            bool FirstReward = false;
            bool ShortageEligible = false;
        };

        class LfgPlayerInfo final : public ServerPacket
        {
        public:
            LfgPlayerInfo() : ServerPacket(SMSG_LFG_PLAYER_INFO, 28) { }

            WorldPacket const* Write() override;

            std::vector<DungeonInfo> Dungeons;
            LFGBlackList BlackList;
        };

        class LfgJoinResult final : public ServerPacket
        {
        public:
            LfgJoinResult() : ServerPacket(SMSG_LFG_JOIN_RESULT) { }

            WorldPacket const* Write() override;

            RideTicket Ride;
            uint8 Result = 0;
            uint8 ResultDetails = 0;
            std::vector<LFGBlackList> Blacklists;
        };

        class LfgUpdateStatus final : public ServerPacket
        {
        public:
            LfgUpdateStatus() : ServerPacket(SMSG_LFG_UPDATE_STATUS) { }

            WorldPacket const* Write() override;

            RideTicket Ride;
            uint8 SubType = 0;
            uint8 Reason = 0;
            std::vector<uint32> Slots;
            std::vector<ObjectGuid> Suspended;
            uint32 RequestedRoles = 0;
            bool IsParty = false;
            bool NotifyUI = false;
            bool Joined = false;
            bool LfgJoined = false;
            bool Queued = false;
        };

        class LfgQueueStatus final : public ServerPacket
        {
        public:
            LfgQueueStatus() : ServerPacket(SMSG_LFG_QUEUE_STATUS) { }

            WorldPacket const* Write() override;

            struct Option
            {
                uint32 AverageWaitTimeByRole = 0;
                uint8 LastNeeded = 0;
            };

            RideTicket Ride;
            uint32 Slot = 0;
            uint32 AverageWaitTime = 0;
            uint32 QueuedTime = 0;
            Option Options[3];
            uint32 AverageWaitTimeSelf = 0;
        };

        class DfGetSystemInfo final : public ClientPacket
        {
        public:
            DfGetSystemInfo(WorldPacket&& packet) : ClientPacket(CMSG_DF_GET_SYSTEM_INFO, std::move(packet)) { }

            void Read() override;

            bool isPlayer = false;
            uint8 PartyIndex = 0;
        };

        class DfJoin final : public ClientPacket
        {
        public:
            DfJoin(WorldPacket&& packet) : ClientPacket(CMSG_DF_JOIN, std::move(packet)) { }

            void Read() override;

            bool AsGroup = false;
            bool Unk = false;
            uint8 PartyIndex = 0;
            uint32 Roles = 0;
            std::vector<uint32> Slots;
        };

        class DfLeave final : public ClientPacket
        {
        public:
            DfLeave(WorldPacket&& packet) : ClientPacket(CMSG_DF_LEAVE, std::move(packet)) { }

            void Read() override;

            RideTicket Ride;
        };

        class DfSetRoles final : public ClientPacket
        {
        public:
            DfSetRoles(WorldPacket&& packet) : ClientPacket(CMSG_DF_SET_ROLES, std::move(packet)) { }

            void Read() override;

            uint32 RolesDesired;
            uint8 PartyIndex;
        };

        class DfProposalResponse final : public ClientPacket
        {
        public:
            DfProposalResponse(WorldPacket&& packet) : ClientPacket(CMSG_DF_PROPOSAL_RESPONSE, std::move(packet)) { }

            void Read() override;

            RideTicket Ride;
            uint64 InstanceID;
            uint32 ProposalID;
            bool Accepted;
        };

        class DfBootPlayerVote final : public ClientPacket
        {
        public:
            DfBootPlayerVote(WorldPacket&& packet) : ClientPacket(CMSG_DF_BOOT_PLAYER_VOTE, std::move(packet)) { }

            void Read() override;

            bool Vote;
        };

        class DfTeleport final : public ClientPacket
        {
        public:
            DfTeleport(WorldPacket&& packet) : ClientPacket(CMSG_DF_TELEPORT, std::move(packet)) { }

            void Read() override;

            bool TeleportOut;
        };

        class LfgProposalUpdate final : public ServerPacket
        {
        public:
            LfgProposalUpdate() : ServerPacket(SMSG_LFG_PROPOSAL_UPDATE) { }

            WorldPacket const* Write() override;

            struct PlayerInfo
            {
                uint32 Roles = 0;
                bool Me = false;
                bool SameParty = false;
                bool MyParty = false;
                bool Responded = false;
                bool Accepted = false;
            };

            RideTicket Ride;
            uint64 InstanceID = 0;
            uint32 ProposalID = 0;
            uint32 Slot = 0;
            uint8 State = 0;
            uint32 CompletedMask = 0;
            uint32 EncounterMask = 0;
            uint8 Unused = 0;
            bool ValidCompletedMask = false;
            bool ProposalSilent = false;
            bool IsRequeue = false;
            std::vector<PlayerInfo> Players;
        };

        class LfgRoleCheckUpdate final : public ServerPacket
        {
        public:
            LfgRoleCheckUpdate() : ServerPacket(SMSG_LFG_ROLE_CHECK_UPDATE, 18) { }

            WorldPacket const* Write() override;

            struct RoleCheckUpdateMember
            {
                ObjectGuid guid = ObjectGuid::Empty;
                uint32 RolesDesired = 0;
                uint8 Level = 0;
                bool Complete = false;
            };

            uint8 PartyIndex = 0;
            uint8 RoleCheckStatus = 0;
            uint64 QueueID = 0;
            uint32 ActivityID = 0;
            std::vector<uint32> Slots;
            std::vector<RoleCheckUpdateMember> MemberUpdate;
            bool IsBeginning = false;
            bool ShowRoleCheck = false;
        };

        class DfGetJoinStatus final : public ClientPacket
        {
        public:
            DfGetJoinStatus(WorldPacket&& packet) : ClientPacket(CMSG_DF_GET_JOIN_STATUS, std::move(packet)) { }

            void Read() override { };
        };
    }
}

ByteBuffer& operator>>(ByteBuffer& data, WorldPackets::LFG::RideTicket& ticket);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::LFG::RideTicket const& ticket);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::LFG::ShortageReward const& shortage);
ByteBuffer& operator<<(ByteBuffer& data, WorldPackets::LFG::LFGBlackListInfo const& entry);

#endif // LFGPackets_h__
