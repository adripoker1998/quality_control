/*
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITYCORE_CONVERSATION_H
#define TRINITYCORE_CONVERSATION_H

#include "Object.h"
#include "ObjectAccessor.h"
#include <cstring>

class Unit;
class SpellInfo;

struct ConversationActorTemplate
{
    uint32 Id;
    uint32 CreatureId;
    uint32 ModelId;
    uint32 Unk2;
    uint32 Unk3;
    uint32 Unk4;
};

#pragma pack(push, 1)
struct ConversationLineTemplate
{
    uint32 Id;      // Link to ConversationLine.db2
    uint32 PreviousLineDuration;
    uint32 CameraID;
    uint32 ActorIdx;
};

struct ConversationTemplate
{
    uint32 Id;
    uint32 FirstLineId;
    uint32 LastLineDuration;
    uint32 TextureKitId;

    std::vector<ConversationActorTemplate> Actors;
    std::vector<ConversationLineTemplate> Lines;
};
#pragma pack(pop)

class TC_GAME_API Conversation : public WorldObject, public GridObject<Conversation>
{
    public:
        Conversation();
        ~Conversation();

        void AddToWorld() override;
        void RemoveFromWorld() override;

        void Update(uint32 p_time) override;
        void Remove();
        int32 GetDuration() const { return _duration; }
        uint32 GetTextureKitId() const { return _textureKitId; }

        bool CreateConversation(ObjectGuid::LowType guidlow, uint32 triggerEntry, Unit* caster, SpellInfo const* spell, Position const& pos);

        ObjectGuid GetCasterGuid() const { return _casterGuid; }

        ObjectGuid _casterGuid;
        uint32 _duration;
        uint32 _textureKitId;
};
#endif
