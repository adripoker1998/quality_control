/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CollectionMgr.h"
#include "AccountCache.h"
#include "CharacterCache.h"
#include "DatabaseEnv.h"
#include "DB2Stores.h"
#include "Item.h"
#include "Log.h"
#include "MiscPackets.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "Timer.h"
#include "TransmogrificationPackets.h"
#include "WorldSession.h"
#include <boost/dynamic_bitset.hpp>
#include "project_config.h"

namespace
{
    MountDefinitionMap FactionSpecificMounts;
}

void CollectionMgr::LoadMountDefinitions()
{
    uint32 oldMSTime = getMSTime();

    QueryResult result = WorldDatabase.Query("SELECT spellId, otherFactionSpellId FROM mount_definitions");

    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 mount definitions. DB table `mount_definitions` is empty.");
        return;
    }

    do
    {
        Field* fields = result->Fetch();

        uint32 spellId = fields[0].GetUInt32();
        uint32 otherFactionSpellId = fields[1].GetUInt32();

        if (!sDB2Manager.GetMount(spellId))
        {
            TC_LOG_ERROR("sql.sql", "Mount spell %u defined in `mount_definitions` does not exist in Mount.db2, skipped", spellId);
            continue;
        }

        if (otherFactionSpellId && !sDB2Manager.GetMount(otherFactionSpellId))
        {
            TC_LOG_ERROR("sql.sql", "otherFactionSpellId %u defined in `mount_definitions` for spell %u does not exist in Mount.db2, skipped", otherFactionSpellId, spellId);
            continue;
        }

        FactionSpecificMounts[spellId] = otherFactionSpellId;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded " SZFMTD " mount definitions in %u ms", FactionSpecificMounts.size(), GetMSTimeDiffToNow(oldMSTime));
}

CollectionMgr::CollectionMgr(WorldSession* owner) : _owner(owner), _appearances(Trinity::make_unique<boost::dynamic_bitset<uint32>>())
{
}

CollectionMgr::~CollectionMgr()
{
}

void CollectionMgr::LoadToys()
{
    for (auto const& t : _toys)
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TOYS, t.first);
}

bool CollectionMgr::AddToy(uint32 itemId, bool isFavourite /*= false*/)
{
    if (UpdateAccountToys(itemId, isFavourite))
    {
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TOYS, itemId);
        return true;
    }

    return false;
}

void CollectionMgr::LoadAccountToys(PreparedQueryResult result, AccountEntityCache const* accountCache, CharacterCacheEntry const* characterCache)
{
    _toys.clear();

    if (result)
    {
        do
        {
            Field* fields = result->Fetch();
            uint32 itemId = fields[0].GetUInt32();
            bool isFavourite = fields[1].GetBool();

            _toys[itemId] = isFavourite;

            if (PROJECT::Config->GetBool("Mounts.LinkedToAccount"))
                sAccountCache->AddAccountToyEntity(_owner->GetAccountId(), itemId, isFavourite);
            else
                sCharacterCache->AddCharacterToyEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, isFavourite);
        } while (result->NextRow());
    }
    else if (accountCache && !accountCache->Toys.empty() || characterCache && !characterCache->Toys.empty())
    {
        auto const& values = accountCache && !accountCache->Toys.empty() ? accountCache->Toys : characterCache->Toys;
        for (AccountToysEntityCache const& toy : values)
            _toys[toy.ItemID] = toy.IsFavorite;
    }
}

void CollectionMgr::SaveAccountToys(SQLTransaction& trans, ObjectGuid guid)
{
    PreparedStatement* stmt = nullptr;
    for (auto const& toy : _toys)
    {
        if (PROJECT::Config->GetBool("Toys.LinkedToAccount"))
        {
            stmt = LoginDatabase.GetPreparedStatement(LOGIN_REP_ACCOUNT_TOYS);
            stmt->setUInt32(0, _owner->GetAccountId());
        }
        else
        {
            stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_REP_CHARACTER_TOYS);
            stmt->setUInt32(0, guid.GetCounter());
        }
        stmt->setUInt32(1, toy.first);
        stmt->setBool(2, toy.second);
        trans->Append(stmt);
    }
}

bool CollectionMgr::UpdateAccountToys(uint32 itemId, bool isFavourite /*= false*/)
{
    if (PROJECT::Config->GetBool("Toys.LinkedToAccount"))
        sAccountCache->AddAccountToyEntity(_owner->GetAccountId(), itemId, isFavourite);
    else
        sCharacterCache->AddCharacterToyEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, isFavourite);

    return _toys.insert(ToyBoxContainer::value_type(itemId, isFavourite)).second;
}

void CollectionMgr::ToySetFavorite(uint32 itemId, bool favorite)
{
    ToyBoxContainer::iterator itr = _toys.find(itemId);
    if (itr == _toys.end())
        return;

    itr->second = favorite;

    if (PROJECT::Config->GetBool("Toys.LinkedToAccount"))
        sAccountCache->UpdateAccountToyEntity(_owner->GetAccountId(), itemId, favorite);
    else
        sCharacterCache->UpdateCharacterToyEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, favorite);
}

void CollectionMgr::OnItemAdded(Item* item)
{
    if (sDB2Manager.GetHeirloomByItemId(item->GetEntry()))
        AddHeirloom(item->GetEntry(), 0);

    AddItemAppearance(item);
}

inline uint32 GetBonusIDForHeirloom(HeirloomEntry const* heirloom, uint32 flags)
{
    uint32 bonusId = 0;

    if (flags & HEIRLOOM_FLAG_BONUS_LEVEL_110)
        bonusId = heirloom->UpgradeItemBonusListID[2];
    else if (flags & HEIRLOOM_FLAG_BONUS_LEVEL_100)
        bonusId = heirloom->UpgradeItemBonusListID[1];
    else if (flags & HEIRLOOM_FLAG_BONUS_LEVEL_90)
        bonusId = heirloom->UpgradeItemBonusListID[0];

    return bonusId;
}

void CollectionMgr::LoadAccountHeirlooms(PreparedQueryResult result, AccountEntityCache const* accountCache, CharacterCacheEntry const* characterCache)
{
    _heirlooms.clear();

    if (result)
    {
        do
        {
            Field* fields = result->Fetch();
            uint32 itemId = fields[0].GetUInt32();
            uint32 flags = fields[1].GetUInt32();

            HeirloomEntry const* heirloom = sDB2Manager.GetHeirloomByItemId(itemId);
            if (!heirloom)
                continue;

            uint32 bonusId = GetBonusIDForHeirloom(heirloom, flags);

            _heirlooms[itemId] = HeirloomData(flags, bonusId);

            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                sAccountCache->AddAccountHeirloomEntity(_owner->GetAccountId(), itemId, flags);
            else
                sCharacterCache->AddCharacterHeirloomEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, flags);
        } while (result->NextRow());
    }
    else if (accountCache && !accountCache->Heirlooms.empty() || characterCache && !characterCache->Heirlooms.empty())
    {
        auto const& values = accountCache && !accountCache->Heirlooms.empty() ? accountCache->Heirlooms : characterCache->Heirlooms;

        for (AccountHeirloomsCache const& heirloomCache : values)
        {
            HeirloomEntry const* heirloom = sDB2Manager.GetHeirloomByItemId(heirloomCache.ItemID);
            if (!heirloom)
                continue;

            uint32 bonusId = GetBonusIDForHeirloom(heirloom, heirloomCache.Flags);

            _heirlooms[heirloomCache.ItemID] = HeirloomData(heirloomCache.Flags, bonusId);
        }
    }
}

void CollectionMgr::SaveAccountHeirlooms(SQLTransaction& trans, ObjectGuid guid)
{
    for (auto const& heirloom : _heirlooms)
    {
        PreparedStatement* stmt = nullptr;
        if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
        {
            stmt = LoginDatabase.GetPreparedStatement(LOGIN_REP_ACCOUNT_HEIRLOOMS);
            stmt->setUInt32(0, _owner->GetAccountId());
        }
        else
        {
            stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_REP_CHARACTER_HEIRLOOMS);
            stmt->setUInt32(0, guid.GetCounter());
        }
        stmt->setUInt32(1, heirloom.first);
        stmt->setUInt32(2, heirloom.second.flags);
        trans->Append(stmt);
    }
}

bool CollectionMgr::UpdateAccountHeirlooms(uint32 itemId, uint32 flags)
{
    if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
        sAccountCache->AddAccountHeirloomEntity(_owner->GetAccountId(), itemId, flags);
    else
        sCharacterCache->AddCharacterHeirloomEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, flags);

    return _heirlooms.insert(HeirloomContainer::value_type(itemId, HeirloomData(flags, 0))).second;
}

uint32 CollectionMgr::GetHeirloomBonus(uint32 itemId) const
{
    HeirloomContainer::const_iterator itr = _heirlooms.find(itemId);
    if (itr != _heirlooms.end())
        return itr->second.bonusId;

    return 0;
}

void CollectionMgr::LoadHeirlooms()
{
    for (auto const& item : _heirlooms)
    {
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOMS, item.first);
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOM_FLAGS, item.second.flags);
    }
}

void CollectionMgr::AddHeirloom(uint32 itemId, uint32 flags)
{
    if (UpdateAccountHeirlooms(itemId, flags))
    {
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOMS, itemId);
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOM_FLAGS, flags);
    }
}

void CollectionMgr::UpgradeHeirloom(uint32 itemId, int32 castItem)
{
    Player* player = _owner->GetPlayer();
    if (!player)
        return;

    HeirloomEntry const* heirloom = sDB2Manager.GetHeirloomByItemId(itemId);
    if (!heirloom)
        return;

    HeirloomContainer::iterator itr = _heirlooms.find(itemId);
    if (itr == _heirlooms.end())
        return;

    uint32 flags = itr->second.flags;
    uint32 bonusId = 0;

    if (heirloom->UpgradeItemID[0] == castItem)
    {
        flags |= HEIRLOOM_FLAG_BONUS_LEVEL_90;
        bonusId = heirloom->UpgradeItemBonusListID[0];
    }
    if (heirloom->UpgradeItemID[1] == castItem)
    {
        flags |= HEIRLOOM_FLAG_BONUS_LEVEL_100;
        bonusId = heirloom->UpgradeItemBonusListID[1];
    }
    if (heirloom->UpgradeItemID[2] == castItem)
    {
        flags |= HEIRLOOM_FLAG_BONUS_LEVEL_110;
        bonusId = heirloom->UpgradeItemBonusListID[2];
    }

    for (Item* item : player->GetItemListByEntry(itemId, true))
        item->AddBonuses(bonusId);

    // Get heirloom offset to update only one part of dynamic field
    std::vector<uint32> const& fields = player->GetDynamicValues(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOMS);
    uint16 offset = uint16(std::find(fields.begin(), fields.end(), itemId) - fields.begin());

    player->SetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOM_FLAGS, offset, flags);
    itr->second.flags = flags;
    itr->second.bonusId = bonusId;

    if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
        sAccountCache->UpdateAccountHeirloomEntity(_owner->GetAccountId(), itemId, flags);
    else
        sCharacterCache->UpdateCharacterHeirloomEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemId, flags);
}

void CollectionMgr::CheckHeirloomUpgrades(Item* item)
{
    Player* player = _owner->GetPlayer();
    if (!player)
        return;

    // Check already owned heirloom for upgrade kits
    if (HeirloomEntry const* heirloom = sDB2Manager.GetHeirloomByItemId(item->GetEntry()))
    {
        HeirloomContainer::iterator itr = _heirlooms.find(item->GetEntry());
        if (itr == _heirlooms.end())
            return;

        // Check for heirloom pairs (normal - heroic, heroic - mythic)
        uint32 heirloomItemId = heirloom->StaticUpgradedItemID;
        uint32 newItemId = 0;
        while (HeirloomEntry const* heirloomDiff = sDB2Manager.GetHeirloomByItemId(heirloomItemId))
        {
            if (player->GetItemByEntry(heirloomDiff->ItemID))
                newItemId = heirloomDiff->ItemID;

            if (HeirloomEntry const* heirloomSub = sDB2Manager.GetHeirloomByItemId(heirloomDiff->StaticUpgradedItemID))
            {
                heirloomItemId = heirloomSub->ItemID;
                continue;
            }

            break;
        }

        if (newItemId)
        {
            std::vector<uint32> const& fields = player->GetDynamicValues(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOMS);
            uint16 offset = uint16(std::find(fields.begin(), fields.end(), itr->first) - fields.begin());

            player->SetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOMS, offset, newItemId);
            player->SetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_HEIRLOOM_FLAGS, offset, 0);

            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                sAccountCache->DeleteAccountHeirloomEntity(_owner->GetAccountId(), item->GetEntry());
            else
                sCharacterCache->DeleteCharacterHeirloomEntity(_owner->GetPlayer()->GetGUID().GetCounter(), item->GetEntry());
            _heirlooms.erase(itr);
            _heirlooms[newItemId] = 0;
            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                sAccountCache->AddAccountHeirloomEntity(_owner->GetAccountId(), newItemId, 0);
            else
                sCharacterCache->AddCharacterHeirloomEntity(_owner->GetPlayer()->GetGUID().GetCounter(), newItemId, 0);

            return;
        }

        std::vector<uint32> const& fields = item->GetBonus()->BonusListIDs;

        for (uint32 bonusId : fields)
            if (bonusId != itr->second.bonusId)
                for (uint32 bonus2 : fields)
                    item->RemoveBonuses(bonus2);

        if (std::find(fields.begin(), fields.end(), itr->second.bonusId) == fields.end())
            item->AddBonuses(itr->second.bonusId);
    }
}

bool CollectionMgr::CanApplyHeirloomXpBonus(uint32 itemId, uint32 level)
{
    if (!sDB2Manager.GetHeirloomByItemId(itemId))
        return false;

    HeirloomContainer::iterator itr = _heirlooms.find(itemId);
    if (itr == _heirlooms.end())
        return false;

    if (itr->second.flags & HEIRLOOM_FLAG_BONUS_LEVEL_110)
        return level <= 110;
    if (itr->second.flags & HEIRLOOM_FLAG_BONUS_LEVEL_100)
        return level <= 100;
    if (itr->second.flags & HEIRLOOM_FLAG_BONUS_LEVEL_90)
        return level <= 90;

    return level <= 60;
}

void CollectionMgr::LoadMounts()
{
    for (auto const& m : _mounts)
        AddMount(m.first, m.second, false, false);
}

void CollectionMgr::LoadAccountMounts(PreparedQueryResult result, AccountEntityCache const* accountCache, CharacterCacheEntry const* characterCache)
{
    _mounts.clear();

    if (result)
    {
        do
        {
            Field* fields = result->Fetch();
            uint32 mountSpellId = fields[0].GetUInt32();
            MountStatusFlags flags = MountStatusFlags(fields[1].GetUInt8());

            if (!sDB2Manager.GetMount(mountSpellId))
                continue;

            _mounts[mountSpellId] = flags;

            if (PROJECT::Config->GetBool("Mounts.LinkedToAccount"))
                sAccountCache->AddAccountMountEntity(_owner->GetAccountId(), mountSpellId, uint32(flags));
            else
                sCharacterCache->AddCharacterMountEntity(_owner->GetPlayer()->GetGUID().GetCounter(), mountSpellId, uint32(flags));
        } while (result->NextRow());
    }
    else if (accountCache && !accountCache->Mounts.empty() || characterCache && !characterCache->Mounts.empty())
    {
        auto const& values = accountCache && !accountCache->Mounts.empty() ? accountCache->Mounts : characterCache->Mounts;
        for (AccountMountsCache const& mount : values)
        {
            if (!sDB2Manager.GetMount(mount.SpellID))
                continue;

            _mounts[mount.SpellID] = MountStatusFlags(mount.Flags);

        }
    }
}

void CollectionMgr::SaveAccountMounts(SQLTransaction& trans, ObjectGuid guid)
{
    for (auto const& mount : _mounts)
    {
        PreparedStatement* stmt = nullptr;
        if (PROJECT::Config->GetBool("Mounts.LinkedToAccount"))
        {
            stmt = LoginDatabase.GetPreparedStatement(LOGIN_REP_ACCOUNT_MOUNTS);
            stmt->setUInt32(0, _owner->GetAccountId());
        }
        else
        {
            stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_REP_CHARACTER_MOUNTS);
            stmt->setUInt32(0, guid.GetCounter());
        }
        stmt->setUInt32(1, mount.first);
        stmt->setUInt8(2, mount.second);
        trans->Append(stmt);
    }
}

bool CollectionMgr::AddMount(uint32 spellId, MountStatusFlags flags, bool factionMount /*= false*/, bool learned /*= false*/)
{
    Player* player = _owner->GetPlayer();
    if (!player)
        return false;

    MountEntry const* mount = sDB2Manager.GetMount(spellId);
    if (!mount)
        return false;

    MountDefinitionMap::const_iterator itr = FactionSpecificMounts.find(spellId);
    if (itr != FactionSpecificMounts.end() && !factionMount)
        AddMount(itr->second, flags, true, learned);

    _mounts.insert(MountContainer::value_type(spellId, flags));

    // Mount condition only applies to using it, should still learn it.
    if (mount->PlayerConditionID)
    {
        PlayerConditionEntry const* playerCondition = sPlayerConditionStore.LookupEntry(mount->PlayerConditionID);
        if (playerCondition && !ConditionMgr::IsPlayerMeetingCondition(player, playerCondition))
            return false;
    }

    if (!learned)
    {
        if (!factionMount)
            SendSingleMountUpdate(std::make_pair(spellId, flags));
        if (!player->HasSpell(spellId))
            player->LearnSpell(spellId, true);
    }

    return true;
}

void CollectionMgr::MountSetFavorite(uint32 spellId, bool favorite)
{
    auto itr = _mounts.find(spellId);
    if (itr == _mounts.end())
        return;

    if (favorite)
        itr->second = MountStatusFlags(itr->second | MOUNT_IS_FAVORITE);
    else
        itr->second = MountStatusFlags(itr->second & ~MOUNT_IS_FAVORITE);


    if (PROJECT::Config->GetBool("Mounts.LinkedToAccount"))
        sAccountCache->UpdateAccountMountEntity(_owner->GetAccountId(), spellId, uint32(itr->second));
    else
        sCharacterCache->UpdateCharacterMountEntity(_owner->GetPlayer()->GetGUID().GetCounter(), spellId, uint32(itr->second));

    SendSingleMountUpdate(*itr);
}

void CollectionMgr::SendSingleMountUpdate(std::pair<uint32, MountStatusFlags> mount)
{
    Player* player = _owner->GetPlayer();
    if (!player)
        return;

    // Temporary container, just need to store only selected mount
    MountContainer tempMounts;
    tempMounts.insert(mount);

    WorldPackets::Misc::AccountMountUpdate mountUpdate;
    mountUpdate.IsFullUpdate = false;
    mountUpdate.Mounts = &tempMounts;
    player->SendDirectMessage(mountUpdate.Write());
}

struct DynamicBitsetBlockOutputIterator : public std::iterator<std::output_iterator_tag, void, void, void, void>
{
    explicit DynamicBitsetBlockOutputIterator(std::function<void(uint32)>&& action) : _action(std::forward<std::function<void(uint32)>>(action)) { }

    DynamicBitsetBlockOutputIterator& operator=(uint32 value)
    {
        _action(value);
        return *this;
    }

    DynamicBitsetBlockOutputIterator& operator*() { return *this; }
    DynamicBitsetBlockOutputIterator& operator++() { return *this; }
    DynamicBitsetBlockOutputIterator operator++(int) { return *this; }

private:
    std::function<void(uint32)> _action;
};

void CollectionMgr::LoadItemAppearances()
{
    boost::to_block_range(*_appearances, DynamicBitsetBlockOutputIterator([this](uint32 blockValue)
    {
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, blockValue);
    }));

    for (auto itr = _temporaryAppearances.begin(); itr != _temporaryAppearances.end(); ++itr)
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_CONDITIONAL_TRANSMOG, itr->first);
}

void CollectionMgr::LoadAccountItemAppearances(PreparedQueryResult knownAppearances, PreparedQueryResult favoriteAppearances, AccountEntityCache const* accountCache, CharacterCacheEntry const* characterCache)
{
    _appearances->clear();
    _favoriteAppearances.clear();

    if (knownAppearances)
    {
        std::vector<uint32> blocks;
        do
        {
            Field* fields = knownAppearances->Fetch();
            uint16 blobIndex = fields[0].GetUInt16();
            if (blobIndex >= blocks.size())
                blocks.resize(blobIndex + 1);

            blocks[blobIndex] = fields[1].GetUInt32();

            if (PROJECT::Config->GetBool("Transmog.LinkedToAccount"))
                sAccountCache->AddAccountItemAppearanceEntity(_owner->GetAccountId(), blobIndex, blocks[blobIndex]);
            else
                sCharacterCache->AddCharacterItemAppearanceEntity(_owner->GetPlayer()->GetGUID().GetCounter(), blobIndex, blocks[blobIndex]);

        } while (knownAppearances->NextRow());

        _appearances->init_from_block_range(blocks.begin(), blocks.end());
    }

    if (favoriteAppearances)
    {
        do
        {
            _favoriteAppearances[favoriteAppearances->Fetch()[0].GetUInt32()] = FavoriteAppearanceState::Unchanged;
            if (PROJECT::Config->GetBool("Transmog.LinkedToAccount"))
                sAccountCache->UpdateAccountItemAppearanceFavoriteEntity(_owner->GetAccountId(), favoriteAppearances->Fetch()[0].GetUInt32(), true);
            else
                sCharacterCache->UpdateCharacterItemAppearanceFavoriteEntity(_owner->GetPlayer()->GetGUID().GetCounter(), favoriteAppearances->Fetch()[0].GetUInt32(), true);
        } while (favoriteAppearances->NextRow());
    }

    if (accountCache && !accountCache->ItemAppearances.empty() || characterCache && !characterCache->ItemAppearances.empty())
    {
        auto const& values1 = accountCache && !accountCache->ItemAppearances.empty() ? accountCache->ItemAppearances : characterCache->ItemAppearances;
        std::vector<uint32> blocks;
        for (AccountItemAppearancesCache const& app : values1)
        {
            if (app.BlobIndex >= blocks.size())
                blocks.resize(app.BlobIndex + 1);

            blocks[app.BlobIndex] = app.Mask;
        }
        _appearances->init_from_block_range(blocks.begin(), blocks.end());

        auto const& values2 = accountCache ? accountCache->ItemFavoriteAppearances : characterCache->ItemFavoriteAppearances;
        for (AccountItemFavoriteAppearancesCache const& app : values2)
            _favoriteAppearances[app.ItemModifiedAppearanceID] = FavoriteAppearanceState::Unchanged;
    }

    // Static item appearances known by every player
    static uint32 constexpr hiddenAppearanceItems[] =
    {
        134110, // Hidden Helm
        134111, // Hidden Cloak
        134112, // Hidden Shoulder
        142503, // Hidden Shirt
        142504, // Hidden Tabard
        143539  // Hidden Belt
    };

    for (uint32 hiddenItem : hiddenAppearanceItems)
    {
        ItemModifiedAppearanceEntry const* hiddenAppearance = sDB2Manager.GetItemModifiedAppearance(hiddenItem, 0);
        ASSERT(hiddenAppearance);
        if (_appearances->size() <= hiddenAppearance->ID)
            _appearances->resize(hiddenAppearance->ID + 1);

        _appearances->set(hiddenAppearance->ID);
    }
}

void CollectionMgr::SaveAccountItemAppearances(SQLTransaction& trans, ObjectGuid guid)
{
    uint16 blockIndex = 0;
    boost::to_block_range(*_appearances, DynamicBitsetBlockOutputIterator([this, &blockIndex, trans, guid](uint32 blockValue)
    {
        if (blockValue) // this table is only appended/bits are set (never cleared) so don't save empty blocks
        {
            PreparedStatement* stmt = nullptr;
            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
            {
                stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_ITEM_APPEARANCES);
                stmt->setUInt32(0, _owner->GetAccountId());
            }
            else
            {
                stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_INS_CHARACTER_ITEM_APPEARANCES);
                stmt->setUInt32(0, guid.GetCounter());
            }
            stmt->setUInt16(1, blockIndex);
            stmt->setUInt32(2, blockValue);
            trans->Append(stmt);
        }

        ++blockIndex;
    }));

    PreparedStatement* stmt;
    for (auto itr = _favoriteAppearances.begin(); itr != _favoriteAppearances.end();)
    {
        switch (itr->second)
        {
            case FavoriteAppearanceState::New:
                if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                {
                    stmt = LoginDatabase.GetPreparedStatement(LOGIN_INS_ACCOUNT_ITEM_FAVORITE_APPEARANCE);
                    stmt->setUInt32(0, _owner->GetAccountId());
                }
                else
                {
                    stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_INS_CHARACTER_ITEM_FAVORITE_APPEARANCE);
                    stmt->setUInt32(0, _owner->GetPlayer()->GetGUID().GetCounter());
                }
                stmt->setUInt32(1, itr->first);
                trans->Append(stmt);
                itr->second = FavoriteAppearanceState::Unchanged;
                ++itr;
                break;
            case FavoriteAppearanceState::Removed:
                if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                {
                    stmt = LoginDatabase.GetPreparedStatement(LOGIN_DEL_ACCOUNT_ITEM_FAVORITE_APPEARANCE);
                    stmt->setUInt32(0, _owner->GetAccountId());
                }
                else
                {
                    stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_DEL_CHARACTER_ITEM_FAVORITE_APPEARANCE);
                    stmt->setUInt32(0, _owner->GetPlayer()->GetGUID().GetCounter());
                }
                stmt->setUInt32(1, itr->first);
                trans->Append(stmt);
                itr = _favoriteAppearances.erase(itr);
                break;
            case FavoriteAppearanceState::Unchanged:
                ++itr;
                break;
        }
    }
}

void CollectionMgr::AddItemAppearance(Item* item)
{
    if (!item->IsSoulBound())
        return;

    ItemModifiedAppearanceEntry const* itemModifiedAppearance = item->GetItemModifiedAppearance();
    if (!CanAddAppearance(itemModifiedAppearance))
        return;

    if (item->GetUInt32Value(ITEM_FIELD_FLAGS) & (ITEM_FIELD_FLAG_BOP_TRADEABLE | ITEM_FIELD_FLAG_REFUNDABLE))
    {
        AddTemporaryAppearance(item->GetGUID(), itemModifiedAppearance);
        return;
    }

    AddItemAppearance(itemModifiedAppearance);
}

void CollectionMgr::AddItemAppearance(uint32 itemId, uint32 appearanceModId /*= 0*/)
{
    ItemModifiedAppearanceEntry const* itemModifiedAppearance = sDB2Manager.GetItemModifiedAppearance(itemId, appearanceModId);
    if (!CanAddAppearance(itemModifiedAppearance))
        return;

    AddItemAppearance(itemModifiedAppearance);
}

void CollectionMgr::AddTransmogSet(uint32 transmogSetId)
{
    std::vector<TransmogSetItemEntry const*> const* items = sDB2Manager.GetTransmogSetItems(transmogSetId);
    if (!items)
        return;

    for (TransmogSetItemEntry const* item : *items)
    {
        ItemModifiedAppearanceEntry const* itemModifiedAppearance = sItemModifiedAppearanceStore.LookupEntry(item->ItemModifiedAppearanceID);
        if (!itemModifiedAppearance)
            continue;

        AddItemAppearance(itemModifiedAppearance);
    }
}

bool CollectionMgr::IsSetCompleted(uint32 transmogSetId) const
{
    std::vector<TransmogSetItemEntry const*> const* transmogSetItems = sDB2Manager.GetTransmogSetItems(transmogSetId);
    if (!transmogSetItems)
        return false;

    std::array<int8, EQUIPMENT_SLOT_END> knownPieces;
    knownPieces.fill(-1);
    for (TransmogSetItemEntry const* transmogSetItem : *transmogSetItems)
    {
        ItemModifiedAppearanceEntry const* itemModifiedAppearance = sItemModifiedAppearanceStore.LookupEntry(transmogSetItem->ItemModifiedAppearanceID);
        if (!itemModifiedAppearance)
            continue;

        ItemEntry const* item = sItemStore.LookupEntry(itemModifiedAppearance->ItemID);
        if (!item)
            continue;

        int32 transmogSlot = ItemTransmogrificationSlots[item->InventoryType];
        if (transmogSlot < 0 || knownPieces[transmogSlot] == 1)
            continue;

        bool hasAppearance, isTemporary;
        std::tie(hasAppearance, isTemporary) = HasItemAppearance(transmogSetItem->ItemModifiedAppearanceID);

        knownPieces[transmogSlot] = (hasAppearance && !isTemporary) ? 1 : 0;
    }

    return std::find(knownPieces.begin(), knownPieces.end(), 0) == knownPieces.end();
}

bool CollectionMgr::CanAddAppearance(ItemModifiedAppearanceEntry const* itemModifiedAppearance) const
{
    if (!itemModifiedAppearance)
        return false;

    if (itemModifiedAppearance->TransmogSourceTypeEnum == 6 || itemModifiedAppearance->TransmogSourceTypeEnum == 9)
        return false;

    if (!sItemSearchNameStore.LookupEntry(itemModifiedAppearance->ItemID))
        return false;

    ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemModifiedAppearance->ItemID);
    if (!itemTemplate)
        return false;

    if (_owner->GetPlayer()->CanUseItem(itemTemplate) != EQUIP_ERR_OK)
        return false;

    if (itemTemplate->GetFlags2() & ITEM_FLAG2_NO_SOURCE_FOR_ITEM_VISUAL || itemTemplate->GetQuality() == ITEM_QUALITY_ARTIFACT)
        return false;

    switch (itemTemplate->GetClass())
    {
        case ITEM_CLASS_WEAPON:
        {
            if (!(_owner->GetPlayer()->GetWeaponProficiency() & (1 << itemTemplate->GetSubClass())))
                return false;
            if (itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_EXOTIC ||
                itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_EXOTIC2 ||
                itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_MISCELLANEOUS ||
                itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_THROWN ||
                itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_SPEAR ||
                itemTemplate->GetSubClass() == ITEM_SUBCLASS_WEAPON_FISHING_POLE)
                return false;
            break;
        }
        case ITEM_CLASS_ARMOR:
        {
            switch (itemTemplate->GetInventoryType())
            {
                case INVTYPE_BODY:
                case INVTYPE_SHIELD:
                case INVTYPE_CLOAK:
                case INVTYPE_TABARD:
                case INVTYPE_HOLDABLE:
                    break;
                case INVTYPE_HEAD:
                case INVTYPE_SHOULDERS:
                case INVTYPE_CHEST:
                case INVTYPE_WAIST:
                case INVTYPE_LEGS:
                case INVTYPE_FEET:
                case INVTYPE_WRISTS:
                case INVTYPE_HANDS:
                case INVTYPE_ROBE:
                    if (itemTemplate->GetSubClass() == ITEM_SUBCLASS_ARMOR_MISCELLANEOUS)
                        return false;
                    break;
                default:
                    return false;
            }
            if (itemTemplate->GetInventoryType() != INVTYPE_CLOAK)
                if (!(PlayerClassByArmorSubclass[itemTemplate->GetSubClass()] & _owner->GetPlayer()->getClassMask()))
                    return false;
            break;
        }
        default:
            return false;
    }

    if (itemTemplate->GetQuality() < ITEM_QUALITY_UNCOMMON)
        if (!(itemTemplate->GetFlags2() & ITEM_FLAG2_IGNORE_QUALITY_FOR_ITEM_VISUAL_SOURCE) || !(itemTemplate->GetFlags3() & ITEM_FLAG3_ACTS_AS_TRANSMOG_HIDDEN_VISUAL_OPTION))
            return false;

    if (itemModifiedAppearance->ID < _appearances->size() && _appearances->test(itemModifiedAppearance->ID))
        return false;

    return true;
}

void CollectionMgr::AddItemAppearance(ItemModifiedAppearanceEntry const* itemModifiedAppearance)
{
    if (_appearances->size() <= itemModifiedAppearance->ID)
    {
        std::size_t numBlocks = _appearances->num_blocks();
        _appearances->resize(itemModifiedAppearance->ID + 1);
        numBlocks = _appearances->num_blocks() - numBlocks;
        while (numBlocks--)
            _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, 0);
    }

    _appearances->set(itemModifiedAppearance->ID);
    uint32 blockIndex = itemModifiedAppearance->ID / 32;
    uint32 bitIndex = itemModifiedAppearance->ID % 32;
    uint32 currentMask = _owner->GetPlayer()->GetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, blockIndex);
    _owner->GetPlayer()->SetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, blockIndex, currentMask | (1 << bitIndex));

    if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
        sAccountCache->AddAccountItemAppearanceEntity(_owner->GetAccountId(), blockIndex, _owner->GetPlayer()->GetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, blockIndex));
    else
        sCharacterCache->AddCharacterItemAppearanceEntity(_owner->GetPlayer()->GetGUID().GetCounter(), blockIndex, _owner->GetPlayer()->GetDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_TRANSMOG, blockIndex));
    auto temporaryAppearance = _temporaryAppearances.find(itemModifiedAppearance->ID);
    if (temporaryAppearance != _temporaryAppearances.end())
    {
        _owner->GetPlayer()->RemoveDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_CONDITIONAL_TRANSMOG, itemModifiedAppearance->ID);
        _temporaryAppearances.erase(temporaryAppearance);
    }

    if (ItemEntry const* item = sItemStore.LookupEntry(itemModifiedAppearance->ItemID))
    {
        int32 transmogSlot = ItemTransmogrificationSlots[item->InventoryType];
        if (transmogSlot >= 0)
            _owner->GetPlayer()->UpdateCriteria(CRITERIA_TYPE_APPEARANCE_UNLOCKED_BY_SLOT, transmogSlot, 1);
    }

    if (std::vector<TransmogSetEntry const*> const* sets = sDB2Manager.GetTransmogSetsForItemModifiedAppearance(itemModifiedAppearance->ID))
        for (TransmogSetEntry const* set : *sets)
            if (IsSetCompleted(set->ID))
                _owner->GetPlayer()->UpdateCriteria(CRITERIA_TYPE_TRANSMOG_SET_UNLOCKED, set->TransmogSetGroupID);
}

void CollectionMgr::AddTemporaryAppearance(ObjectGuid const& itemGuid, ItemModifiedAppearanceEntry const* itemModifiedAppearance)
{
    std::unordered_set<ObjectGuid>& itemsWithAppearance = _temporaryAppearances[itemModifiedAppearance->ID];
    if (itemsWithAppearance.empty())
        _owner->GetPlayer()->AddDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_CONDITIONAL_TRANSMOG, itemModifiedAppearance->ID);

    itemsWithAppearance.insert(itemGuid);
}

void CollectionMgr::RemoveTemporaryAppearance(Item* item)
{
    ItemModifiedAppearanceEntry const* itemModifiedAppearance = item->GetItemModifiedAppearance();
    if (!itemModifiedAppearance)
        return;

    auto itr = _temporaryAppearances.find(itemModifiedAppearance->ID);
    if (itr == _temporaryAppearances.end())
        return;

    itr->second.erase(item->GetGUID());
    if (itr->second.empty())
    {
        _owner->GetPlayer()->RemoveDynamicValue(ACTIVE_PLAYER_DYNAMIC_FIELD_CONDITIONAL_TRANSMOG, itemModifiedAppearance->ID);
        _temporaryAppearances.erase(itr);
    }
}

std::pair<bool, bool> CollectionMgr::HasItemAppearance(uint32 itemModifiedAppearanceId) const
{
    if (itemModifiedAppearanceId < _appearances->size() && _appearances->test(itemModifiedAppearanceId))
        return{ true, false };

    if (_temporaryAppearances.find(itemModifiedAppearanceId) != _temporaryAppearances.end())
        return{ true,true };

    return{ false,false };
}

std::unordered_set<ObjectGuid> CollectionMgr::GetItemsProvidingTemporaryAppearance(uint32 itemModifiedAppearanceId) const
{
    auto temporaryAppearance = _temporaryAppearances.find(itemModifiedAppearanceId);
    if (temporaryAppearance != _temporaryAppearances.end())
        return temporaryAppearance->second;

    return std::unordered_set<ObjectGuid>();
}

void CollectionMgr::SetAppearanceIsFavorite(uint32 itemModifiedAppearanceId, bool apply)
{
    auto itr = _favoriteAppearances.find(itemModifiedAppearanceId);
    if (apply)
    {
        if (itr == _favoriteAppearances.end())
        {
            _favoriteAppearances[itemModifiedAppearanceId] = FavoriteAppearanceState::New;
            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                sAccountCache->UpdateAccountItemAppearanceFavoriteEntity(_owner->GetAccountId(), itemModifiedAppearanceId, true);
            else
                sCharacterCache->UpdateCharacterItemAppearanceFavoriteEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemModifiedAppearanceId, true);
        }
        else if (itr->second == FavoriteAppearanceState::Removed)
            itr->second = FavoriteAppearanceState::Unchanged;
        else
            return;
    }
    else if (itr != _favoriteAppearances.end())
    {
        if (itr->second == FavoriteAppearanceState::New)
        {
            _favoriteAppearances.erase(itemModifiedAppearanceId);
            if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
                sAccountCache->UpdateAccountItemAppearanceFavoriteEntity(_owner->GetAccountId(), itemModifiedAppearanceId, false);
            else
                sCharacterCache->UpdateCharacterItemAppearanceFavoriteEntity(_owner->GetPlayer()->GetGUID().GetCounter(), itemModifiedAppearanceId, false);
        }
        else
            itr->second = FavoriteAppearanceState::Removed;
    }
    else
        return;

    WorldPackets::Transmogrification::TransmogCollectionUpdate transmogCollectionUpdate;
    transmogCollectionUpdate.IsFullUpdate = false;
    transmogCollectionUpdate.IsSetFavorite = apply;
    transmogCollectionUpdate.FavoriteAppearances.push_back(itemModifiedAppearanceId);

    _owner->SendPacket(transmogCollectionUpdate.Write());
}

void CollectionMgr::SendFavoriteAppearances() const
{
    WorldPackets::Transmogrification::TransmogCollectionUpdate transmogCollectionUpdate;
    transmogCollectionUpdate.IsFullUpdate = true;
    transmogCollectionUpdate.FavoriteAppearances.reserve(_favoriteAppearances.size());
    for (auto itr = _favoriteAppearances.begin(); itr != _favoriteAppearances.end(); ++itr)
        if (itr->second != FavoriteAppearanceState::Removed)
            transmogCollectionUpdate.FavoriteAppearances.push_back(itr->first);

    _owner->SendPacket(transmogCollectionUpdate.Write());
}
