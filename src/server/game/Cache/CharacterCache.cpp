/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CharacterCache.h"
#include "ArenaTeam.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "Player.h"
#include "QueryResult.h"
#include "World.h"
#include "WorldPacket.h"
#include "MiscPackets.h"

#include <unordered_map>

namespace
{
    std::unordered_map<ObjectGuid::LowType, CharacterCacheEntry> _characterCacheStore;
    std::unordered_map<std::string, CharacterCacheEntry*> _characterCacheByNameStore;
}

CharacterCache::CharacterCache()
{
}

CharacterCache::~CharacterCache()
{
}

CharacterCache* CharacterCache::instance()
{
    static CharacterCache instance;
    return &instance;
}

/**
* @brief Loads several pieces of information on server startup with the GUID
* There is no further database query necessary.
* These are a number of methods that work into the calling function.
*
* @param guid Requires a guid to call
* @return Name, Gender, Race, Class and Level of player character
* Example Usage:
* @code
*    CharacterInfo const* characterInfo = sCharacterCache->HasCharacterCacheEntry(GUID);
*    if (!characterInfo)
*        return;
*
*    std::string playerName = characterInfo->Name;
*    uint8 playerGender = characterInfo->Sex;
*    uint8 playerRace = characterInfo->Race;
*    uint8 playerClass = characterInfo->Class;
*    uint8 playerLevel = characterInfo->Level;
* @endcode
**/

void CharacterCache::LoadCharacterCacheStorage()
{
    _characterCacheStore.clear();
    uint32 oldMSTime = getMSTime();

    QueryResult result = CharacterDatabase.Query("SELECT guid, name, account, race, gender, class, level, deleteDate FROM characters");
    if (!result)
    {
        TC_LOG_INFO("server.loading", "No character name data loaded, empty query");
        return;
    }

    do
    {
        Field* fields = result->Fetch();
        AddCharacterCacheEntry(fields[0].GetUInt32() /*guid*/, fields[2].GetUInt32() /*account*/, fields[1].GetString() /*name*/,
            fields[4].GetUInt8() /*gender*/, fields[3].GetUInt8() /*race*/, fields[5].GetUInt8() /*class*/, fields[6].GetUInt8() /*level*/, fields[7].GetUInt32() != 0 /*isDeleted*/);
    } while (result->NextRow());

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_TOYS);
    PreparedQueryResult stmtResult = CharacterDatabase.Query(stmt);

    if (stmtResult)
    {
        do
        {
            Field* fields = stmtResult->Fetch();
            AddCharacterToyEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetBool());

        } while (stmtResult->NextRow());
    }

    stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_HEIRLOOMS);
    stmtResult = CharacterDatabase.Query(stmt);

    if (stmtResult)
    {
        do
        {
            Field* fields = stmtResult->Fetch();
            AddCharacterHeirloomEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetUInt32());

        } while (stmtResult->NextRow());

        stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_MOUNTS);
        stmtResult = CharacterDatabase.Query(stmt);
    }
    if (stmtResult)
    {
        do
        {
            Field* fields = stmtResult->Fetch();
            AddCharacterMountEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetUInt8());

        } while (stmtResult->NextRow());

    }

    stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_ITEM_APPEARANCES);
    stmtResult = CharacterDatabase.Query(stmt);

    if (stmtResult)
    {
        do
        {
            Field* fields = stmtResult->Fetch();
            AddCharacterItemAppearanceEntity(fields[2].GetUInt32(), fields[0].GetUInt16(), fields[1].GetUInt32());

        } while (stmtResult->NextRow());

        stmt = CharacterDatabase.GetPreparedStatement(PROJECT_CHAR_SEL_CACHE_CHARACTER_ITEM_FAVORITE_APPEARANCES);
        stmtResult = CharacterDatabase.Query(stmt);
    }
    if (stmtResult)
    {
        do
        {
            Field* fields = stmtResult->Fetch();
            UpdateCharacterItemAppearanceFavoriteEntity(fields[1].GetUInt32(), fields[0].GetUInt32(), true);

        } while (stmtResult->NextRow());
    }
    TC_LOG_INFO("server.loading", "Loaded character infos for " SZFMTD " characters in %u ms", _characterCacheStore.size(), GetMSTimeDiffToNow(oldMSTime));
}

/*
Modifying functions
*/
void CharacterCache::AddCharacterCacheEntry(ObjectGuid::LowType const& guid, uint32 accountId, std::string const& name, uint8 gender, uint8 race, uint8 playerClass, uint8 level, bool isDeleted)
{
    CharacterCacheEntry& data = _characterCacheStore[guid];
    data.Guid = guid;
    data.Name = name;
    data.AccountId = accountId;
    data.Race = race;
    data.Sex = gender;
    data.Class = playerClass;
    data.Level = level;
    data.GuildId = 0;                           // Will be set in guild loading or guild setting
    data.IsDeleted = isDeleted;
    for (uint8 i = 0; i < MAX_ARENA_SLOT; ++i)
        data.ArenaTeamId[i] = 0;                // Will be set in arena teams loading

    // Fill Name to Guid Store
    _characterCacheByNameStore[name] = &data;
}

void CharacterCache::DeleteCharacterCacheEntry(ObjectGuid::LowType const& guid, std::string const& name)
{
    _characterCacheStore.erase(guid);
    _characterCacheByNameStore.erase(name);
}

void CharacterCache::UpdateCharacterData(ObjectGuid::LowType const& guid, std::string const& name, uint8 gender /*= GENDER_NONE*/, uint8 race /*= RACE_NONE*/)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    std::string oldName = itr->second.Name;
    itr->second.Name = name;

    if (gender != GENDER_NONE)
        itr->second.Sex = gender;

    if (race != RACE_NONE)
        itr->second.Race = race;

    WorldPackets::Misc::InvalidatePlayer packet;
    packet.Guid = ObjectGuid::Create<HighGuid::Player>(guid);
    sWorld->SendGlobalMessage(packet.Write());

    // Correct name -> pointer storage
    _characterCacheByNameStore.erase(oldName);
    _characterCacheByNameStore[name] = &itr->second;
}

void CharacterCache::UpdateCharacterLevel(ObjectGuid::LowType const& guid, uint8 level)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    itr->second.Level = level;
}

void CharacterCache::UpdateCharacterAccountId(ObjectGuid::LowType const& guid, uint32 accountId)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    itr->second.AccountId = accountId;
}

void CharacterCache::UpdateCharacterGuildId(ObjectGuid::LowType const& guid, ObjectGuid::LowType guildId)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    itr->second.GuildId = guildId;
}

void CharacterCache::UpdateCharacterArenaTeamId(ObjectGuid::LowType const& guid, uint8 slot, uint32 arenaTeamId)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    itr->second.ArenaTeamId[slot] = arenaTeamId;
}

void CharacterCache::UpdateCharacterIsDeleted(ObjectGuid::LowType const& guid, bool isDeleted, std::string const& name)
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return;

    itr->second.IsDeleted = isDeleted;

    if (!name.empty())
        itr->second.Name = name;
}

/*
Getters
*/
bool CharacterCache::HasCharacterCacheEntry(ObjectGuid::LowType const& guid) const
{
    return _characterCacheStore.find(guid) != _characterCacheStore.end();
}

CharacterCacheEntry* CharacterCache::GetCharacterCacheByGuid(ObjectGuid::LowType const& guid) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr != _characterCacheStore.end())
        return &itr->second;

    return nullptr;
}

CharacterCacheEntry* CharacterCache::GetCharacterCacheByName(std::string const& name) const
{
    auto itr = _characterCacheByNameStore.find(name);
    if (itr != _characterCacheByNameStore.end())
        return itr->second;

    return nullptr;
}

ObjectGuid::LowType CharacterCache::GetCharacterGuidByName(std::string const& name) const
{
    auto itr = _characterCacheByNameStore.find(name);
    if (itr != _characterCacheByNameStore.end())
        return itr->second->Guid;

    return ObjectGuid::Empty.GetCounter();
}

bool CharacterCache::GetCharacterNameByGuid(ObjectGuid::LowType guid, std::string& name) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return false;

    name = itr->second.Name;
    return true;
}

uint32 CharacterCache::GetCharacterTeamByGuid(ObjectGuid::LowType guid) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return 0;

    return Player::TeamForRace(itr->second.Race);
}

uint32 CharacterCache::GetCharacterAccountIdByGuid(ObjectGuid::LowType guid) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return 0;

    return itr->second.AccountId;
}

uint32 CharacterCache::GetCharacterAccountIdByName(std::string const& name) const
{
    auto itr = _characterCacheByNameStore.find(name);
    if (itr != _characterCacheByNameStore.end())
        return itr->second->AccountId;

    return 0;
}

uint8 CharacterCache::GetCharacterLevelByGuid(ObjectGuid::LowType guid) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return 0;

    return itr->second.Level;
}

ObjectGuid::LowType CharacterCache::GetCharacterGuildIdByGuid(ObjectGuid::LowType guid) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return 0;

    return itr->second.GuildId;
}

uint32 CharacterCache::GetCharacterArenaTeamIdByGuid(ObjectGuid::LowType guid, uint8 type) const
{
    auto itr = _characterCacheStore.find(guid);
    if (itr == _characterCacheStore.end())
        return 0;

    return itr->second.ArenaTeamId[ArenaTeam::GetSlotByType(type)];
}

void CharacterCache::AddCharacterToyEntity(ObjectGuid::LowType guid, uint32 itemId, bool isFavorite)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto const& obj : CharacterCache->Toys)
        if (obj.ItemID == itemId)
            return;

    CharacterCache->Toys.push_back(AccountToysEntityCache(itemId, isFavorite));
}

void CharacterCache::UpdateCharacterToyEntity(ObjectGuid::LowType guid, uint32 itemId, bool isFavorite)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto& toy : CharacterCache->Toys)
    {
        if (toy.ItemID == itemId)
        {
            toy.IsFavorite = isFavorite;
            break;
        }
    }
}

void CharacterCache::AddCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId, uint32 flags)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto const& obj : CharacterCache->Heirlooms)
        if (obj.ItemID == itemId)
            return;

    CharacterCache->Heirlooms.push_back(AccountHeirloomsCache(itemId, flags));
}

void CharacterCache::UpdateCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId, uint32 flags)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto& heirloom : CharacterCache->Heirlooms)
    {
        if (heirloom.ItemID == itemId)
        {
            heirloom.Flags = flags;
            break;
        }
    }
}

void CharacterCache::DeleteCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (std::vector<AccountHeirloomsCache>::iterator heirloom = CharacterCache->Heirlooms.begin(); heirloom != CharacterCache->Heirlooms.end(); ++heirloom)
    {
        if (heirloom->ItemID == itemId)
        {
            heirloom = CharacterCache->Heirlooms.erase(heirloom);
            break;
        }
    }
}

void CharacterCache::AddCharacterMountEntity(ObjectGuid::LowType guid, uint32 spellId, uint32 flags)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto const& obj : CharacterCache->Mounts)
        if (obj.SpellID == spellId)
            return;

    CharacterCache->Mounts.push_back(AccountMountsCache(spellId, flags));
}

void CharacterCache::UpdateCharacterMountEntity(ObjectGuid::LowType guid, uint32 spellId, uint32 flags)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto& mount : CharacterCache->Mounts)
    {
        if (mount.SpellID == spellId)
        {
            mount.Flags = flags;
            return;
        }
    }

    AddCharacterMountEntity(guid, spellId, flags);
}

void CharacterCache::AddCharacterItemAppearanceEntity(ObjectGuid::LowType guid, uint32 block, uint32 mask)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    for (auto& item : CharacterCache->ItemAppearances)
    {
        if (item.BlobIndex == block)
        {
            item.Mask = mask;
            return;
        }
    }
    CharacterCache->ItemAppearances.push_back(AccountItemAppearancesCache(block, mask));
}

void CharacterCache::UpdateCharacterItemAppearanceFavoriteEntity(ObjectGuid::LowType guid, uint32 appearance, bool apply)
{
    CharacterCacheEntry* CharacterCache = GetCharacterCacheByGuid(guid);
    if (!CharacterCache)
        return;

    if (apply)
        CharacterCache->ItemFavoriteAppearances.push_back(AccountItemFavoriteAppearancesCache(appearance));
    else
    {
        for (std::vector<AccountItemFavoriteAppearancesCache>::iterator item = CharacterCache->ItemFavoriteAppearances.begin(); item != CharacterCache->ItemFavoriteAppearances.end(); ++item)
        {
            if (item->ItemModifiedAppearanceID == appearance)
            {
                item = CharacterCache->ItemFavoriteAppearances.erase(item);
                break;
            }
        }
    }
}
