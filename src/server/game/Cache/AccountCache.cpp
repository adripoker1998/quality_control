#include "AccountCache.h"
#include "DatabaseEnv.h"
#include "QueryResult.h"
#include "World.h"
#include "Realm.h"
#include "Log.h"
#include "WorldSession.h"
#include "project_config.h"

#include <unordered_map>
#include <boost/dynamic_bitset.hpp>

namespace
{
    std::unordered_map<uint32, AccountEntityCache> _accountCacheStore;
}

AccountCache::AccountCache()
{
}

AccountCache::~AccountCache()
{
}

AccountCache* AccountCache::instance()
{
    static AccountCache instance;
    return &instance;
}

bool AccountCache::IsAccountCacheActive()
{
    return sWorld->getBoolConfig(CONFIG_ACCOUNT_CACHE_ACTIVE);
}

void AccountCache::LoadAccountCacheStorage()
{
    _accountCacheStore.clear();
    uint32 oldMSTime = getMSTime();

    QueryResult result = LoginDatabase.PQuery("SELECT id FROM account WHERE last_login >= %u ORDER BY id ASC", time(NULL) - sWorld->getIntConfig(CONFIG_ACCOUNT_CACHE_LAST_CONNECTION_TIMESTAMP));
    if (!result)
    {
        TC_LOG_INFO("server.loading", "No account loaded, empty query");
        return;
    }

    do
    {
        Field* fields = result->Fetch();
        _accountCacheStore[fields[0].GetUInt32()] = AccountEntityCache();
        
    } while (result->NextRow());


    PreparedStatement* stmt = nullptr;
    PreparedQueryResult stmtResult = nullptr;

    if (PROJECT::Config->GetBool("Toys.LinkedToAccount"))
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_TOYS);
        stmtResult = LoginDatabase.Query(stmt);

        if (stmtResult)
            do
            {
                Field* fields = stmtResult->Fetch();
                AddAccountToyEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetBool());

            } while (stmtResult->NextRow());
    }

    if (PROJECT::Config->GetBool("Heirlooms.LinkedToAccount"))
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_HEIRLOOMS);
        stmtResult = LoginDatabase.Query(stmt);

        if (stmtResult)
            do
            {
                Field* fields = stmtResult->Fetch();
                AddAccountHeirloomEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetUInt32());

            } while (stmtResult->NextRow());
    }

    if (PROJECT::Config->GetBool("Mounts.LinkedToAccount"))
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_MOUNTS);
        stmtResult = LoginDatabase.Query(stmt);

        if (stmtResult)
            do
            {
                Field* fields = stmtResult->Fetch();
                AddAccountMountEntity(fields[2].GetUInt32(), fields[0].GetUInt32(), fields[1].GetUInt8());

            } while (stmtResult->NextRow());
    }
    stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_CHARACTER_COUNTS);
    stmtResult = LoginDatabase.Query(stmt);

    if (stmtResult)
    do
    {
        Field* fields = stmtResult->Fetch();
        AddAccountRealmCharacterCount(fields[0].GetUInt32(), fields[1].GetUInt8(), fields[2].GetUInt32(), fields[3].GetUInt32(), fields[4].GetUInt32());

    } while (stmtResult->NextRow());


    if (PROJECT::Config->GetBool("Transmog.LinkedToAccount"))
    {
        stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_ITEM_APPEARANCES);
        stmtResult = LoginDatabase.Query(stmt);

        if (stmtResult)
            do
            {
                Field* fields = stmtResult->Fetch();
                AddAccountItemAppearanceEntity(fields[2].GetUInt32(), fields[0].GetUInt16(), fields[1].GetUInt32());

            } while (stmtResult->NextRow());

            stmt = LoginDatabase.GetPreparedStatement(LOGIN_SEL_CACHE_ACCOUNT_ITEM_FAVORITE_APPEARANCES);
            stmtResult = LoginDatabase.Query(stmt);

            if (stmtResult)
                do
                {
                    Field* fields = stmtResult->Fetch();
                    UpdateAccountItemAppearanceFavoriteEntity(fields[1].GetUInt32(), fields[0].GetUInt32(), true);

                } while (stmtResult->NextRow());
    }
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CACHE_ACCOUNT_DATA);
    stmtResult = CharacterDatabase.Query(stmt);

    if (stmtResult)
    do
    {
        Field* fields = stmtResult->Fetch();
        AddAccountData(fields[3].GetUInt32(), fields[0].GetUInt8(), fields[1].GetUInt32(), fields[2].GetString());

    } while (stmtResult->NextRow());

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_CACHE_TUTORIALS);
    stmtResult = CharacterDatabase.Query(stmt);

    if (stmtResult)
    do
    {
        Field* fields = stmtResult->Fetch();
        for (uint8 i = 0; i < MAX_ACCOUNT_TUTORIAL_VALUES; ++i)
            _accountCacheStore[fields[MAX_ACCOUNT_TUTORIAL_VALUES].GetUInt32()].AccountTutorials[i] = fields[i].GetUInt32();

    } while (stmtResult->NextRow());


    TC_LOG_INFO("server.loading", "Loaded account infos for " SZFMTD " characters in %u ms", _accountCacheStore.size(), GetMSTimeDiffToNow(oldMSTime));
}

void AccountCache::CreateCacheIfNeeded(uint32 accountId)
{
    if (!GetAccountCacheByAccountId(accountId))
        _accountCacheStore[accountId] = AccountEntityCache();
}

AccountEntityCache* AccountCache::GetAccountCacheByAccountId(uint32 accountId) const
{
    auto itr = _accountCacheStore.find(accountId);
    if (itr != _accountCacheStore.end())
        return &itr->second;

    return nullptr;
}

void AccountCache::AddAccountToyEntity(uint32 accountId, uint32 itemId, bool isFavorite)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto const& obj : accountCache->Toys)
        if (obj.ItemID == itemId)
            return;

    accountCache->Toys.push_back(AccountToysEntityCache(itemId, isFavorite));
}

void AccountCache::UpdateAccountToyEntity(uint32 accountId, uint32 itemId, bool isFavorite)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& toy : accountCache->Toys)
    {
        if (toy.ItemID == itemId)
        {
            toy.IsFavorite = isFavorite;
            break;
        }
    }
}

void AccountCache::AddAccountHeirloomEntity(uint32 accountId, uint32 itemId, uint32 flags)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto const& obj : accountCache->Heirlooms)
        if (obj.ItemID == itemId)
            return;

    accountCache->Heirlooms.push_back(AccountHeirloomsCache(itemId, flags));
}

void AccountCache::UpdateAccountHeirloomEntity(uint32 accountId, uint32 itemId, uint32 flags)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& heirloom : accountCache->Heirlooms)
    {
        if (heirloom.ItemID == itemId)
        {
            heirloom.Flags = flags;
            break;
        }
    }
}

void AccountCache::DeleteAccountHeirloomEntity(uint32 accountId, uint32 itemId)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (std::vector<AccountHeirloomsCache>::iterator heirloom = accountCache->Heirlooms.begin(); heirloom != accountCache->Heirlooms.end(); ++heirloom)
    {
        if (heirloom->ItemID == itemId)
        {
            heirloom = accountCache->Heirlooms.erase(heirloom);
            break;
        }
    }
}

void AccountCache::AddAccountMountEntity(uint32 accountId, uint32 spellId, uint32 flags)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto const& obj : accountCache->Mounts)
        if (obj.SpellID == spellId)
            return;

    accountCache->Mounts.push_back(AccountMountsCache(spellId, flags));
}

void AccountCache::UpdateAccountMountEntity(uint32 accountId, uint32 spellId, uint32 flags)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& mount : accountCache->Mounts)
    {
        if (mount.SpellID == spellId)
        {
            mount.Flags = flags;
            return;
        }
    }

    AddAccountMountEntity(accountId, spellId, flags);
}

void AccountCache::AddAccountItemAppearanceEntity(uint32 accountId, uint32 block, uint32 mask)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& item : accountCache->ItemAppearances)
    {
        if (item.BlobIndex == block)
        {
            item.Mask = mask;
            return;
        }
    }
    accountCache->ItemAppearances.push_back(AccountItemAppearancesCache(block, mask));
}

void AccountCache::UpdateAccountItemAppearanceFavoriteEntity(uint32 accountId, uint32 appearance, bool apply)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    if (apply)
        accountCache->ItemFavoriteAppearances.push_back(AccountItemFavoriteAppearancesCache(appearance));
    else
    {
        for (std::vector<AccountItemFavoriteAppearancesCache>::iterator item = accountCache->ItemFavoriteAppearances.begin(); item != accountCache->ItemFavoriteAppearances.end(); ++item)
        {
            if (item->ItemModifiedAppearanceID == appearance)
            {
                item = accountCache->ItemFavoriteAppearances.erase(item);
                break;
            }
        }
    }
}

void AccountCache::UpdateAccountRealmCharacterCount(uint32 accountId, uint8 count, uint32 realmID, uint32 region, uint32 battleRegion)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& local : accountCache->RealmCharacterCount)
    {
        if (local.RealmID == realmID || (!realmID && local.RealmID == realm.Id.Realm))
        {
            local.Count = count;
            return;
        }
    }

    AddAccountRealmCharacterCount(accountId, count, realmID, region, battleRegion);
}

void AccountCache::AddAccountRealmCharacterCount(uint32 accountId, uint8 count, uint32 realmID, uint32 region, uint32 battleRegion)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    if (realmID)
        accountCache->RealmCharacterCount.push_back(AccountRealmCharactersCache(count, realmID, region, battleRegion));
    else
        accountCache->RealmCharacterCount.push_back(AccountRealmCharactersCache(count, realm.Id.Realm, realm.Id.Region, realm.Id.Site));
}

void AccountCache::UpdateAccountDataCache(uint32 accountId, AccountDataType type, uint32 time, std::string const& data)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    for (auto& acc : accountCache->AccountData)
    {
        if (acc.Type == type)
        {
            acc.Time = time;
            acc.Data = data;
            return;
        }
    }

    accountCache->AccountData.push_back(AccountDataCache(type, time, data));
}

void AccountCache::UpdateAccountTutorial(uint32 accountId, uint8 index, uint32 value)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    accountCache->AccountTutorials[index] = value;
}

void AccountCache::AddAccountData(uint32 accountId, uint8 type, uint32 time, std::string data)
{
    AccountEntityCache* accountCache = GetAccountCacheByAccountId(accountId);
    if (!accountCache)
        return;

    _accountCacheStore[accountId].AccountData.push_back(AccountDataCache(type, time, data));
}
