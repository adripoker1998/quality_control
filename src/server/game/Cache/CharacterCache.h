/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CHARACTERCACHE_H
#define __CHARACTERCACHE_H

#include "Define.h"
#include "ObjectGuid.h"
#include "SharedDefines.h"
#include "AccountCache.h"

#include <string>

struct CharacterCacheEntry
{
    ObjectGuid::LowType Guid;
    std::string Name;
    uint32 AccountId;
    uint8 Class;
    uint8 Race;
    uint8 Sex;
    uint8 Level;
    ObjectGuid::LowType GuildId;
    bool IsDeleted;
    uint32 ArenaTeamId[3];
    std::vector<AccountToysEntityCache> Toys;
    std::vector<AccountHeirloomsCache> Heirlooms;
    std::vector<AccountMountsCache> Mounts;
    std::vector<AccountItemAppearancesCache> ItemAppearances;
    std::vector<AccountItemFavoriteAppearancesCache> ItemFavoriteAppearances;
};

class TC_GAME_API CharacterCache
{
    public:
        CharacterCache();
        ~CharacterCache();
        static CharacterCache* instance();

        void LoadCharacterCacheStorage();
        void AddCharacterCacheEntry(ObjectGuid::LowType const& guid, uint32 accountId, std::string const& name, uint8 gender, uint8 race, uint8 playerClass, uint8 level, bool isDeleted);
        void DeleteCharacterCacheEntry(ObjectGuid::LowType const& guid, std::string const& name);

        void UpdateCharacterData(ObjectGuid::LowType const& guid, std::string const& name, uint8 gender = GENDER_NONE, uint8 race = RACE_NONE);
        void UpdateCharacterLevel(ObjectGuid::LowType const& guid, uint8 level);
        void UpdateCharacterAccountId(ObjectGuid::LowType const& guid, uint32 accountId);
        void UpdateCharacterGuildId(ObjectGuid::LowType const& guid, ObjectGuid::LowType guildId);
        void UpdateCharacterArenaTeamId(ObjectGuid::LowType const& guid, uint8 slot, uint32 arenaTeamId);
        void UpdateCharacterIsDeleted(ObjectGuid::LowType const& guid, bool isDeleted, std::string const& name = NULL);

        void AddCharacterToyEntity(ObjectGuid::LowType guid, uint32 itemId, bool isFavorite);
        void UpdateCharacterToyEntity(ObjectGuid::LowType guid, uint32 itemId, bool isFavorite);
        void AddCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId, uint32 flags);
        void UpdateCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId, uint32 flags);
        void DeleteCharacterHeirloomEntity(ObjectGuid::LowType guid, uint32 itemId);
        void AddCharacterMountEntity(ObjectGuid::LowType guid, uint32 spellId, uint32 flags);
        void UpdateCharacterMountEntity(ObjectGuid::LowType guid, uint32 spellId, uint32 flags);
        void AddCharacterItemAppearanceEntity(ObjectGuid::LowType guid, uint32 block, uint32 mask);
        void UpdateCharacterItemAppearanceFavoriteEntity(ObjectGuid::LowType guid, uint32 appearance, bool apply);

        bool HasCharacterCacheEntry(ObjectGuid::LowType const& guid) const;
        CharacterCacheEntry* GetCharacterCacheByGuid(ObjectGuid::LowType const& guid) const;
        CharacterCacheEntry* GetCharacterCacheByName(std::string const& name) const;

        ObjectGuid::LowType GetCharacterGuidByName(std::string const& name) const;
        bool GetCharacterNameByGuid(ObjectGuid::LowType guid, std::string& name) const;
        uint32 GetCharacterTeamByGuid(ObjectGuid::LowType guid) const;
        uint32 GetCharacterAccountIdByGuid(ObjectGuid::LowType guid) const;
        uint32 GetCharacterAccountIdByName(std::string const& name) const;
        uint8 GetCharacterLevelByGuid(ObjectGuid::LowType guid) const;
        ObjectGuid::LowType GetCharacterGuildIdByGuid(ObjectGuid::LowType guid) const;
        uint32 GetCharacterArenaTeamIdByGuid(ObjectGuid::LowType guid, uint8 type) const;
};

#define sCharacterCache CharacterCache::instance()

#endif
