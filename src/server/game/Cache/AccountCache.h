#pragma once

#include "Define.h"
#include "ObjectGuid.h"
#include "SharedDefines.h"
#include <vector>
#include <boost/dynamic_bitset.hpp>
#include <array>
#include <WorldSession.h>

enum MountStatusFlags : uint8;
enum class FavoriteAppearanceState;
struct Realm;

struct AccountToysEntityCache
{
    AccountToysEntityCache(uint32 itemID, bool isFavorite) : ItemID(itemID), IsFavorite(isFavorite) { }

    uint32 ItemID;
    bool IsFavorite;
};

struct AccountHeirloomsCache
{
    AccountHeirloomsCache(uint32 itemID, uint32 flags) : ItemID(itemID), Flags(flags) { }

    uint32 ItemID;
    uint32 Flags;
};

struct AccountMountsCache
{
    AccountMountsCache(uint32 spellID, uint32 flags) : SpellID(spellID), Flags(flags) { }

    uint32 SpellID;
    uint32 Flags;
};

struct AccountItemAppearancesCache
{
    AccountItemAppearancesCache(uint16 blob, uint32 mask) : BlobIndex(blob), Mask(mask) {}

    uint16 BlobIndex;
    uint32 Mask;
};

struct AccountItemFavoriteAppearancesCache
{
    AccountItemFavoriteAppearancesCache(uint32 item) : ItemModifiedAppearanceID(item) {}

    uint32 ItemModifiedAppearanceID;
};

struct AccountRealmCharactersCache
{
    AccountRealmCharactersCache(uint32 count, uint8 realmID, uint8 region, uint8 battleGroup) : Count(count), RealmID(realmID), Region(region), BattleGroup(battleGroup) {}

    uint8 Count;
    uint32 RealmID;
    uint8 Region;
    uint8 BattleGroup;
};

struct AccountDataCache
{
    AccountDataCache(uint8 type, uint32 time, std::string data) : Type(type), Time(time), Data(data) {}
    uint8 Type;
    uint32 Time;
    std::string Data;
};

struct AccountEntityCache
{
    std::vector<AccountToysEntityCache> Toys;
    std::vector<AccountHeirloomsCache> Heirlooms;
    std::vector<AccountMountsCache> Mounts;
    std::vector<AccountRealmCharactersCache> RealmCharacterCount;
    std::vector<AccountItemAppearancesCache> ItemAppearances;
    std::vector<AccountItemFavoriteAppearancesCache> ItemFavoriteAppearances;
    std::vector<AccountDataCache> AccountData;
    std::array<uint32, MAX_ACCOUNT_TUTORIAL_VALUES> AccountTutorials;
};

class AccountCache
{
    public:
        AccountCache();
        ~AccountCache();

        static AccountCache* instance();

        bool IsAccountCacheActive();
        void LoadAccountCacheStorage();

        void CreateCacheIfNeeded(uint32 accountId);
        AccountEntityCache* GetAccountCacheByAccountId(uint32 accountId) const;

        void AddAccountToyEntity(uint32 accountId, uint32 itemId, bool isFavorite);
        void UpdateAccountToyEntity(uint32 accountId, uint32 itemId, bool isFavorite);
        void AddAccountHeirloomEntity(uint32 accountId, uint32 itemId, uint32 flags);
        void UpdateAccountHeirloomEntity(uint32 accountId, uint32 itemId, uint32 flags);
        void DeleteAccountHeirloomEntity(uint32 accountId, uint32 itemId);
        void AddAccountMountEntity(uint32 accountId, uint32 spellId, uint32 flags);
        void UpdateAccountMountEntity(uint32 accountId, uint32 spellId, uint32 flags);
        void AddAccountItemAppearanceEntity(uint32 accountId, uint32 block, uint32 mask);
        void UpdateAccountItemAppearanceFavoriteEntity(uint32 accountId, uint32 appearance, bool apply);
        void UpdateAccountRealmCharacterCount(uint32 accountId, uint8 count, uint32 realmID = 0, uint32 region = 0, uint32 battleRegion = 0);
        void AddAccountRealmCharacterCount(uint32 accountId, uint8 count, uint32 realmID = 0, uint32 region = 0, uint32 battleRegion = 0);
        void UpdateAccountDataCache(uint32 accountId, AccountDataType type, uint32 time, std::string const& data);
        void UpdateAccountTutorial(uint32 accountId, uint8 index, uint32 value);
        void AddAccountData(uint32 accountId, uint8 type, uint32 time, std::string data);
private:
};

#define sAccountCache AccountCache::instance()
