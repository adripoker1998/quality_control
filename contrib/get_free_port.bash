#!/bin/bash

CHECK="do while"

while [[ ! -z $CHECK ]]; do
    PORT=$(( ( RANDOM % 60000 )  + 1025 ))
    CHECK=$( nmap -p $PORT $1 | awk '{if(NR>5 && NR<7)print}' | awk '{print $2}' | grep "open")
done

echo $PORT
