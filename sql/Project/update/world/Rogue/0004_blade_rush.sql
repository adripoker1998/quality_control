DELETE FROM `spell_script_names` WHERE `ScriptName` = 'pw_spell_rogue_blade_rush';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES (271877, 'pw_spell_rogue_blade_rush');
DELETE FROM `spell_script_names` WHERE `ScriptName` = 'pw_spell_rogue_blade_rush_charge';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES (271878, 'pw_spell_rogue_blade_rush_charge');
DELETE FROM `spell_script_names` WHERE `ScriptName` = 'pw_spell_rogue_blade_rush_damage';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES (271881, 'pw_spell_rogue_blade_rush_damage');