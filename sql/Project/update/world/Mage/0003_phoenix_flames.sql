DELETE FROM `spell_script_names` WHERE `ScriptName` = 'pw_spell_mage_phoenix_flames';
DELETE FROM `spell_script_names` WHERE `spell_id` = 257541 and `ScriptName` = 'project_spell_mage_phoenix_flames';
DELETE FROM `spell_script_names` WHERE `spell_id` = 257542 and `ScriptName` = 'project_spell_mage_phoenix_flames_trigger';
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES (257541, 'project_spell_mage_phoenix_flames');
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES (257542, 'project_spell_mage_phoenix_flames_trigger');