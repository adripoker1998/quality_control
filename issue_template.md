Description:

CHANGEME Description of the problem or issue here.

Current behaviour:

CHANGEME Tell us what happens. If this is a crash, post the crashlog (upload to https://gist.github.com/).

Expected behaviour:

CHANGEME Tell us what should happen instead.

Steps to reproduce the problem:

CHANGEME Step 1 include entries of affected creatures / items / quests with a link to the relevant wowhead page.
Step 2
Step 3
Branch(es):

CHANGEME 3.3.5, master or both

TC rev. hash/commit:

CHANGEME Copy the result of server debug command

Operating system: CHANGEME OS
